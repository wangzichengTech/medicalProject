package com.pug.zixun.controller.chapterlesson;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pug.zixun.bo.ChapterLessonBo;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;
import com.pug.zixun.controller.BaseController;
import com.pug.zixun.pojo.ChapterLesson;
import com.pug.zixun.service.chapterlesson.IChapterLessonService;
import com.pug.zixun.vo.ChapterLessonVo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.pug.generator.anno.PugDoc;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * ChapterLessonController
 * 创建人:yykk<br/>
 * 时间：2022-07-25 20:22:31 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 *
 * @version 1.0.0<br />
 */

@RestController
@RequiredArgsConstructor
@Slf4j
@PugDoc(name = "章节管理", tabname = "kss_course_chapter_lesson")
public class ChapterLessonController extends BaseController {

    private final IChapterLessonService chapterlessonService;


    /**
     * 根据课程ID查询章信息
     *
     * @return
     * @path : /admin/chapterlesson/list
     * @method: findChapterLessons
     * @result : List<ChapterLessonBo>
     * 创建人:yykk
     * 创建时间：2022-07-25 20:22:31
     * @version 1.0.0
     */
    @PostMapping("/chapterlesson/list/{courseId}")
    @PugDoc(name = "查询章节管理列表信息")
    public List<ChapterLessonBo> findChapterList(@PathVariable("courseId") Long courseId) {
        List<ChapterLessonBo> chapterList = chapterlessonService.findChapterList(courseId);
        if (!CollectionUtils.isEmpty(chapterList)) {
            chapterList = chapterList.stream().map(chapter -> {
                // 本章对应的节信息
                List<ChapterLessonBo> lessonList = chapterlessonService.findLessonList(chapter.getId());
                chapter.setLessonList(lessonList);
                return chapter;
            }).collect(Collectors.toList());
        }
        return chapterList;
    }

    /**
     * 根据章节管理id查询明细信息
     *
     * @param : id
     * @method: get/{id}
     * @path : /admin/chapterlesson/get/{id}
     * @result : ChapterLessonBo
     * 创建人:yykk
     * 创建时间：2022-07-25 20:22:31
     * @version 1.0.0
     */
    @PostMapping("/chapterlesson/get/{opid}")
    @PugDoc(name = "根据章节管理id查询明细信息")
    public ChapterLessonBo getChapterLessons(@PathVariable("opid") String opid) {
        if (Vsserts.isEmpty(opid)) {
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        // 本章信息
        ChapterLessonBo chapterLessonBo = chapterlessonService.getChapterLessonById(new Long(opid));
        if (Vsserts.isNotNull(chapterLessonBo)) {
            // 本章对应的节信息
            List<ChapterLessonBo> lessonList = chapterlessonService.findLessonList(chapterLessonBo.getId());
            chapterLessonBo.setLessonList(lessonList);
        }

        return chapterLessonBo;
    }

    /**
     * 拷贝章节信息
     *
     * @param : opid 是章节id
     * @method: saveupdate
     * @path : /admin/chapterlesson/copy/opid
     * @result : ChapterLessonBo
     * 创建人:yykk
     * 创建时间：2022-07-25 20:22:31
     * @version 1.0.0
     */
    @PostMapping("/chapterlesson/copy/{opid}")
    @PugDoc(name = "保存和修改章节管理")
    public ChapterLessonBo copyChapterLesson(@PathVariable("opid") String opid) {
        ChapterLesson chapterLesson = chapterlessonService.getById(new Long(opid));
        if (chapterLesson != null) {
            chapterLesson.setId(null);
            chapterLesson.setStatus(0);
            chapterLesson.setIsdelete(0);
            return chapterlessonService.saveupdateChapterLesson(chapterLesson);
        }
        return null;
    }


    /**
     * 保存和修改章节管理
     *
     * @param : chapterlesson
     * @method: saveupdate
     * @path : /admin/chapterlesson/save
     * @result : ChapterLessonBo
     * 创建人:yykk
     * 创建时间：2022-07-25 20:22:31
     * @version 1.0.0
     */
    @PostMapping("/chapterlesson/saveupdate")
    @PugDoc(name = "保存和修改章节管理")
    public ChapterLessonBo saveupdateChapterLesson(@RequestBody ChapterLesson chapterlesson) {
        return chapterlessonService.saveupdateChapterLesson(chapterlesson);
    }


    /**
     * 根据章节管理id删除章节管理
     *
     * @param : id
     * @method: delete/{id}
     * @path : /admin/chapterlesson/delete/{opid}
     * @result : int
     * 创建人:yykk
     * 创建时间：2022-07-25 20:22:31
     * @version 1.0.0
     */
    @PostMapping("/chapterlesson/delete/{opid}")
    @PugDoc(name = "根据章节管理id删除章节管理")
    public int deleteChapterLessonById(@PathVariable("opid") String opid) {
        if (Vsserts.isEmpty(opid)) {
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return chapterlessonService.deleteChapterLessonById(new Long(opid));
    }


    /**
     * 物理删除章节信息
     *
     * @param opid
     * @return
     */
    @PostMapping("/chapterlesson/remove/{opid}")
    @PugDoc(name = "物理删除章节信息")
    public int removeChapterLessonById(@PathVariable("opid") String opid) {
        if (Vsserts.isEmpty(opid)) {
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return chapterlessonService.removeChapterLessonById(new Long(opid));
    }

    /**
     * 恢复删除
     * @param opid
     * @return
     */
    @PostMapping("/chapterlesson/reback/{opid}")
    @PugDoc(name = "恢复删除")
    public int rebackChapterLessonById(@PathVariable("opid") String opid) {
        if (Vsserts.isEmpty(opid)) {
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        ChapterLesson chapterLesson = new ChapterLesson();
        chapterLesson.setId(new Long(opid));
        chapterLesson.setIsdelete(0);
        return chapterlessonService.updateById(chapterLesson) ? 1 : 0;
    }

}