package com.pug.zixun.controller.adminuser;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pug.zixun.aspect.auth.HasAuth;
import com.pug.zixun.bo.AdminUserBo;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;
import com.pug.zixun.controller.BaseController;
import com.pug.zixun.service.adminuser.IAdminUserService;
import com.pug.zixun.vo.AdminUserRegVo;
import com.pug.zixun.vo.AdminUserVo;
import com.pug.zixun.vo.StatusUpdateVo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.pug.generator.anno.PugDoc;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * AdminUserController
 * 创建人:yykk<br/>
 * 时间：2022-07-15 20:48:26 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/

@RestController
@RequiredArgsConstructor
@Slf4j
@PugDoc(name="后台用户管理",tabname="kss_admin_user")
public class AdminUserController extends BaseController{

    private final IAdminUserService adminuserService;



    /**
     * 给用户绑定角色
     *
     * @param userId
     * @param roleId
     * @return
     */
    @PostMapping("/adminuser/binding/role")
    @PugDoc(name = "给用户绑定角色")
    public int bindingRole(@RequestParam("userId") Long userId, @RequestParam("roleId")Long roleId){
        return adminuserService.saveUserRole(userId,roleId);
    }

    /**
     * 给用户接触绑定角色
     *
     * @param userId
     * @param roleId
     * @return
     */
    @PostMapping("/adminuser/unbind/role")
    @PugDoc(name = "给用户接触绑定角色")
    public int unbindRole(@RequestParam("userId") Long userId, @RequestParam("roleId")Long roleId){
        return adminuserService.delUserRole(userId,roleId);
    }


    /**
     * 查询后台用户管理列表信息
     * @path : /admin/adminuser/list
     * @method: findAdminUsers
     * @result : List<AdminUserBo>
     * 创建人:yykk
     * 创建时间：2022-07-15 20:48:26
     * @version 1.0.0
     * @return
     */
    @PostMapping("/adminuser/list")
    @PugDoc(name="查询后台用户管理列表信息")
    @HasAuth("130104")
    public List<AdminUserBo> findAdminUserList() {
        return adminuserService.findAdminUserList();
    }

	/**
	 * 查询后台用户管理列表信息并分页
	 * @path : /admin/adminuser/load
     * @method: findAdminUsers
     * @param : adminuserVo
     * @result : IPage<AdminUserBo>
	 * 创建人:yykk
	 * 创建时间：2022-07-15 20:48:26
	 * @version 1.0.0
	*/
    @PostMapping("/adminuser/load")
    @PugDoc(name="查询后台用户管理列表信息并分页")
    @HasAuth("130105")
    public IPage<AdminUserBo> findAdminUsers(@RequestBody AdminUserVo adminuserVo) {
        return adminuserService.findAdminUserPage(adminuserVo);
    }


    /**
     * 根据后台用户管理id查询明细信息
     * @method: get/{id}
     * @path : /admin/adminuser/get/{id}
     * @param : id
     * @result : AdminUserBo
     * 创建人:yykk
     * 创建时间：2022-07-15 20:48:26
     * @version 1.0.0
    */
    @PostMapping("/adminuser/get/{id}")
    @PugDoc(name="根据后台用户管理id查询明细信息")
    @HasAuth("130106")
    public AdminUserBo getAdminUserById(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
           throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return adminuserService.getAdminUserById(new Long(id));
    }


	/**
	 * 保存和修改后台用户管理
     * @method: saveupdate
	 * @path : /admin/adminuser/save
     * @param : adminuser
     * @result : AdminUserBo
	 * 创建人:yykk
	 * 创建时间：2022-07-15 20:48:26
	 * @version 1.0.0
	*/
    @PostMapping("/adminuser/saveupdate")
    @PugDoc(name="保存和修改后台用户管理")
    @HasAuth("130107")
    public AdminUserBo saveupdateAdminUser(@RequestBody @Validated AdminUserRegVo adminUserRegVo) {
        return adminuserService.saveupdateAdminUser(adminUserRegVo);
    }


    /**
	 * 根据后台用户管理id删除后台用户管理
     * @method: delete/{id}
     * @path : /admin/adminuser/delete/{id}
     * @param : id
     * @result : int
	 * 创建人:yykk
	 * 创建时间：2022-07-15 20:48:26
	 * @version 1.0.0
	*/
    @PostMapping("/adminuser/delete/{id}")
    @PugDoc(name="根据后台用户管理id删除后台用户管理")
    @HasAuth("130108")
    public int deleteAdminUserById(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return adminuserService.deleteAdminUserById(new Long(id));
    }


   /**
   	 * 根据后台用户管理ids批量删除后台用户管理
     * @method: adminuser/delBatch
     * @path : /admin/adminuser/delBatch
     * @param : adminuserVo
     * @result : boolean
   	 * 创建人:yykk
   	 * 创建时间：2022-07-15 20:48:26
   	 * @version 1.0.0
   	*/
    @PostMapping("/adminuser/delBatch")
    @HasAuth("130109")
    @PugDoc(name="根据后台用户管理ids批量删除后台用户管理")
    public boolean delAdminUser(@RequestBody AdminUserVo adminuserVo) {
        log.info("你要批量删除的IDS是:{}", adminuserVo.getBatchIds());
        if (Vsserts.isEmpty(adminuserVo.getBatchIds())) {
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return adminuserService.delBatchAdminUser(adminuserVo.getBatchIds());
    }


    /**
     * 状态的修改
     * @method: saveupdate
     * @path : /admin/adminuser/save
     * @param : adminuser
     * @result : AdminUserBo
     * 创建人:yykk
     * 创建时间：2022-07-15 20:48:26
     * @version 1.0.0
     */
    @PostMapping("/adminuser/update")
    @PugDoc(name="用于状态更新")
    @HasAuth("130110")
    public boolean updateAdminUser(@RequestBody StatusUpdateVo adminUserRegVo) {
        return adminuserService.updateAdminUser(adminUserRegVo);
    }
}