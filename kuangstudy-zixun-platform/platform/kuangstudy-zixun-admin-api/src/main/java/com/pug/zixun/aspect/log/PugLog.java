package com.pug.zixun.aspect.log;

import java.lang.annotation.*;

@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface PugLog {
    // 模块名
    String value() default  "";
    // 模块的描述2
    String desc() default "";
}
