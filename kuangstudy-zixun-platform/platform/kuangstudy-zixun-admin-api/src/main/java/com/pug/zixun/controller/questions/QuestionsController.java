package com.pug.zixun.controller.questions;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pug.zixun.service.questions.IQuestionsService;
import com.pug.zixun.pojo.Questions;
import com.pug.zixun.vo.QuestionsVo;
import com.pug.zixun.bo.QuestionsBo;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import com.pug.zixun.controller.BaseController;
import java.util.List;
import org.pug.generator.anno.PugDoc;
/**
 * QuestionsController
 * 创建人:yykk<br/>
 * 时间：2022-07-30 23:58:45 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/

@RestController
@RequiredArgsConstructor
@Slf4j
@PugDoc(name="文章管理",tabname="kss_questions")
public class QuestionsController extends BaseController{

    private final IQuestionsService questionsService;


    /**
     * 查询文章管理列表信息
     * @path : /admin/questions/list
     * @method: findQuestionss
     * @result : List<QuestionsBo>
     * 创建人:yykk
     * 创建时间：2022-07-30 23:58:45
     * @version 1.0.0
     * @return
     */
    @PostMapping("/questions/list")
    @PugDoc(name="查询文章管理列表信息")
    public List<QuestionsBo> findQuestionsList() {
        return questionsService.findQuestionsList();
    }

	/**
	 * 查询文章管理列表信息并分页
	 * @path : /admin/questions/load
     * @method: findQuestionss
     * @param : questionsVo
     * @result : IPage<QuestionsBo>
	 * 创建人:yykk
	 * 创建时间：2022-07-30 23:58:45
	 * @version 1.0.0
	*/
    @PostMapping("/questions/load")
    @PugDoc(name="查询文章管理列表信息并分页")
    public IPage<QuestionsBo> findQuestionss(@RequestBody QuestionsVo questionsVo) {
        return questionsService.findQuestionsPage(questionsVo);
    }


    /**
     * 根据文章管理id查询明细信息
     * @method: get/{id}
     * @path : /admin/questions/get/{id}
     * @param : id
     * @result : QuestionsBo
     * 创建人:yykk
     * 创建时间：2022-07-30 23:58:45
     * @version 1.0.0
    */
    @PostMapping("/questions/get/{id}")
    @PugDoc(name="根据文章管理id查询明细信息")
    public QuestionsBo getQuestionsById(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
           throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return questionsService.getQuestionsById(new Long(id));
    }


	/**
	 * 保存和修改文章管理
     * @method: saveupdate
	 * @path : /admin/questions/save
     * @param : questions
     * @result : QuestionsBo
	 * 创建人:yykk
	 * 创建时间：2022-07-30 23:58:45
	 * @version 1.0.0
	*/
    @PostMapping("/questions/saveupdate")
    @PugDoc(name="保存和修改文章管理")
    public QuestionsBo saveupdateQuestions(@RequestBody Questions questions) {
        return questionsService.saveupdateQuestions(questions);
    }


    /**
	 * 根据文章管理id删除文章管理
     * @method: delete/{id}
     * @path : /admin/questions/delete/{id}
     * @param : id
     * @result : int
	 * 创建人:yykk
	 * 创建时间：2022-07-30 23:58:45
	 * @version 1.0.0
	*/
    @PostMapping("/questions/delete/{id}")
    @PugDoc(name="根据文章管理id删除文章管理")
    public int deleteQuestionsById(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return questionsService.deleteQuestionsById(new Long(id));
    }


   /**
   	 * 根据文章管理ids批量删除文章管理
     * @method: questions/delBatch
     * @path : /admin/questions/delBatch
     * @param : questionsVo
     * @result : boolean
   	 * 创建人:yykk
   	 * 创建时间：2022-07-30 23:58:45
   	 * @version 1.0.0
   	*/
    @PostMapping("/questions/delBatch")
    @PugDoc(name="根据文章管理ids批量删除文章管理")
    public boolean delQuestions(@RequestBody QuestionsVo questionsVo) {
        log.info("你要批量删除的IDS是:{}", questionsVo.getBatchIds());
        if (Vsserts.isEmpty(questionsVo.getBatchIds())) {
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return questionsService.delBatchQuestions(questionsVo.getBatchIds());
    }
}