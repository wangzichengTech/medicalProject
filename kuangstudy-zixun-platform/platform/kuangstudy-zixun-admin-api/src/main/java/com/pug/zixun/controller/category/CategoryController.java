package com.pug.zixun.controller.category;

import com.pug.zixun.bo.CategoryBo;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;
import com.pug.zixun.controller.BaseController;
import com.pug.zixun.pojo.Category;
import com.pug.zixun.service.category.ICategoryService;
import com.pug.zixun.vo.CategoryVo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.pug.generator.anno.PugDoc;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * CategoryController
 * 创建人:yykk<br/>
 * 时间：2022-05-23 14:38:43 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/

@RestController
@RequiredArgsConstructor
@Slf4j
@PugDoc(name="文章分类管理",tabname="kss_category")
public class CategoryController extends BaseController{

    private final ICategoryService categoryService;


    /**
     * 查询管理分类
     * @return
     */
    @PostMapping("/category/tree")
    @PugDoc(name="查询管理分类")
    public List<CategoryBo> findCategoryTree(@RequestParam("keyword") String keyword){
        return categoryService.findCategoryTree(keyword);
    }
    @PostMapping("/category/get/{id}")
    public CategoryBo getCategoryById(@PathVariable("id") String id){
        if(Vsserts.isEmpty(id)){
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return categoryService.getCategoryById(new Long(id));
    }
	/**
	 * 保存和修改文章分类管理
     * @method: saveupdate
	 * @path : /admin/category/save
     * @param : category
     * @result : CategoryBo
	 * 创建人:yykk
	 * 创建时间：2022-05-23 14:38:43
	 * @version 1.0.0
	*/
    @PostMapping("/category/saveupdate")
    @PugDoc(name="保存和修改文章分类管理")
    public CategoryBo saveupdateCategory(@RequestBody Category category) {
        return categoryService.saveupdateCategory(category);
    }


    /**
	 * 根据文章分类管理id删除文章分类管理
     * @method: delete/{id}
     * @path : /admin/category/delete/{id}
     * @param : id
     * @result : int
	 * 创建人:yykk
	 * 创建时间：2022-05-23 14:38:43
	 * @version 1.0.0
	*/
    @PostMapping("/category/delete/{id}")
    @PugDoc(name="根据文章分类管理id删除文章分类管理")
    public int deleteCategoryById(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return categoryService.deleteCategoryById(new Long(id));
    }


   /**
   	 * 根据文章分类管理ids批量删除文章分类管理
     * @method: category/delBatch
     * @path : /admin/category/delBatch
     * @param : categoryVo
     * @result : boolean
   	 * 创建人:yykk
   	 * 创建时间：2022-05-23 14:38:43
   	 * @version 1.0.0
   	*/
    @PostMapping("/category/delBatch")
    @PugDoc(name="根据文章分类管理ids批量删除文章分类管理")
    public boolean delCategory(@RequestBody CategoryVo categoryVo) {
        log.info("你要批量删除的IDS是:{}", categoryVo.getBatchIds());
        if (Vsserts.isEmpty(categoryVo.getBatchIds())) {
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return categoryService.delBatchCategory(categoryVo.getBatchIds());
    }
}