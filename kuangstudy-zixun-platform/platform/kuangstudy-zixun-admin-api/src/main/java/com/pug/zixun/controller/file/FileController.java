package com.pug.zixun.controller.file;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pug.zixun.commons.enums.AdminErrorResultEnum;
import com.pug.zixun.mapper.FileMapper;
import com.pug.zixun.service.file.IFileService;
import com.pug.zixun.pojo.File;
import com.pug.zixun.vo.FileVo;
import com.pug.zixun.bo.FileBo;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import com.pug.zixun.controller.BaseController;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import org.pug.generator.anno.PugDoc;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

/**
 * FileController
 * 创建人:yykk<br/>
 * 时间：2022-10-28 13:23:37 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/

@RestController
@RequiredArgsConstructor
@Slf4j
@PugDoc(name="文件管理",tabname="kss_file")
public class FileController extends BaseController{

    private final IFileService fileService;
    @Value("${file.uploadFolder}")
    private String fileUploadPath;
    @Resource
    private FileMapper fileMapper;
    @Value("${file.staticPatternPath}")
    private String staticPatternPath;

    /**
     * 查询文件管理列表信息
     * @path : /admin/file/list
     * @method: findFiles
     * @result : List<FileBo>
     * 创建人:yykk
     * 创建时间：2022-10-28 13:23:37
     * @version 1.0.0
     * @return
     */
    //上传地址：http://localhost:8877/admin/file/upload
    @PostMapping("/file/upload")
    public String upload(@RequestParam MultipartFile file) throws IOException {
        if (file.isEmpty()) {
            return null;
        }
        if(file.getSize() > 1024 * 1024 * 10){
            throw new PugValidatorException(AdminErrorResultEnum.FILE_UPLOAD_LIMIT_ERROR);
        }
        String originalFilename = file.getOriginalFilename();
        String type = FileUtil.extName(originalFilename);
        long size = file.getSize();

        // 定义一个文件唯一的标识码
        String fileUUID = IdUtil.fastSimpleUUID() + StrUtil.DOT + type;

        java.io.File uploadFile = new java.io.File(fileUploadPath + fileUUID);
        // 判断配置的文件目录是否存在，若不存在则创建一个新的文件目录
        java.io.File parentFile = uploadFile.getParentFile();
        if(!parentFile.exists()) {
            parentFile.mkdirs();
        }

        String url;
        // 获取文件的md5
        String md5 = SecureUtil.md5(file.getInputStream());
        // 从数据库查询是否存在相同的记录
        File dbFiles = getFileByMd5(md5);
        if (dbFiles != null) {
            url = dbFiles.getUrl();
        } else {
            // 上传文件到磁盘
            file.transferTo(uploadFile);
            // 数据库若不存在重复文件，则不删除刚才上传的文件
            url = "http://localhost:8877/admin/resource/" + fileUUID;
        }


        // 存储数据库
        File saveFile = new File();
        saveFile.setName(originalFilename);
        saveFile.setType(type);
        saveFile.setSize(size/1024); // 单位 kb
        saveFile.setUrl(url);
        saveFile.setMd5(md5);
        fileMapper.insert(saveFile);

        return url;
    }
    private File getFileByMd5(String md5) {
        // 查询文件的md5是否存在
        QueryWrapper<File> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("md5", md5);
        List<File> filesList = fileMapper.selectList(queryWrapper);
        return filesList.size() == 0 ? null : filesList.get(0);
    }
    @GetMapping("/resource/{fileUUID}")
    public void download(@PathVariable String fileUUID, HttpServletResponse response) throws IOException {
        // 根据文件的唯一标识码获取文件
        java.io.File uploadFile = new java.io.File(fileUploadPath + fileUUID);
        // 设置输出流的格式
        ServletOutputStream os = response.getOutputStream();
        response.addHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileUUID, "UTF-8"));
        response.setContentType("application/octet-stream");

        // 读取文件的字节流
        os.write(FileUtil.readBytes(uploadFile));
        os.flush();
        os.close();
    }

    @PostMapping("/file/list")
    @PugDoc(name="查询文件管理列表信息")
    public List<FileBo> findFileList() {
        return fileService.findFileList();
    }

	/**
	 * 查询文件管理列表信息并分页
	 * @path : /admin/file/load
     * @method: findFiles
     * @param : fileVo
     * @result : IPage<FileBo>
	 * 创建人:yykk
	 * 创建时间：2022-10-28 13:23:37
	 * @version 1.0.0
	*/
    @PostMapping("/file/load")
    @PugDoc(name="查询文件管理列表信息并分页")
    public IPage<FileBo> findFiles(@RequestBody FileVo fileVo) {

        return fileService.findFilePage(fileVo);
    }
    @PostMapping("/file/load2")
    @PugDoc(name="查询文件管理列表信息并分页")
    public IPage<FileBo> findFiles2(@RequestBody FileVo fileVo) {

        return fileService.findFilePage2(fileVo);
    }

    /**
     * 根据文件管理id查询明细信息
     * @method: get/{id}
     * @path : /admin/file/get/{id}
     * @param : id
     * @result : FileBo
     * 创建人:yykk
     * 创建时间：2022-10-28 13:23:37
     * @version 1.0.0
    */
    @PostMapping("/file/get/{id}")
    @PugDoc(name="根据文件管理id查询明细信息")
    public FileBo getFileById(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
           throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return fileService.getFileById(new Long(id));
    }


	/**
	 * 保存和修改文件管理
     * @method: saveupdate
	 * @path : /admin/file/save
     * @param : file
     * @result : FileBo
	 * 创建人:yykk
	 * 创建时间：2022-10-28 13:23:37
	 * @version 1.0.0
	*/
    @PostMapping("/file/saveupdate")
    @PugDoc(name="保存和修改文件管理")
    public FileBo saveupdateFile(@RequestBody File file) {
        return fileService.saveupdateFile(file);
    }


    /**
	 * 根据文件管理id删除文件管理
     * @method: delete/{id}
     * @path : /admin/file/delete/{id}
     * @param : id
     * @result : int
	 * 创建人:yykk
	 * 创建时间：2022-10-28 13:23:37
	 * @version 1.0.0
	*/
    @PostMapping("/file/delete/{id}")
    @PugDoc(name="根据文件管理id删除文件管理")
    public int deleteFileById(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return fileService.deleteFileById(new Long(id));
    }
    @PostMapping("/file/delete2/{id}")
    @PugDoc(name="根据文件管理id删除文件管理")
    public int deleteFileById2(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return fileService.deleteFileById2(new Long(id));
    }

   /**
   	 * 根据文件管理ids批量删除文件管理
     * @method: file/delBatch
     * @path : /admin/file/delBatch
     * @param : fileVo
     * @result : boolean
   	 * 创建人:yykk
   	 * 创建时间：2022-10-28 13:23:37
   	 * @version 1.0.0
   	*/
    @PostMapping("/file/delBatch")
    @PugDoc(name="根据文件管理ids批量删除文件管理")
    public boolean delFile(@RequestBody FileVo fileVo) {
        log.info("你要批量删除的IDS是:{}", fileVo.getBatchIds());
        if (Vsserts.isEmpty(fileVo.getBatchIds())) {
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return fileService.delBatchFile(fileVo.getBatchIds());
    }
    @PostMapping("/file/delBatch2")
    @PugDoc(name="根据文件管理ids批量删除文件管理")
    public boolean delFile2(@RequestBody FileVo fileVo) {
        log.info("你要批量删除的IDS是:{}", fileVo.getBatchIds());
        if (Vsserts.isEmpty(fileVo.getBatchIds())) {
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return fileService.delBatchFile2(fileVo.getBatchIds());
    }
}