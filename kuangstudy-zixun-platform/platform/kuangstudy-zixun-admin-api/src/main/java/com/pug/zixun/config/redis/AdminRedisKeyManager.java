package com.pug.zixun.config.redis;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 飞哥B站地址：https://space.bilibili.com/490711252
 * 记得关注和三连哦！
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2022/5/20$ 23:58$
 */
public interface AdminRedisKeyManager {

    // 登录token续期使用的key
    String USER_LOGIN_TOKEN_KEY = "pug:user:login:token:";
    // 下线使用的rediskey
    String USER_LOGIN_LOGOUT_KEY = "pug:user:logout:";
    // 续期返回的新的token的key
    String RESPONSE_AUTH_TOKEN = "x-auth-token";
    //指定t生成oken的claim的key的名字
    String PUG_USER_ID = "pug-user-id";

    // header中userid 接口校验和线下使用
    String TOKEN_USERID_NAME = "token-userid";
    // header中token 接口校验使用
    String TOKEN_NAME = "token";
    // header中token_uuid 下线使用
    String TOKEN_UUID_NAME = "token-uuid";
}