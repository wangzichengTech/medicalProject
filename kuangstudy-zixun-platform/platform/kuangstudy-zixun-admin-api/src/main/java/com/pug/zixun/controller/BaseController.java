package com.pug.zixun.controller;

import org.pug.generator.anno.PugDocIgnore;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 飞哥B站地址：https://space.bilibili.com/490711252
 * 记得关注和三连哦！
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2022/5/13$ 23:44$
 */
@RestController
@RequestMapping("/admin")
@PugDocIgnore
public abstract class BaseController {

}