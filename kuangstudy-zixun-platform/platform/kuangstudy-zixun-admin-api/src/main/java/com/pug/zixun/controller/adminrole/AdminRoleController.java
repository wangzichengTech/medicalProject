package com.pug.zixun.controller.adminrole;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pug.zixun.bo.AdminRoleBo;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;
import com.pug.zixun.controller.BaseController;
import com.pug.zixun.pojo.AdminRole;
import com.pug.zixun.service.adminrole.IAdminRoleService;
import com.pug.zixun.service.adminuser.IAdminUserService;
import com.pug.zixun.vo.AdminRolePermissionVo;
import com.pug.zixun.vo.AdminRoleQueryVo;
import com.pug.zixun.vo.AdminRoleVo;
import com.pug.zixun.vo.StatusUpdateVo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.pug.generator.anno.PugDoc;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * AdminRoleController
 * 创建人:yykk<br/>
 * 时间：2022-07-15 22:04:49 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 *
 * @version 1.0.0<br />
 */

@RestController
@RequiredArgsConstructor
@Slf4j
@PugDoc(name = "后台角色管理", tabname = "kss_roles")
public class AdminRoleController extends BaseController {

    private final IAdminRoleService adminroleService;
    private final IAdminUserService adminUserService;



    /**
     * 查询后台角色管理列表信息
     *
     * @return
     * @path : /admin/adminrole/list
     * @method: findAdminRoles
     * @result : List<AdminRoleBo>
     * 创建人:yykk
     * 创建时间：2022-07-15 22:04:49
     * @version 1.0.0
     */
    @PostMapping("/adminrole/user/role")
    @PugDoc(name = "查询用户授权的角色信息")
    public List<AdminRoleBo> findAdminRoleUserList(@RequestParam("userId")Long userId) {
        // 查询所有的角色信息
        List<AdminRoleBo> adminRoleList = adminroleService.findAdminRoleList();
        // 查询某个用户对应的角色信息
        List<Long> byUserRoleIds = adminUserService.findByUserRoleIds(userId);
        return adminRoleList.stream().map(role->{
            long count = byUserRoleIds.stream().filter(roleid -> role.getId().equals(roleid)).count();
            role.setAuth(count > 0);
            return role;
        }).collect(Collectors.toList());
    }


    /**
     * 查询后台角色管理列表信息
     *
     * @return
     * @path : /admin/adminrole/list
     * @method: findAdminRoles
     * @result : List<AdminRoleBo>
     * 创建人:yykk
     * 创建时间：2022-07-15 22:04:49
     * @version 1.0.0
     */
    @PostMapping("/adminrole/list")
    @PugDoc(name = "查询后台角色管理列表信息")
    public List<AdminRoleBo> findAdminRoleList() {
        return adminroleService.findAdminRoleList();
    }

    /**
     * 查询后台角色管理列表信息并分页
     *
     * @param : adminroleVo
     * @path : /admin/adminrole/load
     * @method: findAdminRoles
     * @result : IPage<AdminRoleBo>
     * 创建人:yykk
     * 创建时间：2022-07-15 22:04:49
     * @version 1.0.0
     */
    @PostMapping("/adminrole/load")
    @PugDoc(name = "查询后台角色管理列表信息并分页")
    public IPage<AdminRoleBo> findAdminRoles(@RequestBody AdminRoleVo adminroleVo) {
        return adminroleService.findAdminRolePage(adminroleVo);
    }


    /**
     * 根据后台角色管理id查询明细信息
     *
     * @param : id
     * @method: get/{id}
     * @path : /admin/adminrole/get/{id}
     * @result : AdminRoleBo
     * 创建人:yykk
     * 创建时间：2022-07-15 22:04:49
     * @version 1.0.0
     */
    @PostMapping("/adminrole/get/{id}")
    @PugDoc(name = "根据后台角色管理id查询明细信息")
    public AdminRoleBo getAdminRoleById(@PathVariable("id") String id) {
        if (Vsserts.isEmpty(id)) {
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return adminroleService.getAdminRoleById(new Long(id));
    }


    /**
     * 保存和修改后台角色管理
     *
     * @param : adminrole
     * @method: saveupdate
     * @path : /admin/adminrole/save
     * @result : AdminRoleBo
     * 创建人:yykk
     * 创建时间：2022-07-15 22:04:49
     * @version 1.0.0
     */
    @PostMapping("/adminrole/saveupdate")
    @PugDoc(name = "保存和修改后台角色管理")
    public AdminRoleBo saveupdateAdminRole(@RequestBody AdminRole adminrole) {
        return adminroleService.saveupdateAdminRole(adminrole);
    }


    /**
     * 根据后台角色管理id删除后台角色管理
     *
     * @param : id
     * @method: delete/{id}
     * @path : /admin/adminrole/delete/{id}
     * @result : int
     * 创建人:yykk
     * 创建时间：2022-07-15 22:04:49
     * @version 1.0.0
     */
    @PostMapping("/adminrole/delete/{id}")
    @PugDoc(name = "根据后台角色管理id删除后台角色管理")
    public int deleteAdminRoleById(@PathVariable("id") String id) {
        if (Vsserts.isEmpty(id)) {
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return adminroleService.deleteAdminRoleById(new Long(id));
    }


    /**
     * 根据后台角色管理ids批量删除后台角色管理
     *
     * @param : adminroleVo
     * @method: adminrole/delBatch
     * @path : /admin/adminrole/delBatch
     * @result : boolean
     * 创建人:yykk
     * 创建时间：2022-07-15 22:04:49
     * @version 1.0.0
     */
    @PostMapping("/adminrole/delBatch")
    @PugDoc(name = "根据后台角色管理ids批量删除后台角色管理")
    public boolean delAdminRole(@RequestBody AdminRoleVo adminroleVo) {
        log.info("你要批量删除的IDS是:{}", adminroleVo.getBatchIds());
        if (Vsserts.isEmpty(adminroleVo.getBatchIds())) {
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return adminroleService.delBatchAdminRole(adminroleVo.getBatchIds());
    }


    /**
     * 状态的修改
     *
     * @param : adminuser
     * @method: saveupdate
     * @path : /admin/adminuser/save
     * @result : AdminUserBo
     * 创建人:yykk
     * 创建时间：2022-07-15 20:48:26
     * @version 1.0.0
     */
    @PostMapping("/adminrole/update")
    @PugDoc(name = "用于状态更新")
    public boolean updateAdminRole(@RequestBody StatusUpdateVo statusUpdateVo) {
        return adminroleService.updateAdminRole(statusUpdateVo);
    }


    /**
     * 查询角色对用的权限
     *
     * @param adminRoleQueryVo
     * @return
     */
    @PostMapping("/adminrole/findPermission")
    @PugDoc(name = "查询角色对用的权限")
    public List<String> findPermissionByRoleId(@RequestBody @Validated AdminRoleQueryVo adminRoleQueryVo) {
        return adminroleService.findPermissionByRoleId(adminRoleQueryVo.getRoleId());
    }


    /**
     * 给角色授权
     *
     * @return
     */
    @PostMapping("/adminrole/savepermission")
    @PugDoc(name = "给角色授权")
    public Boolean savePermissionByRoleId(@RequestBody @Validated AdminRolePermissionVo
                                              adminRolePermissionVo) {
        return adminroleService.savePermissionByRoleId(adminRolePermissionVo.getRoleId(),
                adminRolePermissionVo.getPermissionIds());
    }
}