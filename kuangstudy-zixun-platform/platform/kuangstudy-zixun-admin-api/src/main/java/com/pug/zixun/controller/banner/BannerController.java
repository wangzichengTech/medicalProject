package com.pug.zixun.controller.banner;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pug.zixun.bo.BannerBo;
import com.pug.zixun.commons.anno.IgnoreToken;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;
import com.pug.zixun.config.mvc.MessageUtils;
import com.pug.zixun.controller.BaseController;
import com.pug.zixun.pojo.Banner;
import com.pug.zixun.service.banner.IBannerService;
import com.pug.zixun.vo.BannerVo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.pug.generator.anno.PugDoc;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * BannerController
 * 创建人:yykk<br/>
 * 时间：2022-05-23 03:05:21 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/

@RestController
@RequiredArgsConstructor
@Slf4j
@PugDoc(name="轮播图管理",tabname="kss_banner")
@IgnoreToken
public class BannerController extends BaseController{

    private final IBannerService bannerService;

    /**
     * 查询轮播图管理列表信息
     * @path : /admin/banner/list
     * @method: findBanners
     * @result : List<BannerBo>
     * 创建人:yykk
     * 创建时间：2022-05-23 03:05:21
     * @version 1.0.0
     * @return
     */
    @PostMapping("/banner/list")
    @PugDoc(name="查询轮播图管理列表信息")
    @IgnoreToken
    public List<BannerBo> findBannerList() {
        return bannerService.findBannerList();
    }

	/**
	 * 查询轮播图管理列表信息并分页
	 * @path : /admin/banner/load
     * @method: findBanners
     * @param : bannerVo
     * @result : IPage<BannerBo>
	 * 创建人:yykk
	 * 创建时间：2022-05-23 03:05:21
	 * @version 1.0.0
	*/
    @PostMapping("/banner/load")
    @PugDoc(name="查询轮播图管理列表信息并分页")
    public IPage<BannerBo> findBanners(@RequestBody BannerVo bannerVo) {
        return bannerService.findBannerPage(bannerVo);
    }


    /**
     * 根据轮播图管理id查询明细信息
     * @method: get/{id}
     * @path : /admin/banner/get/{id}
     * @param : id
     * @result : BannerBo
     * 创建人:yykk
     * 创建时间：2022-05-23 03:05:21
     * @version 1.0.0
    */
    @PostMapping("/banner/get/{id}")
    @PugDoc(name="根据轮播图管理id查询明细信息")
    public BannerBo getBannerById(  @PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
           throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return bannerService.getBannerById(new Long(id));
    }


	/**
	 * 保存和修改轮播图管理
     * @method: saveupdate
	 * @path : /admin/banner/save
     * @param : banner
     * @result : BannerBo
	 * 创建人:yykk
	 * 创建时间：2022-05-23 03:05:21
	 * @version 1.0.0
	*/
    @PostMapping("/banner/saveupdate")
    @PugDoc(name="保存和修改轮播图管理")
    public BannerBo saveupdateBanner(@RequestBody @Validated  Banner banner) {
        banner.setDescription(MessageUtils.get("ValidPhone.phone"));
        return bannerService.saveupdateBanner(banner);
    }


    /**
	 * 根据轮播图管理id删除轮播图管理
     * @method: delete/{id}
     * @path : /admin/banner/delete/{id}
     * @param : id
     * @result : int
	 * 创建人:yykk
	 * 创建时间：2022-05-23 03:05:21
	 * @version 1.0.0
	*/
    @PostMapping("/banner/delete/{id}")
    @PugDoc(name="根据轮播图管理id删除轮播图管理")
    public int deleteBannerById(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return bannerService.deleteBannerById(new Long(id));
    }


   /**
   	 * 根据轮播图管理ids批量删除轮播图管理
     * @method: banner/delBatch
     * @path : /admin/banner/delBatch
     * @param : bannerVo
     * @result : boolean
   	 * 创建人:yykk
   	 * 创建时间：2022-05-23 03:05:21
   	 * @version 1.0.0
   	*/
    @PostMapping("/banner/delBatch")
    @PugDoc(name="根据轮播图管理ids批量删除轮播图管理")
    public boolean delBanner(@RequestBody BannerVo bannerVo) {
        log.info("你要批量删除的IDS是:{}", bannerVo.getBatchIds());
        if (Vsserts.isEmpty(bannerVo.getBatchIds())) {
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return bannerService.delBatchBanner(bannerVo.getBatchIds());
    }
}