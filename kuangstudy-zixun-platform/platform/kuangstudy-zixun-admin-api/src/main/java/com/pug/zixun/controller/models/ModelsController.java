package com.pug.zixun.controller.models;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pug.zixun.aspect.log.PugLog;
import com.pug.zixun.service.models.IModelsService;
import com.pug.zixun.pojo.Models;
import com.pug.zixun.vo.ModelsVo;
import com.pug.zixun.bo.ModelsBo;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import com.pug.zixun.controller.BaseController;
import java.util.List;
import org.pug.generator.anno.PugDoc;
/**
 * ModelsController
 * 创建人:yykk<br/>
 * 时间：2022-10-08 21:13:25 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/

@RestController
@RequiredArgsConstructor
@Slf4j
@PugDoc(name="模型管理",tabname="kss_models ")
public class ModelsController extends BaseController{

    private final IModelsService modelsService;


    /**
     * 查询模型管理列表信息
     * @path : /admin/models/list
     * @method: findModelss
     * @result : List<ModelsBo>
     * 创建人:yykk
     * 创建时间：2022-10-08 21:13:25
     * @version 1.0.0
     * @return
     */
    @PostMapping("/models/list")
    @PugDoc(name="查询模型管理列表信息")
    @PugLog(value = "models",desc = "查询模型管理列表信息")
    public List<ModelsBo> findModelsList() {
        return modelsService.findModelsList();
    }

	/**
	 * 查询模型管理列表信息并分页
	 * @path : /admin/models/load
     * @method: findModelss
     * @param : modelsVo
     * @result : IPage<ModelsBo>
	 * 创建人:yykk
	 * 创建时间：2022-10-08 21:13:25
	 * @version 1.0.0
	*/
    @PostMapping("/models/load")
    @PugDoc(name="查询模型管理列表信息并分页")
    @PugLog(value = "models",desc = "查询模型管理列表信息并分页")
    public IPage<ModelsBo> findModelss(@RequestBody ModelsVo modelsVo) {
        return modelsService.findModelsPage(modelsVo);
    }


    /**
     * 根据模型管理id查询明细信息
     * @method: get/{id}
     * @path : /admin/models/get/{id}
     * @param : id
     * @result : ModelsBo
     * 创建人:yykk
     * 创建时间：2022-10-08 21:13:25
     * @version 1.0.0
    */
    @PostMapping("/models/get/{id}")
    @PugDoc(name="根据模型管理id查询明细信息")
    @PugLog(value = "models",desc = "根据模型管理id查询明细信息")
    public ModelsBo getModelsById(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
           throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return modelsService.getModelsById(new Long(id));
    }


	/**
	 * 保存和修改模型管理
     * @method: saveupdate
	 * @path : /admin/models/save
     * @param : models
     * @result : ModelsBo
	 * 创建人:yykk
	 * 创建时间：2022-10-08 21:13:25
	 * @version 1.0.0
	*/
    @PostMapping("/models/saveupdate")
    @PugDoc(name="保存和修改模型管理")
    @PugLog(value = "models",desc = "保存和修改模型管理")
    public ModelsBo saveupdateModels(@RequestBody Models models) {
        return modelsService.saveupdateModels(models);
    }


    /**
	 * 根据模型管理id删除模型管理
     * @method: delete/{id}
     * @path : /admin/models/delete/{id}
     * @param : id
     * @result : int
	 * 创建人:yykk
	 * 创建时间：2022-10-08 21:13:25
	 * @version 1.0.0
	*/
    @PostMapping("/models/delete/{id}")
    @PugDoc(name="根据模型管理id删除模型管理")
    @PugLog(value = "models",desc = "根据模型管理id删除模型管理")
    public int deleteModelsById(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return modelsService.deleteModelsById(new Long(id));
    }


   /**
   	 * 根据模型管理ids批量删除模型管理
     * @method: models/delBatch
     * @path : /admin/models/delBatch
     * @param : modelsVo
     * @result : boolean
   	 * 创建人:yykk
   	 * 创建时间：2022-10-08 21:13:25
   	 * @version 1.0.0
   	*/
    @PostMapping("/models/delBatch")
    @PugDoc(name="根据模型管理ids批量删除模型管理")
    @PugLog(value = "models",desc = "根据模型管理ids批量删除模型管理")
    public boolean delModels(@RequestBody ModelsVo modelsVo) {
        log.info("你要批量删除的IDS是:{}", modelsVo.getBatchIds());
        if (Vsserts.isEmpty(modelsVo.getBatchIds())) {
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return modelsService.delBatchModels(modelsVo.getBatchIds());
    }
}