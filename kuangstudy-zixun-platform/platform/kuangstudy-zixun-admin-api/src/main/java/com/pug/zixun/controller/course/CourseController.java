package com.pug.zixun.controller.course;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pug.zixun.aspect.log.PugLog;
import com.pug.zixun.bo.ChapterLessonBo;
import com.pug.zixun.bo.CourseBo;
import com.pug.zixun.commons.anno.IgnoreToken;
import com.pug.zixun.commons.enums.AdminUserResultEnum;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugBusinessException;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;
import com.pug.zixun.controller.BaseController;
import com.pug.zixun.pojo.ChapterLesson;
import com.pug.zixun.pojo.Course;
import com.pug.zixun.service.course.ICourseService;
import com.pug.zixun.vo.CourseStatusUpdateVo;
import com.pug.zixun.vo.CourseVo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.pug.generator.anno.PugDoc;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * CourseController
 * 创建人:yykk<br/>
 * 时间：2022-07-25 20:21:39 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/

@RestController
@RequiredArgsConstructor
@Slf4j
@PugDoc(name="课程",tabname="kss_course")
public class CourseController extends BaseController{

    private final ICourseService courseService;




    /**
     * 查询课程列表信息
     * @path : /admin/course/list
     * @method: findCourses
     * @result : List<CourseBo>
     * 创建人:yykk
     * 创建时间：2022-07-25 20:21:39
     * @version 1.0.0
     * @return
     */
    @PostMapping("/course/list")
    @PugDoc(name="查询课程列表信息")
    @PugLog(value="course",desc = "查询课程列表信息")
    public List<CourseBo> findCourseList() {
        return courseService.findCourseList();
    }

	/**
	 * 查询课程列表信息并分页
	 * @path : /admin/course/load
     * @method: findCourses
     * @param : courseVo
     * @result : IPage<CourseBo>
	 * 创建人:yykk
	 * 创建时间：2022-07-25 20:21:39
	 * @version 1.0.0
	*/
    @PostMapping("/course/load")
    @PugDoc(name="查询课程列表信息并分页")
    public IPage<CourseBo> findCourses(@RequestBody CourseVo courseVo) {
        return courseService.findCoursePage(courseVo);
    }


    /**
     * 拷贝课程信息
     * @param : opid 是章节id
     * @method: saveupdate
     * @path : /admin/course/copy/opid
     * @result : ChapterLessonBo
     * 创建人:yykk
     * 创建时间：2022-07-25 20:22:31
     * @version 1.0.0
     */
    @PostMapping("/course/copy/{courseId}")
    @PugDoc(name = "保存和修改章节管理")
    public CourseBo copyChapterLesson(@PathVariable("courseId") String courseId) {
        Course course = courseService.getById(new Long(courseId));
        if (course != null) {
            course.setId(null);
            course.setStatus(0);
            course.setIsdelete(0);
            return courseService.saveupdateCourse(course);
        }
        return null;
    }


    /**
     * 根据课程id查询明细信息
     * @method: get/{id}
     * @path : /admin/course/get/{id}
     * @param : id
     * @result : CourseBo
     * 创建人:yykk
     * 创建时间：2022-07-25 20:21:39
     * @version 1.0.0
    */
    @PostMapping("/course/get/{id}")
    @PugDoc(name="根据课程id查询明细信息")
    public CourseBo getCourseById(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
           throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return courseService.getCourseById(new Long(id));
    }


	/**
	 * 保存和修改课程
     * @method: saveupdate
	 * @path : /admin/course/save
     * @param : course
     * @result : CourseBo
	 * 创建人:yykk
	 * 创建时间：2022-07-25 20:21:39
	 * @version 1.0.0
	*/
    @PostMapping("/course/saveupdate")
    @PugDoc(name="保存和修改课程")
    public CourseBo saveupdateCourse(@RequestBody Course course) {
        return courseService.saveupdateCourse(course);
    }


    /**
	 * 根据课程id删除课程
     * @method: delete/{id}
     * @path : /admin/course/delete/{id}
     * @param : id
     * @result : int
	 * 创建人:yykk
	 * 创建时间：2022-07-25 20:21:39
	 * @version 1.0.0
	*/
    @PostMapping("/course/delete/{id}")
    @PugDoc(name="根据课程id删除课程")
    public int deleteCourseById(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return courseService.deleteCourseById(new Long(id));
    }


   /**
   	 * 根据课程ids批量删除课程
     * @method: course/delBatch
     * @path : /admin/course/delBatch
     * @param : courseVo
     * @result : boolean
   	 * 创建人:yykk
   	 * 创建时间：2022-07-25 20:21:39
   	 * @version 1.0.0
   	*/
    @PostMapping("/course/delBatch")
    @PugDoc(name="根据课程ids批量删除课程")
    public boolean delCourse(@RequestBody CourseVo courseVo) {
        log.info("你要批量删除的IDS是:{}", courseVo.getBatchIds());
        if (Vsserts.isEmpty(courseVo.getBatchIds())) {
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return courseService.delBatchCourse(courseVo.getBatchIds());
    }

    /**
     * 状态的修改
     *
     * @param : adminuser
     * @method: saveupdate
     * @path : /admin/course/update
     * @result : AdminUserBo
     * 创建人:yykk
     * 创建时间：2022-07-15 20:48:26
     * @version 1.0.0
     */
    @PostMapping("/course/update")
    @PugDoc(name = "用于状态更新")
    public boolean updateAdminRole(@RequestBody CourseStatusUpdateVo courseStatusUpdateVo) {
        if(courseStatusUpdateVo == null || Vsserts.isNull(courseStatusUpdateVo.getId())){
            throw new PugBusinessException(AdminUserResultEnum.NULL_ERROR);
        }
        Course course = new Course();
        BeanUtils.copyProperties(courseStatusUpdateVo,course);
        return courseService.updateById(course);
    }

}