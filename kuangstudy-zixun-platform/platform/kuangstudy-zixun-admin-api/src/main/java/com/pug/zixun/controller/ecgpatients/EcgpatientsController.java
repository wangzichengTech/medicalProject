package com.pug.zixun.controller.ecgpatients;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pug.zixun.pojo.EcgPatientsBo;
import com.pug.zixun.service.ecgpatients.IEcgpatientsService;
import com.pug.zixun.pojo.Ecgpatients;
import com.pug.zixun.vo.EcgpatientsVo;
import com.pug.zixun.bo.EcgpatientsBo;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;
import com.pug.zixun.controller.BaseController;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.pug.generator.anno.PugDoc;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

/**
 * EcgpatientsController
 * 创建人:yykk<br/>
 * 时间：2022-11-02 19:18:02 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/

@RestController
@RequiredArgsConstructor
@Slf4j
@PugDoc(name="脓毒症信息管理",tabname="kss_ecg_patients")
public class EcgpatientsController extends BaseController{

    private final IEcgpatientsService ecgpatientsService;


    /**
     * 查询脓毒症信息管理列表信息
     * @path : /admin/ecgpatients/list
     * @method: findEcgpatientss
     * @result : List<EcgpatientsBo>
     * 创建人:yykk
     * 创建时间：2022-11-02 19:18:02
     * @version 1.0.0
     * @return
     */
    @PostMapping ("/ecgpatients/export")
    public Map<String, String> export(HttpServletResponse response) throws Exception {
        // 从数据库查询出所有的数据
        List<Ecgpatients> list = ecgpatientsService.list();
        List<EcgPatientsBo> list1 = list.stream().map(info -> {
            EcgPatientsBo ecg = new EcgPatientsBo();
            BeanUtils.copyProperties(info, ecg);
            return ecg;
        }).collect(Collectors.toList());
        // 在内存操作，写出到浏览器
        ExcelWriter writer = ExcelUtil.getWriter(true);
        writer.addHeaderAlias("id","Number_1695");
        writer.addHeaderAlias("b", "CHARGEAF");
        writer.addHeaderAlias("c", "Status");
        writer.addHeaderAlias("d", "FollowUpTime");
        writer.addHeaderAlias("e", "HF");
        writer.addHeaderAlias("f", "NYHAclassification");
        writer.addHeaderAlias("g", "HBP");
        writer.addHeaderAlias("h", "HBPstage");
        writer.addHeaderAlias("i", "DM");
        writer.addHeaderAlias("j", "CKD");
        writer.addHeaderAlias("k", "CKDstage");
        writer.addHeaderAlias("l", "OSAHS");
        writer.addHeaderAlias("m", "COPD");
        writer.addHeaderAlias("n", "StrokeTIA");
        writer.addHeaderAlias("o", "Embolism");
        writer.addHeaderAlias("p", "CHD");
        writer.addHeaderAlias("q", "CHADSVASC");
        writer.addHeaderAlias("r", "CHADS");
        writer.addHeaderAlias("s", "R2CHADS");
        writer.addHeaderAlias("t", "HATCH");
        writer.addHeaderAlias("u", "Age");
        writer.addHeaderAlias("v", "Height");
        writer.addHeaderAlias("w", "Weight");
        writer.addHeaderAlias("x", "Bmi");
        writer.addHeaderAlias("y", "SBP");
        writer.addHeaderAlias("z", "DBP");
        writer.addHeaderAlias("aa", "eGFR");
        writer.addHeaderAlias("ab", "LAD");
        writer.addHeaderAlias("ac", "LVD");
        writer.addHeaderAlias("ad", "EDT");
        writer.addHeaderAlias("ae", "LVEF");
        writer.addHeaderAlias("af", "BNP");
        writer.addHeaderAlias("ag", "Vppercentage");
        writer.addHeaderAlias("ah", "Pmax");
        writer.addHeaderAlias("ai", "Pdispersion");
        writer.addHeaderAlias("aj", "PDurationMean");
        writer.addHeaderAlias("ak", "PDurationMax");
        writer.addHeaderAlias("al", "PTFV1");
        writer.addHeaderAlias("am", "PAreaMean");
        writer.addHeaderAlias("an", "PAreaMax");
        writer.addHeaderAlias("ao", "PAxis");
        // 一次性写出list内的对象到excel，使用默认样式，强制输出标题
        writer.write(list1, true);
        String filePath = "D:\\project\\PycharmProjects\\wzc\\pure-design-vip\\doc\\template1.xlsx";

        OutputStream out = new FileOutputStream(filePath);
        writer.flush(out, true);
        out.close();
        writer.close();
        Map<String, String> map = new HashMap<>();
        map.put("path",filePath);
        return map;
    }
    @PostMapping("/ecgpatients/predict")
    public int save1(@RequestBody String list) {
//        Map<Integer,Double> map = JSONUtil.toBean(list,HashMap.class);
//        List<EcgPatients> list1 = ecgPatientsService.list();
//        int i = 0;
//        for (EcgPatients ecp : list1) {
//            String s = String.valueOf(map.get(String.valueOf(i)));
//            ecp.setData(Double.parseDouble(s));
//            i++;
//           ecgPatientsService.saveOrUpdate(ecp);
//        }
        String list2 = list.replace("%5B", "[");
        String list3 = list2.replace("%2C", ",");
        String list4 = list3.replace("%5D", "]");
        String list5 = list4.replace("=", "");
        System.out.println(list5);
        Double[][] db = JSON.parseObject(list5, Double[][].class);
        System.out.println(db.length);
        System.out.println(db[0].length);
        List<Ecgpatients> list1 = ecgpatientsService.list();
        int i = 0;
        for (Ecgpatients ecg : list1) {
            ecg.setPy(db[i][1]);
            i++;
            ecgpatientsService.saveOrUpdate(ecg);
        }
        return 1;
    }
    @PostMapping("/ecgpatients/reset")
    public int reset(){
        List<Ecgpatients> list1 = ecgpatientsService.list();
        for (Ecgpatients ecg : list1) {
            ecg.setPy(0.0);
            ecgpatientsService.saveOrUpdate(ecg);
        }
        return 1;
    }
    @PostMapping("/ecgpatients/list")
    @PugDoc(name="查询脓毒症信息管理列表信息")
    public List<EcgpatientsBo> findEcgpatientsList() {
        return ecgpatientsService.findEcgpatientsList();
    }
    @GetMapping  ("/ecgpatients/export")
    public void export1(HttpServletResponse response) throws Exception {
        // 从数据库查询出所有的数据
        List<Ecgpatients> list = ecgpatientsService.list();
        List<EcgPatientsBo> list1 = list.stream().map(info -> {
            EcgPatientsBo ecg = new EcgPatientsBo();
            BeanUtils.copyProperties(info, ecg);
            return ecg;
        }).collect(Collectors.toList());
        // 在内存操作，写出到浏览器
        ExcelWriter writer = ExcelUtil.getWriter(true);
        writer.addHeaderAlias("id","Number_1695");
        writer.addHeaderAlias("b", "CHARGEAF");
        writer.addHeaderAlias("c", "Status");
        writer.addHeaderAlias("d", "FollowUpTime");
        writer.addHeaderAlias("e", "HF");
        writer.addHeaderAlias("f", "NYHAclassification");
        writer.addHeaderAlias("g", "HBP");
        writer.addHeaderAlias("h", "HBPstage");
        writer.addHeaderAlias("i", "DM");
        writer.addHeaderAlias("j", "CKD");
        writer.addHeaderAlias("k", "CKDstage");
        writer.addHeaderAlias("l", "OSAHS");
        writer.addHeaderAlias("m", "COPD");
        writer.addHeaderAlias("n", "StrokeTIA");
        writer.addHeaderAlias("o", "Embolism");
        writer.addHeaderAlias("p", "CHD");
        writer.addHeaderAlias("q", "CHADSVASC");
        writer.addHeaderAlias("r", "CHADS");
        writer.addHeaderAlias("s", "R2CHADS");
        writer.addHeaderAlias("t", "HATCH");
        writer.addHeaderAlias("u", "Age");
        writer.addHeaderAlias("v", "Height");
        writer.addHeaderAlias("w", "Weight");
        writer.addHeaderAlias("x", "Bmi");
        writer.addHeaderAlias("y", "SBP");
        writer.addHeaderAlias("z", "DBP");
        writer.addHeaderAlias("aa", "eGFR");
        writer.addHeaderAlias("ab", "LAD");
        writer.addHeaderAlias("ac", "LVD");
        writer.addHeaderAlias("ad", "EDT");
        writer.addHeaderAlias("ae", "LVEF");
        writer.addHeaderAlias("af", "BNP");
        writer.addHeaderAlias("ag", "Vppercentage");
        writer.addHeaderAlias("ah", "Pmax");
        writer.addHeaderAlias("ai", "Pdispersion");
        writer.addHeaderAlias("aj", "PDurationMean");
        writer.addHeaderAlias("ak", "PDurationMax");
        writer.addHeaderAlias("al", "PTFV1");
        writer.addHeaderAlias("am", "PAreaMean");
        writer.addHeaderAlias("an", "PAreaMax");
        writer.addHeaderAlias("ao", "PAxis");
        // 一次性写出list内的对象到excel，使用默认样式，强制输出标题
        writer.write(list1, true);

        // 设置浏览器响应的格式
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8");
        String fileName = URLEncoder.encode("EcgPatients信息表", "UTF-8");
        response.setHeader("Content-Disposition", "attachment;filename=" + fileName + ".xlsx");

        ServletOutputStream out = response.getOutputStream();
        writer.flush(out, true);
        out.close();
        writer.close();

    }
    @PostMapping("/ecgpatients/import")
    public boolean imp(MultipartFile file) throws Exception {
        InputStream inputStream = file.getInputStream();
        ExcelReader reader = ExcelUtil.getReader(inputStream);
        List<Map<String,Object>> readAll = reader.readAll();
        System.out.println(readAll);
        // 通过 javabean的方式读取Excel内的对象，但是要求表头必须是英文，跟javabean的属性要对应起来
        List<List<Object>> list = reader.read(1);
        List<Ecgpatients> sps = CollUtil.newArrayList();
        for (List<Object> row : list) {
            Ecgpatients ecgPatients = new Ecgpatients();
            if (row.get(0).toString() != ""){
                ecgPatients.setId(Integer.parseInt(row.get(0).toString()));
            }
            if (row.get(1).toString() != ""){
                ecgPatients.setB(Double.parseDouble(row.get(1).toString()));
            }
            if (row.get(2).toString() != ""){
                ecgPatients.setC(Double.parseDouble(row.get(2).toString()));
            }
            if (row.get(3).toString() != ""){
                ecgPatients.setD(Double.parseDouble(row.get(3).toString()));
            }
            if (row.get(4).toString() != ""){
                ecgPatients.setE(Double.parseDouble(row.get(4).toString()));
            }
            if (row.get(5).toString() != ""){
                ecgPatients.setF(Double.parseDouble(row.get(5).toString()));
            }
            if (row.get(6).toString() != ""){
                ecgPatients.setG(Double.parseDouble(row.get(6).toString()));
            }
            if (row.get(7).toString() != ""){
                ecgPatients.setH(Double.parseDouble(row.get(7).toString()));
            }
            if (row.get(8).toString() != ""){
                ecgPatients.setI(Double.parseDouble(row.get(8).toString()));
            }
            if (row.get(9).toString() != ""){
                ecgPatients.setJ(Double.parseDouble(row.get(9).toString()));
            }
            if (row.get(10).toString() != ""){
                ecgPatients.setK(Double.parseDouble(row.get(10).toString()));
            }
            if (row.get(11).toString() != ""){
                ecgPatients.setL(Double.parseDouble(row.get(11).toString()));
            }
            if (row.get(12).toString() != ""){
                ecgPatients.setM(Double.parseDouble(row.get(12).toString()));
            }
            if (row.get(13).toString() != ""){
                ecgPatients.setN(Double.parseDouble(row.get(13).toString()));
            }
            if (row.get(14).toString() != ""){
                ecgPatients.setO(Double.parseDouble(row.get(14).toString()));
            }
            if (row.get(15).toString() != ""){
                ecgPatients.setP(Double.parseDouble(row.get(15).toString()));
            }
            if (row.get(16).toString() != ""){
                ecgPatients.setQ(Double.parseDouble(row.get(16).toString()));
            }
            if (row.get(17).toString() != ""){
                ecgPatients.setR(Double.parseDouble(row.get(17).toString()));
            }
            if (row.get(18).toString() != ""){
                ecgPatients.setS(Double.parseDouble(row.get(18).toString()));
            }
            if (row.get(19).toString() != ""){
                ecgPatients.setT(Double.parseDouble(row.get(19).toString()));
            }
            if (row.get(20).toString() != ""){
                ecgPatients.setU(Double.parseDouble(row.get(20).toString()));
            }
            if (row.get(21).toString() != ""){
                ecgPatients.setV(Double.parseDouble(row.get(21).toString()));
            }
            if (row.get(22).toString() != ""){
                ecgPatients.setW(Double.parseDouble(row.get(22).toString()));
            }
            if (row.get(23).toString() != ""){
                ecgPatients.setX(Double.parseDouble(row.get(23).toString()));
            }
            if (row.get(24).toString() != ""){
                ecgPatients.setY(Double.parseDouble(row.get(24).toString()));
            }
            if (row.get(25).toString() != ""){
                ecgPatients.setZ(Double.parseDouble(row.get(25).toString()));
            }
            if (row.get(26).toString() != ""){
                ecgPatients.setAa(Double.parseDouble(row.get(26).toString()));
            }
            if (row.get(27).toString() != ""){
                ecgPatients.setAb(Double.parseDouble(row.get(27).toString()));
            }
            if (row.get(28).toString() != ""){
                ecgPatients.setAc(Double.parseDouble(row.get(28).toString()));
            }
            if (row.get(29).toString() != ""){
                ecgPatients.setAd(Double.parseDouble(row.get(29).toString()));
            }
            if (row.get(30).toString() != ""){
                ecgPatients.setAe(Double.parseDouble(row.get(30).toString()));
            }
            if (row.get(31).toString() != ""){
                ecgPatients.setAf(Double.parseDouble(row.get(31).toString()));
            }
            if (row.get(32).toString() != ""){
                ecgPatients.setAg(Double.parseDouble(row.get(32).toString()));
            }
            if (row.get(33).toString() != ""){
                ecgPatients.setAh(Double.parseDouble(row.get(33).toString()));
            }
            if (row.get(34).toString() != ""){
                ecgPatients.setAi(Double.parseDouble(row.get(34).toString()));
            }
            if (row.get(35).toString() != ""){
                ecgPatients.setAj(Double.parseDouble(row.get(35).toString()));
            }
            if (row.get(36).toString() != ""){
                ecgPatients.setAk(Double.parseDouble(row.get(36).toString()));
            }
            if (row.get(37).toString() != ""){
                ecgPatients.setAl(Double.parseDouble(row.get(37).toString()));
            }
            if (row.get(38).toString() != ""){
                ecgPatients.setAm(Double.parseDouble(row.get(38).toString()));
            }
            if (row.get(39).toString() != ""){
                ecgPatients.setAn(Double.parseDouble(row.get(39).toString()));
            }
            if (row.get(40).toString() != ""){
                ecgPatients.setAo(Double.parseDouble(row.get(40).toString()));
            }
            sps.add(ecgPatients);
        }

        return ecgpatientsService.saveBatch(sps);
    }
	/**
	 * 查询脓毒症信息管理列表信息并分页
	 * @path : /admin/ecgpatients/load
     * @method: findEcgpatientss
     * @param : ecgpatientsVo
     * @result : IPage<EcgpatientsBo>
	 * 创建人:yykk
	 * 创建时间：2022-11-02 19:18:02
	 * @version 1.0.0
	*/
    @PostMapping("/ecgpatients/load")
    @PugDoc(name="查询脓毒症信息管理列表信息并分页")
    public IPage<EcgpatientsBo> findEcgpatientss(@RequestBody EcgpatientsVo ecgpatientsVo) {
        return ecgpatientsService.findEcgpatientsPage(ecgpatientsVo);
    }
    @PostMapping("/ecgpatients/load2")
    @PugDoc(name="查询脓毒症信息管理列表信息并分页")
    public IPage<EcgpatientsBo> findEcgpatientss2(@RequestBody EcgpatientsVo ecgpatientsVo) {
        return ecgpatientsService.findEcgpatientsPage2(ecgpatientsVo);
    }
    /**
     * 根据脓毒症信息管理id查询明细信息
     * @method: get/{id}
     * @path : /admin/ecgpatients/get/{id}
     * @param : id
     * @result : EcgpatientsBo
     * 创建人:yykk
     * 创建时间：2022-11-02 19:18:02
     * @version 1.0.0
    */
    @PostMapping("/ecgpatients/get/{id}")
    @PugDoc(name="根据脓毒症信息管理id查询明细信息")
    public EcgpatientsBo getEcgpatientsById(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
           throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return ecgpatientsService.getEcgpatientsById(new Integer(id));
    }


	/**
	 * 保存和修改脓毒症信息管理
     * @method: saveupdate
	 * @path : /admin/ecgpatients/save
     * @param : ecgpatients
     * @result : EcgpatientsBo
	 * 创建人:yykk
	 * 创建时间：2022-11-02 19:18:02
	 * @version 1.0.0
	*/
    @PostMapping("/ecgpatients/saveupdate")
    @PugDoc(name="保存和修改脓毒症信息管理")
    public EcgpatientsBo saveupdateEcgpatients(@RequestBody Ecgpatients ecgpatients) {
        return ecgpatientsService.saveupdateEcgpatients(ecgpatients);
    }


    /**
	 * 根据脓毒症信息管理id删除脓毒症信息管理
     * @method: delete/{id}
     * @path : /admin/ecgpatients/delete/{id}
     * @param : id
     * @result : int
	 * 创建人:yykk
	 * 创建时间：2022-11-02 19:18:02
	 * @version 1.0.0
	*/
    @PostMapping("/ecgpatients/delete/{id}")
    @PugDoc(name="根据脓毒症信息管理id删除脓毒症信息管理")
    public int deleteEcgpatientsById(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        Ecgpatients ecgpatients = new Ecgpatients();
        ecgpatients.setId(new Integer(id));
        ecgpatients.setIsdelete(1);

        return 0;
    }
    @PostMapping("/ecgpatients/delete2/{id}")
    @PugDoc(name="根据脓毒症信息管理id删除脓毒症信息管理")
    public int deleteEcgpatientsById2(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        boolean flag = ecgpatientsService.removeById(new Integer(id));
        if (flag){
            return 1;
        }
        return 0;
    }


   /**
   	 * 根据脓毒症信息管理ids批量删除脓毒症信息管理
     * @method: ecgpatients/delBatch
     * @path : /admin/ecgpatients/delBatch
     * @param : ecgpatientsVo
     * @result : boolean
   	 * 创建人:yykk
   	 * 创建时间：2022-11-02 19:18:02
   	 * @version 1.0.0
   	*/
    @PostMapping("/ecgpatients/delBatch")
    @PugDoc(name="根据脓毒症信息管理ids批量删除脓毒症信息管理")
    public boolean delEcgpatients(@RequestBody EcgpatientsVo ecgpatientsVo) {
        log.info("你要批量删除的IDS是:{}", ecgpatientsVo.getBatchIds());
        if (Vsserts.isEmpty(ecgpatientsVo.getBatchIds())) {
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }

        return ecgpatientsService.delBatchEcgpatients(ecgpatientsVo.getBatchIds());
    }
    @PostMapping("/ecgpatients/delBatch2")
    @PugDoc(name="根据脓毒症信息管理ids批量删除脓毒症信息管理")
    public boolean delEcgpatients2(@RequestBody EcgpatientsVo ecgpatientsVo) {
        log.info("你要批量删除的IDS是:{}", ecgpatientsVo.getBatchIds());
        if (Vsserts.isEmpty(ecgpatientsVo.getBatchIds())) {
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }

        return ecgpatientsService.delBatchEcgpatients2(ecgpatientsVo.getBatchIds());
    }
}