package com.pug.zixun;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 飞哥B站地址：https://space.bilibili.com/490711252
 * 记得关注和三连哦！
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2022/5/13$ 21:00$
 *
 * xml
 * <context-scan basePackage="com.pug.zixun"></context-scan>
 * <context-scan basePackage="org.pug.zixun"></context-scan>
 */
@SpringBootApplication
@MapperScan("com.pug.zixun.mapper")
public class AdminWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdminWebApplication.class);
    }

}
