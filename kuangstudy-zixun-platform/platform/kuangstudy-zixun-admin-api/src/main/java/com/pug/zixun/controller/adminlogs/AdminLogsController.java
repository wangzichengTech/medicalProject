package com.pug.zixun.controller.adminlogs;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pug.zixun.service.adminlogs.IAdminLogsService;
import com.pug.zixun.pojo.AdminLogs;
import com.pug.zixun.vo.AdminLogsVo;
import com.pug.zixun.bo.AdminLogsBo;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import com.pug.zixun.controller.BaseController;
import java.util.List;
import org.pug.generator.anno.PugDoc;
/**
 * AdminLogsController
 * 创建人:yykk<br/>
 * 时间：2022-07-26 20:20:25 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/

@RestController
@RequiredArgsConstructor
@Slf4j
@PugDoc(name="后台操作日志",tabname="kss_admin_logs")
public class AdminLogsController extends BaseController{

    private final IAdminLogsService adminlogsService;


    /**
     * 查询后台操作日志列表信息
     * @path : /admin/adminlogs/list
     * @method: findAdminLogss
     * @result : List<AdminLogsBo>
     * 创建人:yykk
     * 创建时间：2022-07-26 20:20:25
     * @version 1.0.0
     * @return
     */
    @PostMapping("/adminlogs/list")
    @PugDoc(name="查询后台操作日志列表信息")
    public List<AdminLogsBo> findAdminLogsList() {
        return adminlogsService.findAdminLogsList();
    }

	/**
	 * 查询后台操作日志列表信息并分页
	 * @path : /admin/adminlogs/load
     * @method: findAdminLogss
     * @param : adminlogsVo
     * @result : IPage<AdminLogsBo>
	 * 创建人:yykk
	 * 创建时间：2022-07-26 20:20:25
	 * @version 1.0.0
	*/
    @PostMapping("/adminlogs/load")
    @PugDoc(name="查询后台操作日志列表信息并分页")
    public IPage<AdminLogsBo> findAdminLogss(@RequestBody AdminLogsVo adminlogsVo) {
        return adminlogsService.findAdminLogsPage(adminlogsVo);
    }


    /**
     * 根据后台操作日志id查询明细信息
     * @method: get/{id}
     * @path : /admin/adminlogs/get/{id}
     * @param : id
     * @result : AdminLogsBo
     * 创建人:yykk
     * 创建时间：2022-07-26 20:20:25
     * @version 1.0.0
    */
    @PostMapping("/adminlogs/get/{id}")
    @PugDoc(name="根据后台操作日志id查询明细信息")
    public AdminLogsBo getAdminLogsById(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
           throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return adminlogsService.getAdminLogsById(new Long(id));
    }


	/**
	 * 保存和修改后台操作日志
     * @method: saveupdate
	 * @path : /admin/adminlogs/save
     * @param : adminlogs
     * @result : AdminLogsBo
	 * 创建人:yykk
	 * 创建时间：2022-07-26 20:20:25
	 * @version 1.0.0
	*/
    @PostMapping("/adminlogs/saveupdate")
    @PugDoc(name="保存和修改后台操作日志")
    public AdminLogsBo saveupdateAdminLogs(@RequestBody AdminLogs adminlogs) {
        return adminlogsService.saveupdateAdminLogs(adminlogs);
    }


    /**
	 * 根据后台操作日志id删除后台操作日志
     * @method: delete/{id}
     * @path : /admin/adminlogs/delete/{id}
     * @param : id
     * @result : int
	 * 创建人:yykk
	 * 创建时间：2022-07-26 20:20:25
	 * @version 1.0.0
	*/
    @PostMapping("/adminlogs/delete/{id}")
    @PugDoc(name="根据后台操作日志id删除后台操作日志")
    public int deleteAdminLogsById(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        boolean flag = adminlogsService.removeById(new Long(id));
        if (flag){
            return 1;
        }
        return 0;
    }


   /**
   	 * 根据后台操作日志ids批量删除后台操作日志
     * @method: adminlogs/delBatch
     * @path : /admin/adminlogs/delBatch
     * @param : adminlogsVo
     * @result : boolean
   	 * 创建人:yykk
   	 * 创建时间：2022-07-26 20:20:25
   	 * @version 1.0.0
   	*/
    @PostMapping("/adminlogs/delBatch")
    @PugDoc(name="根据后台操作日志ids批量删除后台操作日志")
    public boolean delAdminLogs(@RequestBody AdminLogsVo adminlogsVo) {
        log.info("你要批量删除的IDS是:{}", adminlogsVo.getBatchIds());
        if (Vsserts.isEmpty(adminlogsVo.getBatchIds())) {
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return adminlogsService.delBatchAdminLogs(adminlogsVo.getBatchIds());
    }
}