package com.pug.zixun.controller.resourcecategory;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pug.zixun.bo.ResourceCategoryBo;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;
import com.pug.zixun.controller.BaseController;
import com.pug.zixun.pojo.ResourceCategory;
import com.pug.zixun.service.resourcecategory.IResourceCategoryService;
import com.pug.zixun.vo.ResourceCategoryVo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.pug.generator.anno.PugDoc;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
/**
 * ResourceCategoryController
 * 创建人:yykk<br/>
 * 时间：2022-07-19 23:49:04 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/

@RestController
@RequiredArgsConstructor
@Slf4j
@PugDoc(name="资源分类",tabname="kss_resource_category")
public class ResourceCategoryController extends BaseController{

    private final IResourceCategoryService resourcecategoryService;


    /**
     * 查询资源分类列表信息
     * @path : /admin/resourcecategory/list
     * @method: findResourceCategorys
     * @result : List<ResourceCategoryBo>
     * 创建人:yykk
     * 创建时间：2022-07-19 23:49:04
     * @version 1.0.0
     * @return
     */
    @PostMapping("/resourcecategory/list")
    @PugDoc(name="查询资源分类列表信息")
    public List<ResourceCategoryBo> findResourceCategoryList() {
        return resourcecategoryService.findResourceCategoryList();
    }

	/**
	 * 查询资源分类列表信息并分页
	 * @path : /admin/resourcecategory/load
     * @method: findResourceCategorys
     * @param : resourcecategoryVo
     * @result : IPage<ResourceCategoryBo>
	 * 创建人:yykk
	 * 创建时间：2022-07-19 23:49:04
	 * @version 1.0.0
	*/
    @PostMapping("/resourcecategory/load")
    @PugDoc(name="查询资源分类列表信息并分页")
    public IPage<ResourceCategoryBo> findResourceCategorys(@RequestBody ResourceCategoryVo resourcecategoryVo) {
        return resourcecategoryService.findResourceCategoryPage(resourcecategoryVo);
    }


    /**
     * 根据资源分类id查询明细信息
     * @method: get/{id}
     * @path : /admin/resourcecategory/get/{id}
     * @param : id
     * @result : ResourceCategoryBo
     * 创建人:yykk
     * 创建时间：2022-07-19 23:49:04
     * @version 1.0.0
    */
    @PostMapping("/resourcecategory/get/{id}")
    @PugDoc(name="根据资源分类id查询明细信息")
    public ResourceCategoryBo getResourceCategoryById(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
           throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return resourcecategoryService.getResourceCategoryById(new Long(id));
    }


	/**
	 * 保存和修改资源分类
     * @method: saveupdate
	 * @path : /admin/resourcecategory/save
     * @param : resourcecategory
     * @result : ResourceCategoryBo
	 * 创建人:yykk
	 * 创建时间：2022-07-19 23:49:04
	 * @version 1.0.0
	*/
    @PostMapping("/resourcecategory/saveupdate")
    @PugDoc(name="保存和修改资源分类")
    public ResourceCategoryBo saveupdateResourceCategory(@RequestBody ResourceCategory resourcecategory) {
        return resourcecategoryService.saveupdateResourceCategory(resourcecategory);
    }


    /**
	 * 根据资源分类id删除资源分类
     * @method: delete/{id}
     * @path : /admin/resourcecategory/delete/{id}
     * @param : id
     * @result : int
	 * 创建人:yykk
	 * 创建时间：2022-07-19 23:49:04
	 * @version 1.0.0
	*/
    @PostMapping("/resourcecategory/delete/{id}")
    @PugDoc(name="根据资源分类id删除资源分类")
    public int deleteResourceCategoryById(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return resourcecategoryService.deleteResourceCategoryById(new Long(id));
    }


   /**
   	 * 根据资源分类ids批量删除资源分类
     * @method: resourcecategory/delBatch
     * @path : /admin/resourcecategory/delBatch
     * @param : resourcecategoryVo
     * @result : boolean
   	 * 创建人:yykk
   	 * 创建时间：2022-07-19 23:49:04
   	 * @version 1.0.0
   	*/
    @PostMapping("/resourcecategory/delBatch")
    @PugDoc(name="根据资源分类ids批量删除资源分类")
    public boolean delResourceCategory(@RequestBody ResourceCategoryVo resourcecategoryVo) {
        log.info("你要批量删除的IDS是:{}", resourcecategoryVo.getBatchIds());
        if (Vsserts.isEmpty(resourcecategoryVo.getBatchIds())) {
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return resourcecategoryService.delBatchResourceCategory(resourcecategoryVo.getBatchIds());
    }


    /**
     * 根据资源分类ids批量删除资源分类
     * @method: resourcecategory/delBatch
     * @path : /admin/resourcecategory/delBatch
     * @param : resourcecategoryVo
     * @result : boolean
     * 创建人:yykk
     * 创建时间：2022-07-19 23:49:04
     * @version 1.0.0
     */
    @PostMapping("/resourcecategory/update")
    @PugDoc(name="根据id更改排序和相关信息")
    public boolean updateRelation(@RequestBody ResourceCategory resourceCategory) {
        return resourcecategoryService.updateById(resourceCategory);
    }
}