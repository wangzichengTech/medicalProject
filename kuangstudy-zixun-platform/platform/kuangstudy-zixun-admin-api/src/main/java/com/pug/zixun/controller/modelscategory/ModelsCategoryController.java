package com.pug.zixun.controller.modelscategory;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pug.zixun.service.modelscategory.IModelsCategoryService;
import com.pug.zixun.pojo.ModelsCategory;
import com.pug.zixun.vo.ModelsCategoryVo;
import com.pug.zixun.bo.ModelsCategoryBo;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import com.pug.zixun.controller.BaseController;
import java.util.List;
import org.pug.generator.anno.PugDoc;
/**
 * ModelsCategoryController
 * 创建人:yykk<br/>
 * 时间：2022-12-15 20:23:14 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/

@RestController
@RequiredArgsConstructor
@Slf4j
@PugDoc(name="模型分类管理",tabname="kss_models_category")
public class ModelsCategoryController extends BaseController{

    private final IModelsCategoryService modelscategoryService;


    /**
     * 查询模型分类管理列表信息
     * @path : /admin/modelscategory/list
     * @method: findModelsCategorys
     * @result : List<ModelsCategoryBo>
     * 创建人:yykk
     * 创建时间：2022-12-15 20:23:14
     * @version 1.0.0
     * @return
     */
    @PostMapping("/modelscategory/list")
    @PugDoc(name="查询模型分类管理列表信息")
    public List<ModelsCategoryBo> findModelsCategoryList() {
        return modelscategoryService.findModelsCategoryList();
    }

	/**
	 * 查询模型分类管理列表信息并分页
	 * @path : /admin/modelscategory/load
     * @method: findModelsCategorys
     * @param : modelscategoryVo
     * @result : IPage<ModelsCategoryBo>
	 * 创建人:yykk
	 * 创建时间：2022-12-15 20:23:14
	 * @version 1.0.0
	*/
    @PostMapping("/modelscategory/load")
    @PugDoc(name="查询模型分类管理列表信息并分页")
    public IPage<ModelsCategoryBo> findModelsCategorys(@RequestBody ModelsCategoryVo modelscategoryVo) {
        return modelscategoryService.findModelsCategoryPage(modelscategoryVo);
    }


    /**
     * 根据模型分类管理id查询明细信息
     * @method: get/{id}
     * @path : /admin/modelscategory/get/{id}
     * @param : id
     * @result : ModelsCategoryBo
     * 创建人:yykk
     * 创建时间：2022-12-15 20:23:14
     * @version 1.0.0
    */
    @PostMapping("/modelscategory/get/{id}")
    @PugDoc(name="根据模型分类管理id查询明细信息")
    public ModelsCategoryBo getModelsCategoryById(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
           throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return modelscategoryService.getModelsCategoryById(new Long(id));
    }


	/**
	 * 保存和修改模型分类管理
     * @method: saveupdate
	 * @path : /admin/modelscategory/save
     * @param : modelscategory
     * @result : ModelsCategoryBo
	 * 创建人:yykk
	 * 创建时间：2022-12-15 20:23:14
	 * @version 1.0.0
	*/
    @PostMapping("/modelscategory/saveupdate")
    @PugDoc(name="保存和修改模型分类管理")
    public ModelsCategoryBo saveupdateModelsCategory(@RequestBody ModelsCategory modelscategory) {
        return modelscategoryService.saveupdateModelsCategory(modelscategory);
    }


    /**
	 * 根据模型分类管理id删除模型分类管理
     * @method: delete/{id}
     * @path : /admin/modelscategory/delete/{id}
     * @param : id
     * @result : int
	 * 创建人:yykk
	 * 创建时间：2022-12-15 20:23:14
	 * @version 1.0.0
	*/
    @PostMapping("/modelscategory/delete/{id}")
    @PugDoc(name="根据模型分类管理id删除模型分类管理")
    public int deleteModelsCategoryById(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return modelscategoryService.deleteModelsCategoryById(new Long(id));
    }


   /**
   	 * 根据模型分类管理ids批量删除模型分类管理
     * @method: modelscategory/delBatch
     * @path : /admin/modelscategory/delBatch
     * @param : modelscategoryVo
     * @result : boolean
   	 * 创建人:yykk
   	 * 创建时间：2022-12-15 20:23:14
   	 * @version 1.0.0
   	*/
    @PostMapping("/modelscategory/delBatch")
    @PugDoc(name="根据模型分类管理ids批量删除模型分类管理")
    public boolean delModelsCategory(@RequestBody ModelsCategoryVo modelscategoryVo) {
        log.info("你要批量删除的IDS是:{}", modelscategoryVo.getBatchIds());
        if (Vsserts.isEmpty(modelscategoryVo.getBatchIds())) {
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return modelscategoryService.delBatchModelsCategory(modelscategoryVo.getBatchIds());
    }
}