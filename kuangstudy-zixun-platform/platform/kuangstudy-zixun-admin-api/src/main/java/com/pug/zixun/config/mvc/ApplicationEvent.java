package com.pug.zixun.config.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.MessageSource;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
 
@Component
public class ApplicationEvent implements ApplicationListener<ContextRefreshedEvent> {
    @Autowired
    protected MessageSource messageSource;
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        MessageUtils.setMessageSource(messageSource);
    }
}
 