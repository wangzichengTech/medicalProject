package com.pug.zixun.controller.note;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pug.zixun.service.note.INoteService;
import com.pug.zixun.pojo.Note;
import com.pug.zixun.vo.NoteVo;
import com.pug.zixun.bo.NoteBo;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import com.pug.zixun.controller.BaseController;
import java.util.List;
import org.pug.generator.anno.PugDoc;
/**
 * NoteController
 * 创建人:yykk<br/>
 * 时间：2022-07-30 21:42:35 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/

@RestController
@RequiredArgsConstructor
@Slf4j
@PugDoc(name="笔记专栏管理",tabname="kss_note")
public class NoteController extends BaseController{

    private final INoteService noteService;


    /**
     * 查询笔记专栏管理列表信息
     * @path : /admin/note/list
     * @method: findNotes
     * @result : List<NoteBo>
     * 创建人:yykk
     * 创建时间：2022-07-30 21:42:35
     * @version 1.0.0
     * @return
     */
    @PostMapping("/note/list")
    @PugDoc(name="查询笔记专栏管理列表信息")
    public List<NoteBo> findNoteList() {
        return noteService.findNoteList();
    }

	/**
	 * 查询笔记专栏管理列表信息并分页
	 * @path : /admin/note/load
     * @method: findNotes
     * @param : noteVo
     * @result : IPage<NoteBo>
	 * 创建人:yykk
	 * 创建时间：2022-07-30 21:42:35
	 * @version 1.0.0
	*/
    @PostMapping("/note/load")
    @PugDoc(name="查询笔记专栏管理列表信息并分页")
    public IPage<NoteBo> findNotes(@RequestBody NoteVo noteVo) {
        return noteService.findNotePage(noteVo);
    }


    /**
     * 根据笔记专栏管理id查询明细信息
     * @method: get/{id}
     * @path : /admin/note/get/{id}
     * @param : id
     * @result : NoteBo
     * 创建人:yykk
     * 创建时间：2022-07-30 21:42:35
     * @version 1.0.0
    */
    @PostMapping("/note/get/{id}")
    @PugDoc(name="根据笔记专栏管理id查询明细信息")
    public NoteBo getNoteById(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
           throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return noteService.getNoteById(new Long(id));
    }


	/**
	 * 保存和修改笔记专栏管理
     * @method: saveupdate
	 * @path : /admin/note/save
     * @param : note
     * @result : NoteBo
	 * 创建人:yykk
	 * 创建时间：2022-07-30 21:42:35
	 * @version 1.0.0
	*/
    @PostMapping("/note/saveupdate")
    @PugDoc(name="保存和修改笔记专栏管理")
    public NoteBo saveupdateNote(@RequestBody Note note) {
        return noteService.saveupdateNote(note);
    }


    /**
	 * 根据笔记专栏管理id删除笔记专栏管理
     * @method: delete/{id}
     * @path : /admin/note/delete/{id}
     * @param : id
     * @result : int
	 * 创建人:yykk
	 * 创建时间：2022-07-30 21:42:35
	 * @version 1.0.0
	*/
    @PostMapping("/note/delete/{id}")
    @PugDoc(name="根据笔记专栏管理id删除笔记专栏管理")
    public int deleteNoteById(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return noteService.deleteNoteById(new Long(id));
    }


   /**
   	 * 根据笔记专栏管理ids批量删除笔记专栏管理
     * @method: note/delBatch
     * @path : /admin/note/delBatch
     * @param : noteVo
     * @result : boolean
   	 * 创建人:yykk
   	 * 创建时间：2022-07-30 21:42:35
   	 * @version 1.0.0
   	*/
    @PostMapping("/note/delBatch")
    @PugDoc(name="根据笔记专栏管理ids批量删除笔记专栏管理")
    public boolean delNote(@RequestBody NoteVo noteVo) {
        log.info("你要批量删除的IDS是:{}", noteVo.getBatchIds());
        if (Vsserts.isEmpty(noteVo.getBatchIds())) {
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return noteService.delBatchNote(noteVo.getBatchIds());
    }
}