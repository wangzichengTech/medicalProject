//package com.pug.zixun.config.jwt;
//
//import com.alibaba.fastjson.JSON;
//import com.auth0.jwt.JWT;
//import com.auth0.jwt.JWTVerifier;
//import com.auth0.jwt.algorithms.Algorithm;
//import com.auth0.jwt.exceptions.TokenExpiredException;
//import com.auth0.jwt.interfaces.DecodedJWT;
//import com.pug.zixun.commons.enums.AdminUserResultEnum;
//import com.pug.zixun.commons.ex.PugValidatorException;
//import com.pug.zixun.commons.utils.date.TmDateUtil;
//import com.pug.zixun.commons.utils.fn.asserts.Vsserts;
//import com.pug.zixun.commons.utils.json.FastJsonUtil;
//import com.pug.zixun.commons.utils.json.JsonUtil;
//import com.pug.zixun.config.redis.AdminRedisKeyManager;
//import freemarker.template.ObjectWrapper;
//import org.apache.commons.codec.binary.Base64;
//import org.apache.commons.io.FileUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.core.io.ClassPathResource;
//import org.springframework.data.redis.core.StringRedisTemplate;
//import org.springframework.security.core.userdetails.User;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.jwt.Jwt;
//import org.springframework.security.jwt.JwtHelper;
//import org.springframework.security.jwt.crypto.sign.RsaSigner;
//import org.springframework.security.jwt.crypto.sign.RsaVerifier;
//import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;
//import org.springframework.stereotype.Component;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.security.*;
//import java.security.interfaces.RSAPrivateKey;
//import java.security.interfaces.RSAPublicKey;
//import java.security.spec.InvalidKeySpecException;
//import java.security.spec.PKCS8EncodedKeySpec;
//import java.security.spec.X509EncodedKeySpec;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.concurrent.TimeUnit;
//
///**
// * @author 飞哥
// * @Title: 学相伴出品
// * @Description: 飞哥B站地址：https://space.bilibili.com/490711252
// * 记得关注和三连哦！
// * @Description: 我们有一个学习网站：https://www.kuangstudy.com
// * @date 2022/5/19$ 23:25$
// */
//// 问题1：别人工具类 static方法，方便进行操作和获取。
//// 问题2：为什么要让上面容器管理，因为我要考虑把这里常量用配置文件管理，甚至用统一配置中心。所以
//@Component
//public class JwtKeyService implements AdminRedisKeyManager {
//    //    加密算法
//    private static final String KEY_ALGORITHM = "RSA";
//    //    公钥key
//    private static final String PUB_KEY = "publicKey";
//    //    私钥key
//    private static final String PRI_KEY = "privateKey";
//
//    public static String createJwt2(){
//        KeyStoreKeyFactory  keyStoreKeyFactory = new KeyStoreKeyFactory(
//                new ClassPathResource("jwt/yykk.jks"),"yykk123456".toCharArray()
//        );
//
//        KeyPair keyPair = keyStoreKeyFactory.getKeyPair("yykk","yykk123456".toCharArray());
//
//        RSAPrivateKey privateKey =  (RSAPrivateKey)keyPair.getPrivate();
//        PublicKey publicKey = keyPair.getPublic();
//
//        Map<String,Object> map = new HashMap<>();
//        map.put("id","123");
//        map.put("name","yykk");
//
//        String token = JwtHelper.encode(FastJsonUtil.toJSON(map), new RsaSigner(privateKey)).getEncoded();
//        return token;
//    }
//
//    public static Map<String,Object> parseToken(String token) throws IOException {
//        ClassPathResource classPathResource = new ClassPathResource("jwt/public.key");
//        String publicKey = FileUtils.readFileToString(classPathResource.getFile());
//        Jwt jwt = JwtHelper.decodeAndVerify(token, new RsaVerifier(publicKey));
//        String claims = jwt.getClaims();
//        return JsonUtil.string2Obj(claims,Map.class);
//    }
//
//
//    public static void main(String[] args) throws NoSuchAlgorithmException, InvalidKeySpecException, IOException {
//        String token = createJwt2();
//        System.out.println(token);
//
//        Map<String, Object> map = parseToken(token);
//        System.out.println(map);
//
////        Map<String, String> jwt = createJwt();
////        String publicKey = jwt.get("publicKey");
////        String token = jwt.get("token");
////
////        String s = parseToken(token, publicKey);
////        System.out.println(s);
//
//    }
//
//
//    public static Map<String, String> createJwt() throws NoSuchAlgorithmException, InvalidKeySpecException {
//        Map<String, String> keyMap = generateKey();
//        RSAPrivateKey privateKey = null;
//        privateKey = getPrivateKey(keyMap.get(PRI_KEY));
//        Map<String, Object> map = new HashMap<>();
//        map.put("id", "123");
//        map.put("name", "yykk");
//
//        String token = JwtHelper.encode(JSON.toJSONString(map), new RsaSigner(privateKey)).getEncoded();
//        Map<String, String> rmap = new HashMap<>();
//        rmap.put("token", token);
//        rmap.put("publicKey", keyMap.get(PUB_KEY));
//        return rmap;
//    }
//
//    public static String parseToken(String token,String publicKey) throws NoSuchAlgorithmException, InvalidKeySpecException {
//        Jwt jwt=null;
//        RSAPublicKey rsaPublicKey;
//        rsaPublicKey=getPublicKey(publicKey);
//        jwt=JwtHelper.decodeAndVerify(token, new RsaVerifier(rsaPublicKey) );
//        String claims= jwt.getClaims();
//        return claims;
//    }
//
//    public static Map<String, String> generateKey() throws NoSuchAlgorithmException {
//        Map<String, String> keyMap = new HashMap<>();
//        KeyPairGenerator instance = KeyPairGenerator.getInstance(KEY_ALGORITHM);
//        KeyPair keyPair = instance.generateKeyPair();
//        PrivateKey privateKey = keyPair.getPrivate();
//        PublicKey publicKey = keyPair.getPublic();
//        //Base64 编码
//        byte[] privateKeyEncoded = privateKey.getEncoded();
//        String privateKeyStr = Base64.encodeBase64String(privateKeyEncoded);
//        byte[] publicKeyEncoded = publicKey.getEncoded();
//        String publicKeyStr = Base64.encodeBase64String(publicKeyEncoded);
//        keyMap.put(PUB_KEY, publicKeyStr);
//        keyMap.put(PRI_KEY, privateKeyStr);
//        return keyMap;
//    }
//
//    /**
//     * 得到公钥
//     *
//     * @param publicKey 密钥字符串（经过base64编码）
//     * @throws Exception
//     */
//    public static RSAPublicKey getPublicKey(String publicKey) throws NoSuchAlgorithmException, InvalidKeySpecException {
//        // 通过X509编码的Key指令获得公钥对象
//        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
//        X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(Base64.decodeBase64(publicKey));
//        RSAPublicKey key = (RSAPublicKey) keyFactory.generatePublic(x509KeySpec);
//        return key;
//    }
//
//    /**
//     * 得到私钥pkcs8
//     *
//     * @param privateKey 密钥字符串（经过base64编码）
//     * @throws Exception
//     */
//    public static RSAPrivateKey getPrivateKey(String privateKey)
//            throws NoSuchAlgorithmException, InvalidKeySpecException {
//        // 通过PKCS#8编码的Key指令获得私钥对象
//        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
//        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(Base64.decodeBase64(privateKey));
//        RSAPrivateKey key = (RSAPrivateKey) keyFactory.generatePrivate(pkcs8KeySpec);
//        return key;
//    }
//}
