package com.pug.zixun.controller.upload;

import com.pug.zixun.commons.enums.AdminErrorResultEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.config.minio.MinIOConfig;
import com.pug.zixun.controller.BaseController;
import com.pug.zixun.pojo.Resources;
import com.pug.zixun.service.resources.IResourcesService;
import com.pug.zixun.service.upload.MinIoUploadService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 飞哥B站地址：https://space.bilibili.com/490711252
 * 记得关注和三连哦！
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2022/3/6 17:25
 */
@RestController
@Slf4j
public class MinioUploadController extends BaseController {

    @Autowired
    private MinIoUploadService minIoUploadService;
    @Autowired
    private MinIOConfig minIOConfig;
    @Autowired
    private IResourcesService resourcesService;

    @PostMapping("/upload/minio/file")
    public Map<String, Object> upload(@RequestParam(value = "file") MultipartFile file, HttpServletRequest request) throws Exception {
        // 如果文件是空直接不上传
        if (file.isEmpty()) {
            return null;
        }

        // 如果文件大小大于2M直接直接抛出异常
        if (file.getSize() > 1024 * 1024 * 2) {
            throw new PugValidatorException(AdminErrorResultEnum.FILE_UPLOAD_LIMIT_ERROR);
        }

        // 上传的类型必须是图片
        if (!file.getContentType().equalsIgnoreCase("image/png") &&
                !file.getContentType().equalsIgnoreCase("image/jpg") &&
                !file.getContentType().equalsIgnoreCase("image/jpeg")) {
            throw new PugValidatorException(AdminErrorResultEnum.FILE_UPLOAD_TYPE_ERROR);
        }

        // 指定一个业务的目录进行文件的存储
        String dir = request.getParameter("dir");
        if (StringUtils.isEmpty(dir)) {
            // 如果不指定默认是course
            dir = "course";
        }

        // 获取文件的原始名字
        String originname = file.getOriginalFilename();
        // 文件更名，防止多个用户上传相同的文件进行冲突，所以要更名
        String filename = UUID.randomUUID().toString();

        // 构建日期目录
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        String datePath = dateFormat.format(new Date());// 日期目录：2021/10/27

        String suffix = originname.substring(originname.lastIndexOf("."));
        String newName = filename + suffix;
        String fileUrl = dir + "/" + datePath + "/" + newName;
        /*
          参数1：文件存储的bucket
          参数2：存储在服务商的具体文件名
          参数3：上传的文件
         */
        try {
            // handler调用文件上传的service 得到文件的虚拟路径
            if (!minIoUploadService.isObjectExist(minIOConfig.getBucketName(), fileUrl)) {
                // 创建目录
                minIoUploadService.createDir(minIOConfig.getBucketName(), fileUrl);
            }
            // 开始把文件上传到指定的目录中即可
            minIoUploadService.uploadFile(minIOConfig.getBucketName(), fileUrl, file.getInputStream());
        }catch (Exception ex){
            throw new PugValidatorException(AdminErrorResultEnum.FILE_UPLOAD_FAIL_ERROR);
        }

        // 如果上传成功，就开始组装访问路径
        String imgUrl = minIOConfig.getFileHost()
                + "/"
                + minIOConfig.getBucketName()
                + "/"
                + fileUrl;

        Map<String,Object> map = new HashMap<>();
        map.put("url",imgUrl);
        map.put("size",file.getSize()+"");
        map.put("ext",suffix);
        map.put("filename",originname);
        map.put("newname",filename);
        map.put("rpath",fileUrl);

        // 资源入库 设计模式--- 观察设计模式
        String cid = request.getParameter("cid");
        Resources resources = new Resources();
        resources.setFilename(originname);
        resources.setNewname(newName);
        resources.setStatus(1);
        resources.setIsdelete(0);
        resources.setResourceId(new Long(cid));
        resources.setType(1);
        resources.setFilesize(file.getSize() + "");
        resources.setExt(suffix);
        resources.setPath(imgUrl);
        resources.setDownpath(imgUrl);
        resources.setIspublic(1);
        resourcesService.saveOrUpdate(resources);
        // 返回资源ID
        map.put("resources", resources);
        return map;

    }
}
