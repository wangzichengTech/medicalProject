package com.pug.zixun.controller.sepsispatients;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.json.JSONUtil;
import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pug.zixun.bo.SepsisPatientsTwoBo;
import com.pug.zixun.pojo.Ecgpatients;
import com.pug.zixun.service.sepsispatients.ISepsisPatientsService;
import com.pug.zixun.pojo.SepsisPatients;
import com.pug.zixun.vo.SepsisPatientsVo;
import com.pug.zixun.bo.SepsisPatientsBo;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;
import com.pug.zixun.controller.BaseController;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.pug.generator.anno.PugDoc;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

/**
 * SepsisPatientsController
 * 创建人:yykk<br/>
 * 时间：2022-11-14 19:09:44 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/

@RestController
@RequiredArgsConstructor
@Slf4j
@PugDoc(name="脓毒症信息管理",tabname="kss_sepsis_patients")
public class SepsisPatientsController extends BaseController{

    private final ISepsisPatientsService sepsispatientsService;


    /**
     * 查询脓毒症信息管理列表信息
     * @path : /admin/sepsispatients/list
     * @method: findSepsisPatientss
     * @result : List<SepsisPatientsBo>
     * 创建人:yykk
     * 创建时间：2022-11-14 19:09:44
     * @version 1.0.0
     * @return
     */
    @PostMapping("/sepsispatients/reset")
    public int reset(){
        List<SepsisPatients> list1 = sepsispatientsService.list();
        for (SepsisPatients sepsisPatients : list1) {
            sepsisPatients.setData(0.0);
            sepsispatientsService.saveOrUpdate(sepsisPatients);
        }
        return 1;
    }
    @PostMapping("/sepsispatients/predict")
    public int save1(@RequestBody String list) {
        Map<Integer,Double> map = JSONUtil.toBean(list, HashMap.class);
        List<SepsisPatients> list1 = sepsispatientsService.list();
        int i = 0;
        for (SepsisPatients sepsisPatients : list1) {
            String s = String.valueOf(map.get(String.valueOf(i)));
            sepsisPatients.setData(Double.parseDouble(s));
            i++;
            sepsispatientsService.saveOrUpdate(sepsisPatients);
        }
        System.out.println(list1);
        return 1;
    }
    @PostMapping("/sepsispatients/import")
    public int imp(MultipartFile file) throws Exception {
        InputStream inputStream = file.getInputStream();
        ExcelReader reader = ExcelUtil.getReader(inputStream);
        List<Map<String,Object>> readAll = reader.readAll();
        System.out.println(readAll);
        // 通过 javabean的方式读取Excel内的对象，但是要求表头必须是英文，跟javabean的属性要对应起来
        List<List<Object>> list = reader.read(1);
        List<SepsisPatients> sps = CollUtil.newArrayList();
        for (List<Object> row : list) {
            SepsisPatients sepsisPatients = new SepsisPatients();
            if (row.get(0).toString() != ""){
                sepsisPatients.setSex(Double.parseDouble(row.get(0).toString()));
            }
            if (row.get(1).toString() != ""){
                sepsisPatients.setAge(Double.parseDouble(row.get(1).toString()));
            }
            if (row.get(2).toString() != ""){
                sepsisPatients.setA(Double.parseDouble(row.get(2).toString()));
            }
            if (row.get(3).toString() != ""){
                sepsisPatients.setB(Double.parseDouble(row.get(3).toString()));
            }
            if (row.get(4).toString() != ""){
                sepsisPatients.setC(Double.parseDouble(row.get(4).toString()));
            }
            if (row.get(5).toString() != ""){
                sepsisPatients.setD(Double.parseDouble(row.get(5).toString()));
            }
            if (row.get(6).toString() != ""){
                sepsisPatients.setE(Double.parseDouble(row.get(6).toString()));
            }
            if (row.get(7).toString() != ""){
                sepsisPatients.setF(Double.parseDouble(row.get(7).toString()));
            }
            if (row.get(8).toString() != ""){
                sepsisPatients.setG(Double.parseDouble(row.get(8).toString()));
            }
            if (row.get(9).toString() != ""){
                sepsisPatients.setH(Double.parseDouble(row.get(9).toString()));
            }
            if (row.get(10).toString() != ""){
                sepsisPatients.setI(Double.parseDouble(row.get(10).toString()));
            }
            if (row.get(11).toString() != ""){
                sepsisPatients.setJ(Double.parseDouble(row.get(11).toString()));
            }
            if (row.get(12).toString() != ""){
                sepsisPatients.setK(Double.parseDouble(row.get(12).toString()));
            }
            if (row.get(13).toString() != ""){
                sepsisPatients.setL(Double.parseDouble(row.get(13).toString()));
            }
            if (row.get(14).toString() != ""){
                sepsisPatients.setM(Double.parseDouble(row.get(14).toString()));
            }
            if (row.get(15).toString() != ""){
                sepsisPatients.setN(Double.parseDouble(row.get(15).toString()));
            }
            if (row.get(16).toString() != ""){
                sepsisPatients.setO(Double.parseDouble(row.get(16).toString()));
            }
            if (row.get(17).toString() != ""){
                sepsisPatients.setP(Double.parseDouble(row.get(17).toString()));
            }
            if (row.get(18).toString() != ""){
                sepsisPatients.setQ(Double.parseDouble(row.get(18).toString()));
            }
            if (row.get(19).toString() != ""){
                sepsisPatients.setR(Double.parseDouble(row.get(19).toString()));
            }
            if (row.get(20).toString() != ""){
                sepsisPatients.setS(Double.parseDouble(row.get(20).toString()));
            }
            if (row.get(21).toString() != ""){
                sepsisPatients.setT(Double.parseDouble(row.get(21).toString()));
            }
            if (row.get(22).toString() != ""){
                sepsisPatients.setU(Double.parseDouble(row.get(22).toString()));
            }
            if (row.get(23).toString() != ""){
                sepsisPatients.setV(Double.parseDouble(row.get(23).toString()));
            }
            sps.add(sepsisPatients);
        }
        sepsispatientsService.saveBatch(sps);
        return 1;
    }
    @GetMapping("/sepsispatients/export")
    public void export(HttpServletResponse response) throws Exception {
        // 从数据库查询出所有的数据
        List<SepsisPatients> list = sepsispatientsService.list();

        List<SepsisPatientsTwoBo> list1 = list.stream().map(info -> {
            SepsisPatientsTwoBo sepsis = new SepsisPatientsTwoBo();
            BeanUtils.copyProperties(info, sepsis);
            return sepsis;
        }).collect(Collectors.toList());
        // 在内存操作，写出到浏览器
        ExcelWriter writer = ExcelUtil.getWriter(true);
        writer.addHeaderAlias("sex","sex");
        writer.addHeaderAlias("age","age");
        writer.addHeaderAlias("a", "FR");
        writer.addHeaderAlias("b", "RF");
        writer.addHeaderAlias("c", "highT");
        writer.addHeaderAlias("d", "maxPCT");
        writer.addHeaderAlias("e", "1stPCT");
        writer.addHeaderAlias("f", "lastPCT");
        writer.addHeaderAlias("g", "1stWBC");
        writer.addHeaderAlias("h", "maxWBC");
        writer.addHeaderAlias("i", "lastWBC");
        writer.addHeaderAlias("j", "CRP");
        writer.addHeaderAlias("k", "1stQTc");
        writer.addHeaderAlias("l", "lastQTc");
        writer.addHeaderAlias("m", "maxQTc");
        writer.addHeaderAlias("n", "1stDD");
        writer.addHeaderAlias("o", "lastDD");
        writer.addHeaderAlias("p", "maxDD");
        writer.addHeaderAlias("q", "result");
        writer.addHeaderAlias("r", "Chol");
        writer.addHeaderAlias("s", "TG");
        writer.addHeaderAlias("t", "HDL_C");
        writer.addHeaderAlias("u", "LDL_C");
        writer.addHeaderAlias("v", "LP");
        // 一次性写出list内的对象到excel，使用默认样式，强制输出标题
        writer.write(list1, true);

        // 设置浏览器响应的格式
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8");
        String fileName = URLEncoder.encode("SepsisPatients信息表", "UTF-8");
        response.setHeader("Content-Disposition", "attachment;filename=" + fileName + ".xlsx");

        ServletOutputStream out = response.getOutputStream();
        writer.flush(out, true);
        out.close();
        writer.close();

    }
    @PostMapping  ("/sepsispatients/export")
    public Map<String,String> export1() throws Exception {
        // 从数据库查询出所有的数据
        List<SepsisPatients> list = sepsispatientsService.list();

        List<SepsisPatientsTwoBo> list1 = list.stream().map(info -> {
            SepsisPatientsTwoBo sepsis = new SepsisPatientsTwoBo();
            BeanUtils.copyProperties(info, sepsis);
            return sepsis;
        }).collect(Collectors.toList());
        // 在内存操作，写出到浏览器
        ExcelWriter writer = ExcelUtil.getWriter(true);
        writer.addHeaderAlias("sex","sex");
        writer.addHeaderAlias("age","age");
        writer.addHeaderAlias("a", "FR");
        writer.addHeaderAlias("b", "RF");
        writer.addHeaderAlias("c", "highT");
        writer.addHeaderAlias("d", "maxPCT");
        writer.addHeaderAlias("e", "1stPCT");
        writer.addHeaderAlias("f", "lastPCT");
        writer.addHeaderAlias("g", "1stWBC");
        writer.addHeaderAlias("h", "maxWBC");
        writer.addHeaderAlias("i", "lastWBC");
        writer.addHeaderAlias("j", "CRP");
        writer.addHeaderAlias("k", "1stQTc");
        writer.addHeaderAlias("l", "lastQTc");
        writer.addHeaderAlias("m", "maxQTc");
        writer.addHeaderAlias("n", "1stDD");
        writer.addHeaderAlias("o", "lastDD");
        writer.addHeaderAlias("p", "maxDD");
        writer.addHeaderAlias("q", "result");
        writer.addHeaderAlias("r", "Chol");
        writer.addHeaderAlias("s", "TG");
        writer.addHeaderAlias("t", "HDL_C");
        writer.addHeaderAlias("u", "LDL_C");
        writer.addHeaderAlias("v", "LP");
        // 一次性写出list内的对象到excel，使用默认样式，强制输出标题
        writer.write(list1, true);
        String filePath = "D:\\project\\PycharmProjects\\wzc\\pure-design-vip\\doc\\template.xlsx";
        OutputStream out = new FileOutputStream(filePath);

        writer.flush(out, true);
        out.close();
        writer.close();
        Map<String, String> map = new HashMap<>();
        map.put("path",filePath);
        return map;
    }
    @PostMapping("/sepsispatients/list")
    @PugDoc(name="查询脓毒症信息管理列表信息")
    public List<SepsisPatientsBo> findSepsisPatientsList() {
        return sepsispatientsService.findSepsisPatientsList();
    }

	/**
	 * 查询脓毒症信息管理列表信息并分页
	 * @path : /admin/sepsispatients/load
     * @method: findSepsisPatientss
     * @param : sepsispatientsVo
     * @result : IPage<SepsisPatientsBo>
	 * 创建人:yykk
	 * 创建时间：2022-11-14 19:09:44
	 * @version 1.0.0
	*/
    @PostMapping("/sepsispatients/load")
    @PugDoc(name="查询脓毒症信息管理列表信息并分页")
    public IPage<SepsisPatientsBo> findSepsisPatientss(@RequestBody SepsisPatientsVo sepsispatientsVo) {
        return sepsispatientsService.findSepsisPatientsPage(sepsispatientsVo);
    }
    @PostMapping("/sepsispatients/load2")
    @PugDoc(name="查询脓毒症信息管理列表信息并分页")
    public IPage<SepsisPatientsBo> findSepsisPatientss2(@RequestBody SepsisPatientsVo sepsispatientsVo) {
        return sepsispatientsService.findSepsisPatientsPage2(sepsispatientsVo);
    }

    /**
     * 根据脓毒症信息管理id查询明细信息
     * @method: get/{id}
     * @path : /admin/sepsispatients/get/{id}
     * @param : id
     * @result : SepsisPatientsBo
     * 创建人:yykk
     * 创建时间：2022-11-14 19:09:44
     * @version 1.0.0
    */
    @PostMapping("/sepsispatients/get/{id}")
    @PugDoc(name="根据脓毒症信息管理id查询明细信息")
    public SepsisPatientsBo getSepsisPatientsById(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
           throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return sepsispatientsService.getSepsisPatientsById(new Integer(id));
    }


	/**
	 * 保存和修改脓毒症信息管理
     * @method: saveupdate
	 * @path : /admin/sepsispatients/save
     * @param : sepsispatients
     * @result : SepsisPatientsBo
	 * 创建人:yykk
	 * 创建时间：2022-11-14 19:09:44
	 * @version 1.0.0
	*/
    @PostMapping("/sepsispatients/saveupdate")
    @PugDoc(name="保存和修改脓毒症信息管理")
    public SepsisPatientsBo saveupdateSepsisPatients(@RequestBody SepsisPatients sepsispatients) {
        return sepsispatientsService.saveupdateSepsisPatients(sepsispatients);
    }


    /**
	 * 根据脓毒症信息管理id删除脓毒症信息管理
     * @method: delete/{id}
     * @path : /admin/sepsispatients/delete/{id}
     * @param : id
     * @result : int
	 * 创建人:yykk
	 * 创建时间：2022-11-14 19:09:44
	 * @version 1.0.0
	*/
    @PostMapping("/sepsispatients/delete/{id}")
    @PugDoc(name="根据脓毒症信息管理id删除脓毒症信息管理")
    public int deleteSepsisPatientsById(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        SepsisPatients sepsisPatients = new SepsisPatients();
        sepsisPatients.setId(new Integer(id));
        sepsisPatients.setIsdelete(1);

        return 0;
    }
    @PostMapping("/sepsispatients/delete2/{id}")
    @PugDoc(name="根据脓毒症信息管理id删除脓毒症信息管理")
    public int deleteSepsisPatientsById2(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        boolean flag = sepsispatientsService.removeById(new Integer(id));
        if (flag){
            return 1;
        }
        return 0;
    }


   /**
   	 * 根据脓毒症信息管理ids批量删除脓毒症信息管理
     * @method: sepsispatients/delBatch
     * @path : /admin/sepsispatients/delBatch
     * @param : sepsispatientsVo
     * @result : boolean
   	 * 创建人:yykk
   	 * 创建时间：2022-11-14 19:09:44
   	 * @version 1.0.0
   	*/
    @PostMapping("/sepsispatients/delBatch")
    @PugDoc(name="根据脓毒症信息管理ids批量删除脓毒症信息管理")
    public boolean delSepsisPatients(@RequestBody SepsisPatientsVo sepsispatientsVo) {
        log.info("你要批量删除的IDS是:{}", sepsispatientsVo.getBatchIds());
        if (Vsserts.isEmpty(sepsispatientsVo.getBatchIds())) {
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return sepsispatientsService.delBatchSepsisPatients(sepsispatientsVo.getBatchIds());
    }
    @PostMapping("/sepsispatients/delBatch2")
    @PugDoc(name="根据脓毒症信息管理ids批量删除脓毒症信息管理")
    public boolean delSepsisPatients2(@RequestBody SepsisPatientsVo sepsispatientsVo) {
        log.info("你要批量删除的IDS是:{}", sepsispatientsVo.getBatchIds());
        if (Vsserts.isEmpty(sepsispatientsVo.getBatchIds())) {
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return sepsispatientsService.delBatchSepsisPatients2(sepsispatientsVo.getBatchIds());
    }
}