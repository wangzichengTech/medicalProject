package com.pug.zixun.controller.coursecomment;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pug.zixun.bo.CourseCommentBo;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;
import com.pug.zixun.controller.BaseController;
import com.pug.zixun.pojo.CourseComment;
import com.pug.zixun.service.coursecomment.ICourseCommentService;
import com.pug.zixun.vo.CourseCommentVo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.pug.generator.anno.PugDoc;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
/**
 * CourseCommentController
 * 创建人:yykk<br/>
 * 时间：2022-07-25 20:22:03 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/

@RestController
@RequiredArgsConstructor
@Slf4j
@PugDoc(name="课程评论",tabname="kss_course_comment")
public class CourseCommentController extends BaseController{

    private final ICourseCommentService coursecommentService;


    /**
     * 查询课程评论列表信息
     * @path : /admin/coursecomment/list
     * @method: findCourseComments
     * @result : List<CourseCommentBo>
     * 创建人:yykk
     * 创建时间：2022-07-25 20:22:03
     * @version 1.0.0
     * @return
     */
    @PostMapping("/coursecomment/list")
    @PugDoc(name="查询课程评论列表信息")
    public List<CourseCommentBo> findCourseCommentList() {
        return coursecommentService.findCourseCommentList();
    }

	/**
	 * 查询课程评论列表信息并分页
	 * @path : /admin/coursecomment/load
     * @method: findCourseComments
     * @param : coursecommentVo
     * @result : IPage<CourseCommentBo>
	 * 创建人:yykk
	 * 创建时间：2022-07-25 20:22:03
	 * @version 1.0.0
	*/
    @PostMapping("/coursecomment/load")
    @PugDoc(name="查询课程评论列表信息并分页")
    public IPage<CourseCommentBo> findCourseComments(@RequestBody CourseCommentVo coursecommentVo) {
        return coursecommentService.findCourseCommentPage(coursecommentVo);
    }


    /**
     * 根据课程评论id查询明细信息
     * @method: get/{id}
     * @path : /admin/coursecomment/get/{id}
     * @param : id
     * @result : CourseCommentBo
     * 创建人:yykk
     * 创建时间：2022-07-25 20:22:03
     * @version 1.0.0
    */
    @PostMapping("/coursecomment/get/{id}")
    @PugDoc(name="根据课程评论id查询明细信息")
    public CourseCommentBo getCourseCommentById(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
           throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return coursecommentService.getCourseCommentById(new Long(id));
    }


	/**
	 * 保存和修改课程评论
     * @method: saveupdate
	 * @path : /admin/coursecomment/save
     * @param : coursecomment
     * @result : CourseCommentBo
	 * 创建人:yykk
	 * 创建时间：2022-07-25 20:22:03
	 * @version 1.0.0
	*/
    @PostMapping("/coursecomment/saveupdate")
    @PugDoc(name="保存和修改课程评论")
    public CourseCommentBo saveupdateCourseComment(@RequestBody CourseComment coursecomment) {
        return coursecommentService.saveupdateCourseComment(coursecomment);
    }


    /**
	 * 根据课程评论id删除课程评论
     * @method: delete/{id}
     * @path : /admin/coursecomment/delete/{id}
     * @param : id
     * @result : int
	 * 创建人:yykk
	 * 创建时间：2022-07-25 20:22:03
	 * @version 1.0.0
	*/
    @PostMapping("/coursecomment/delete/{id}")
    @PugDoc(name="根据课程评论id删除课程评论")
    public int deleteCourseCommentById(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return coursecommentService.deleteCourseCommentById(new Long(id));
    }


   /**
   	 * 根据课程评论ids批量删除课程评论
     * @method: coursecomment/delBatch
     * @path : /admin/coursecomment/delBatch
     * @param : coursecommentVo
     * @result : boolean
   	 * 创建人:yykk
   	 * 创建时间：2022-07-25 20:22:03
   	 * @version 1.0.0
   	*/
    @PostMapping("/coursecomment/delBatch")
    @PugDoc(name="根据课程评论ids批量删除课程评论")
    public boolean delCourseComment(@RequestBody CourseCommentVo coursecommentVo) {
        log.info("你要批量删除的IDS是:{}", coursecommentVo.getBatchIds());
        if (Vsserts.isEmpty(coursecommentVo.getBatchIds())) {
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return coursecommentService.delBatchCourseComment(coursecommentVo.getBatchIds());
    }
}