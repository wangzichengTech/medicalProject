package com.pug.zixun.controller.adminpermission;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pug.zixun.bo.AdminPermissionBo;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;
import com.pug.zixun.controller.BaseController;
import com.pug.zixun.pojo.AdminPermission;
import com.pug.zixun.service.adminpermission.IAdminPermissionService;
import com.pug.zixun.vo.AdminPermissionVo;
import com.pug.zixun.vo.StatusUpdateVo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.pug.generator.anno.PugDoc;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
/**
 * AdminPermissionController
 * 创建人:yykk<br/>
 * 时间：2022-07-17 18:38:05 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/

@RestController
@RequiredArgsConstructor
@Slf4j
@PugDoc(name="后台权限管理",tabname="kss_permission")
public class AdminPermissionController extends BaseController{

    private final IAdminPermissionService adminpermissionService;


    /**
     * 查询分类的接口信息-tree
     * 这里是递归查询所有的权限，让角色去授予权限使用
     *
     * @return
     */
    @PostMapping("/adminpermission/tree")
    @PugDoc(name="查询菜单和权限信息tree")
    public List<AdminPermission> permissionTree() {
        return adminpermissionService.findAdminPermissionTree();
    }


    /**
     * 查询分类的接口信息-tree
     * 查询后台左边侧边栏的导航 type=1
     *
     * @return
     */
    @PostMapping("/adminpermission/menu/tree")
    @PugDoc(name="查询菜单和权限信息menu/tree")
    public List<AdminPermission> permissionMenuTree() {
        return adminpermissionService.findAdminPermissionMenuTree();
    }





    /**
     * 查询后台权限管理列表信息
     * @path : /admin/adminpermission/list
     * @method: findAdminPermissions
     * @result : List<AdminPermissionBo>
     * 创建人:yykk
     * 创建时间：2022-07-17 18:38:05
     * @version 1.0.0
     * @return
     */
    @PostMapping("/adminpermission/list")
    @PugDoc(name="查询后台权限管理列表信息")
    public List<AdminPermissionBo> findAdminPermissionList() {
        return adminpermissionService.findAdminPermissionList();
    }

	/**
	 * 查询后台权限管理列表信息并分页
	 * @path : /admin/adminpermission/load
     * @method: findAdminPermissions
     * @param : adminpermissionVo
     * @result : IPage<AdminPermissionBo>
	 * 创建人:yykk
	 * 创建时间：2022-07-17 18:38:05
	 * @version 1.0.0
	*/
    @PostMapping("/adminpermission/load")
    @PugDoc(name="查询后台权限管理列表信息并分页")
    public IPage<AdminPermissionBo> findAdminPermissions(@RequestBody AdminPermissionVo adminpermissionVo) {
        return adminpermissionService.findAdminPermissionPage(adminpermissionVo);
    }


    /**
     * 根据后台权限管理id查询明细信息
     * @method: get/{id}
     * @path : /admin/adminpermission/get/{id}
     * @param : id
     * @result : AdminPermissionBo
     * 创建人:yykk
     * 创建时间：2022-07-17 18:38:05
     * @version 1.0.0
    */
    @PostMapping("/adminpermission/get/{id}")
    @PugDoc(name="根据后台权限管理id查询明细信息")
    public AdminPermissionBo getAdminPermissionById(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
           throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return adminpermissionService.getAdminPermissionById(new Long(id));
    }


	/**
	 * 保存和修改后台权限管理
     * @method: saveupdate
	 * @path : /admin/adminpermission/save
     * @param : adminpermission
     * @result : AdminPermissionBo
	 * 创建人:yykk
	 * 创建时间：2022-07-17 18:38:05
	 * @version 1.0.0
	*/
    @PostMapping("/adminpermission/saveupdate")
    @PugDoc(name="保存和修改后台权限管理")
    public AdminPermissionBo saveupdateAdminPermission(@RequestBody AdminPermission adminpermission) {
        return adminpermissionService.saveupdateAdminPermission(adminpermission);
    }


    /**
	 * 根据后台权限管理id删除后台权限管理
     * @method: delete/{id}
     * @path : /admin/adminpermission/delete/{id}
     * @param : id
     * @result : int
	 * 创建人:yykk
	 * 创建时间：2022-07-17 18:38:05
	 * @version 1.0.0
	*/
    @PostMapping("/adminpermission/delete/{id}")
    @PugDoc(name="根据后台权限管理id删除后台权限管理")
    public int deleteAdminPermissionById(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return adminpermissionService.deleteAdminPermissionById(new Long(id));
    }


   /**
   	 * 根据后台权限管理ids批量删除后台权限管理
     * @method: adminpermission/delBatch
     * @path : /admin/adminpermission/delBatch
     * @param : adminpermissionVo
     * @result : boolean
   	 * 创建人:yykk
   	 * 创建时间：2022-07-17 18:38:05
   	 * @version 1.0.0
   	*/
    @PostMapping("/adminpermission/delBatch")
    @PugDoc(name="根据后台权限管理ids批量删除后台权限管理")
    public boolean delAdminPermission(@RequestBody AdminPermissionVo adminpermissionVo) {
        log.info("你要批量删除的IDS是:{}", adminpermissionVo.getBatchIds());
        if (Vsserts.isEmpty(adminpermissionVo.getBatchIds())) {
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return adminpermissionService.delBatchAdminPermission(adminpermissionVo.getBatchIds());
    }

    /**
     * 状态的修改
     *
     * @param : adminuser
     * @method: saveupdate
     * @path : /admin/adminuser/save
     * @result : AdminUserBo
     * 创建人:yykk
     * 创建时间：2022-07-15 20:48:26
     * @version 1.0.0
     */
    @PostMapping("/adminpermission/update")
    @PugDoc(name = "用于状态更新")
    public boolean updateAdminPermission(@RequestBody StatusUpdateVo statusUpdateVo) {
        AdminPermission adminPermission = new AdminPermission();
        BeanUtils.copyProperties(statusUpdateVo,adminPermission);
        return adminpermissionService.updateById(adminPermission);
    }

}