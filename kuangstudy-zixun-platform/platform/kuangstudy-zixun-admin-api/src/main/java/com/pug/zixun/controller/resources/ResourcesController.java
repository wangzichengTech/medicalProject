package com.pug.zixun.controller.resources;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pug.zixun.bo.ResourcesBo;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;
import com.pug.zixun.controller.BaseController;
import com.pug.zixun.pojo.Resources;
import com.pug.zixun.service.resources.IResourcesService;
import com.pug.zixun.vo.ResourcesVo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.pug.generator.anno.PugDoc;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
/**
 * ResourcesController
 * 创建人:yykk<br/>
 * 时间：2022-07-19 23:58:26 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/

@RestController
@RequiredArgsConstructor
@Slf4j
@PugDoc(name="资源库",tabname="kss_resources")
public class ResourcesController extends BaseController{

    private final IResourcesService resourcesService;


    /**
     * 查询资源库列表信息
     * @path : /admin/resources/list
     * @method: findResourcess
     * @result : List<ResourcesBo>
     * 创建人:yykk
     * 创建时间：2022-07-19 23:58:26
     * @version 1.0.0
     * @return
     */
    @PostMapping("/resources/list")
    @PugDoc(name="查询资源库列表信息")
    public List<ResourcesBo> findResourcesList() {
        return resourcesService.findResourcesList();
    }

	/**
	 * 查询资源库列表信息并分页
	 * @path : /admin/resources/load
     * @method: findResourcess
     * @param : resourcesVo
     * @result : IPage<ResourcesBo>
	 * 创建人:yykk
	 * 创建时间：2022-07-19 23:58:26
	 * @version 1.0.0
	*/
    @PostMapping("/resources/load")
    @PugDoc(name="查询资源库列表信息并分页")
    public IPage<ResourcesBo> findResourcess(@RequestBody ResourcesVo resourcesVo) {
        return resourcesService.findResourcesPage(resourcesVo);
    }


    /**
     * 根据资源库id查询明细信息
     * @method: get/{id}
     * @path : /admin/resources/get/{id}
     * @param : id
     * @result : ResourcesBo
     * 创建人:yykk
     * 创建时间：2022-07-19 23:58:26
     * @version 1.0.0
    */
    @PostMapping("/resources/get/{id}")
    @PugDoc(name="根据资源库id查询明细信息")
    public ResourcesBo getResourcesById(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
           throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return resourcesService.getResourcesById(new Long(id));
    }


	/**
	 * 保存和修改资源库
     * @method: saveupdate
	 * @path : /admin/resources/save
     * @param : resources
     * @result : ResourcesBo
	 * 创建人:yykk
	 * 创建时间：2022-07-19 23:58:26
	 * @version 1.0.0
	*/
    @PostMapping("/resources/saveupdate")
    @PugDoc(name="保存和修改资源库")
    public ResourcesBo saveupdateResources(@RequestBody Resources resources) {
        return resourcesService.saveupdateResources(resources);
    }


    /**
	 * 根据资源库id删除资源库
     * @method: delete/{id}
     * @path : /admin/resources/delete/{id}
     * @param : id
     * @result : int
	 * 创建人:yykk
	 * 创建时间：2022-07-19 23:58:26
	 * @version 1.0.0
	*/
    @PostMapping("/resources/delete/{id}")
    @PugDoc(name="根据资源库id删除资源库")
    public int deleteResourcesById(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return resourcesService.deleteResourcesById(new Long(id));
    }


   /**
   	 * 根据资源库ids批量删除资源库
     * @method: resources/delBatch
     * @path : /admin/resources/delBatch
     * @param : resourcesVo
     * @result : boolean
   	 * 创建人:yykk
   	 * 创建时间：2022-07-19 23:58:26
   	 * @version 1.0.0
   	*/
    @PostMapping("/resources/delBatch")
    @PugDoc(name="根据资源库ids批量删除资源库")
    public boolean delResources(@RequestBody ResourcesVo resourcesVo) {
        log.info("你要批量删除的IDS是:{}", resourcesVo.getBatchIds());
        if (Vsserts.isEmpty(resourcesVo.getBatchIds())) {
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return resourcesService.delBatchResources(resourcesVo.getBatchIds());
    }


    /**
     * 查询资源库列表信息
     * @path : /admin/resources/list
     * @method: findResourcess
     * @result : List<ResourcesBo>
     * 创建人:yykk
     * 创建时间：2022-07-19 23:58:26
     * @version 1.0.0
     * @return
     */
    @PostMapping("/resources/count/{cid}")
    @PugDoc(name="统计分类下是否存在资源信息")
    public int countResourceList(@PathVariable("cid") Long cid) {
        LambdaQueryWrapper<Resources> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(Resources::getResourceId,cid);
        return resourcesService.count(lambdaQueryWrapper);
    }

}