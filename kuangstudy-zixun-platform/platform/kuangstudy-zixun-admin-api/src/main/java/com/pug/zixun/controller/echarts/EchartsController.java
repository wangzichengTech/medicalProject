package com.pug.zixun.controller.echarts;

import cn.hutool.core.collection.CollUtil;
import com.pug.zixun.controller.BaseController;
import com.pug.zixun.pojo.Ecgpatients;
import com.pug.zixun.pojo.SepsisPatients;
import com.pug.zixun.service.ecgpatients.IEcgpatientsService;
import com.pug.zixun.service.sepsispatients.ISepsisPatientsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
public class EchartsController extends BaseController {
    @Resource
    private ISepsisPatientsService sepsisPatientsService;
    @Resource
    private IEcgpatientsService ecgpatientsService;
    @PostMapping("/echarts/numbersOne")
    public List<ArrayList<Integer>> numbersOne(){
        List<SepsisPatients> list = sepsisPatientsService.list();
        int a=0,b=0,c=0,d=0,e=0,f=0,g=0,h=0,i=0,j=0;
        int A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0;
        for (SepsisPatients sepsisPatients : list) {
            if (sepsisPatients.getAge()>0&&sepsisPatients.getAge()<=10){
                a++;
                if (sepsisPatients.getData()>0.80){
                    A++;
                }
            }
            if (sepsisPatients.getAge()>10&&sepsisPatients.getAge()<=20){
                b++;
                if (sepsisPatients.getData()>0.80){
                    B++;
                }
            }
            if (sepsisPatients.getAge()>20&&sepsisPatients.getAge()<=30){
                c++;
                if (sepsisPatients.getData()>0.80){
                    C++;
                }
            }
            if (sepsisPatients.getAge()>30&&sepsisPatients.getAge()<=40){
                d++;
                if (sepsisPatients.getData()>0.80){
                    D++;
                }
            }
            if (sepsisPatients.getAge()>40&&sepsisPatients.getAge()<=50){
                e++;
                if (sepsisPatients.getData()>0.80){
                    E++;
                }
            }
            if (sepsisPatients.getAge()>50&&sepsisPatients.getAge()<=60){
                f++;
                if (sepsisPatients.getData()>0.80){
                    F++;
                }
            }
            if (sepsisPatients.getAge()>60&&sepsisPatients.getAge()<=70){
                g++;
                if (sepsisPatients.getData()>0.80){
                    G++;
                }
            }
            if (sepsisPatients.getAge()>70&&sepsisPatients.getAge()<=80){
                h++;
                if (sepsisPatients.getData()>0.80){
                    H++;
                }
            }
            if (sepsisPatients.getAge()>80&&sepsisPatients.getAge()<=90){
                i++;
                if (sepsisPatients.getData()>0.80){
                    I++;
                }
            }
            if (sepsisPatients.getAge()>90&&sepsisPatients.getAge()<=100){
                j++;
                if (sepsisPatients.getData()>0.80){
                    J++;
                }
            }
        }
        return CollUtil.newArrayList(CollUtil.newArrayList(a,b,c,d,e,f,g,h,i,j),CollUtil.newArrayList(A,B,C,D,E,F,G,H,I,J));
    }
    @PostMapping("/echarts/numbersTwo")
    public List<Integer> numbersTwo(){
        List<SepsisPatients> list = sepsisPatientsService.list();
        int a=0,A=0,b=0,B=0;
        for (SepsisPatients sepsisPatients : list) {
            if (sepsisPatients.getSex()==1){
                a++;
                if (sepsisPatients.getData() >0.80){
                    A++;
                }
            }
            if (sepsisPatients.getSex()==0){
                b++;
                if (sepsisPatients.getData() >0.80){
                    B++;
                }
            }
        }
        return CollUtil.newArrayList(a,A,b,B);
    }
    @PostMapping("/echarts/sepsisPatientsMember")
    public List<Integer> count1(){
        int i = 0;
        int j = 0;
        int m = 0;
        int n = 0;
        int a = 0;
        int b = 0;
        List<SepsisPatients> list = sepsisPatientsService.list();
        for (SepsisPatients sepsisPatients : list) {
            if (sepsisPatients.getSex() == 1){
                m++;
            }
            if (sepsisPatients.getSex() == 0){
                n++;
            }
            if (sepsisPatients.getData() >0.80){
                j++;
            }
            if (sepsisPatients.getData()>0.80&&sepsisPatients.getSex()==1){
                a++;
            }
            if (sepsisPatients.getData()>0.80&&sepsisPatients.getSex()==0){
                b++;
            }
            i++;
        }

        return CollUtil.newArrayList(i,j,m,n,a,b);
    }
    @PostMapping("/echarts/ecgPatientsMember")
    public List<Integer> count2(){
        int i = 0;
        int j = 0;
        int l = 0;
        List<Ecgpatients> list = ecgpatientsService.list();
        for (Ecgpatients ecgPatients : list) {
            if (ecgPatients.getPy() > 0.50){
                j++;
            }
            if (ecgPatients.getC() == 1){
                l++;
            }
            i++;
        }
        return CollUtil.newArrayList(i,j,l);
    }
    @PostMapping("/echarts/ecgPatientsNumbers")
    public List<ArrayList<Integer>> count3(){
        int a=0,b=0,c=0,d=0,e=0,f=0,g=0,h=0,i=0,j=0;
        int A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0;
        List<Ecgpatients> list = ecgpatientsService.list();
        for (Ecgpatients ecgPatients : list) {
            if (ecgPatients.getU()>0&&ecgPatients.getU()<=10){
                a++;
                if (ecgPatients.getPy() > 0.50){
                    A++;
                }
            }
            if (ecgPatients.getU()>10&&ecgPatients.getU()<=20){
                b++;
                if (ecgPatients.getPy() > 0.50){
                    B++;
                }
            }
            if (ecgPatients.getU()>20&&ecgPatients.getU()<=30){
                c++;
                if (ecgPatients.getPy() > 0.50){
                    C++;
                }
            }
            if (ecgPatients.getU()>30&&ecgPatients.getU()<=40){
                d++;
                if (ecgPatients.getPy() > 0.50){
                    D++;
                }
            }
            if (ecgPatients.getU()>40&&ecgPatients.getU()<=50){
                e++;
                if (ecgPatients.getPy() > 0.50){
                    E++;
                }
            }
            if (ecgPatients.getU()>50&&ecgPatients.getU()<=60){
                f++;
                if (ecgPatients.getPy() > 0.50){
                    F++;
                }
            }
            if (ecgPatients.getU()>60&&ecgPatients.getU()<=70){
                g++;
                if (ecgPatients.getPy() > 0.50){
                    G++;
                }
            }
            if (ecgPatients.getU()>70&&ecgPatients.getU()<=80){
                h++;
                if (ecgPatients.getPy() > 0.50){
                    H++;
                }
            }
            if (ecgPatients.getU()>80&&ecgPatients.getU()<=90){
                i++;
                if (ecgPatients.getPy() > 0.50){
                    I++;
                }
            }
            if (ecgPatients.getU()>90&&ecgPatients.getU()<=100){
                j++;
                if (ecgPatients.getPy() > 0.50){
                    J++;
                }
            }
        }
        return CollUtil.newArrayList(CollUtil.newArrayList(a,b,c,d,e,f,g,h,i,j),CollUtil.newArrayList(A,B,C,D,E,F,G,H,I,J));
    }
}
