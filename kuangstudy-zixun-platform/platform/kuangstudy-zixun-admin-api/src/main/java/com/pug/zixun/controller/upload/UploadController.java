package com.pug.zixun.controller.upload;

import cn.hutool.core.io.FileUtil;
import com.pug.zixun.commons.enums.AdminErrorResultEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.controller.BaseController;
import com.pug.zixun.pojo.Resources;
import com.pug.zixun.service.resources.IResourcesService;
import com.pug.zixun.service.upload.UploadService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

/**
 * @Description
 * @Author sgl
 * @Date 2018-05-15 14:04
 */
@Controller
public class UploadController extends BaseController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UploadController.class);



    @Autowired
    private UploadService uploadService;
    @Autowired
    private IResourcesService resourcesService;


    //上传地址：http://localhost:8877/admin/upload/file
    @PostMapping("/upload/file")
    @ResponseBody
    public String upload(@RequestParam("file") MultipartFile file, HttpServletRequest request) throws IOException, ServletException {
        if (file.isEmpty()) {
            return "上传失败，请选择文件";
        }

        if(file.getSize() > 1024 * 1024 * 2){
            throw new PugValidatorException(AdminErrorResultEnum.FILE_UPLOAD_LIMIT_ERROR);
        }

        if(!file.getContentType().equalsIgnoreCase("image/png") &&
                !file.getContentType().equalsIgnoreCase("image/jpg") &&
                !file.getContentType().equalsIgnoreCase("image/jpeg")){
            throw new PugValidatorException(AdminErrorResultEnum.FILE_UPLOAD_TYPE_ERROR);
        }



        // 文件上传如何传递参数
        String dir = request.getParameter("dir");
        if(StringUtils.isEmpty(dir)){
            dir = "course";
        }
        // handler调用文件上传的service 得到文件的虚拟路径
        String filepath = uploadService.uploadImg(file,dir);
        return filepath;
    }
    @PostMapping("/upload2/filemap")
    @ResponseBody
    public Map<String,Object> upload2(@RequestBody Map<String,String> maps) throws IOException, ServletException {
        String path = maps.get("SAVE_PATH");
        File file1 = new File(path);
        InputStream inputStream = new FileInputStream(file1);
        MultipartFile file = new MockMultipartFile("predict.png", inputStream);
        // handler调用文件上传的service 得到文件的虚拟路径
        String dir = "predict";
        Map<String,Object> map = uploadService.uploadImgMap2(file,dir);
        // 资源入库 设计模式--- 观察设计模式
        String cid = "5";
        Resources resources = new Resources();
        resources.setFilename(String.valueOf(map.get("filename")));
        resources.setNewname(String.valueOf(map.get("newname")));
        resources.setStatus(1);
        resources.setIsdelete(0);
        resources.setResourceId(new Long(cid));
        resources.setType(1);
        resources.setFilesize(String.valueOf(map.get("size")));
        resources.setExt(String.valueOf(map.get("ext")));
        resources.setPath(String.valueOf(map.get("url")));
        resources.setDownpath(String.valueOf(map.get("url")));
        resources.setIspublic(1);
        resourcesService.saveOrUpdate(resources);
        // 返回资源ID
        map.put("resources",resources);
        return map;
    }

    /**
     * 本地文件上传返回json
     * @param file
     * @param request
     * @return
     * @throws IOException
     * @throws ServletException
     */
    @PostMapping("/upload/filemap")
    @ResponseBody
    public Map<String,Object> uploadJson(@RequestParam("file") MultipartFile file, HttpServletRequest request) throws IOException, ServletException {
        if (file.isEmpty()) {
            return null;
        }

        if(file.getSize() > 1024 * 1024 * 2){
            throw new PugValidatorException(AdminErrorResultEnum.FILE_UPLOAD_LIMIT_ERROR);
        }

        if(!file.getContentType().equalsIgnoreCase("image/png") &&
                !file.getContentType().equalsIgnoreCase("image/jpg") &&
        !file.getContentType().equalsIgnoreCase("image/jpeg")){
            throw new PugValidatorException(AdminErrorResultEnum.FILE_UPLOAD_TYPE_ERROR);
        }

        String dir = request.getParameter("dir");
        if(StringUtils.isEmpty(dir)){
            dir = "course";
        }

        // handler调用文件上传的service 得到文件的虚拟路径
        Map<String,Object> map = uploadService.uploadImgMap(file,dir);
        // 资源入库 设计模式--- 观察设计模式
        String cid = request.getParameter("cid");
        Resources resources = new Resources();
        resources.setFilename(String.valueOf(map.get("filename")));
        resources.setNewname(String.valueOf(map.get("newname")));
        resources.setStatus(1);
        resources.setIsdelete(0);
        resources.setResourceId(new Long(cid));
        resources.setType(1);
        resources.setFilesize(String.valueOf(map.get("size")));
        resources.setExt(String.valueOf(map.get("ext")));
        resources.setPath(String.valueOf(map.get("url")));
        resources.setDownpath(String.valueOf(map.get("url")));
        resources.setIspublic(1);
        resourcesService.saveOrUpdate(resources);
        // 返回资源ID
        map.put("resources",resources);
        return map;
    }
}
