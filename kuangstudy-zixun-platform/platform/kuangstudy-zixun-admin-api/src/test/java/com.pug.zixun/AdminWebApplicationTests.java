package com.pug.zixun;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pug.zixun.mapper.BannerMapper;
import com.pug.zixun.mapper.UserMapper;
import com.pug.zixun.pojo.Banner;
import com.pug.zixun.vo.BannerUserVo;
import com.pug.zixun.vo.BannerVo;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 飞哥B站地址：https://space.bilibili.com/490711252
 * 记得关注和三连哦！
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2022-05-27$ 22:01$
 */
@SpringBootTest
public class AdminWebApplicationTests {

    @Resource
    private BannerMapper bannerMapper;
    @Resource
    private UserMapper userMapper;


    // 多表查询 - 原始的mybatis写法-- 分页
    @Test
    public void findBanners4(){
        // 参数
        BannerVo bannerVo = new BannerVo();
        bannerVo.setKeyword("");
        // 执行
        IPage<BannerUserVo> page = findBannerPage(bannerVo);
        // 结果
        System.out.println(page.getTotal());//3
        System.out.println(page.getCurrent());//1
        System.out.println(page.getSize());//2
        System.out.println(page.getPages());//2
        page.getRecords().forEach(AdminWebApplicationTests::print);
    }

    private IPage<BannerUserVo> findBannerPage(BannerVo bannerVo){
        // 设置分页
        Page<BannerUserVo> page = new Page<>(bannerVo.getPageNo(),bannerVo.getPageSize());
        // 设置条件
        QueryWrapper<BannerVo> queryWrapper = new QueryWrapper<>();
        queryWrapper.first("t1.user_id = t2.id");
        queryWrapper.like(StringUtils.isNotEmpty(bannerVo.getKeyword()),"t1.title",bannerVo.getKeyword());
        return bannerMapper.findBannerUsersVoPage(page, queryWrapper);
    }


    // 多表查询 - 原始的mybatis写法 -- 查询返回 vo
    @Test
    public void findBanners3(){
        BannerVo bannerVo = new BannerVo();
        bannerVo.setKeyword("慕尚");
        List<BannerUserVo> banners = bannerMapper.findBannerUsersVo(bannerVo);
        banners.forEach(AdminWebApplicationTests::print);
    }

    // 多表查询 - 原始的mybatis写法-- 查询返回 Map
    @Test
    public void findBanners2(){
        BannerVo bannerVo = new BannerVo();
        bannerVo.setKeyword("慕尚");
        List<Map<String,Object>> banners = bannerMapper.findBannerUsers(bannerVo);
        banners.forEach(AdminWebApplicationTests::print);
    }

    // 原始的mybatis写法
    @Test
    public void findBanners(){
        BannerVo bannerVo = new BannerVo();
        bannerVo.setKeyword("慕尚");
        List<Banner> banners = bannerMapper.findBanners(bannerVo);
        banners.forEach(AdminWebApplicationTests::print);
    }


    // 删除
    @Test
    public void delBanner() {
        // 根据id删除
        int i = bannerMapper.deleteById(1530199499360088065L);
        System.out.println(i > 0 ? "删除成功" + i : "删除失败");

        // 按照非id的条件删除
        LambdaQueryWrapper<Banner> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(Banner::getTitle, "1");
        int deleteCount = bannerMapper.delete(lambdaQueryWrapper);
        System.out.println(deleteCount > 0 ? "删除成功" + deleteCount : "删除失败");


        // 批量删除
        List<Long> ids = new ArrayList<>();
        ids.add(1524404522439491586L);
        ids.add(1530198897427140610L);
        ids.add(1530199006726529026L);
        int i1 = bannerMapper.deleteBatchIds(ids);
        System.out.println(i1 > 0 ? "删除成功" + i1 : "删除失败");
    }


    // 修改
    @Test
    public void updateBanner() {
        Banner banner = new Banner();
        banner.setId(1530199499360088065L);
        banner.setTitle("我修改了标题了1111111111....");
        banner.setDescription("desc desc....");
        Banner banner1 = updateBanner(banner);
        System.out.println(banner1);
    }

    // 修改
    public Banner updateBanner(Banner banner) {
        int insert = bannerMapper.updateById(banner);
        if (insert > 0) {
            return banner;
        }
        return null;

//        UpdateWrapper<Banner> updateWrapper = new UpdateWrapper<>();
//        updateWrapper.eq("id",banner.getId());
//        return bannerMapper.update(banner,updateWrapper) > 0 ? banner : null;
    }


    // 保存
    @Test
    public void saveBanner() {
        BannerVo bannerVo = new BannerVo();
        bannerVo.setTitle("圣诞福利开始的减肥了水电费");
        Banner banner = saveBanner(bannerVo);
        System.out.println(banner);
    }


    public Banner saveBanner(BannerVo bannerVo) {
        Banner banner = new Banner();
        banner.setTitle(bannerVo.getTitle());
        int insert = bannerMapper.insert(banner);
        if (insert > 0) {
            return banner;
        }
        return null;
    }


    // 查询分页
    @Test
    public void bannerPage() {
        Page<Banner> page = this.bannerPage(1, 2, "卡米");
        System.out.println(page.getTotal());//3
        System.out.println(page.getCurrent());//1
        System.out.println(page.getSize());//2
        System.out.println(page.getPages());//2
        page.getRecords().forEach(AdminWebApplicationTests::print);
    }

    // 基于----LambdaQueryWrapper
    private Page<Banner> bannerPage(Integer pageNo, Integer pageSize, String keyword) {
        // 设置分页信息
        Page<Banner> page = new Page<>(pageNo, pageSize);
        // 设置查询条件
        LambdaQueryWrapper<Banner> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // 注意: eq 条件 代表等于 --column 这个数据列+
        lambdaQueryWrapper.like(StringUtils.isNotEmpty(keyword), Banner::getTitle, keyword);//title like '%keyword%'
        // 查询返回
        Page<Banner> bannerList = bannerMapper.selectPage(page, lambdaQueryWrapper);
        return bannerList;
    }


    @Test
    public void bannerSelectList() {
        // 模糊查询 - 动态SQL QueryWrapper
        //List<Banner> bannerList = findBanners("");
        //bannerList.forEach(AdminWebApplicationTests::print);

        // 模糊查询 - 动态SQL LambdaQueryWrapper
        List<Banner> bannerList2 = findBanners2("卡米");
        bannerList2.forEach(AdminWebApplicationTests::print);
    }

    // 基于----QueryWrapper
    private List<Banner> findBanners(String keyword) {
        // 设置查询条件
        QueryWrapper<Banner> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("id","title","create_time");
        // 注意: eq 条件 代表等于 --column 这个数据列+
        queryWrapper.like(StringUtils.isNotEmpty(keyword), "title", keyword);//title like '%keyword%'
        // 查询返回
        List<Banner> bannerList = bannerMapper.selectList(queryWrapper);
        return bannerList;
    }


    // 基于----LambdaQueryWrapper
    private List<Banner> findBanners2(String keyword) {
        // 设置查询条件
        LambdaQueryWrapper<Banner> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // 按需选择
        //lambdaQueryWrapper.select(Banner::getId,Banner::getTitle,Banner::getCreateTime);
        // 把不需要的排除
        //lambdaQueryWrapper.select(Banner.class,tableFieldInfo-> !tableFieldInfo.getColumn().equals("hreflink"));
        lambdaQueryWrapper.select(Banner.class,  BannerFilter::filter);
        // 注意: eq 条件 代表等于 --column 这个数据列+
        lambdaQueryWrapper.like(StringUtils.isNotEmpty(keyword), Banner::getTitle, keyword);//title like '%keyword%'
        // 查询返回
        List<Banner> bannerList = bannerMapper.selectList(lambdaQueryWrapper);
        return bannerList;
    }


    private static void print(Object t) {
        System.out.println(t);
    }
}
