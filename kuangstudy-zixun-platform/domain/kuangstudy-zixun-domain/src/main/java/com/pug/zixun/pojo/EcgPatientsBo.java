package com.pug.zixun.pojo;

import java.util.Date;
import lombok.*;
import com.baomidou.mybatisplus.annotation.*;

import org.pug.generator.anno.PugDoc;
/**
 * Ecgpatients实体
 * 创建人:yykk<br/>
 * 时间：2022-11-02 19:18:02 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("kss_ecg_patients")
public class EcgPatientsBo  implements java.io.Serializable {

    @PugDoc(name="患者id")
    @TableId(type = IdType.AUTO)
    private Integer id;
    @PugDoc(name="")
    private Double b;
    @PugDoc(name="")
    private Double c;
    @PugDoc(name="")
    private Double d;
    @PugDoc(name="")
    private Double e;
    @PugDoc(name="")
    private Double f;
    @PugDoc(name="")
    private Double g;
    @PugDoc(name="")
    private Double h;
    @PugDoc(name="")
    private Double i;
    @PugDoc(name="")
    private Double j;
    @PugDoc(name="")
    private Double k;
    @PugDoc(name="")
    private Double l;
    @PugDoc(name="")
    private Double m;
    @PugDoc(name="")
    private Double n;
    @PugDoc(name="")
    private Double o;
    @PugDoc(name="")
    private Double p;
    @PugDoc(name="")
    private Double q;
    @PugDoc(name="")
    private Double r;
    @PugDoc(name="")
    private Double s;
    @PugDoc(name="")
    private Double t;
    @PugDoc(name="")
    private Double u;
    @PugDoc(name="")
    private Double v;
    @PugDoc(name="")
    private Double w;
    @PugDoc(name="")
    private Double x;
    @PugDoc(name="")
    private Double y;
    @PugDoc(name="")
    private Double z;
    @PugDoc(name="")
    private Double aa;
    @PugDoc(name="")
    private Double ab;
    @PugDoc(name="")
    private Double ac;
    @PugDoc(name="")
    private Double ad;
    @PugDoc(name="")
    private Double ae;
    @PugDoc(name="")
    private Double af;
    @PugDoc(name="")
    private Double ag;
    @PugDoc(name="")
    private Double ah;
    @PugDoc(name="")
    private Double ai;
    @PugDoc(name="")
    private Double aj;
    @PugDoc(name="")
    private Double ak;
    @PugDoc(name="")
    private Double al;
    @PugDoc(name="")
    private Double am;
    @PugDoc(name="")
    private Double an;
    @PugDoc(name="")
    private Double ao;

}