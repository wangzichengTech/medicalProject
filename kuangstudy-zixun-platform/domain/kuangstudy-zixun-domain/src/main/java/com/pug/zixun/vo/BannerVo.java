package com.pug.zixun.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * BannerVo参数类
 * 创建人:yykk<br/>
 * 时间：2022-05-23 03:05:21 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BannerVo implements java.io.Serializable  {

    private Long id;
	private Integer status;
	private String title;
	private Integer pageNo = 1;
	private Integer pageSize = 10;
	private String keyword;
    private String batchIds;
    private Integer isDelete = 0;
    private Long categoryId;
}