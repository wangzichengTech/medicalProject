package com.pug.zixun.vo;

import com.pug.zixun.valid.annotation.ValidEmail;
import com.pug.zixun.valid.annotation.ValidPassword;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegUserVo implements Serializable {
    @NotNull
    @NotBlank
    @Size(min = 4, max = 50, message = "用户名长度必须在4到50个字符之间")
    private String username;



    @NotNull
    @ValidPassword
    private String password;

    @NotNull
    @ValidPassword
    private String cpassword;


    @NotNull
    @ValidEmail
    private String email;
    @NotNull
    @NotBlank
    @Size(min = 4, max = 50, message = "姓名长度必须在4到50个字符之间")
    private String name;
}
