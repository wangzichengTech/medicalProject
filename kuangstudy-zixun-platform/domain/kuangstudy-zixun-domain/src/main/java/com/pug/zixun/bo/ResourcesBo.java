package com.pug.zixun.bo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.pug.generator.anno.PugDoc;

import java.util.Date;

/**
 * Resources实体
 * 创建人:yykk<br/>
 * 时间：2022-07-19 23:58:26 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResourcesBo  implements java.io.Serializable {

    @PugDoc(name="主键")
    private Long id;
    @PugDoc(name="标题")
    private String filename;
    @PugDoc(name="描述")
    private String newname;
    @PugDoc(name="创建时间")
    private Date createTime;
    @PugDoc(name="更新时间")
    private Date updateTime;
    @PugDoc(name="发布状态")
    private Integer status;
    @PugDoc(name="删除状态")
    private Integer isdelete;
    @PugDoc(name="资源ID")
    private Long resourceId;
    @PugDoc(name="1图片 2 文件")
    private Integer type;
    @PugDoc(name="文件的大小")
    private String filesize;
    @PugDoc(name="文件后缀")
    private String ext;
    @PugDoc(name="文件的路径")
    private String path;
    @PugDoc(name="文件下载路径")
    private String downpath;
    @PugDoc(name="是否公开")
    private Integer ispublic;
}