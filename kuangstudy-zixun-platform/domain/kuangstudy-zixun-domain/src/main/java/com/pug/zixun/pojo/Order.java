package com.pug.zixun.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 飞哥B站地址：https://space.bilibili.com/490711252
 * 记得关注和三连哦！
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2022/5/16$ 23:15$
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order implements java.io.Serializable {

    private Integer id;
    private String orderNum;
}
