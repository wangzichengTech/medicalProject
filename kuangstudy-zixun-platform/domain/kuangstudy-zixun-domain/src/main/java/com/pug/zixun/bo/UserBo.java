package com.pug.zixun.bo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


/**
 * todo:用户管理
 * User<br/>
 * ******************************************
 * 更多精彩B站搜索:学相伴飞哥
 * IUserService
 * 资料下载学习平台：http://www.itbooking.net
 * 创世神B站：https://space.bilibili.com/490711252
 * ******************************************
 * 创建人:yykk<br/>
 * 时间：2021年09月26日 21:21:00 <br/>
 *
 * @version 1.0.0<br />
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserBo implements java.io.Serializable {
    // 主键
    private Long id;
    // 昵称
    private String nickname;
    // 真实姓名
    private String username;
    // 年龄
    private Integer age;
    // 签名
    private String sign;
    // 性别
    private Integer sex;
    // 浏览数
    private Integer views;
    // 职业
    private String job;
    // 工作年限
    private String jobyear;
    // 地址
    private String address;
    // 国家
    private String country;
    // 省份
    private String province;
    // 城市
    private String city;
    // 电话号码
    private String telephone;
    // 微信
    private String weixincode;
    // 学历
    private String education;
    // 生日
    private String birthday;
    // 头像
    private String avatar;
    // 背景图
    private String bgimg;
    // 1 普通 2 月VIP 3季度会员 4 年VIP 5超级VIP 6报名VIP 0过期
    private Integer vip;
    // vip时间
    private Date vipTime;
    // 学习积分
    private Integer cron;
    // 学习天数
    private Integer studydays;
    // 是否删除
    private Integer isDelete;
    // 是否禁止 1 拉黑 0 未拉黑
    private Integer  forbbiden;
    // qq
    private String qqcode;
    // 序号
    private String code;
    //角色
    private String role;
    //密码
    private String password;
    //微信登录的唯一id
    private String openid;
    //微信登录的唯一id
    private String unionid;
    //粉丝数
    private Integer fans;
    //关注数
    private Integer gznums;
    //个人博客地址
    private String bloglink;
    //gitee地址
    private String giteelink;
    // bibibi个人地址
    private String bilibililink;
    // 创建时间
    private Date createTime;
    // 更新时间
    private Date updateTime;
}