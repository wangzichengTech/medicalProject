package com.pug.zixun.bo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.pug.generator.anno.PugDoc;

import java.util.Date;

/**
 * AdminUser实体
 * 创建人:yykk<br/>
 * 时间：2022-07-15 20:48:26 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AdminUserBo  implements java.io.Serializable {

    @PugDoc(name="注解")
    private Long id;
    @PugDoc(name="用户名")
    private String username;
    @PugDoc(name="密码")
    private String password;
    @PugDoc(name="账号")
    private String account;
    @PugDoc(name="创建时间")
    private Date createTime;
    @PugDoc(name="更新时间")
    private Date updateTime;
    @PugDoc(name="头像")
    private String avatar;
    @PugDoc(name="发布状态 0 未发布  1发布")
    private Integer status;
    @PugDoc(name="删除状态 0 未删除 1删除")
    private Integer isdelete;
}