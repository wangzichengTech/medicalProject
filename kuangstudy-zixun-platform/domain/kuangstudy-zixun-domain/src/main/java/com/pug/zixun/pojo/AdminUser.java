package com.pug.zixun.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.pug.generator.anno.PugDoc;

import java.util.Date;
/**
 * AdminUser实体
 * 创建人:yykk<br/>
 * 时间：2022-07-15 20:48:26 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("kss_admin_user")
public class AdminUser  implements java.io.Serializable {

    @PugDoc(name="注解")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;
    @PugDoc(name="姓名")
    private String username;
    @PugDoc(name="密码")
    private String password;
    @PugDoc(name="账号")
    private String account;
    @PugDoc(name="手机号码")
    private String telephone;
    @PugDoc(name = "邮箱")
    private String email;
    @PugDoc(name="创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @PugDoc(name="更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
    @PugDoc(name="头像")
    private String avatar;
    @PugDoc(name="发布状态 0 未发(离职) 1已发布(入职)")
    private Integer status;
    @PugDoc(name="删除状态 0 未删除  1 删除")
    private Integer isdelete;
}