package com.pug.zixun.pojo;

import java.util.Date;
import lombok.*;
import com.baomidou.mybatisplus.annotation.*;

import org.pug.generator.anno.PugDoc;
/**
 * File实体
 * 创建人:yykk<br/>
 * 时间：2022-10-28 13:23:37 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("kss_file")
public class File  implements java.io.Serializable {

    @PugDoc(name="主键")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;
    @PugDoc(name="文件名称")
    private String name;
    @PugDoc(name="文件类型")
    private String type;
    @PugDoc(name="文件大小(kb)")
    private Long size;
    @PugDoc(name="预处理结果绝对路径")
    private String imageoutpath;
    @PugDoc(name="下载链接")
    private String url;
    @PugDoc(name="文件md5")
    private String md5;
    @PugDoc(name="是否删除")
    private Integer isdelete;
    @PugDoc(name="发布状态")
    private Integer status;
    @PugDoc(name="创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @PugDoc(name="更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

}