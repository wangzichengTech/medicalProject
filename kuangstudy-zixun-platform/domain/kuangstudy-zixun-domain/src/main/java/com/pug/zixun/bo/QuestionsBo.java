package com.pug.zixun.bo;

import java.util.Date;
import lombok.*;
import org.pug.generator.anno.PugDoc;

/**
 * Questions实体
 * 创建人:yykk<br/>
 * 时间：2022-07-30 23:58:45 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class QuestionsBo  implements java.io.Serializable {

    @PugDoc(name="")
    private Long id;
    @PugDoc(name="主题标题")
    private String title;
    @PugDoc(name="主题内容")
    private String content;
    @PugDoc(name="主题标签")
    private String tags;
    @PugDoc(name="主题缩略描述")
    private String description;
    @PugDoc(name="主题分类ID")
    private Long categoryid;
    @PugDoc(name="0普通 1置顶")
    private Integer gotop;
    @PugDoc(name="主题浏览次数")
    private Integer views;
    @PugDoc(name="发布时间")
    private Date createTime;
    @PugDoc(name="更新时间")
    private Date updateTime;
    @PugDoc(name="0不可以评论 1可以评论")
    private Integer comment;
    @PugDoc(name="静态链接")
    private String staticlink;
    @PugDoc(name="删除状态0未删除1删除")
    private Integer isdelete;
    @PugDoc(name="html内容")
    private String htmlcontent;
    @PugDoc(name="分类标题")
    private String categorytitle;
    @PugDoc(name="是否VIP")
    private String vip;
    @PugDoc(name="作者头像")
    private String avatar;
    @PugDoc(name="昵称")
    private String nickname;
    @PugDoc(name="发布状态")
    private Integer status;
    @PugDoc(name="用户")
    private Long userid;
    @PugDoc(name="封面图")
    private String img;
    @PugDoc(name="收藏数")
    private Integer collects;
    @PugDoc(name="评论数")
    private Integer comments;
    @PugDoc(name="QQ")
    private String qq;
    @PugDoc(name="下载地址")
    private String downlink;
    @PugDoc(name="下载密码")
    private String downpwd;
    @PugDoc(name="价格")
    private String price;
    @PugDoc(name="子分类")
    private Long categorycid;
    @PugDoc(name="")
    private String qqimg;
    @PugDoc(name="")
    private String gzcode;
    @PugDoc(name="")
    private String gzimg;
    @PugDoc(name="")
    private String bgimg;
    @PugDoc(name="")
    private String openlink;
    @PugDoc(name="")
    private Integer ispublic;
    @PugDoc(name="")
    private Integer vipicon;
    @PugDoc(name="")
    private Integer isline;
    @PugDoc(name="")
    private Integer sorted;
    @PugDoc(name="")
    private String categoryctitle;
}