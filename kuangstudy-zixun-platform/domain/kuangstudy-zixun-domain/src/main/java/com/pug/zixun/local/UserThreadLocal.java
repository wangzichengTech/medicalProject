package com.pug.zixun.local;

import com.pug.zixun.pojo.AdminUser;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 飞哥B站地址：https://space.bilibili.com/490711252
 * 记得关注和三连哦！
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2022/5/16$ 22:28$
 */
public class UserThreadLocal {
    // 实现一个threadlocal线程副本
    static ThreadLocal<AdminUser> userThreadLocal = new ThreadLocal<>();

    // 添加
    public static void put(AdminUser isFlag) {
        userThreadLocal.set(isFlag);
    }

    // 获取
    public static AdminUser get() {
        return userThreadLocal.get();
    }

    // 删除
    public static void remove() {
        userThreadLocal.remove();
    }
}
