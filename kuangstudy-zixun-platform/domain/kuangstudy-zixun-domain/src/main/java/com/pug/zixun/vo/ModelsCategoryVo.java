package com.pug.zixun.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.pug.generator.anno.PugDoc;

/**
 * ModelsCategoryVo参数类
 * 创建人:yykk<br/>
 * 时间：2022-12-15 20:23:14 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ModelsCategoryVo implements java.io.Serializable  {

    @PugDoc(name="操作ID",desc="删除使用")
    private Long id;
	@PugDoc(name="查询状态",desc="0未发布1发布")
	private Integer status;
	@PugDoc(name="分页起始",desc="1")
	private Integer pageNo = 1;
	@PugDoc(name="每页显示记录数",desc="10")
	private Integer pageSize = 10;
	@PugDoc(name="搜索关键词",desc="")
	private String keyword;
	@PugDoc(name="批量删除IDS",desc="用户表格批量删除")
    private String batchIds;
    @PugDoc(name="删除状态",desc="0未删除1删除")
    private Integer isDelete = 0;
    @PugDoc(name="搜索分类")
    private Long categoryId;
}