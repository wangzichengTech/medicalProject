package com.pug.zixun.vo;

import lombok.Data;

import java.util.Date;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 飞哥B站地址：https://space.bilibili.com/490711252
 * 记得关注和三连哦！
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2022-05-27$ 23:44$
 */
@Data
public class BannerUserVo {
    private Long bannerId;
    private String title;
    private Date createTime;
    private Long userId;
    private Date ctime;
    private String username;
}