package com.pug.zixun.vo;

import lombok.Data;

@Data
public class UserRequest {
    private String username;
    private String password;
    private String account;
    private String email;
    private String emailCode;
}
