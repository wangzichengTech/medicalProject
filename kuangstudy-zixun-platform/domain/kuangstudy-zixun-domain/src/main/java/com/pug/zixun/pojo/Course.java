package com.pug.zixun.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.pug.generator.anno.PugDoc;

import java.util.Date;
/**
 * Course实体
 * 创建人:yykk<br/>
 * 时间：2022-07-25 20:21:39 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("kss_course")
public class Course  implements java.io.Serializable {

    @PugDoc(name="")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;
    @PugDoc(name="主题标题")
    private String title;
    @PugDoc(name="主题内容")
    private String content;
    @PugDoc(name="主题标签")
    private String tags;
    @PugDoc(name="主题缩略描述")
    private String description;
    @PugDoc(name="主题分类ID")
    private Long categoryid;
    @PugDoc(name="0普通 1置顶")
    private Integer gotop;
    @PugDoc(name="主题浏览次数")
    private Integer views;
    @PugDoc(name="发布时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @PugDoc(name="更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
    @PugDoc(name="0不可以评论 1可以评论")
    private Integer comment;
    @PugDoc(name="删除状态0未删除1删除")
    private Integer isdelete;
    @PugDoc(name="html内容")
    private String htmlcontent;
    @PugDoc(name="分类标题")
    private String categorytitle;
    @PugDoc(name="用户")
    private Long userid;
    @PugDoc(name="是否VIP 1 所有人免费 2 月VIP 3季度会员 4 年VIP 5超级VIP")
    private Integer vip;
    @PugDoc(name="作者头像")
    private String avatar;
    @PugDoc(name="昵称")
    private String nickname;
    @PugDoc(name="发布状态")
    private Integer status;
    @PugDoc(name="封面图")
    private String img;
    @PugDoc(name="收藏数")
    private Integer collects;
    @PugDoc(name="评论数量")
    private Integer comments;
    @PugDoc(name="课程时长")
    private String coursetimer;
    @PugDoc(name="原始价格2499")
    private String price;
    @PugDoc(name="真实价格1499")
    private String realprice;
    @PugDoc(name="课程类型 1 基础课  2 进阶课  4 面试课  3 实战课程")
    private Integer coursetype;
    @PugDoc(name="排序")
    private Integer sorted;
    @PugDoc(name="")
    private String beginer;
    @PugDoc(name="")
    private Integer isnew;
    @PugDoc(name="")
    private Integer ishot;
    @PugDoc(name="")
    private Integer ispush;

}