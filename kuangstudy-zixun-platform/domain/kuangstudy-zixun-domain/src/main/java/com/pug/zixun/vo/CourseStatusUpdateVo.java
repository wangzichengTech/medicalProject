package com.pug.zixun.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.pug.generator.anno.PugDoc;

/**
 * AdminUserVo参数类
 * 创建人:yykk<br/>
 * 时间：2022-07-15 20:48:26 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CourseStatusUpdateVo implements java.io.Serializable  {
	@PugDoc(name="注解")
	private Long id;

	@PugDoc(name="标题")
	private String title;

	@PugDoc(name="发布状态 0 未发(离职) 1已发布(入职)")
	private Integer status;

	@PugDoc(name="删除状态 0 未删除  1 删除")
	private Integer isdelete;

	@PugDoc(name="是否评论 0否1是")
	private Integer comment;

	@PugDoc(name="置顶 0否1是")
	private Integer gotop;

	@PugDoc(name="课程类型 1 基础课  2 进阶课  4 面试课  3 实战课程")
	private Integer coursetype;

}