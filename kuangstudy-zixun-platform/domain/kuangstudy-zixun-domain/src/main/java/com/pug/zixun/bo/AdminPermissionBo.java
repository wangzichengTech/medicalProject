package com.pug.zixun.bo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.pug.generator.anno.PugDoc;

import java.util.Date;

/**
 * AdminPermission实体
 * 创建人:yykk<br/>
 * 时间：2022-07-17 18:38:05 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AdminPermissionBo  implements java.io.Serializable {

    @PugDoc(name="主键")
    private Long id;
    @PugDoc(name="菜单名词")
    private String name;
    @PugDoc(name="菜单排序")
    private Integer sorted;
    @PugDoc(name="菜单链接")
    private String path;
    @PugDoc(name="菜单图标")
    private String icon;
    @PugDoc(name="菜单发布")
    private Integer status;
    @PugDoc(name="创建时间")
    private Date createTime;
    @PugDoc(name="更新时间")
    private Date updateTime;
    @PugDoc(name="菜单名称")
    private Long pid;
    @PugDoc(name="路径名称")
    private String pathname;
    @PugDoc(name="删除状态 0未删除 1删除")
    private Integer isdelete;
    @PugDoc(name="1菜单 2 权限")
    private Integer type;
}