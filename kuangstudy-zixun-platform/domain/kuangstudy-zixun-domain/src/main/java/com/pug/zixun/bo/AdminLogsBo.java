package com.pug.zixun.bo;

import java.util.Date;
import lombok.*;
import org.pug.generator.anno.PugDoc;

/**
 * AdminLogs实体
 * 创建人:yykk<br/>
 * 时间：2022-07-26 20:20:25 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AdminLogsBo  implements java.io.Serializable {

    @PugDoc(name="主健")
    private Long id;
    @PugDoc(name="方法名")
    private String methodname;
    @PugDoc(name="类名")
    private String classname;
    @PugDoc(name="请求方法")
    private String requestmethod;
    @PugDoc(name="方法执行时间")
    private Long methodtime;
    @PugDoc(name="请求地址")
    private String requesturi;
    @PugDoc(name="请求IP")
    private String requestip;
    @PugDoc(name="浏览器")
    private String requestbrower;
    @PugDoc(name="操作人")
    private Long userId;
    @PugDoc(name="姓名")
    private String username;
    @PugDoc(name="联系方式")
    private String telephone;
    @PugDoc(name="创建时间")
    private Date createTime;
    @PugDoc(name="更新时间")
    private Date updateTime;
    @PugDoc(name="方法参数")
    private String methodparams;
    @PugDoc(name="操作系统")
    private String osversion;
    @PugDoc(name="模块")
    private String model;
    @PugDoc(name="描述")
    private String description;
    @PugDoc(name="省份")
    private String province;
    @PugDoc(name="城市")
    private String city;
}