package com.pug.zixun.bo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.pug.generator.anno.PugDoc;

import java.util.Date;

/**
 * ResourceCategory实体
 * 创建人:yykk<br/>
 * 时间：2022-07-19 23:49:04 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResourceCategoryBo  implements java.io.Serializable {

    @PugDoc(name="主键")
    private Long id;
    @PugDoc(name="标题")
    private String title;
    @PugDoc(name="创建时间")
    private Date createTime;
    @PugDoc(name="更新时间")
    private Date updateTime;
    @PugDoc(name="发布状态")
    private Integer status;
    @PugDoc(name="排序")
    private Integer sorted;
    @PugDoc(name="删除状态")
    private Integer isdelete;
    @PugDoc(name="父级")
    private Long pid;
}