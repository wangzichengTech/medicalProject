package com.pug.zixun.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.pug.generator.anno.PugDoc;

import java.util.Date;
/**
 * CourseComment实体
 * 创建人:yykk<br/>
 * 时间：2022-07-25 20:22:03 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("kss_course_comment")
public class CourseComment  implements java.io.Serializable {

    @PugDoc(name="主键")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;
    @PugDoc(name="用户ID")
    private Long userid;
    @PugDoc(name="文件ID")
    private Long courseid;
    @PugDoc(name="评论的内容")
    private String content;
    @PugDoc(name="添加时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @PugDoc(name="更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
    @PugDoc(name="用户的昵称")
    private String nickname;
    @PugDoc(name="用户的头像")
    private String avatar;
    @PugDoc(name="用户的签名")
    private String sign;
    @PugDoc(name="用户身份")
    private Integer vip;
    @PugDoc(name="问题的标题")
    private String coursetitle;
    @PugDoc(name="问题的封面")
    private String courseimg;
    @PugDoc(name="问题的描述")
    private String coursedesc;
    @PugDoc(name="分类id")
    private Long coursecategoryid;
    @PugDoc(name="分类名称")
    private String coursecatename;
    @PugDoc(name="回复评论的ID")
    private Long pid;
    @PugDoc(name="回复人")
    private Long replyuserid;
    @PugDoc(name="回复人的昵称")
    private String replyusernickname;
    @PugDoc(name="回复人的身份")
    private Integer replayvip;
    @PugDoc(name="回复人的签名")
    private String replysign;
    @PugDoc(name="回复人的头像")
    private String replyuseravatar;

}