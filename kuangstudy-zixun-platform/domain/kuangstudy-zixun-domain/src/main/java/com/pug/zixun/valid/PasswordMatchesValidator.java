//package com.pug.zixun.valid;
//
//import com.pug.zixun.valid.annotation.PasswordMatches;
//import com.pug.zixun.vo.AdminUserRegVo;
//import com.pug.zixun.vo.RegUserVo;
//import org.apache.commons.lang3.StringUtils;
//
//import javax.validation.ConstraintValidator;
//import javax.validation.ConstraintValidatorContext;
//
//public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, AdminUserRegVo> {
//
//    @Override
//    public void initialize(final PasswordMatches constraintAnnotation) { }
//
//    @Override
//    public boolean isValid(final AdminUserRegVo adminUserRegVo, final ConstraintValidatorContext context) {
//        if(adminUserRegVo == null){
//            return true;
//        }
//
//        if(StringUtils.isEmpty(adminUserRegVo.getPassword())){
//            return true;
//        }
//
//        if(StringUtils.isEmpty(adminUserRegVo.getConfirmPassword())){
//            return true;
//        }
//
//        boolean equals = adminUserRegVo.getPassword().equals(adminUserRegVo.getConfirmPassword());
//        return equals;
//    }
//}
