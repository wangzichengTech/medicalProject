package com.pug.zixun.bo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.pug.generator.anno.PugDoc;

import java.util.Date;

/**
 * AdminMenu实体
 * 创建人:yykk<br/>
 * 时间：2022-07-07 19:53:39 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AdminMenuBo  implements java.io.Serializable {

    @PugDoc(name="主键")
    private Long id;
    @PugDoc(name="菜单名词")
    private String name;
    @PugDoc(name="菜单排序")
    private Integer sorted;
    @PugDoc(name="菜单链接")
    private String path;
    @PugDoc(name="菜单图标")
    private String icon;
    @PugDoc(name="菜单发布")
    private Integer status;
    @PugDoc(name="创建时间")
    private Date createTime;
    @PugDoc(name="更新时间")
    private Date updateTime;
    @PugDoc(name="菜单名称")
    private Long pid;
    @PugDoc(name="组件名称")
    private String componentname;
    @PugDoc(name="路径名称")
    private String pathname;
    @PugDoc(name="父组件")
    private String layout;
    @PugDoc(name="排序")
    private Integer indexon;
    @PugDoc(name="是否展示")
    private Integer showflag;
    @PugDoc(name="删除状态 0未删除 1删除")
    private Integer isdelete;
}