package com.pug.zixun.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 飞哥B站地址：https://space.bilibili.com/490711252
 * 记得关注和三连哦！
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2022/5/16$ 22:00$
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserVo extends ParentVo implements java.io.Serializable {
    // id
    private Long id;
    // pageNo
    private Integer pageNo;
    // pageSize
    private Integer pageSize;
    // 用户姓名
    private String username;
    // 密码
    private String password;
    // 验证码
    private String code;
    // 验证码的UUID
    private String uuid;
    // token
    private String token;
}
