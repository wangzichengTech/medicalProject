package com.pug.zixun.bo;

import com.pug.zixun.pojo.AdminUser;
import lombok.Data;

import java.util.List;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 飞哥B站地址：https://space.bilibili.com/490711252
 * 记得关注和三连哦！
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2022/5/19$ 22:10$
 */
@Data
public class PugUserBo implements java.io.Serializable {
    // 接口校验使用
    private String token;
    // 下线使用
    private String tokenUuid;
    //  登录的用户信息
    private AdminUser user;
    // 返回对应角色
    private List<String> roleNames;
    // 返回对应权限
    private List<String> permissions;

    @Override
    public String toString() {
        return "PugUserBo{" +
                "token='" + token + '\'' +
                ", tokenUuid='" + tokenUuid + '\'' +
                ", user=" + user +
                ", roleNames=" + roleNames +
                ", permissions=" + permissions +
                '}';
    }
}
