package com.pug.zixun.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.pug.generator.anno.PugDoc;

/**
 * AdminUserVo参数类
 * 创建人:yykk<br/>
 * 时间：2022-07-15 20:48:26 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StatusUpdateVo implements java.io.Serializable  {
	@PugDoc(name="注解")
	private Long id;

	@PugDoc(name="发布状态 0 未发(离职) 1已发布(入职)")
	private Integer status;

	@PugDoc(name="删除状态 0 未删除  1 删除")
	private Integer isdelete;

}