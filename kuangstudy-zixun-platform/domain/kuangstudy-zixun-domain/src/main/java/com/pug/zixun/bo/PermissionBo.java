package com.pug.zixun.bo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.pug.generator.anno.PugDoc;

import java.util.Date;

/**
 * Permission实体
 * 创建人:yykk<br/>
 * 时间：2022-05-23 20:06:20 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PermissionBo  implements java.io.Serializable {

    @PugDoc(name="主键")
    private Long id;
    @PugDoc(name="权限名称")
    private String name;
    @PugDoc(name="创建时间")
    private Date createTime;
    @PugDoc(name="更新时间")
    private Date updateTime;
    @PugDoc(name="权限代号")
    private String code;
    @PugDoc(name="权限URL")
    private String url;
    @PugDoc(name="权限排序")
    private Integer sorted;
    @PugDoc(name="删除状态 0未删除 1删除")
    private Integer isdelete;
    @PugDoc(name="发布状态 1发布中 0 未发布")
    private Integer status;
    @PugDoc(name="父id")
    private Long pid;
    @PugDoc(name="")
    private String pathname;
}