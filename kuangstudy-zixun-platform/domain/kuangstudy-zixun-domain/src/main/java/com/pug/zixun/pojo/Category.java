package com.pug.zixun.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.pug.generator.anno.PugDoc;

import java.util.Date;

/**
 * Category实体
 * 创建人:yykk<br/>
 * 时间：2022-05-23 14:38:43 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("kss_category")
public class Category  implements java.io.Serializable {

    @PugDoc(name="主键")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;
    @PugDoc(name="标题")
    private String title;
    @PugDoc(name="描述")
    private String description;
    @PugDoc(name="创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @PugDoc(name="更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
    @PugDoc(name="发布状态")
    private Integer status;
    @PugDoc(name="排序")
    private Integer sorted;
    @PugDoc(name="删除状态")
    private Integer isdelete;
    @PugDoc(name="父id")
    private Long pid;
}