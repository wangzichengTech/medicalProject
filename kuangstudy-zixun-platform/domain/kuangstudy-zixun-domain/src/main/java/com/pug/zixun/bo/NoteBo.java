package com.pug.zixun.bo;

import java.util.Date;
import lombok.*;
import org.pug.generator.anno.PugDoc;

/**
 * Note实体
 * 创建人:yykk<br/>
 * 时间：2022-07-30 21:42:35 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NoteBo  implements java.io.Serializable {

    @PugDoc(name="")
    private Long id;
    @PugDoc(name="主题标题")
    private String title;
    @PugDoc(name="主题内容")
    private String content;
    @PugDoc(name="主题标签")
    private String tags;
    @PugDoc(name="主题缩略描述")
    private String description;
    @PugDoc(name="主题分类ID")
    private Long categoryid;
    @PugDoc(name="0普通 1置顶")
    private Integer gotop;
    @PugDoc(name="主题浏览次数")
    private Integer views;
    @PugDoc(name="发布时间")
    private Date createTime;
    @PugDoc(name="更新时间")
    private Date updateTime;
    @PugDoc(name="0不可以评论 1可以评论")
    private Integer comment;
    @PugDoc(name="静态链接")
    private String staticlink;
    @PugDoc(name="删除状态0未删除1删除")
    private Integer isdelete;
    @PugDoc(name="html内容")
    private String htmlcontent;
    @PugDoc(name="分类标题")
    private String categorytitle;
    @PugDoc(name="是否VIP 1 所有人免费 2 月VIP 3季度会员 4 年VIP 5超级VIP")
    private Integer vip;
    @PugDoc(name="作者头像")
    private String avatar;
    @PugDoc(name="昵称")
    private String nickname;
    @PugDoc(name="发布状态")
    private Integer status;
    @PugDoc(name="用户")
    private Long userid;
    @PugDoc(name="封面图")
    private String img;
    @PugDoc(name="收藏数")
    private Integer collects;
    @PugDoc(name="评论数量")
    private Integer comments;
    @PugDoc(name="课程时长")
    private String coursetimer;
    @PugDoc(name="原始价格2499")
    private String price;
    @PugDoc(name="真实价格1499")
    private String realprice;
    @PugDoc(name="课程类型 1 基础课  2 进阶课  4 面试课  3 实战课程")
    private Integer coursetype;
    @PugDoc(name="排序")
    private Integer sorted;
    @PugDoc(name="体验地址")
    private String trylink;
    @PugDoc(name="下载地址")
    private String downlink;
    @PugDoc(name="下载密码")
    private String downpwd;
    @PugDoc(name="播放地址")
    private String playvideo;
    @PugDoc(name="0 不展示 1 展示")
    private Integer isapp;
    @PugDoc(name="0 录播 1 直播")
    private Integer isonline;
    @PugDoc(name="章节数量")
    private String lessoncount;
    @PugDoc(name="")
    private String giteelink;
    @PugDoc(name="")
    private String coursecss;
    @PugDoc(name="")
    private Integer coursebuy;
    @PugDoc(name="")
    private String courseimg;
    @PugDoc(name="")
    private String coursedesc1;
    @PugDoc(name="")
    private String coursedesc2;
    @PugDoc(name="")
    private String coursedesc3;
    @PugDoc(name="")
    private String courseintro;
    @PugDoc(name="")
    private String qqcode;
    @PugDoc(name="")
    private String qqimg;
    @PugDoc(name="")
    private String qqgroupcode;
    @PugDoc(name="")
    private String qqgroupimg;
    @PugDoc(name="")
    private String gzcode;
    @PugDoc(name="")
    private String gzimg;
    @PugDoc(name="")
    private Integer isnote;
    @PugDoc(name="")
    private Integer isfree;
    @PugDoc(name="")
    private String userintro;
    @PugDoc(name="")
    private String appcode;
    @PugDoc(name="")
    private String appimg;
    @PugDoc(name="")
    private String wxcode;
    @PugDoc(name="")
    private String wximg;
    @PugDoc(name="")
    private Integer zans;
    @PugDoc(name="")
    private String beginer;
    @PugDoc(name="")
    private Integer isnew;
    @PugDoc(name="")
    private Integer ishot;
    @PugDoc(name="")
    private Integer ispush;
    @PugDoc(name="")
    private String openlink;
    @PugDoc(name="")
    private Integer isline;
    @PugDoc(name="")
    private String alipanlink;
    @PugDoc(name="")
    private String alipanpwd;
}