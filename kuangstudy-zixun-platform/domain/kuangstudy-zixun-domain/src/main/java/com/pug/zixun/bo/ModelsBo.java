package com.pug.zixun.bo;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.*;
import org.pug.generator.anno.PugDoc;

/**
 * Models实体
 * 创建人:yykk<br/>
 * 时间：2022-10-08 21:13:25 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ModelsBo  implements java.io.Serializable {

    @PugDoc(name="模型ID")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;
    @PugDoc(name="模型内容")
    private String content;
    @PugDoc(name="模型缩略描述")
    private String description;
    @PugDoc(name="模型标签")
    private String tags;
    @PugDoc(name="模型作者")
    private String author;
    @PugDoc(name="模型绝对路径")
    private String path;
    @PugDoc(name="模型名称")
    private String name;
    @PugDoc(name="模型分类id")
    private Long categoryid;
    @PugDoc(name="分类标题")
    private String categorytitle;
    @PugDoc(name="发布状态")
    private Integer status;
    @PugDoc(name="发布时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @PugDoc(name="更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
    @PugDoc(name="删除状态0未删除1删除")
    private Integer isdelete;
    @PugDoc(name="html内容")
    private String htmlcontent;
    @PugDoc(name="发布人昵称")
    private String nickname;
    @PugDoc(name="发布人头像")
    private String avatar;
    @PugDoc(name="发布人用户id")
    private Long userid;
    @PugDoc(name="封面图")
    private String img;
    @PugDoc(name="排序")
    private Integer sorted;
}