package com.pug.zixun.pojo;

import com.baomidou.mybatisplus.annotation.*;
import com.pug.zixun.valid.annotation.Phone;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.pug.generator.anno.PugDoc;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Banner实体
 * 创建人:yykk<br/>
 * 时间：2022-05-23 03:05:21 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("kss_banner")
public class Banner  implements java.io.Serializable {

    @PugDoc(name="主键")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;
    @PugDoc(name="标题")
    @NotNull(message = "标题不允许为空")
    @Phone
    private String title;
    @PugDoc(name="链接地址")
    private String hreflink;
    @PugDoc(name="打开方式")
    private String opentype;
    @PugDoc(name="描述")
    private String description;
    @PugDoc(name="封面图标")
    private String img;
    @PugDoc(name="排序字段")
    private Integer sorted;
    @PugDoc(name="发布状态")
    private Integer status;
    @PugDoc(name="创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @PugDoc(name="更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
    @PugDoc(name="")
    private Integer isdelete;
}