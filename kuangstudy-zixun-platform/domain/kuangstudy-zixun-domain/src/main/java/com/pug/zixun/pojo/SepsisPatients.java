package com.pug.zixun.pojo;

import java.util.Date;
import lombok.*;
import com.baomidou.mybatisplus.annotation.*;

import org.pug.generator.anno.PugDoc;
/**
 * SepsisPatients实体
 * 创建人:yykk<br/>
 * 时间：2022-11-14 19:09:44 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("kss_sepsis_patients")
public class SepsisPatients  implements java.io.Serializable {

    @PugDoc(name="ID")
    @TableId(type = IdType.AUTO)
    private Integer id;
    @PugDoc(name="姓名")
    private String name;
    @PugDoc(name="性别")
    private Double sex;
    @PugDoc(name="年龄")
    private Double age;
    @PugDoc(name="")
    private Double a;
    @PugDoc(name="")
    private Double b;
    @PugDoc(name="")
    private Double c;
    @PugDoc(name="")
    private Double d;
    @PugDoc(name="")
    private Double e;
    @PugDoc(name="")
    private Double f;
    @PugDoc(name="")
    private Double g;
    @PugDoc(name="")
    private Double h;
    @PugDoc(name="")
    private Double i;
    @PugDoc(name="")
    private Double j;
    @PugDoc(name="")
    private Double k;
    @PugDoc(name="")
    private Double l;
    @PugDoc(name="")
    private Double m;
    @PugDoc(name="")
    private Double n;
    @PugDoc(name="")
    private Double o;
    @PugDoc(name="")
    private Double p;
    @PugDoc(name="")
    private Double q;
    @PugDoc(name="")
    private Double r;
    @PugDoc(name="")
    private Double s;
    @PugDoc(name="")
    private Double t;
    @PugDoc(name="")
    private Double u;
    @PugDoc(name="")
    private Double v;
    @PugDoc(name="")
    private Double data;
    @PugDoc(name="是否删除")
    private Integer isdelete;

}