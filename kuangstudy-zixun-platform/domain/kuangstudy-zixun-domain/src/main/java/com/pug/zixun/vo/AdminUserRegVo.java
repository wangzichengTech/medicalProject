package com.pug.zixun.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.pug.generator.anno.PugDoc;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * AdminUserVo参数类
 * 创建人:yykk<br/>
 * 时间：2022-07-15 20:48:26 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AdminUserRegVo implements java.io.Serializable  {
	@PugDoc(name="注解")
	private Long id;

	@NotBlank(message = "{AdminUserRegVo.NotBlank.username.message}")
	@Size(min = 2, max = 20, message = "{AdminUserRegVo.username}")
	@PugDoc(name="姓名")
	private String username;

	@PugDoc
	//@ValidPassword
	private String password;

	@NotBlank(message = "{AdminUserRegVo.NotBlank.account.message}")
	@PugDoc(name="账号")
	private String account;

	@NotBlank
	@PugDoc(name="头像")
	private String avatar;

	@PugDoc(name="发布状态 0 未发(离职) 1已发布(入职)")
	private Integer status;

	@PugDoc(name="删除状态 0 未删除  1 删除")
	private Integer isdelete;

	@NotNull
	@PugDoc(name="角色ID")
	private Long roleId;
}