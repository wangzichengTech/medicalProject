package com.pug.zixun.vo;

import lombok.Data;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 飞哥B站地址：https://space.bilibili.com/490711252
 * 记得关注和三连哦！
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2022/5/16$ 22:00$
 */
@Data
public class ParentVo implements java.io.Serializable{
    private Integer isflag;
}
