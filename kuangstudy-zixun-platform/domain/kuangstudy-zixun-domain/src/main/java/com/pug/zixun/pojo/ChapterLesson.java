package com.pug.zixun.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.pug.generator.anno.PugDoc;

import java.util.Date;
/**
 * ChapterLesson实体
 * 创建人:yykk<br/>
 * 时间：2022-07-25 20:22:31 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("kss_course_chapter_lesson")
public class ChapterLesson  implements java.io.Serializable {

    @PugDoc(name="主键")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;
    @PugDoc(name="章节标题")
    private String title;
    @PugDoc(name="章节内容")
    private String content;
    @PugDoc(name="标签")
    private String tags;
    @PugDoc(name="主题缩略描述")
    private String description;
    @PugDoc(name="发布时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @PugDoc(name="更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
    @PugDoc(name="删除状态0未删除1删除")
    private Integer isdelete;
    @PugDoc(name="html内容")
    private String htmlcontent;
    @PugDoc(name="作者头像")
    private String avatar;
    @PugDoc(name="昵称")
    private String nickname;
    @PugDoc(name="发布状态")
    private Integer status;
    @PugDoc(name="用户")
    private Long userid;
    @PugDoc(name="章节ID")
    private Long pid;
    @PugDoc(name="笔记ID")
    private Long courseid;
    @PugDoc(name="排序")
    private Integer sorted;
    @PugDoc(name="时长")
    private String coursetimer;
    @PugDoc(name="")
    private String coursetimersize;
    @PugDoc(name="")
    private String videoid;
    @PugDoc(name="")
    private String videofirstimage;
    @PugDoc(name="")
    private String videolink;
    @PugDoc(name="")
    private String videofilesize;
    @PugDoc(name="")
    private String videotag;
    @PugDoc(name="")
    private String videoplayerheight;
    @PugDoc(name="")
    private String videomp4;
    @PugDoc(name="")
    private String videocategoryid;
    @PugDoc(name="")
    private String videocategorytitle;
    @PugDoc(name="")
    private String videoplayerwidth;
    @PugDoc(name="")
    private String videoswflink;
    @PugDoc(name="")
    private String videooriginal;
    @PugDoc(name="")
    private String videodf;
    @PugDoc(name="")
    private String videoseed;
    @PugDoc(name="")
    private String videostatus;
    @PugDoc(name="")
    private String videotimes;
    @PugDoc(name="")
    private String videocontext;
    @PugDoc(name="")
    private Integer isfree;

}