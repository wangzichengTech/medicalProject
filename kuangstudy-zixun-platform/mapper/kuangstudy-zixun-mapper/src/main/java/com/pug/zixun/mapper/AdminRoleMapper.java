package com.pug.zixun.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pug.zixun.pojo.AdminRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * AdminRoleMapper
 * 创建人:yykk<br/>
 * 时间：2022-07-15 22:04:49 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 *
 * @version 1.0.0<br />
 */
public interface AdminRoleMapper extends BaseMapper<AdminRole> {


    /**
     * 查询角色对用的权限
     *
     * @param roleId
     * @return
     */
    List<String> findPermissionByRoleId(@Param("roleId") Long roleId);


    /**
     * 删除角色对应的权限
     *
     * @param roleId
     * @return
     */
    int deletePermissionByRoleId(@Param("roleId") Long roleId);


    /**
     * 给角色授权
     *
     * @param roleId
     * @param permissionId
     * @return
     */
    int savePermissionByRoleId(@Param("roleId") Long roleId, @Param("permissionId") Long permissionId);


}