package com.pug.zixun.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pug.zixun.pojo.AdminPermission;
import org.apache.ibatis.annotations.Param;

/**
 * AdminPermissionMapper
 * 创建人:yykk<br/>
 * 时间：2022-07-17 18:38:05 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 *
 * @version 1.0.0<br />
 */
public interface AdminPermissionMapper extends BaseMapper<AdminPermission> {

    int deleteByPermissionId(@Param("permissionId") Long permissionId);

}