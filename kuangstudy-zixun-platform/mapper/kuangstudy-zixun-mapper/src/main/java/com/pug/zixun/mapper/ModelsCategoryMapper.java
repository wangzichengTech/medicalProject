package com.pug.zixun.mapper;

import com.pug.zixun.pojo.ModelsCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * ModelsCategoryMapper
 * 创建人:yykk<br/>
 * 时间：2022-12-15 20:23:14 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
public interface ModelsCategoryMapper extends BaseMapper<ModelsCategory>{
}