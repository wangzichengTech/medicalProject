package com.pug.zixun.mapper;

import com.pug.zixun.pojo.Models;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * ModelsMapper
 * 创建人:yykk<br/>
 * 时间：2022-10-08 21:13:25 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
public interface ModelsMapper extends BaseMapper<Models>{
}