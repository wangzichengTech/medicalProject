package com.pug.zixun.mapper;

import com.pug.zixun.pojo.Note;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * NoteMapper
 * 创建人:yykk<br/>
 * 时间：2022-07-30 21:42:35 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
public interface NoteMapper extends BaseMapper<Note>{
}