package com.pug.zixun.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pug.zixun.pojo.Banner;
import com.pug.zixun.vo.BannerUserVo;
import com.pug.zixun.vo.BannerVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * BannerMapper
 * 创建人:yykk<br/>
 * 时间：2022-05-23 03:05:21 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
public interface BannerMapper extends BaseMapper<Banner>{

    /**
     *
     * @param bannerVo
     * @return
     */
    List<Banner> findBanners(@Param("bannerVo") BannerVo bannerVo);

    /**
     * 查询轮播图和对应用户信息
     * @param bannerVo
     * @return
     */
    List<Map<String,Object>> findBannerUsers(@Param("bannerVo") BannerVo bannerVo);
    List<BannerUserVo> findBannerUsersVo(@Param("bannerVo") BannerVo bannerVo);


    /**
     * 分页写法
     * @param page
     * @param queryWrapper
     * @return
     */
    IPage<BannerUserVo> findBannerUsersVoPage(Page page, @Param(Constants.WRAPPER) Wrapper<BannerVo> queryWrapper);
}