package com.pug.zixun.mapper;

import com.pug.zixun.pojo.File;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * FileMapper
 * 创建人:yykk<br/>
 * 时间：2022-10-28 13:23:37 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
public interface FileMapper extends BaseMapper<File>{
}