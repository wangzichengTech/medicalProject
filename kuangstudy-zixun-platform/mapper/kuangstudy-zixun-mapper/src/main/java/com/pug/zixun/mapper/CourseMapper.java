package com.pug.zixun.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pug.zixun.pojo.Course;

/**
 * CourseMapper
 * 创建人:yykk<br/>
 * 时间：2022-07-25 20:21:39 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
public interface CourseMapper extends BaseMapper<Course>{
}