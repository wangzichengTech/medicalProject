package com.pug.zixun.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pug.zixun.pojo.AdminRole;
import com.pug.zixun.pojo.AdminUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * AdminUserMapper
 * 创建人:yykk<br/>
 * 时间：2022-07-15 20:48:26 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 *
 * @version 1.0.0<br />
 */
public interface AdminUserMapper extends BaseMapper<AdminUser> {


    /**
     * 查询用户对应的角色
     * @param userId
     * @return
     */
    List<AdminRole> findByUserRoles(@Param("userId")Long userId);

    /**
     * 查询用户对应的权限
     * @param userId
     * @return
     */
    List<String> findByUserPermission(@Param("userId")Long userId);



    /**
     * 查询用户是否已经绑定角色
     *
     * @param userId
     * @param roleId
     * @return
     */
    int countUserRole(@Param("userId") Long userId, @Param("roleId") Long roleId);


    /**
     * 保存用户角色信息
     *
     * @param userId
     * @param roleId
     * @return
     */
    int saveUserRole(@Param("userId") Long userId, @Param("roleId") Long roleId);

    /**
     * 删除用户角色信息
     *
     * @param userId
     * @param roleId
     * @return
     */
    int delUserRole(@Param("userId") Long userId, @Param("roleId") Long roleId);
}