package org.pug.alipay;

import org.pug.service.AliPayServiceImpl;
import org.pug.service.IAliPayService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 飞哥B站地址：https://space.bilibili.com/490711252
 * 记得关注和三连哦！
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2022/5/17$ 22:45$
 */
@Configuration
@ConditionalOnClass(IAliPayService.class)
@EnableConfigurationProperties(AliPayProperties.class)
public class AliPayAutoConfiguration  {


    @Bean
    public IAliPayService aliPayService(){
        return new AliPayServiceImpl();
    }

}
