    package org.pug.service;

    import org.pug.alipay.AliPayProperties;
    import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 飞哥B站地址：https://space.bilibili.com/490711252
 * 记得关注和三连哦！
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2022/5/17$ 22:54$
 */
public class AliPayServiceImpl implements IAliPayService{


    @Autowired
    AliPayProperties aliPayProperties;
    /**
     * 阿里支付
     * @return
     */
    @Override
    public String alipay(){
        System.out.println(aliPayProperties);
        return "alipaysuccess";
    }
}
