package org.pug.alipay;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 飞哥B站地址：https://space.bilibili.com/490711252
 * 记得关注和三连哦！
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2022/5/17$ 22:45$
 */
@ConfigurationProperties(prefix = "pug.alipay")
public class AliPayProperties implements java.io.Serializable {

    private String appid = "1111";
    private String notifyUrl = "http://www.baidu.com";
    private String key = "222";
    private String publiekey = "3333";

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setPubliekey(String publiekey) {
        this.publiekey = publiekey;
    }


}
