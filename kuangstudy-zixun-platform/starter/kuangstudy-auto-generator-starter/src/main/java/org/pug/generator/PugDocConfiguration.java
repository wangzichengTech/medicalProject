package org.pug.generator;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 飞哥B站地址：https://space.bilibili.com/490711252
 * 记得关注和三连哦！
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2022-05-23$ 1:54$
 */
@Configuration
@EnableConfigurationProperties(GeneratorProperties.class)
public class PugDocConfiguration {


    @Bean
    public GeneratorService generatorService() {
        return new GeneratorService();
    }

}
