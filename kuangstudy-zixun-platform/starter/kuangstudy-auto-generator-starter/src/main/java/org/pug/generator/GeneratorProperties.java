package org.pug.generator;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 飞哥B站地址：https://space.bilibili.com/490711252
 * 记得关注和三连哦！
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2022-05-23$ 14:24$
 */
@ConfigurationProperties(prefix = "pug.generator")
@Data
public class GeneratorProperties implements java.io.Serializable{

    private String basePackage;
    private String outPath;
    private String filename;

}
