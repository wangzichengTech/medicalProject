package org.pug.generator.anno;

import java.lang.annotation.*;

@Target({ElementType.METHOD, ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)// 你可以通过反射获取到注解消息
@Documented
public @interface PugDoc {
    String name() default "";
    boolean required() default false;
    boolean token() default true;
    String desc() default "";
    String tabname() default "";
    String model() default "";
}
