package com.pug.zixun.service.chapterlesson;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pug.zixun.bo.ChapterLessonBo;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;
import com.pug.zixun.mapper.ChapterLessonMapper;
import com.pug.zixun.pojo.ChapterLesson;
import com.pug.zixun.vo.ChapterLessonVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;



/**
 * ChapterLessonServiceImpl实现类
 * 创建人:yykk<br/>
 * 时间：2022-07-25 20:22:31 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Service
@Slf4j
public class ChapterLessonServiceImpl extends ServiceImpl<ChapterLessonMapper,ChapterLesson> implements IChapterLessonService  {

    /**
     * 根据课程查询对应的章信息
     * @method: findChapterLessonList
     * @result : List<ChapterLesson>
     * 创建人:yykk
     * 创建时间：2022-07-25 20:22:31
     * @version 1.0.0
     * @return
    */
    @Override
    public List<ChapterLessonBo> findChapterList(Long courseId) {
     	// 1：设置查询条件
        LambdaQueryWrapper<ChapterLesson> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // 2：查询发布的 0 未发布  1 发布
        lambdaQueryWrapper.eq(ChapterLesson::getCourseid,courseId);
        lambdaQueryWrapper.eq(ChapterLesson::getPid,0);
        lambdaQueryWrapper.orderByAsc(ChapterLesson::getIsdelete,ChapterLesson::getSorted);
        // 3: 查询返回
        return tranferListBo(this.list(lambdaQueryWrapper),ChapterLessonBo.class);
    }

    /**
     * 根据章ID查询对应节嘻嘻
     * @method: findChapterLessonList
     * @result : List<ChapterLesson>
     * 创建人:yykk
     * 创建时间：2022-07-25 20:22:31
     * @version 1.0.0
     * @return
     */
    @Override
    public List<ChapterLessonBo> findLessonList(Long chapterId) {
        // 1：设置查询条件
        LambdaQueryWrapper<ChapterLesson> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // 2：查询发布的 0 未发布  1 发布
        lambdaQueryWrapper.eq(ChapterLesson::getPid,chapterId);
        lambdaQueryWrapper.orderByAsc(ChapterLesson::getIsdelete,ChapterLesson::getSorted);
        // 3: 查询返回
        return tranferListBo(this.list(lambdaQueryWrapper),ChapterLessonBo.class);
    }

	/**
     * 根据id查询章节管理明细信息
     * @param id
     * @return ChapterLesson
     * 创建人:yykk
     * 创建时间：2022-07-25 20:22:31
     * @version 1.0.0
     */
    @Override
    public ChapterLessonBo getChapterLessonById(Long id) {
        return tranferBo(this.getById(id),ChapterLessonBo.class);
    }


    /**
     * 保存&修改章节管理
     * @param chapterlesson
     * @return ChapterLesson
     * 创建人:yykk
     * 创建时间：2022-07-25 20:22:31
     * @version 1.0.0
     */
    @Override

	public ChapterLessonBo saveupdateChapterLesson(ChapterLesson chapterlesson){
		boolean flag = this.saveOrUpdate(chapterlesson);// 保存会自动回填id
		return flag ? tranferBo(chapterlesson,ChapterLessonBo.class)  : null;
	}


    /**
     * 根据id删除章节管理
     * @param id
     * @return int
     * 创建人:yykk
     * 创建时间：2022-07-25 20:22:31
     * @version 1.0.0
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteChapterLessonById(Long id) {
        ChapterLesson chapterLesson = this.getById(id);
        if (chapterLesson != null) {
            ChapterLesson chapterlesson2 = new ChapterLesson();
            chapterlesson2.setId(id);
            chapterlesson2.setIsdelete(1);
            boolean b = this.updateById(chapterlesson2);
            return 1;
        }
        return 0;
    }

    /**
     * 根据id删除章节管理
     * @param id
     * @return int
     * 创建人:yykk
     * 创建时间：2022-07-25 20:22:31
     * @version 1.0.0
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int removeChapterLessonById(Long id) {
       ChapterLesson chapterLesson = this.getById(id);
       if (chapterLesson != null && !chapterLesson .getPid().equals(0L)) {
           // 删除的节
           boolean b = this.removeById(chapterLesson.getId());
           return  b ? 1 : 0;
       }else{
           // 删除章
           boolean b = this.removeById(chapterLesson.getId());
           // 查询章下面所有的节
           List<ChapterLessonBo>  chapterLessonBos =  findLessonList (chapterLesson.getId());
           // 删除章下面的节
           if(!CollectionUtils.isEmpty(chapterLessonBos)){
               List<Long> lessonIds = chapterLessonBos.stream().map(chapter -> chapter.getId()).collect(Collectors.toList());
               this.removeByIds(lessonIds);
           }
           return  b ? 1 : 0;
       }
    }
}