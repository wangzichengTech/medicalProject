package com.pug.zixun.service.chapterlesson;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pug.zixun.bo.ChapterLessonBo;
import com.pug.zixun.pojo.ChapterLesson;
import com.pug.zixun.service.BaseService;
import com.pug.zixun.vo.ChapterLessonVo;

import java.util.List;

/**
 * IChapterLessonService接口
 * 创建人:yykk<br/>
 * 时间：2022-07-25 20:22:31 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
public interface IChapterLessonService extends IService<ChapterLesson>,BaseService{


    /**
     * 查询章节管理列表信息
     * @method: findChapterLessonList
     * @result : List<ChapterLessonBo>
     * 创建人:yykk
     * 创建时间：2022-07-25 20:22:31
     * @version 1.0.0
     * @return
     */
    List<ChapterLessonBo> findChapterList(Long courseId) ;

    /**
     * 查询章节管理列表信息
     * @method: findChapterLessonList
     * @result : List<ChapterLessonBo>
     * 创建人:yykk
     * 创建时间：2022-07-25 20:22:31
     * @version 1.0.0
     * @return
     */
    List<ChapterLessonBo> findLessonList(Long chapterId) ;


    /**
     * 保存&修改章节管理
     * 方法名：saveupdateChapterLesson<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-25 20:22:31<br/>
     * @param chapterlesson 
     * @return ChapterLesson<br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    ChapterLessonBo saveupdateChapterLesson(ChapterLesson chapterlesson);

    /**
     * 根据Id删除章节管理
     * 方法名：deleteChapterLessonById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-25 20:22:31<br/>
     * @param id
     * @return int <br />
     * @throws <br/>
     * @since 1.0.0<br />
     */
    int deleteChapterLessonById(Long id) ;

    /**
     * 根据Id查询章节管理明细信息
     * 方法名：getChapterLessonById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-25 20:22:31<br/>
     * @param id
     * @return ChapterLesson <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    ChapterLessonBo getChapterLessonById(Long id);


    /**
     * 物理删除章节信息
     * @param id
     * @return
     */
    int removeChapterLessonById(Long id);

}