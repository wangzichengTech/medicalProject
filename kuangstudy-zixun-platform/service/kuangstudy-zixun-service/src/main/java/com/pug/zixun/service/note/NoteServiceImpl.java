package com.pug.zixun.service.note;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pug.zixun.mapper.NoteMapper;
import com.pug.zixun.pojo.Note;
import com.pug.zixun.vo.NoteVo;
import com.pug.zixun.bo.NoteBo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;



/**
 * NoteServiceImpl实现类
 * 创建人:yykk<br/>
 * 时间：2022-07-30 21:42:35 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Service
@Slf4j
public class NoteServiceImpl extends ServiceImpl<NoteMapper,Note> implements INoteService  {

    /**
     * 查询分页&搜索笔记专栏管理
     * @param noteVo
     * @return IPage<Note>
     * 创建人:yykk
     * 创建时间：2022-07-30 21:42:35
     * @version 1.0.0
     */
    @Override
	public IPage<NoteBo> findNotePage(NoteVo noteVo){
	    // 设置分页信息
		Page<Note> page = new Page<>(noteVo.getPageNo(),noteVo.getPageSize());
		// 设置查询条件
        LambdaQueryWrapper<Note> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // lambdaQueryWrapper.select(Note.class, column -> !column.getColumn().equals("description"));
        // 根据关键词搜索信息
        lambdaQueryWrapper.like(Vsserts.isNotEmpty(noteVo.getKeyword()), Note::getTitle,noteVo.getKeyword());
        // 根据分类查询
        lambdaQueryWrapper.eq(Vsserts.isNotNull(noteVo.getCategoryId()), Note::getCategoryid,noteVo.getCategoryId());
        // 根据关键词搜索信息
        lambdaQueryWrapper.like(Vsserts.isNotEmpty(noteVo.getKeyword()), Note::getCategorytitle,noteVo.getKeyword());
        // 根据关键词搜索信息
        lambdaQueryWrapper.like(Vsserts.isNotEmpty(noteVo.getKeyword()), Note::getNickname,noteVo.getKeyword());
         //查询发布的 0 未发布  1 发布
        lambdaQueryWrapper.eq(noteVo.getStatus() != null ,Note::getStatus,noteVo.getStatus());
        // 多列搜索
        // lambdaQueryWrapper.and(Vsserts.isNotEmpty(noteVo.getKeyword()),wrapper -> wrapper
        //         .like(Note::getName, noteVo.getKeyword())
        //         .or()
        //         .like(Note::getCategoryName, noteVo.getKeyword())
        // );
        // 根据时间排降序
        lambdaQueryWrapper.orderByDesc(Note::getCreateTime);
        // 查询分页返回
		IPage<Note> results = this.page(page,lambdaQueryWrapper);
		return tranferPageBo(results,NoteBo.class);
	}

    /**
     * 查询笔记专栏管理列表信息
     * @method: findNoteList
     * @result : List<Note>
     * 创建人:yykk
     * 创建时间：2022-07-30 21:42:35
     * @version 1.0.0
     * @return
    */
    @Override
    public List<NoteBo> findNoteList() {
     	// 1：设置查询条件
        LambdaQueryWrapper<Note> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // 2：查询发布的 0 未发布  1 发布
        lambdaQueryWrapper.eq(Note::getStatus,1);
        lambdaQueryWrapper.eq(Note::getIsdelete,0);
        // 3: 查询返回
        return tranferListBo(this.list(lambdaQueryWrapper),NoteBo.class);
    }

	/**
     * 根据id查询笔记专栏管理明细信息
     * @param id
     * @return Note
     * 创建人:yykk
     * 创建时间：2022-07-30 21:42:35
     * @version 1.0.0
     */
    @Override
    public NoteBo getNoteById(Long id) {
        return tranferBo(this.getById(id),NoteBo.class);
    }


    /**
     * 保存&修改笔记专栏管理
     * @param note
     * @return Note
     * 创建人:yykk
     * 创建时间：2022-07-30 21:42:35
     * @version 1.0.0
     */
    @Override
	public NoteBo saveupdateNote(Note note){
		boolean flag = this.saveOrUpdate(note);
		return flag ? tranferBo(note,NoteBo.class)  : null;
	}


    /**
     * 根据id删除笔记专栏管理
     * @param id
     * @return int
     * 创建人:yykk
     * 创建时间：2022-07-30 21:42:35
     * @version 1.0.0
     */
    @Override
    public int deleteNoteById(Long id) {
        Note adminRole = this.getById(id);
        if (adminRole != null) {
            Note note2 = new Note();
            note2.setId(id);
            note2.setIsdelete(1);
            boolean b = this.updateById(note2);
            return 1;
        }
        return 0;
    }

    /**
     * 根据id删除
     * @param ids
     * @return boolean
     * 创建人:yykk
     * 创建时间：2022-07-30 21:42:35
     * @version 1.0.0
     */
    @Override
    public boolean delBatchNote(String ids) {
        try {
            // 1 : 把数据分割
            String[] strings = ids.split(",");
            // 2: 组装成一个List<Note>
            List<Note> noteList = Arrays.stream(strings).map(idstr -> {
                Note note = new Note();
                note.setId(new Long(idstr));
                note.setIsdelete(1);
                return note;
            }).collect(Collectors.toList());
            // 3: 批量删除
            return this.updateBatchById(noteList);
        } catch (Exception ex) {
            throw new PugValidatorException(ResultStatusEnum.SERVER_DB_ERROR);
        }
    }


}