package com.pug.zixun.service.resources;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pug.zixun.bo.ResourcesBo;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;
import com.pug.zixun.mapper.ResourcesMapper;
import com.pug.zixun.pojo.Resources;
import com.pug.zixun.vo.ResourcesVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


/**
 * ResourcesServiceImpl实现类
 * 创建人:yykk<br/>
 * 时间：2022-07-19 23:58:26 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 *
 * @version 1.0.0<br />
 */
@Service
@Slf4j
public class ResourcesServiceImpl extends ServiceImpl<ResourcesMapper, Resources> implements IResourcesService {

    /**
     * 查询分页&搜索资源库
     *
     * @param resourcesVo
     * @return IPage<Resources>
     * 创建人:yykk
     * 创建时间：2022-07-19 23:58:26
     * @version 1.0.0
     */
    @Override
    public IPage<ResourcesBo> findResourcesPage(ResourcesVo resourcesVo) {
        // 设置分页信息
        Page<Resources> page = new Page<>(resourcesVo.getPageNo(), resourcesVo.getPageSize());
        // 设置查询条件
        LambdaQueryWrapper<Resources> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // 根据关键词搜索信息
        lambdaQueryWrapper.like(Vsserts.isNotEmpty(resourcesVo.getKeyword()), Resources::getFilename, resourcesVo.getKeyword());
        // 根据分类id查询对应的资源库信息
        lambdaQueryWrapper.eq(Vsserts.isNotNull(resourcesVo.getCategoryId()), Resources::getResourceId, resourcesVo.getCategoryId());
        //查询出未删除
        lambdaQueryWrapper.eq( Resources::getIsdelete,0);
        // 根据时间排降序
        lambdaQueryWrapper.orderByDesc(Resources::getCreateTime);
        // 查询分页返回
        IPage<Resources> results = this.page(page, lambdaQueryWrapper);
        return tranferPageBo(results, ResourcesBo.class);
    }

    /**
     * 查询资源库列表信息
     *
     * @return
     * @method: findResourcesList
     * @result : List<Resources>
     * 创建人:yykk
     * 创建时间：2022-07-19 23:58:26
     * @version 1.0.0
     */
    @Override
    public List<ResourcesBo> findResourcesList() {
        // 1：设置查询条件
        LambdaQueryWrapper<Resources> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // 2：查询发布的 0 未发布  1 发布
        lambdaQueryWrapper.eq(Resources::getStatus, 1);
        lambdaQueryWrapper.eq(Resources::getIsdelete, 0);
        // 3: 查询返回
        return tranferListBo(this.list(lambdaQueryWrapper), ResourcesBo.class);
    }

    /**
     * 根据id查询资源库明细信息
     *
     * @param id
     * @return Resources
     * 创建人:yykk
     * 创建时间：2022-07-19 23:58:26
     * @version 1.0.0
     */
    @Override
    public ResourcesBo getResourcesById(Long id) {
        return tranferBo(this.getById(id), ResourcesBo.class);
    }


    /**
     * 保存&修改资源库
     *
     * @param resources
     * @return Resources
     * 创建人:yykk
     * 创建时间：2022-07-19 23:58:26
     * @version 1.0.0
     */
    @Override
    public ResourcesBo saveupdateResources(Resources resources) {
        boolean flag = this.saveOrUpdate(resources);
        return flag ? tranferBo(resources, ResourcesBo.class) : null;
    }


    /**
     * 根据id删除资源库
     *
     * @param id
     * @return int
     * 创建人:yykk
     * 创建时间：2022-07-19 23:58:26
     * @version 1.0.0
     */
    @Override
    public int deleteResourcesById(Long id) {
        Resources adminRole = this.getById(id);
        if (adminRole != null) {
            Resources resources2 = new Resources();
            resources2.setId(id);
            resources2.setIsdelete(1);
            boolean b = this.updateById(resources2);
            return 1;
        }
        return 0;
    }

    /**
     * 根据id删除
     *
     * @param ids
     * @return boolean
     * 创建人:yykk
     * 创建时间：2022-07-19 23:58:26
     * @version 1.0.0
     */
    @Override
    public boolean delBatchResources(String ids) {
        try {
            // 1 : 把数据分割
            String[] strings = ids.split(",");
            // 2: 组装成一个List<Resources>
            List<Resources> resourcesList = Arrays.stream(strings).map(idstr -> {
                Resources resources = new Resources();
                resources.setId(new Long(idstr));
                resources.setIsdelete(1);
                return resources;
            }).collect(Collectors.toList());
            // 3: 批量删除
            return this.updateBatchById(resourcesList);
        } catch (Exception ex) {
            throw new PugValidatorException(ResultStatusEnum.SERVER_DB_ERROR);
        }
    }


}