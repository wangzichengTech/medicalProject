package com.pug.zixun.service.sepsispatients;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pug.zixun.mapper.SepsisPatientsMapper;
import com.pug.zixun.pojo.Ecgpatients;
import com.pug.zixun.pojo.SepsisPatients;
import com.pug.zixun.vo.SepsisPatientsVo;
import com.pug.zixun.bo.SepsisPatientsBo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;



/**
 * SepsisPatientsServiceImpl实现类
 * 创建人:yykk<br/>
 * 时间：2022-11-14 19:09:44 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Service
@Slf4j
public class SepsisPatientsServiceImpl extends ServiceImpl<SepsisPatientsMapper,SepsisPatients> implements ISepsisPatientsService  {

    /**
     * 查询分页&搜索脓毒症信息管理
     * @param sepsispatientsVo
     * @return IPage<SepsisPatients>
     * 创建人:yykk
     * 创建时间：2022-11-14 19:09:44
     * @version 1.0.0
     */
    @Resource
    private ISepsisPatientsService sepsisPatientsService;
    @Override
	public IPage<SepsisPatientsBo> findSepsisPatientsPage(SepsisPatientsVo sepsispatientsVo){
	    // 设置分页信息
		Page<SepsisPatients> page = new Page<>(sepsispatientsVo.getPageNo(),sepsispatientsVo.getPageSize());
		// 设置查询条件
        LambdaQueryWrapper<SepsisPatients> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // lambdaQueryWrapper.select(SepsisPatients.class, column -> !column.getColumn().equals("description"));
        // 根据关键词搜索信息
        lambdaQueryWrapper.like(Vsserts.isNotEmpty(sepsispatientsVo.getKeyword()), SepsisPatients::getName,sepsispatientsVo.getKeyword());
        lambdaQueryWrapper.eq(sepsispatientsVo.getIsDelete() != null , SepsisPatients::getIsdelete,0);
        // 多列搜索
        // lambdaQueryWrapper.and(Vsserts.isNotEmpty(sepsispatientsVo.getKeyword()),wrapper -> wrapper
        //         .like(SepsisPatients::getName, sepsispatientsVo.getKeyword())
        //         .or()
        //         .like(SepsisPatients::getCategoryName, sepsispatientsVo.getKeyword())
        // );
        // 根据时间排降序
        // 查询分页返回
		IPage<SepsisPatients> results = this.page(page,lambdaQueryWrapper);
		return tranferPageBo(results,SepsisPatientsBo.class);
	}
    public IPage<SepsisPatientsBo> findSepsisPatientsPage2(SepsisPatientsVo sepsispatientsVo){
        // 设置分页信息
        Page<SepsisPatients> page = new Page<>(sepsispatientsVo.getPageNo(),sepsispatientsVo.getPageSize());
        // 设置查询条件
        LambdaQueryWrapper<SepsisPatients> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // lambdaQueryWrapper.select(SepsisPatients.class, column -> !column.getColumn().equals("description"));
        // 根据关键词搜索信息
        lambdaQueryWrapper.like(Vsserts.isNotEmpty(sepsispatientsVo.getKeyword()), SepsisPatients::getName,sepsispatientsVo.getKeyword());
        lambdaQueryWrapper.eq(sepsispatientsVo.getIsDelete() != null , SepsisPatients::getIsdelete,1);
        // 多列搜索
        // lambdaQueryWrapper.and(Vsserts.isNotEmpty(sepsispatientsVo.getKeyword()),wrapper -> wrapper
        //         .like(SepsisPatients::getName, sepsispatientsVo.getKeyword())
        //         .or()
        //         .like(SepsisPatients::getCategoryName, sepsispatientsVo.getKeyword())
        // );
        // 根据时间排降序
        // 查询分页返回
        IPage<SepsisPatients> results = this.page(page,lambdaQueryWrapper);
        return tranferPageBo(results,SepsisPatientsBo.class);
    }

    /**
     * 查询脓毒症信息管理列表信息
     * @method: findSepsisPatientsList
     * @result : List<SepsisPatients>
     * 创建人:yykk
     * 创建时间：2022-11-14 19:09:44
     * @version 1.0.0
     * @return
    */
    @Override
    public List<SepsisPatientsBo> findSepsisPatientsList() {
     	// 1：设置查询条件
        LambdaQueryWrapper<SepsisPatients> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // 3: 查询返回
        return tranferListBo(this.list(lambdaQueryWrapper),SepsisPatientsBo.class);
    }

	/**
     * 根据id查询脓毒症信息管理明细信息
     * @param id
     * @return SepsisPatients
     * 创建人:yykk
     * 创建时间：2022-11-14 19:09:44
     * @version 1.0.0
     */
    @Override
    public SepsisPatientsBo getSepsisPatientsById(Integer id) {
        return tranferBo(this.getById(id),SepsisPatientsBo.class);
    }


    /**
     * 保存&修改脓毒症信息管理
     * @param sepsispatients
     * @return SepsisPatients
     * 创建人:yykk
     * 创建时间：2022-11-14 19:09:44
     * @version 1.0.0
     */
    @Override
	public SepsisPatientsBo saveupdateSepsisPatients(SepsisPatients sepsispatients){
		boolean flag = this.saveOrUpdate(sepsispatients);
		return flag ? tranferBo(sepsispatients,SepsisPatientsBo.class)  : null;
	}


    /**
     * 根据id删除脓毒症信息管理
     * @param id
     * @return int
     * 创建人:yykk
     * 创建时间：2022-11-14 19:09:44
     * @version 1.0.0
     */
    @Override
    public int deleteSepsisPatientsById(Integer id) {
        SepsisPatients adminRole = this.getById(id);
        if (adminRole != null) {
            SepsisPatients sepsispatients2 = new SepsisPatients();
            sepsispatients2.setId(id);
            boolean b = this.updateById(sepsispatients2);
            return 1;
        }
        return 0;
    }

    /**
     * 根据id删除
     * @param ids
     * @return boolean
     * 创建人:yykk
     * 创建时间：2022-11-14 19:09:44
     * @version 1.0.0
     */
    @Override
    public boolean delBatchSepsisPatients(String ids) {
        try {
            // 1 : 把数据分割
            String[] strings = ids.split(",");
            // 2: 组装成一个List<SepsisPatients>
            List<SepsisPatients> fileList = Arrays.stream(strings).map(idstr -> {
                SepsisPatients sepsisPatients = new SepsisPatients();
                sepsisPatients.setId(new Integer(idstr));
                sepsisPatients.setIsdelete(1);
                return sepsisPatients;
            }).collect(Collectors.toList());
            // 3: 批量删除
            return this.updateBatchById(fileList);
        } catch (Exception ex) {
            throw new PugValidatorException(ResultStatusEnum.SERVER_DB_ERROR);
        }
    }
    public boolean delBatchSepsisPatients2(String ids) {
        try {
            // 1 : 把数据分割
            String[] strings = ids.split(",");
            // 2: 组装成一个List<SepsisPatients>
            List<Integer> integers = Arrays.stream(strings).map(idstr -> {
                return new Integer(idstr);
            }).collect(Collectors.toList());
            // 3: 批量删除
            return sepsisPatientsService.removeByIds(integers);
        } catch (Exception ex) {
            throw new PugValidatorException(ResultStatusEnum.SERVER_DB_ERROR);
        }
    }


}