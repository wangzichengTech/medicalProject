package com.pug.zixun.service.adminrole;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pug.zixun.bo.AdminRoleBo;
import com.pug.zixun.commons.enums.AdminUserResultEnum;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugBusinessException;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;
import com.pug.zixun.mapper.AdminRoleMapper;
import com.pug.zixun.pojo.AdminRole;
import com.pug.zixun.vo.AdminRoleVo;
import com.pug.zixun.vo.StatusUpdateVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


/**
 * AdminRoleServiceImpl实现类
 * 创建人:yykk<br/>
 * 时间：2022-07-15 22:04:49 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 *
 * @version 1.0.0<br />
 */
@Service
@Slf4j
public class AdminRoleServiceImpl extends ServiceImpl<AdminRoleMapper, AdminRole> implements IAdminRoleService {

    /**
     * 查询分页&搜索后台角色管理
     *
     * @param adminroleVo
     * @return IPage<AdminRole>
     * 创建人:yykk
     * 创建时间：2022-07-15 22:04:49
     * @version 1.0.0
     */
    @Override
    public IPage<AdminRoleBo> findAdminRolePage(AdminRoleVo adminroleVo) {
        // 设置分页信息
        Page<AdminRole> page = new Page<>(adminroleVo.getPageNo(), adminroleVo.getPageSize());
        // 设置查询条件
        LambdaQueryWrapper<AdminRole> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // lambdaQueryWrapper.select(AdminRole.class, column -> !column.getColumn().equals("description"));
        //查询发布的 0 未发布  1 发布
        lambdaQueryWrapper.eq(adminroleVo.getStatus() != null, AdminRole::getStatus, adminroleVo.getStatus());
        // 多列搜索
        lambdaQueryWrapper.like(Vsserts.isNotEmpty(adminroleVo.getKeyword()), AdminRole::getRoleName, adminroleVo.getKeyword());
        // 根据时间排降序
        lambdaQueryWrapper.orderByDesc(AdminRole::getCreateTime);
        // 查询分页返回
        IPage<AdminRole> results = this.page(page, lambdaQueryWrapper);
        return tranferPageBo(results, AdminRoleBo.class);
    }

    /**
     * 查询后台角色管理列表信息
     *
     * @return
     * @method: findAdminRoleList
     * @result : List<AdminRole>
     * 创建人:yykk
     * 创建时间：2022-07-15 22:04:49
     * @version 1.0.0
     */
    @Override
    public List<AdminRoleBo> findAdminRoleList() {
        // 1：设置查询条件
        LambdaQueryWrapper<AdminRole> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // 2：查询发布的 0 未发布  1 发布
        lambdaQueryWrapper.eq(AdminRole::getStatus, 1);
        lambdaQueryWrapper.eq(AdminRole::getIsdelete, 0);
        // 3: 查询返回
        return tranferListBo(this.list(lambdaQueryWrapper), AdminRoleBo.class);
    }

    /**
     * 根据id查询后台角色管理明细信息
     *
     * @param id
     * @return AdminRole
     * 创建人:yykk
     * 创建时间：2022-07-15 22:04:49
     * @version 1.0.0
     */
    @Override
    public AdminRoleBo getAdminRoleById(Long id) {
        return tranferBo(this.getById(id), AdminRoleBo.class);
    }


    /**
     * 保存&修改后台角色管理
     *
     * @param adminrole
     * @return AdminRole
     * 创建人:yykk
     * 创建时间：2022-07-15 22:04:49
     * @version 1.0.0
     */
    @Override
    public AdminRoleBo saveupdateAdminRole(AdminRole adminrole) {
        boolean flag = this.saveOrUpdate(adminrole);
        return flag ? tranferBo(adminrole, AdminRoleBo.class) : null;
    }


    /**
     * 根据id删除后台角色管理
     *
     * @param id
     * @return int
     * 创建人:yykk
     * 创建时间：2022-07-15 22:04:49
     * @version 1.0.0
     */
    @Override
    public int deleteAdminRoleById(Long id) {
        AdminRole adminRole = this.getById(id);
        if (adminRole != null) {
            AdminRole adminRole2 = new AdminRole();
            adminRole2.setId(id);
            adminRole2.setIsdelete(1);
            boolean b = this.updateById(adminRole2);
            return 1;
        }
        return 0;
    }

    /**
     * 根据id删除
     *
     * @param ids
     * @return boolean
     * 创建人:yykk
     * 创建时间：2022-07-15 22:04:49
     * @version 1.0.0
     */
    @Override
    public boolean delBatchAdminRole(String ids) {
        try {
            // 1 : 把数据分割
            String[] strings = ids.split(",");
            // 2: 组装成一个List<AdminRole>
            List<AdminRole> adminroleList = Arrays.stream(strings).map(idstr -> {
                AdminRole adminrole = new AdminRole();
                adminrole.setId(new Long(idstr));
                adminrole.setIsdelete(1);
                return adminrole;
            }).collect(Collectors.toList());
            // 3: 批量删除
            return this.updateBatchById(adminroleList);
        } catch (Exception ex) {
            throw new PugValidatorException(ResultStatusEnum.SERVER_DB_ERROR);
        }
    }


    /**
     * 状态修改
     *
     * @param statusUpdateVo
     * @return
     */
    @Override
    public boolean updateAdminRole(StatusUpdateVo statusUpdateVo) {
        // 保存数据转换成DO
        AdminRole adminRole = tranferBo(statusUpdateVo, AdminRole.class);
        return updateById(adminRole);
    }


    /**
     * 查询角色对用的权限
     *
     * @param roleId
     * @return
     */
    @Override
    public List<String> findPermissionByRoleId(Long roleId) {
        return this.baseMapper.findPermissionByRoleId(roleId);
    }


    /**
     * 给角色授权
     *
     * @param roleId
     * @param permissionIds
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean savePermissionByRoleId(Long roleId, String permissionIds) {
        // 1: 删除角色对应的权限
        this.baseMapper.deletePermissionByRoleId(roleId);
        // 2：授予权限
        if (Vsserts.isNotEmpty(permissionIds)) {
            String[] permissonIdStrs = permissionIds.split(",");
            for (String permissonId : permissonIdStrs) {
                this.baseMapper.savePermissionByRoleId(roleId, new Long(permissonId));
            }
            return true;
        } else {
            throw new PugBusinessException(AdminUserResultEnum.USER_ROLE_AUTH_ERROR);
        }
    }

}