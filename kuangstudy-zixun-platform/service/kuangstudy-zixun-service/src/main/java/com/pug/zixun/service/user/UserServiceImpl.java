package com.pug.zixun.service.user;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pug.zixun.bo.UserBo;
import com.pug.zixun.mapper.UserMapper;
import com.pug.zixun.pojo.User;
import com.pug.zixun.service.BaseService;
import com.pug.zixun.vo.UserVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 飞哥B站地址：https://space.bilibili.com/490711252
 * 记得关注和三连哦！
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2022/5/13$ 21:29$
 */

@Service
@Slf4j
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService, BaseService {


    @Override
    public UserBo saveUser(UserVo userVo) {
        User user = tranferBo(userVo, User.class);
        // 把vo的数据放入到pojo
        // 保存数据
        this.saveOrUpdate(user);
        // 把vo的数据放入到pojo
        return tranferBo(user, UserBo.class);
    }


    @Override
    public List<UserBo> findUserList(UserVo userVo) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id", userVo.getId());
        List<User> userList = this.list(queryWrapper);
        return tranferListBo(userList, UserBo.class);
    }


    @Override
    public IPage<UserBo> pageUser(UserVo userVo) {
        // 设置分页
        Page<User> page = new Page<>(userVo.getPageNo(), userVo.getPageSize());
        // 设置条件
        LambdaQueryWrapper<User> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.like(StringUtils.isNotEmpty(userVo.getUsername()), User::getUsername, userVo.getUsername());
        // 分页反射
        IPage<User> userIPage = this.page(page, lambdaQueryWrapper);

        return tranferPageBo(userIPage, UserBo.class);
    }



}

