package com.pug.zixun.service.course;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pug.zixun.bo.CourseBo;
import com.pug.zixun.pojo.Course;
import com.pug.zixun.service.BaseService;
import com.pug.zixun.vo.CourseVo;

import java.util.List;

/**
 * ICourseService接口
 * 创建人:yykk<br/>
 * 时间：2022-07-25 20:21:39 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
public interface ICourseService extends IService<Course>,BaseService{


    /**
     * 查询课程列表信息
     * @method: findCourseList
     * @result : List<CourseBo>
     * 创建人:yykk
     * 创建时间：2022-07-25 20:21:39
     * @version 1.0.0
     * @return
     */
    List<CourseBo> findCourseList() ;

	/**
     * 查询课程列表信息并分页
     * 方法名：findCourses<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-25 20:21:39<br/>
     * @param courseVo
     * @return IPage<Course><br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
	IPage<CourseBo> findCoursePage(CourseVo courseVo);

    /**
     * 保存&修改课程
     * 方法名：saveupdateCourse<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-25 20:21:39<br/>
     * @param course 
     * @return Course<br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    CourseBo saveupdateCourse(Course course);

    /**
     * 根据Id删除课程
     * 方法名：deleteCourseById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-25 20:21:39<br/>
     * @param id
     * @return int <br />
     * @throws <br/>
     * @since 1.0.0<br />
     */
    int deleteCourseById(Long id) ;

    /**
     * 根据Id查询课程明细信息
     * 方法名：getCourseById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-25 20:21:39<br/>
     * @param id
     * @return Course <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    CourseBo getCourseById(Long id);

    /**
     * 根据课程ids批量删除课程
     * 方法名：delBatchCourse<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-25 20:21:39<br/>
     * @param ids
     * @return boolean <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    boolean delBatchCourse(String ids);

}