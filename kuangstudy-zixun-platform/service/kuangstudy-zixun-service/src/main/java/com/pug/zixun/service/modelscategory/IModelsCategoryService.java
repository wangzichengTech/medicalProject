package com.pug.zixun.service.modelscategory;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pug.zixun.pojo.ModelsCategory;
import com.pug.zixun.vo.ModelsCategoryVo;
import com.pug.zixun.bo.ModelsCategoryBo;
import com.pug.zixun.service.BaseService;
import java.util.List;

/**
 * IModelsCategoryService接口
 * 创建人:yykk<br/>
 * 时间：2022-12-15 20:23:14 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
public interface IModelsCategoryService extends IService<ModelsCategory>,BaseService{


    /**
     * 查询模型分类管理列表信息
     * @method: findModelsCategoryList
     * @result : List<ModelsCategoryBo>
     * 创建人:yykk
     * 创建时间：2022-12-15 20:23:14
     * @version 1.0.0
     * @return
     */
    List<ModelsCategoryBo> findModelsCategoryList() ;

	/**
     * 查询模型分类管理列表信息并分页
     * 方法名：findModelsCategorys<br/>
     * 创建人：yykk <br/>
     * 时间：2022-12-15 20:23:14<br/>
     * @param modelscategoryVo
     * @return IPage<ModelsCategory><br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
	IPage<ModelsCategoryBo> findModelsCategoryPage(ModelsCategoryVo modelscategoryVo);

    /**
     * 保存&修改模型分类管理
     * 方法名：saveupdateModelsCategory<br/>
     * 创建人：yykk <br/>
     * 时间：2022-12-15 20:23:14<br/>
     * @param modelscategory 
     * @return ModelsCategory<br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    ModelsCategoryBo saveupdateModelsCategory(ModelsCategory modelscategory);

    /**
     * 根据Id删除模型分类管理
     * 方法名：deleteModelsCategoryById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-12-15 20:23:14<br/>
     * @param id
     * @return int <br />
     * @throws <br/>
     * @since 1.0.0<br />
     */
    int deleteModelsCategoryById(Long id) ;

    /**
     * 根据Id查询模型分类管理明细信息
     * 方法名：getModelsCategoryById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-12-15 20:23:14<br/>
     * @param id
     * @return ModelsCategory <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    ModelsCategoryBo getModelsCategoryById(Long id);

    /**
     * 根据模型分类管理ids批量删除模型分类管理
     * 方法名：delBatchModelsCategory<br/>
     * 创建人：yykk <br/>
     * 时间：2022-12-15 20:23:14<br/>
     * @param ids
     * @return boolean <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    boolean delBatchModelsCategory(String ids);

}