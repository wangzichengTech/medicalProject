package com.pug.zixun.service.questions;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pug.zixun.mapper.QuestionsMapper;
import com.pug.zixun.pojo.Questions;
import com.pug.zixun.vo.QuestionsVo;
import com.pug.zixun.bo.QuestionsBo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;



/**
 * QuestionsServiceImpl实现类
 * 创建人:yykk<br/>
 * 时间：2022-07-30 23:58:45 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Service
@Slf4j
public class QuestionsServiceImpl extends ServiceImpl<QuestionsMapper,Questions> implements IQuestionsService  {

    /**
     * 查询分页&搜索文章管理
     * @param questionsVo
     * @return IPage<Questions>
     * 创建人:yykk
     * 创建时间：2022-07-30 23:58:45
     * @version 1.0.0
     */
    @Override
	public IPage<QuestionsBo> findQuestionsPage(QuestionsVo questionsVo){
	    // 设置分页信息
		Page<Questions> page = new Page<>(questionsVo.getPageNo(),questionsVo.getPageSize());
		// 设置查询条件
        LambdaQueryWrapper<Questions> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // lambdaQueryWrapper.select(Questions.class, column -> !column.getColumn().equals("description"));
        // 根据关键词搜索信息
        lambdaQueryWrapper.like(Vsserts.isNotEmpty(questionsVo.getKeyword()), Questions::getTitle,questionsVo.getKeyword());
        // 根据分类查询
        lambdaQueryWrapper.eq(Vsserts.isNotNull(questionsVo.getCategoryId()), Questions::getCategoryid,questionsVo.getCategoryId());
        // 根据关键词搜索信息
        lambdaQueryWrapper.like(Vsserts.isNotEmpty(questionsVo.getKeyword()), Questions::getCategorytitle,questionsVo.getKeyword());
        // 根据关键词搜索信息
        lambdaQueryWrapper.like(Vsserts.isNotEmpty(questionsVo.getKeyword()), Questions::getNickname,questionsVo.getKeyword());
         //查询发布的 0 未发布  1 发布
        lambdaQueryWrapper.eq(questionsVo.getStatus() != null ,Questions::getStatus,questionsVo.getStatus());
        // 根据分类查询
        lambdaQueryWrapper.eq(Vsserts.isNotNull(questionsVo.getCategoryId()), Questions::getCategorycid,questionsVo.getCategoryId());
        // 根据关键词搜索信息
        lambdaQueryWrapper.like(Vsserts.isNotEmpty(questionsVo.getKeyword()), Questions::getCategoryctitle,questionsVo.getKeyword());
        // 多列搜索
        // lambdaQueryWrapper.and(Vsserts.isNotEmpty(questionsVo.getKeyword()),wrapper -> wrapper
        //         .like(Questions::getName, questionsVo.getKeyword())
        //         .or()
        //         .like(Questions::getCategoryName, questionsVo.getKeyword())
        // );
        // 根据时间排降序
        lambdaQueryWrapper.orderByDesc(Questions::getCreateTime);
        // 查询分页返回
		IPage<Questions> results = this.page(page,lambdaQueryWrapper);
		return tranferPageBo(results,QuestionsBo.class);
	}

    /**
     * 查询文章管理列表信息
     * @method: findQuestionsList
     * @result : List<Questions>
     * 创建人:yykk
     * 创建时间：2022-07-30 23:58:45
     * @version 1.0.0
     * @return
    */
    @Override
    public List<QuestionsBo> findQuestionsList() {
     	// 1：设置查询条件
        LambdaQueryWrapper<Questions> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // 2：查询发布的 0 未发布  1 发布
        lambdaQueryWrapper.eq(Questions::getStatus,1);
        lambdaQueryWrapper.eq(Questions::getIsdelete,0);
        // 3: 查询返回
        return tranferListBo(this.list(lambdaQueryWrapper),QuestionsBo.class);
    }

	/**
     * 根据id查询文章管理明细信息
     * @param id
     * @return Questions
     * 创建人:yykk
     * 创建时间：2022-07-30 23:58:45
     * @version 1.0.0
     */
    @Override
    public QuestionsBo getQuestionsById(Long id) {
        return tranferBo(this.getById(id),QuestionsBo.class);
    }


    /**
     * 保存&修改文章管理
     * @param questions
     * @return Questions
     * 创建人:yykk
     * 创建时间：2022-07-30 23:58:45
     * @version 1.0.0
     */
    @Override
	public QuestionsBo saveupdateQuestions(Questions questions){
		boolean flag = this.saveOrUpdate(questions);
		return flag ? tranferBo(questions,QuestionsBo.class)  : null;
	}


    /**
     * 根据id删除文章管理
     * @param id
     * @return int
     * 创建人:yykk
     * 创建时间：2022-07-30 23:58:45
     * @version 1.0.0
     */
    @Override
    public int deleteQuestionsById(Long id) {
        Questions adminRole = this.getById(id);
        if (adminRole != null) {
            Questions questions2 = new Questions();
            questions2.setId(id);
            questions2.setIsdelete(1);
            boolean b = this.updateById(questions2);
            return 1;
        }
        return 0;
    }

    /**
     * 根据id删除
     * @param ids
     * @return boolean
     * 创建人:yykk
     * 创建时间：2022-07-30 23:58:45
     * @version 1.0.0
     */
    @Override
    public boolean delBatchQuestions(String ids) {
        try {
            // 1 : 把数据分割
            String[] strings = ids.split(",");
            // 2: 组装成一个List<Questions>
            List<Questions> questionsList = Arrays.stream(strings).map(idstr -> {
                Questions questions = new Questions();
                questions.setId(new Long(idstr));
                questions.setIsdelete(1);
                return questions;
            }).collect(Collectors.toList());
            // 3: 批量删除
            return this.updateBatchById(questionsList);
        } catch (Exception ex) {
            throw new PugValidatorException(ResultStatusEnum.SERVER_DB_ERROR);
        }
    }


}