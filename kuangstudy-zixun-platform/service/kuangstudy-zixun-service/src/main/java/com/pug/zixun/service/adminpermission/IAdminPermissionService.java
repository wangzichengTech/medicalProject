package com.pug.zixun.service.adminpermission;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pug.zixun.bo.AdminPermissionBo;
import com.pug.zixun.pojo.AdminPermission;
import com.pug.zixun.service.BaseService;
import com.pug.zixun.vo.AdminPermissionVo;

import java.util.List;

/**
 * IAdminPermissionService接口
 * 创建人:yykk<br/>
 * 时间：2022-07-17 18:38:05 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
public interface IAdminPermissionService extends IService<AdminPermission>,BaseService{

    /**
     * 查询菜单
     * @return
     */
    List<AdminPermission> findAdminPermissionTree();

    /**
     * 查询菜单
     * @return
     */
    List<AdminPermission> findAdminPermissionMenuTree();

    /**
     * 查询后台权限管理列表信息
     * @method: findAdminPermissionList
     * @result : List<AdminPermissionBo>
     * 创建人:yykk
     * 创建时间：2022-07-17 18:38:05
     * @version 1.0.0
     * @return
     */
    List<AdminPermissionBo> findAdminPermissionList() ;

	/**
     * 查询后台权限管理列表信息并分页
     * 方法名：findAdminPermissions<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-17 18:38:05<br/>
     * @param adminpermissionVo
     * @return IPage<AdminPermission><br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
	IPage<AdminPermissionBo> findAdminPermissionPage(AdminPermissionVo adminpermissionVo);

    /**
     * 保存&修改后台权限管理
     * 方法名：saveupdateAdminPermission<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-17 18:38:05<br/>
     * @param adminpermission 
     * @return AdminPermission<br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    AdminPermissionBo saveupdateAdminPermission(AdminPermission adminpermission);

    /**
     * 根据Id删除后台权限管理
     * 方法名：deleteAdminPermissionById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-17 18:38:05<br/>
     * @param id
     * @return int <br />
     * @throws <br/>
     * @since 1.0.0<br />
     */
    int deleteAdminPermissionById(Long id) ;

    /**
     * 根据Id查询后台权限管理明细信息
     * 方法名：getAdminPermissionById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-17 18:38:05<br/>
     * @param id
     * @return AdminPermission <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    AdminPermissionBo getAdminPermissionById(Long id);

    /**
     * 根据后台权限管理ids批量删除后台权限管理
     * 方法名：delBatchAdminPermission<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-17 18:38:05<br/>
     * @param ids
     * @return boolean <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    boolean delBatchAdminPermission(String ids);

}