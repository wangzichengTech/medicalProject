package com.pug.zixun.service.modelscategory;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pug.zixun.mapper.ModelsCategoryMapper;
import com.pug.zixun.pojo.ModelsCategory;
import com.pug.zixun.vo.ModelsCategoryVo;
import com.pug.zixun.bo.ModelsCategoryBo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;



/**
 * ModelsCategoryServiceImpl实现类
 * 创建人:yykk<br/>
 * 时间：2022-12-15 20:23:14 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Service
@Slf4j
public class ModelsCategoryServiceImpl extends ServiceImpl<ModelsCategoryMapper,ModelsCategory> implements IModelsCategoryService  {

    /**
     * 查询分页&搜索模型分类管理
     * @param modelscategoryVo
     * @return IPage<ModelsCategory>
     * 创建人:yykk
     * 创建时间：2022-12-15 20:23:14
     * @version 1.0.0
     */
    @Override
	public IPage<ModelsCategoryBo> findModelsCategoryPage(ModelsCategoryVo modelscategoryVo){
	    // 设置分页信息
		Page<ModelsCategory> page = new Page<>(modelscategoryVo.getPageNo(),modelscategoryVo.getPageSize());
		// 设置查询条件
        LambdaQueryWrapper<ModelsCategory> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // lambdaQueryWrapper.select(ModelsCategory.class, column -> !column.getColumn().equals("description"));
        // 根据关键词搜索信息
        lambdaQueryWrapper.like(Vsserts.isNotEmpty(modelscategoryVo.getKeyword()), ModelsCategory::getTitle,modelscategoryVo.getKeyword());
         //查询发布的 0 未发布  1 发布
        lambdaQueryWrapper.eq(modelscategoryVo.getStatus() != null ,ModelsCategory::getStatus,modelscategoryVo.getStatus());
        // 多列搜索
        // lambdaQueryWrapper.and(Vsserts.isNotEmpty(modelscategoryVo.getKeyword()),wrapper -> wrapper
        //         .like(ModelsCategory::getName, modelscategoryVo.getKeyword())
        //         .or()
        //         .like(ModelsCategory::getCategoryName, modelscategoryVo.getKeyword())
        // );
        // 根据时间排降序
        lambdaQueryWrapper.orderByDesc(ModelsCategory::getCreateTime);
        // 查询分页返回
		IPage<ModelsCategory> results = this.page(page,lambdaQueryWrapper);
		return tranferPageBo(results,ModelsCategoryBo.class);
	}

    /**
     * 查询模型分类管理列表信息
     * @method: findModelsCategoryList
     * @result : List<ModelsCategory>
     * 创建人:yykk
     * 创建时间：2022-12-15 20:23:14
     * @version 1.0.0
     * @return
    */
    @Override
    public List<ModelsCategoryBo> findModelsCategoryList() {
     	// 1：设置查询条件
        LambdaQueryWrapper<ModelsCategory> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // 2：查询发布的 0 未发布  1 发布
        lambdaQueryWrapper.eq(ModelsCategory::getStatus,1);
        lambdaQueryWrapper.eq(ModelsCategory::getIsdelete,0);
        // 3: 查询返回
        return tranferListBo(this.list(lambdaQueryWrapper),ModelsCategoryBo.class);
    }

	/**
     * 根据id查询模型分类管理明细信息
     * @param id
     * @return ModelsCategory
     * 创建人:yykk
     * 创建时间：2022-12-15 20:23:14
     * @version 1.0.0
     */
    @Override
    public ModelsCategoryBo getModelsCategoryById(Long id) {
        return tranferBo(this.getById(id),ModelsCategoryBo.class);
    }


    /**
     * 保存&修改模型分类管理
     * @param modelscategory
     * @return ModelsCategory
     * 创建人:yykk
     * 创建时间：2022-12-15 20:23:14
     * @version 1.0.0
     */
    @Override
	public ModelsCategoryBo saveupdateModelsCategory(ModelsCategory modelscategory){
		boolean flag = this.saveOrUpdate(modelscategory);
		return flag ? tranferBo(modelscategory,ModelsCategoryBo.class)  : null;
	}


    /**
     * 根据id删除模型分类管理
     * @param id
     * @return int
     * 创建人:yykk
     * 创建时间：2022-12-15 20:23:14
     * @version 1.0.0
     */
    @Override
    public int deleteModelsCategoryById(Long id) {
        ModelsCategory adminRole = this.getById(id);
        if (adminRole != null) {
            ModelsCategory modelscategory2 = new ModelsCategory();
            modelscategory2.setId(id);
            modelscategory2.setIsdelete(1);
            boolean b = this.updateById(modelscategory2);
            return 1;
        }
        return 0;
    }

    /**
     * 根据id删除
     * @param ids
     * @return boolean
     * 创建人:yykk
     * 创建时间：2022-12-15 20:23:14
     * @version 1.0.0
     */
    @Override
    public boolean delBatchModelsCategory(String ids) {
        try {
            // 1 : 把数据分割
            String[] strings = ids.split(",");
            // 2: 组装成一个List<ModelsCategory>
            List<ModelsCategory> modelscategoryList = Arrays.stream(strings).map(idstr -> {
                ModelsCategory modelscategory = new ModelsCategory();
                modelscategory.setId(new Long(idstr));
                modelscategory.setIsdelete(1);
                return modelscategory;
            }).collect(Collectors.toList());
            // 3: 批量删除
            return this.updateBatchById(modelscategoryList);
        } catch (Exception ex) {
            throw new PugValidatorException(ResultStatusEnum.SERVER_DB_ERROR);
        }
    }


}