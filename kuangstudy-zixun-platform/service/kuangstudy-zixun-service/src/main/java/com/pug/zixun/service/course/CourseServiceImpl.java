package com.pug.zixun.service.course;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pug.zixun.bo.CourseBo;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;
import com.pug.zixun.local.UserThreadLocal;
import com.pug.zixun.mapper.CourseMapper;
import com.pug.zixun.pojo.AdminUser;
import com.pug.zixun.pojo.Course;
import com.pug.zixun.vo.CourseVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;



/**
 * CourseServiceImpl实现类
 * 创建人:yykk<br/>
 * 时间：2022-07-25 20:21:39 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Service
@Slf4j
public class CourseServiceImpl extends ServiceImpl<CourseMapper,Course> implements ICourseService  {

    /**
     * 查询分页&搜索课程
     * @param courseVo
     * @return IPage<Course>
     * 创建人:yykk
     * 创建时间：2022-07-25 20:21:39
     * @version 1.0.0
     */
    @Override
	public IPage<CourseBo> findCoursePage(CourseVo courseVo){
	    // 设置分页信息
		Page<Course> page = new Page<>(courseVo.getPageNo(),courseVo.getPageSize());
		// 设置查询条件
        LambdaQueryWrapper<Course> lambdaQueryWrapper = new LambdaQueryWrapper<>();

        // 第一种做法: 按需加载，需要返回什么
//        lambdaQueryWrapper.select(Course::getId,
//                Course::getImg,
//                Course::getCategoryid )

        // 第二种做法：过滤掉不需要字段
        //性能查询隐患
        lambdaQueryWrapper.select(Course.class, column -> !column.getColumn().equals("content")
                && !column.getColumn().equals("htmlcontent"));

        // 第三种做法：数据表拆分，把大字段列从数据表分离，把主表-明细表（大字段）
        // 第三种做法：产品表 --- 产品主表（页面需要）----产品明细表（content,content_id,htmlcontent）----产品价格表(price,repricc,content_id)
         // 根据关键词搜索信息
        // 多列搜索 根据标题和标签和分类名进行模糊搜索
         lambdaQueryWrapper.and(Vsserts.isNotEmpty(courseVo.getKeyword()),wrapper -> wrapper
                 .like(Course::getTitle, courseVo.getKeyword())
                 .or()
                 .like(Course::getCategorytitle, courseVo.getKeyword())
                 .or()
                 .like(Course::getNickname, courseVo.getKeyword())
                 .or()
                 .like(Course::getTags, courseVo.getKeyword())
         );
        // 根据分类查询
        lambdaQueryWrapper.eq(Vsserts.isNotNull(courseVo.getCid()), Course::getCategoryid,courseVo.getCid());
        // 根据分类查询
        lambdaQueryWrapper.eq(Vsserts.isNotNull(courseVo.getCoursetype()), Course::getCoursetype,courseVo.getCoursetype());
         //查询发布的 0 未发布  1 发布
        lambdaQueryWrapper.eq(courseVo.getStatus() != null ,Course::getStatus,courseVo.getStatus());

        lambdaQueryWrapper.eq(Vsserts.isNotNull(courseVo.getIsDelete()) ,Course::getIsdelete,courseVo.getIsDelete());

        // 时间范围搜索
        if(Vsserts.isNotEmpty(courseVo.getStarttime()) && Vsserts.isNotEmpty(courseVo.getEndtime()) ){
            lambdaQueryWrapper.between(Course::getCreateTime,courseVo.getStarttime(),courseVo.getEndtime());
        }else if(Vsserts.isNotEmpty(courseVo.getStarttime()) && Vsserts.isEmpty(courseVo.getEndtime())){
            lambdaQueryWrapper.ge(Course::getCreateTime,courseVo.getStarttime());
        }else if(Vsserts.isEmpty(courseVo.getStarttime()) && Vsserts.isNotEmpty(courseVo.getEndtime())){
            lambdaQueryWrapper.le(Course::getCreateTime,courseVo.getEndtime());
        }
        // 根据时间排降序
        lambdaQueryWrapper.orderByDesc(Course::getCreateTime);
        // 查询分页返回
		IPage<Course> results = this.page(page,lambdaQueryWrapper);
		return tranferPageBo(results,CourseBo.class);
	}

    /**
     * 查询课程列表信息
     * @method: findCourseList
     * @result : List<Course>
     * 创建人:yykk
     * 创建时间：2022-07-25 20:21:39
     * @version 1.0.0
     * @return
    */
    @Override
    public List<CourseBo> findCourseList() {
     	// 1：设置查询条件
        LambdaQueryWrapper<Course> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // 第二种做法：过滤掉不需要字段
        //性能查询隐患
        lambdaQueryWrapper.select(Course.class, column -> !column.getColumn().equals("content")
                && !column.getColumn().equals("htmlcontent"));

        // 2：查询发布的 0 未发布  1 发布
        lambdaQueryWrapper.eq(Course::getStatus,1);
        lambdaQueryWrapper.eq(Course::getIsdelete,0);
        // 3: 查询返回
        return tranferListBo(this.list(lambdaQueryWrapper),CourseBo.class);
    }

	/**
     * 根据id查询课程明细信息
     * @param id
     * @return Course
     * 创建人:yykk
     * 创建时间：2022-07-25 20:21:39
     * @version 1.0.0
     */
    @Override
    public CourseBo getCourseById(Long id) {
        return tranferBo(this.getById(id),CourseBo.class);
    }


    /**
     * 保存&修改课程
     * @param course
     * @return Course
     * 创建人:yykk
     * 创建时间：2022-07-25 20:21:39
     * @version 1.0.0
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
	public CourseBo saveupdateCourse(Course course){
        AdminUser adminUser = UserThreadLocal.get();
        if(adminUser != null){
            course.setUserid(adminUser.getId());
            course.setNickname(adminUser.getUsername());
            course.setVip(1);
            course.setAvatar(adminUser.getAvatar());
        }
        boolean flag = this.saveOrUpdate(course);
		return flag ? tranferBo(course,CourseBo.class)  : null;
	}


    /**
     * 根据id删除课程
     * @param id
     * @return int
     * 创建人:yykk
     * 创建时间：2022-07-25 20:21:39
     * @version 1.0.0
     */
    @Override
    public int deleteCourseById(Long id) {
        Course adminRole = this.getById(id);
        if (adminRole != null) {
            Course course2 = new Course();
            course2.setId(id);
            course2.setIsdelete(1);
            boolean b = this.updateById(course2);
            return 1;
        }
        return 0;
    }

    /**
     * 根据id删除
     * @param ids
     * @return boolean
     * 创建人:yykk
     * 创建时间：2022-07-25 20:21:39
     * @version 1.0.0
     */
    @Override
    public boolean delBatchCourse(String ids) {
        try {
            // 1 : 把数据分割
            String[] strings = ids.split(",");
            // 2: 组装成一个List<Course>
            List<Course> courseList = Arrays.stream(strings).map(idstr -> {
                Course course = new Course();
                course.setId(new Long(idstr));
                course.setIsdelete(1);
                return course;
            }).collect(Collectors.toList());
            // 3: 批量删除
            return this.updateBatchById(courseList);
        } catch (Exception ex) {
            throw new PugValidatorException(ResultStatusEnum.SERVER_DB_ERROR);
        }
    }


}