package com.pug.zixun.service.category;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pug.zixun.bo.CategoryBo;
import com.pug.zixun.pojo.Category;
import com.pug.zixun.service.BaseService;
import com.pug.zixun.vo.CategoryVo;

import java.util.List;

/**
 * ICategoryService接口
 * 创建人:yykk<br/>
 * 时间：2022-05-23 14:38:43 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
public interface ICategoryService extends IService<Category>,BaseService{



    /**
     * 查询文章分类管理列表信息
     * @method: findCategoryList
     * @result : List<CategoryBo>
     * 创建人:yykk
     * 创建时间：2022-05-23 14:38:43
     * @version 1.0.0
     * @return
     */
    List<CategoryBo> findCategoryTree(String keyword) ;


    /**
     * 查询文章分类管理列表信息
     * @method: findCategoryList
     * @result : List<CategoryBo>
     * 创建人:yykk
     * 创建时间：2022-05-23 14:38:43
     * @version 1.0.0
     * @return
     */
    List<CategoryBo> findCategoryList() ;

	/**
     * 查询文章分类管理列表信息并分页
     * 方法名：findCategorys<br/>
     * 创建人：yykk <br/>
     * 时间：2022-05-23 14:38:43<br/>
     * @param categoryVo
     * @return IPage<Category><br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
	IPage<CategoryBo> findCategoryPage(CategoryVo categoryVo);

    /**
     * 保存&修改文章分类管理
     * 方法名：saveupdateCategory<br/>
     * 创建人：yykk <br/>
     * 时间：2022-05-23 14:38:43<br/>
     * @param category 
     * @return Category<br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    CategoryBo saveupdateCategory(Category category);

    /**
     * 根据Id删除文章分类管理
     * 方法名：deleteCategoryById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-05-23 14:38:43<br/>
     * @param id
     * @return int <br />
     * @throws <br/>
     * @since 1.0.0<br />
     */
    int deleteCategoryById(Long id) ;

    /**
     * 根据Id查询文章分类管理明细信息
     * 方法名：getCategoryById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-05-23 14:38:43<br/>
     * @param id
     * @return Category <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    CategoryBo getCategoryById(Long id);

    /**
     * 根据文章分类管理ids批量删除文章分类管理
     * 方法名：delBatchCategory<br/>
     * 创建人：yykk <br/>
     * 时间：2022-05-23 14:38:43<br/>
     * @param ids
     * @return boolean <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    boolean delBatchCategory(String ids);

}