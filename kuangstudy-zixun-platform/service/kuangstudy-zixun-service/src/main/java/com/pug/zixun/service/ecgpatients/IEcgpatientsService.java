package com.pug.zixun.service.ecgpatients;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pug.zixun.pojo.Ecgpatients;
import com.pug.zixun.vo.EcgpatientsVo;
import com.pug.zixun.bo.EcgpatientsBo;
import com.pug.zixun.service.BaseService;
import java.util.List;

/**
 * IEcgpatientsService接口
 * 创建人:yykk<br/>
 * 时间：2022-11-02 19:18:02 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
public interface IEcgpatientsService extends IService<Ecgpatients>,BaseService{


    /**
     * 查询脓毒症信息管理列表信息
     * @method: findEcgpatientsList
     * @result : List<EcgpatientsBo>
     * 创建人:yykk
     * 创建时间：2022-11-02 19:18:02
     * @version 1.0.0
     * @return
     */
    List<EcgpatientsBo> findEcgpatientsList() ;

	/**
     * 查询脓毒症信息管理列表信息并分页
     * 方法名：findEcgpatientss<br/>
     * 创建人：yykk <br/>
     * 时间：2022-11-02 19:18:02<br/>
     * @param ecgpatientsVo
     * @return IPage<Ecgpatients><br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
	IPage<EcgpatientsBo> findEcgpatientsPage(EcgpatientsVo ecgpatientsVo);
    IPage<EcgpatientsBo> findEcgpatientsPage2(EcgpatientsVo ecgpatientsVo);
    /**
     * 保存&修改脓毒症信息管理
     * 方法名：saveupdateEcgpatients<br/>
     * 创建人：yykk <br/>
     * 时间：2022-11-02 19:18:02<br/>
     * @param ecgpatients 
     * @return Ecgpatients<br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    EcgpatientsBo saveupdateEcgpatients(Ecgpatients ecgpatients);

    /**
     * 根据Id删除脓毒症信息管理
     * 方法名：deleteEcgpatientsById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-11-02 19:18:02<br/>
     * @param id
     * @return int <br />
     * @throws <br/>
     * @since 1.0.0<br />
     */

    /**
     * 根据Id查询脓毒症信息管理明细信息
     * 方法名：getEcgpatientsById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-11-02 19:18:02<br/>
     * @param id
     * @return Ecgpatients <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    EcgpatientsBo getEcgpatientsById(Integer id);

    /**
     * 根据脓毒症信息管理ids批量删除脓毒症信息管理
     * 方法名：delBatchEcgpatients<br/>
     * 创建人：yykk <br/>
     * 时间：2022-11-02 19:18:02<br/>
     * @param ids
     * @return boolean <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    boolean delBatchEcgpatients(String ids);
    boolean delBatchEcgpatients2(String ids);
}