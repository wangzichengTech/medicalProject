package com.pug.zixun.service.adminrole;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pug.zixun.bo.AdminRoleBo;
import com.pug.zixun.pojo.AdminRole;
import com.pug.zixun.service.BaseService;
import com.pug.zixun.vo.AdminRoleVo;
import com.pug.zixun.vo.StatusUpdateVo;

import java.util.List;

/**
 * IAdminRoleService接口
 * 创建人:yykk<br/>
 * 时间：2022-07-15 22:04:49 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
public interface IAdminRoleService extends IService<AdminRole>,BaseService{

    /**
     * 查询角色对用的权限
     *
     * @param roleId
     * @return
     */
    List<String> findPermissionByRoleId( Long roleId);


    /**
     * 给角色授权
     *
     * @param roleId
     * @param  permissionIds
     * @return
     */
    Boolean savePermissionByRoleId( Long roleId, String permissionIds);


    /**
     * 查询后台角色管理列表信息
     * @method: findAdminRoleList
     * @result : List<AdminRoleBo>
     * 创建人:yykk
     * 创建时间：2022-07-15 22:04:49
     * @version 1.0.0
     * @return
     */
    List<AdminRoleBo> findAdminRoleList() ;

	/**
     * 查询后台角色管理列表信息并分页
     * 方法名：findAdminRoles<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-15 22:04:49<br/>
     * @param adminroleVo
     * @return IPage<AdminRole><br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
	IPage<AdminRoleBo> findAdminRolePage(AdminRoleVo adminroleVo);

    /**
     * 保存&修改后台角色管理
     * 方法名：saveupdateAdminRole<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-15 22:04:49<br/>
     * @param adminrole 
     * @return AdminRole<br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    AdminRoleBo saveupdateAdminRole(AdminRole adminrole);

    /**
     * 根据Id删除后台角色管理
     * 方法名：deleteAdminRoleById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-15 22:04:49<br/>
     * @param id
     * @return int <br />
     * @throws <br/>
     * @since 1.0.0<br />
     */
    int deleteAdminRoleById(Long id) ;

    /**
     * 根据Id查询后台角色管理明细信息
     * 方法名：getAdminRoleById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-15 22:04:49<br/>
     * @param id
     * @return AdminRole <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    AdminRoleBo getAdminRoleById(Long id);

    /**
     * 根据后台角色管理ids批量删除后台角色管理
     * 方法名：delBatchAdminRole<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-15 22:04:49<br/>
     * @param ids
     * @return boolean <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    boolean delBatchAdminRole(String ids);

    /**
     * 状态修改
     * @param statusUpdateVo
     * @return
     */
    boolean updateAdminRole(StatusUpdateVo statusUpdateVo);
}