package com.pug.zixun.service.category;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pug.zixun.bo.CategoryBo;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;
import com.pug.zixun.mapper.CategoryMapper;
import com.pug.zixun.pojo.Category;
import com.pug.zixun.vo.CategoryVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;



/**
 * CategoryServiceImpl实现类
 * 创建人:yykk<br/>
 * 时间：2022-05-23 14:38:43 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Service
@Slf4j
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper,Category> implements ICategoryService  {

    /**
     * 查询文章分类管理列表信息
     * @method: findCategoryList
     * @result : List<CategoryBo>
     * 创建人:yykk
     * 创建时间：2022-05-23 14:38:43
     * @version 1.0.0
     * @return
     */
    @Override
    public List<CategoryBo> findCategoryTree(String keyword){
        // 1 :查询表中所有的数据
        LambdaQueryWrapper<Category> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.like(Vsserts.isNotEmpty(keyword),Category::getTitle,keyword);
        List<Category> allList = this.list(lambdaQueryWrapper); // 思考空间，为什么查询的是所有

        List<CategoryBo> categoryBos = tranferListBo(allList, CategoryBo.class);
        // 2: 找到所有的根节点 pid = 0
        List<CategoryBo> rootList = categoryBos.stream().filter(category -> category.getPid().equals(0L))
                .sorted((a, b) -> a.getSorted() - b.getSorted()).collect(Collectors.toList());
        // 3 : 查询所有的非根节点
        List<CategoryBo> subList = categoryBos.stream().filter(category -> !category.getPid().equals(0L)).collect(Collectors.toList());
        // 4 : 循环根节点去subList去找对应的子节点
        rootList.forEach(root -> buckForback(root, subList));

        return rootList;
    }

    private void buckForback(CategoryBo root, List<CategoryBo> subList) {
        // 通过根节点去id和子节点的pid是否相等，如果相等的话，代表是当前根的子集
        List<CategoryBo> childrenList = subList.stream().filter(category -> category.getPid().equals(root.getId()))
                .sorted((a, b) -> a.getSorted() - b.getSorted())
                .collect(Collectors.toList());
        // 如果你当前没一个子集，初始化一个空数组
        if (!CollectionUtils.isEmpty(childrenList)) {
            // 查询以后放回去
            root.setChildren(childrenList);
            // 再次递归构建即可
            childrenList.forEach(category -> buckForback(category, subList));
        } else {
            root.setChildren(new ArrayList<>());
        }
    }

    /**
     * 查询分页&搜索文章分类管理
     * @param categoryVo
     * @return IPage<Category>
     * 创建人:yykk
     * 创建时间：2022-05-23 14:38:43
     * @version 1.0.0
     */
    @Override
	public IPage<CategoryBo> findCategoryPage(CategoryVo categoryVo){
	    // 设置分页信息
		Page<Category> page = new Page<>(categoryVo.getPageNo(),categoryVo.getPageSize());
		// 设置查询条件
        LambdaQueryWrapper<Category> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // lambdaQueryWrapper.select(Category.class, column -> !column.getColumn().equals("description"));
        // 根据关键词搜索信息
        lambdaQueryWrapper.like(Vsserts.isNotEmpty(categoryVo.getKeyword()), Category::getTitle,categoryVo.getKeyword());
         //查询发布的 0 未发布  1 发布
        lambdaQueryWrapper.eq(categoryVo.getStatus() != null ,Category::getStatus,categoryVo.getStatus());
        // 多列搜索
        // lambdaQueryWrapper.and(Vsserts.isNotEmpty(categoryVo.getKeyword()),wrapper -> wrapper
        //         .like(Category::getName, categoryVo.getKeyword())
        //         .or()
        //         .like(Category::getCategoryName, categoryVo.getKeyword())
        // );
        // 根据时间排降序
        lambdaQueryWrapper.orderByDesc(Category::getCreateTime);
        // 查询分页返回
		IPage<Category> results = this.page(page,lambdaQueryWrapper);
		return tranferPageBo(results,CategoryBo.class);
	}

    /**
     * 查询文章分类管理列表信息
     * @method: findCategoryList
     * @result : List<Category>
     * 创建人:yykk
     * 创建时间：2022-05-23 14:38:43
     * @version 1.0.0
     * @return
    */
    @Override
    public List<CategoryBo> findCategoryList() {
     	// 1：设置查询条件
        LambdaQueryWrapper<Category> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // 2：查询发布的 0 未发布  1 发布
        lambdaQueryWrapper.eq(Category::getStatus,1);
        lambdaQueryWrapper.eq(Category::getIsdelete,0);
        // 3: 查询返回
        return tranferListBo(this.list(lambdaQueryWrapper),CategoryBo.class);
    }

	/**
     * 根据id查询文章分类管理明细信息
     * @param id
     * @return Category
     * 创建人:yykk
     * 创建时间：2022-05-23 14:38:43
     * @version 1.0.0
     */
    @Override
    public CategoryBo getCategoryById(Long id) {
        return tranferBo(this.getById(id),CategoryBo.class);
    }


    /**
     * 保存&修改文章分类管理
     * @param category
     * @return Category
     * 创建人:yykk
     * 创建时间：2022-05-23 14:38:43
     * @version 1.0.0
     */
    @Override
	public CategoryBo saveupdateCategory(Category category){
		boolean flag = this.saveOrUpdate(category);
		return flag ? tranferBo(category,CategoryBo.class)  : null;
	}



    /**
     * 根据id删除文章分类管理
     * @param id
     * @return int
     * 创建人:yykk
     * 创建时间：2022-05-23 14:38:43
     * @version 1.0.0
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteCategoryById(Long id) {
        // 物理删除removeByID
        //List<Category> categoryTreeByPid = new CategoryServiceImpl().findCategoryTreeByPid(id);
        //this.removeByIds(categoryTreeByPid);

        // 物理删除
        Category category = this.getById(id);
        if(category!=null) {
            Category newCategory = new Category();
            newCategory.setId(id);
            newCategory.setIsdelete(1);
            boolean b = this.updateById(newCategory);
            return 1;
        }
        return 0;
    }


    /**
     * 根据id找到所有孙子元素包含自己
     * 主要是为了进行批量删除
     * @method: findCategoryList
     * @result : List<CategoryBo>
     * 创建人:yykk
     * 创建时间：2022-05-23 14:38:43
     * @version 1.0.0
     * @return
     */
    public List<Category> findCategoryTreeByPid(Long pid){
        // 1 :查询表中所有的数据
        LambdaQueryWrapper<Category> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        List<Category> allList = this.list(lambdaQueryWrapper); // 思考空间，为什么查询的是所有
        List<Category> resultList = new ArrayList<>();
        // 2: 找到所有的根节点 pid = 0
        List<Category> rootList = allList.stream().filter(item->item.getId().equals(pid)).collect(Collectors.toList());
        // 4 : 循环根节点去subList去找对应的子节点
        rootList.forEach(root -> {
            resultList.add(root);
            buckForbackByPid(resultList,root, allList);
        });
        return resultList;
    }


    private void buckForbackByPid( List<Category> rootList , Category root, List<Category> subList) {
        // 通过根节点去id和子节点的pid是否相等，如果相等的话，代表是当前根的子集
        List<Category> childrenList = subList.stream().filter(category -> category.getPid().equals(root.getId()))
                .sorted((a, b) -> a.getSorted() - b.getSorted())
                .collect(Collectors.toList());
        // 如果你当前没一个子集，初始化一个空数组
        if (!CollectionUtils.isEmpty(childrenList)) {
            // 查询以后放回去
            rootList.addAll(childrenList);
            // 再次递归构建即可
            childrenList.forEach(category -> buckForbackByPid(rootList,category, subList));
        }
    }


    /**
     * 根据id删除
     * @param ids
     * @return boolean
     * 创建人:yykk
     * 创建时间：2022-05-23 14:38:43
     * @version 1.0.0
     */
    @Override
    public boolean delBatchCategory(String ids) {
        try {
            // 1 : 把数据分割
            String[] strings = ids.split(",");
            // 2: 组装成一个List<Category>
            List<Category> categoryList = Arrays.stream(strings).map(idstr -> {
                Category category = new Category();
                category.setId(new Long(idstr));
                category.setIsdelete(1);
                return category;
            }).collect(Collectors.toList());
            // 3: 批量删除
            return this.updateBatchById(categoryList);
        } catch (Exception ex) {
            throw new PugValidatorException(ResultStatusEnum.SERVER_DB_ERROR);
        }
    }


}