package com.pug.zixun.service.pay;

import com.pug.zixun.pojo.User;

import java.math.BigDecimal;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 飞哥B站地址：https://space.bilibili.com/490711252
 * 记得关注和三连哦！
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2022/5/13$ 23:18$
 */
public interface IPayService {


    default void aliyPay(PayVo payVo) {

    }

    default void weixin(PayVo payVo) {
    }


    static BigDecimal calcPrice(PayVo payVo, User user) {
        double discount = 1d;
        if (user.getVip().equals(2)) {
            discount = 0.8;
        } else if (user.getVip().equals(3)) {
            discount = 0.5;
        }
        double v = payVo.getPrice().doubleValue() * discount;
        return new BigDecimal(v);
    }
}
