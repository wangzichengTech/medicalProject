package com.pug.zixun.service.models;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pug.zixun.bo.CourseBo;
import com.pug.zixun.local.UserThreadLocal;
import com.pug.zixun.mapper.ModelsMapper;
import com.pug.zixun.pojo.AdminUser;
import com.pug.zixun.pojo.Course;
import com.pug.zixun.pojo.Models;
import com.pug.zixun.vo.ModelsVo;
import com.pug.zixun.bo.ModelsBo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;



/**
 * ModelsServiceImpl实现类
 * 创建人:yykk<br/>
 * 时间：2022-10-08 21:13:25 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Service
@Slf4j
public class ModelsServiceImpl extends ServiceImpl<ModelsMapper,Models> implements IModelsService  {

    /**
     * 查询分页&搜索模型管理
     * @param modelsVo
     * @return IPage<Models>
     * 创建人:yykk
     * 创建时间：2022-10-08 21:13:25
     * @version 1.0.0
     */
    @Override
	public IPage<ModelsBo> findModelsPage(ModelsVo modelsVo){
	    // 设置分页信息
		Page<Models> page = new Page<>(modelsVo.getPageNo(),modelsVo.getPageSize());
		// 设置查询条件
        LambdaQueryWrapper<Models> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // lambdaQueryWrapper.select(Models.class, column -> !column.getColumn().equals("description"));
        // 根据关键词搜索信息
        lambdaQueryWrapper.select(Models.class, column -> !column.getColumn().equals("content")
                && !column.getColumn().equals("htmlcontent"));
        // 多列搜索 根据标题和标签和分类名进行模糊搜索
        lambdaQueryWrapper.and(Vsserts.isNotEmpty(modelsVo.getKeyword()),wrapper -> wrapper
                .like(Models::getName, modelsVo.getKeyword())
                .or()
                .like(Models::getCategorytitle, modelsVo.getKeyword())
                .or()
                .like(Models::getNickname, modelsVo.getKeyword())
                .or()
                .like(Models::getTags, modelsVo.getKeyword())
        );
        // 根据分类查询
        lambdaQueryWrapper.eq(Vsserts.isNotNull(modelsVo.getCid()), Models::getCategoryid,modelsVo.getCid());

         //查询发布的 0 未发布  1 发布
        lambdaQueryWrapper.eq(modelsVo.getStatus() != null ,Models::getStatus,modelsVo.getStatus());
        lambdaQueryWrapper.eq(Vsserts.isNotNull(modelsVo.getIsDelete()) ,Models::getIsdelete,modelsVo.getIsDelete());
        // 多列搜索
        // lambdaQueryWrapper.and(Vsserts.isNotEmpty(modelsVo.getKeyword()),wrapper -> wrapper
        //         .like(Models::getName, modelsVo.getKeyword())
        //         .or()
        //         .like(Models::getCategoryName, modelsVo.getKeyword())
        // );
        // 时间范围搜索
        if(Vsserts.isNotEmpty(modelsVo.getStarttime()) && Vsserts.isNotEmpty(modelsVo.getEndtime()) ){
            lambdaQueryWrapper.between(Models::getCreateTime,modelsVo.getStarttime(),modelsVo.getEndtime());
        }else if(Vsserts.isNotEmpty(modelsVo.getStarttime()) && Vsserts.isEmpty(modelsVo.getEndtime())){
            lambdaQueryWrapper.ge(Models::getCreateTime,modelsVo.getStarttime());
        }else if(Vsserts.isEmpty(modelsVo.getStarttime()) && Vsserts.isNotEmpty(modelsVo.getEndtime())){
            lambdaQueryWrapper.le(Models::getCreateTime,modelsVo.getEndtime());
        }
        // 根据时间排降序
        lambdaQueryWrapper.orderByDesc(Models::getCreateTime);
        // 查询分页返回
		IPage<Models> results = this.page(page,lambdaQueryWrapper);
		return tranferPageBo(results,ModelsBo.class);
	}

    /**
     * 查询模型管理列表信息
     * @method: findModelsList
     * @result : List<Models>
     * 创建人:yykk
     * 创建时间：2022-10-08 21:13:25
     * @version 1.0.0
     * @return
    */
    @Override
    public List<ModelsBo> findModelsList() {
     	// 1：设置查询条件
        LambdaQueryWrapper<Models> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.select(Models.class, column -> !column.getColumn().equals("content")
                && !column.getColumn().equals("htmlcontent"));

        // 2：查询发布的 0 未发布  1 发布
        lambdaQueryWrapper.eq(Models::getStatus,1);
        lambdaQueryWrapper.eq(Models::getIsdelete,0);
        // 3: 查询返回
        return tranferListBo(this.list(lambdaQueryWrapper),ModelsBo.class);
    }

	/**
     * 根据id查询模型管理明细信息
     * @param id
     * @return Models
     * 创建人:yykk
     * 创建时间：2022-10-08 21:13:25
     * @version 1.0.0
     */
    @Override
    public ModelsBo getModelsById(Long id) {
        return tranferBo(this.getById(id),ModelsBo.class);
    }


    /**
     * 保存&修改模型管理
     * @param models
     * @return Models
     * 创建人:yykk
     * 创建时间：2022-10-08 21:13:25
     * @version 1.0.0
     */
    @Override
	public ModelsBo saveupdateModels(Models models){
        AdminUser adminUser = UserThreadLocal.get();
        if(adminUser != null){
            models.setUserid(adminUser.getId());
            models.setNickname(adminUser.getUsername());
            models.setAvatar(adminUser.getAvatar());
        }
        boolean flag = this.saveOrUpdate(models);
        return flag ? tranferBo(models, ModelsBo.class)  : null;
	}


    /**
     * 根据id删除模型管理
     * @param id
     * @return int
     * 创建人:yykk
     * 创建时间：2022-10-08 21:13:25
     * @version 1.0.0
     */
    @Override
    public int deleteModelsById(Long id) {
        Models adminRole = this.getById(id);
        if (adminRole != null) {
            Models models2 = new Models();
            models2.setId(id);
            models2.setIsdelete(1);
            boolean b = this.updateById(models2);
            return 1;
        }
        return 0;
    }

    /**
     * 根据id删除
     * @param ids
     * @return boolean
     * 创建人:yykk
     * 创建时间：2022-10-08 21:13:25
     * @version 1.0.0
     */
    @Override
    public boolean delBatchModels(String ids) {
        try {
            // 1 : 把数据分割
            String[] strings = ids.split(",");
            // 2: 组装成一个List<Models>
            List<Models> modelsList = Arrays.stream(strings).map(idstr -> {
                Models models = new Models();
                models.setId(new Long(idstr));
                models.setIsdelete(1);
                return models;
            }).collect(Collectors.toList());
            // 3: 批量删除
            return this.updateBatchById(modelsList);
        } catch (Exception ex) {
            throw new PugValidatorException(ResultStatusEnum.SERVER_DB_ERROR);
        }
    }


}