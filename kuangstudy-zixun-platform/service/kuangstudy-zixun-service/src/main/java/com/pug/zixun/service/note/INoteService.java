package com.pug.zixun.service.note;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pug.zixun.pojo.Note;
import com.pug.zixun.vo.NoteVo;
import com.pug.zixun.bo.NoteBo;
import com.pug.zixun.service.BaseService;
import java.util.List;

/**
 * INoteService接口
 * 创建人:yykk<br/>
 * 时间：2022-07-30 21:42:35 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
public interface INoteService extends IService<Note>,BaseService{


    /**
     * 查询笔记专栏管理列表信息
     * @method: findNoteList
     * @result : List<NoteBo>
     * 创建人:yykk
     * 创建时间：2022-07-30 21:42:35
     * @version 1.0.0
     * @return
     */
    List<NoteBo> findNoteList() ;

	/**
     * 查询笔记专栏管理列表信息并分页
     * 方法名：findNotes<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-30 21:42:35<br/>
     * @param noteVo
     * @return IPage<Note><br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
	IPage<NoteBo> findNotePage(NoteVo noteVo);

    /**
     * 保存&修改笔记专栏管理
     * 方法名：saveupdateNote<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-30 21:42:35<br/>
     * @param note 
     * @return Note<br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    NoteBo saveupdateNote(Note note);

    /**
     * 根据Id删除笔记专栏管理
     * 方法名：deleteNoteById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-30 21:42:35<br/>
     * @param id
     * @return int <br />
     * @throws <br/>
     * @since 1.0.0<br />
     */
    int deleteNoteById(Long id) ;

    /**
     * 根据Id查询笔记专栏管理明细信息
     * 方法名：getNoteById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-30 21:42:35<br/>
     * @param id
     * @return Note <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    NoteBo getNoteById(Long id);

    /**
     * 根据笔记专栏管理ids批量删除笔记专栏管理
     * 方法名：delBatchNote<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-30 21:42:35<br/>
     * @param ids
     * @return boolean <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    boolean delBatchNote(String ids);

}