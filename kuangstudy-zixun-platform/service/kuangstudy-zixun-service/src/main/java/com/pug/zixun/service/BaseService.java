package com.pug.zixun.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pug.zixun.commons.utils.Tool;
import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 飞哥B站地址：https://space.bilibili.com/490711252
 * 记得关注和三连哦！
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2022/3/19$ 17:35$
 */
public interface BaseService {

    /**
     * @param clz
     * @param <T>
     * @param <R>
     * @return
     */
    default <T, R> R tranferBo(T pojo, Class<R> clz) {
        try {
            R r = clz.newInstance();
            BeanUtils.copyProperties(pojo, r);
            return r;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @param clz
     * @param <T>
     * @param <R>
     * @return
     */
    default <T, R> List<R> tranferListBo(List<T> list, Class<R> clz) {
        try {
            List<R> collect = list.stream()
                    .map(pojo -> this.tranferBo(pojo, clz))
                    .collect(Collectors.toList());
            return collect;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @param clz
     * @param <T>
     * @param <R>
     * @return
     */
    default <T, R> IPage<R> tranferPageBo(IPage<T> poPage, Class<R> clz) {
        List<R> hotelBos = tranferListBo(poPage.getRecords(), clz);
        IPage<R> boPage = new Page<>();
        boPage.setTotal(poPage.getTotal());
        boPage.setPages(poPage.getPages());
        boPage.setSize(poPage.getSize());
        boPage.setCurrent(poPage.getCurrent());
        boPage.setRecords(hotelBos);
        return boPage;
    }


    default  <T> String[] getFilterFields(Class<T> clz, String ...filterField) {
        List<String> list = new ArrayList<>();
        List<String> result = new ArrayList<>();
        Field[] fields = clz.getDeclaredFields();
        for (Field field : fields) {
            list.add(Tool.humpToLine(field.getName()));
        }
        if (filterField!=null && filterField.length > 0) {
            result = list.stream().filter(field -> Arrays.asList(filterField).stream()
                    .filter(f -> f.equals(field)).count() == 0).collect(Collectors.toList());
        } else {
            result =  list;
        }

        if(!CollectionUtils.isEmpty(result)) {
            String[] strings = new String[result.size()];
            for (int i = 0; i < result.size(); i++) {
                strings[i] = result.get(i);
            }
            return strings;
        }
        return null;
    }


    @Deprecated
    default <T> List<String> getFilterFields(Class<T> clz, List<String> filterField) {
        List<String> list = new ArrayList<>();
        Field[] fields = clz.getDeclaredFields();
        for (Field field : fields) {
            list.add(Tool.humpToLine(field.getName()));
        }
        List<String> collect = list.stream().filter(field -> filterField.stream()
                .filter(f -> f.equals(field)).count() == 0).collect(Collectors.toList());

        return collect;
    }
}
