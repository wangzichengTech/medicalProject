package com.pug.zixun.service.resourcecategory;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pug.zixun.bo.ResourceCategoryBo;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;
import com.pug.zixun.mapper.ResourceCategoryMapper;
import com.pug.zixun.pojo.ResourceCategory;
import com.pug.zixun.vo.ResourceCategoryVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;



/**
 * ResourceCategoryServiceImpl实现类
 * 创建人:yykk<br/>
 * 时间：2022-07-19 23:49:04 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Service
@Slf4j
public class ResourceCategoryServiceImpl extends ServiceImpl<ResourceCategoryMapper,ResourceCategory> implements IResourceCategoryService  {

    /**
     * 查询分页&搜索资源分类
     * @param resourcecategoryVo
     * @return IPage<ResourceCategory>
     * 创建人:yykk
     * 创建时间：2022-07-19 23:49:04
     * @version 1.0.0
     */
    @Override
	public IPage<ResourceCategoryBo> findResourceCategoryPage(ResourceCategoryVo resourcecategoryVo){
	    // 设置分页信息
		Page<ResourceCategory> page = new Page<>(resourcecategoryVo.getPageNo(),resourcecategoryVo.getPageSize());
		// 设置查询条件
        LambdaQueryWrapper<ResourceCategory> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // lambdaQueryWrapper.select(ResourceCategory.class, column -> !column.getColumn().equals("description"));
        // 根据关键词搜索信息
        lambdaQueryWrapper.like(Vsserts.isNotEmpty(resourcecategoryVo.getKeyword()), ResourceCategory::getTitle,resourcecategoryVo.getKeyword());
         //查询发布的 0 未发布  1 发布
        lambdaQueryWrapper.eq(resourcecategoryVo.getStatus() != null ,ResourceCategory::getStatus,resourcecategoryVo.getStatus());
        lambdaQueryWrapper.eq(ResourceCategory::getIsdelete,0);
        // 多列搜索
        // lambdaQueryWrapper.and(Vsserts.isNotEmpty(resourcecategoryVo.getKeyword()),wrapper -> wrapper
        //         .like(ResourceCategory::getName, resourcecategoryVo.getKeyword())
        //         .or()
        //         .like(ResourceCategory::getCategoryName, resourcecategoryVo.getKeyword())
        // );
        // 根据时间排降序
        lambdaQueryWrapper.orderByDesc(ResourceCategory::getCreateTime);
        // 查询分页返回
		IPage<ResourceCategory> results = this.page(page,lambdaQueryWrapper);
		return tranferPageBo(results,ResourceCategoryBo.class);
	}

    /**
     * 查询资源分类列表信息
     * @method: findResourceCategoryList
     * @result : List<ResourceCategory>
     * 创建人:yykk
     * 创建时间：2022-07-19 23:49:04
     * @version 1.0.0
     * @return
    */
    @Override
    public List<ResourceCategoryBo> findResourceCategoryList() {
     	// 1：设置查询条件
        LambdaQueryWrapper<ResourceCategory> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // 2：查询发布的 0 未发布  1 发布
        lambdaQueryWrapper.eq(ResourceCategory::getStatus,1);
        lambdaQueryWrapper.eq(ResourceCategory::getIsdelete,0);
        // 3: 查询返回
        return tranferListBo(this.list(lambdaQueryWrapper),ResourceCategoryBo.class);
    }

	/**
     * 根据id查询资源分类明细信息
     * @param id
     * @return ResourceCategory
     * 创建人:yykk
     * 创建时间：2022-07-19 23:49:04
     * @version 1.0.0
     */
    @Override
    public ResourceCategoryBo getResourceCategoryById(Long id) {
        return tranferBo(this.getById(id),ResourceCategoryBo.class);
    }


    /**
     * 保存&修改资源分类
     * @param resourcecategory
     * @return ResourceCategory
     * 创建人:yykk
     * 创建时间：2022-07-19 23:49:04
     * @version 1.0.0
     */
    @Override
	public ResourceCategoryBo saveupdateResourceCategory(ResourceCategory resourcecategory){
		boolean flag = this.saveOrUpdate(resourcecategory);
		return flag ? tranferBo(resourcecategory,ResourceCategoryBo.class)  : null;
	}


    /**
     * 根据id删除资源分类
     * @param id
     * @return int
     * 创建人:yykk
     * 创建时间：2022-07-19 23:49:04
     * @version 1.0.0
     */
    @Override
    public int deleteResourceCategoryById(Long id) {
        ResourceCategory adminRole = this.getById(id);
        if (adminRole != null) {
            ResourceCategory resourcecategory2 = new ResourceCategory();
            resourcecategory2.setId(id);
            resourcecategory2.setIsdelete(1);
            boolean b = this.updateById(resourcecategory2);
            return 1;
        }
        return 0;
    }

    /**
     * 根据id删除
     * @param ids
     * @return boolean
     * 创建人:yykk
     * 创建时间：2022-07-19 23:49:04
     * @version 1.0.0
     */
    @Override
    public boolean delBatchResourceCategory(String ids) {
        try {
            // 1 : 把数据分割
            String[] strings = ids.split(",");
            // 2: 组装成一个List<ResourceCategory>
            List<ResourceCategory> resourcecategoryList = Arrays.stream(strings).map(idstr -> {
                ResourceCategory resourcecategory = new ResourceCategory();
                resourcecategory.setId(new Long(idstr));
                resourcecategory.setIsdelete(1);
                return resourcecategory;
            }).collect(Collectors.toList());
            // 3: 批量删除
            return this.updateBatchById(resourcecategoryList);
        } catch (Exception ex) {
            throw new PugValidatorException(ResultStatusEnum.SERVER_DB_ERROR);
        }
    }


}