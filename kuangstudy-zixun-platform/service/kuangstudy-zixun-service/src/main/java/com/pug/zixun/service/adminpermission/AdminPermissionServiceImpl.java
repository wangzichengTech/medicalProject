package com.pug.zixun.service.adminpermission;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pug.zixun.bo.AdminPermissionBo;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;
import com.pug.zixun.mapper.AdminPermissionMapper;
import com.pug.zixun.pojo.AdminPermission;
import com.pug.zixun.vo.AdminPermissionVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;



/**
 * AdminPermissionServiceImpl实现类
 * 创建人:yykk<br/>
 * 时间：2022-07-17 18:38:05 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Service
@Slf4j
public class AdminPermissionServiceImpl extends ServiceImpl<AdminPermissionMapper,AdminPermission> implements IAdminPermissionService  {

    /**
     * 查询分页&搜索后台权限管理
     * @param adminpermissionVo
     * @return IPage<AdminPermission>
     * 创建人:yykk
     * 创建时间：2022-07-17 18:38:05
     * @version 1.0.0
     */
    @Override
	public IPage<AdminPermissionBo> findAdminPermissionPage(AdminPermissionVo adminpermissionVo){
	    // 设置分页信息
		Page<AdminPermission> page = new Page<>(adminpermissionVo.getPageNo(),adminpermissionVo.getPageSize());
		// 设置查询条件
        LambdaQueryWrapper<AdminPermission> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // lambdaQueryWrapper.select(AdminPermission.class, column -> !column.getColumn().equals("description"));
        // 根据关键词搜索信息
        lambdaQueryWrapper.like(Vsserts.isNotEmpty(adminpermissionVo.getKeyword()), AdminPermission::getName,adminpermissionVo.getKeyword());
         //查询发布的 0 未发布  1 发布
        lambdaQueryWrapper.eq(adminpermissionVo.getStatus() != null ,AdminPermission::getStatus,adminpermissionVo.getStatus());
        // 根据关键词搜索信息
        lambdaQueryWrapper.like(Vsserts.isNotEmpty(adminpermissionVo.getKeyword()), AdminPermission::getPathname,adminpermissionVo.getKeyword());
        // 多列搜索
        // lambdaQueryWrapper.and(Vsserts.isNotEmpty(adminpermissionVo.getKeyword()),wrapper -> wrapper
        //         .like(AdminPermission::getName, adminpermissionVo.getKeyword())
        //         .or()
        //         .like(AdminPermission::getCategoryName, adminpermissionVo.getKeyword())
        // );
        // 根据时间排降序
        lambdaQueryWrapper.orderByDesc(AdminPermission::getCreateTime);
        // 查询分页返回
		IPage<AdminPermission> results = this.page(page,lambdaQueryWrapper);
		return tranferPageBo(results,AdminPermissionBo.class);
	}

    /**
     * 查询后台权限管理列表信息
     * @method: findAdminPermissionList
     * @result : List<AdminPermission>
     * 创建人:yykk
     * 创建时间：2022-07-17 18:38:05
     * @version 1.0.0
     * @return
    */
    @Override
    public List<AdminPermissionBo> findAdminPermissionList() {
     	// 1：设置查询条件
        LambdaQueryWrapper<AdminPermission> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // 2：查询发布的 0 未发布  1 发布
        lambdaQueryWrapper.eq(AdminPermission::getStatus,1);
        lambdaQueryWrapper.eq(AdminPermission::getIsdelete,0);
        // 3: 查询返回
        return tranferListBo(this.list(lambdaQueryWrapper),AdminPermissionBo.class);
    }

	/**
     * 根据id查询后台权限管理明细信息
     * @param id
     * @return AdminPermission
     * 创建人:yykk
     * 创建时间：2022-07-17 18:38:05
     * @version 1.0.0
     */
    @Override
    public AdminPermissionBo getAdminPermissionById(Long id) {
        return tranferBo(this.getById(id),AdminPermissionBo.class);
    }


    /**
     * 保存&修改后台权限管理
     * @param adminpermission
     * @return AdminPermission
     * 创建人:yykk
     * 创建时间：2022-07-17 18:38:05
     * @version 1.0.0
     */
    @Override
	public AdminPermissionBo saveupdateAdminPermission(AdminPermission adminpermission){
		boolean flag = this.saveOrUpdate(adminpermission);
		return flag ? tranferBo(adminpermission,AdminPermissionBo.class)  : null;
	}


    /**
     * 根据id删除后台权限管理
     * @param id
     * @return int
     * 创建人:yykk
     * 创建时间：2022-07-17 18:38:05
     * @version 1.0.0
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteAdminPermissionById(Long id) {
        // 根据id删除中间表绑定的权限信息
        this.baseMapper.deleteByPermissionId(id);
        // 删除自己
        boolean b = this.removeById(id);
        return b ? 1 : 0;
    }

    /**
     * 根据id删除
     * @param ids
     * @return boolean
     * 创建人:yykk
     * 创建时间：2022-07-17 18:38:05
     * @version 1.0.0
     */
    @Override
    public boolean delBatchAdminPermission(String ids) {
        try {
            // 1 : 把数据分割
            String[] strings = ids.split(",");
            // 2: 组装成一个List<AdminPermission>
            List<AdminPermission> adminpermissionList = Arrays.stream(strings).map(idstr -> {
                AdminPermission adminpermission = new AdminPermission();
                adminpermission.setId(new Long(idstr));
                adminpermission.setIsdelete(1);
                return adminpermission;
            }).collect(Collectors.toList());
            // 3: 批量删除
            return this.updateBatchById(adminpermissionList);
        } catch (Exception ex) {
            throw new PugValidatorException(ResultStatusEnum.SERVER_DB_ERROR);
        }
    }


    @Override
    public List<AdminPermission> findAdminPermissionTree(){
        // 1 :查询表中所有的数据
        LambdaQueryWrapper<AdminPermission> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(AdminPermission::getStatus,1);
        List<AdminPermission> allList = this.list(lambdaQueryWrapper); // 思考空间，为什么查询的是所有
        // 2: 找到所有的根节点 pid = 0
        List<AdminPermission> rootList = allList.stream().filter(category -> category.getPid().equals(0L))
                .sorted((a, b) -> a.getSorted() - b.getSorted()).collect(Collectors.toList());
        // 3 : 查询所有的非根节点
        List<AdminPermission> subList = allList.stream().filter(category -> !category.getPid().equals(0L)).collect(Collectors.toList());
        // 4 : 循环根节点去subList去找对应的子节点
        rootList.forEach(root -> buckForback(root, subList));

        return rootList;
    }

    private void buckForback(AdminPermission root, List<AdminPermission> subList) {
        // 通过根节点去id和子节点的pid是否相等，如果相等的话，代表是当前根的子集
        List<AdminPermission> childrenList = subList.stream().filter(category -> category.getPid().equals(root.getId()))
                .sorted((a, b) -> a.getSorted() - b.getSorted())
                .collect(Collectors.toList());
        // 如果你当前没一个子集，初始化一个空数组
        if (!CollectionUtils.isEmpty(childrenList)) {
            // 查询以后放回去
            root.setChildren(childrenList);
            // 再次递归构建即可
            childrenList.forEach(category -> buckForback(category, subList));
        } else {
            root.setChildren(new ArrayList<>());
        }
    }



    @Override
    public List<AdminPermission> findAdminPermissionMenuTree(){
        // 1 :查询表中所有的数据
        LambdaQueryWrapper<AdminPermission> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(AdminPermission::getStatus,1);
        lambdaQueryWrapper.eq(AdminPermission::getType,1);//查询菜单的
        List<AdminPermission> allList = this.list(lambdaQueryWrapper); // 思考空间，为什么查询的是所有
        // 2: 找到所有的根节点 pid = 0
        List<AdminPermission> rootList = allList.stream().filter(category -> category.getPid().equals(0L))
                .sorted((a, b) -> a.getSorted() - b.getSorted()).collect(Collectors.toList());
        // 3 : 查询所有的非根节点
        List<AdminPermission> subList = allList.stream().filter(category -> !category.getPid().equals(0L)).collect(Collectors.toList());
        // 4 : 循环根节点去subList去找对应的子节点
        rootList.forEach(root -> buckForbackMenu(root, subList));

        return rootList;
    }

    private void buckForbackMenu(AdminPermission root, List<AdminPermission> subList) {
        // 通过根节点去id和子节点的pid是否相等，如果相等的话，代表是当前根的子集
        List<AdminPermission> childrenList = subList.stream().filter(category -> category.getPid().equals(root.getId()))
                .sorted((a, b) -> a.getSorted() - b.getSorted())
                .collect(Collectors.toList());
        // 如果你当前没一个子集，初始化一个空数组
        if (!CollectionUtils.isEmpty(childrenList)) {
            // 查询以后放回去
            root.setChildren(childrenList);
            // 再次递归构建即可
            childrenList.forEach(category -> buckForbackMenu(category, subList));
        } else {
            root.setChildren(new ArrayList<>());
        }
    }

}