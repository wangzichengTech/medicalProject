package com.pug.zixun.service.user;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pug.zixun.bo.UserBo;
import com.pug.zixun.pojo.User;
import com.pug.zixun.vo.UserVo;

import java.util.List;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 飞哥B站地址：https://space.bilibili.com/490711252
 * 记得关注和三连哦！
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2022/5/13$ 21:29$
 */

public interface IUserService extends IService<User> {



    /**
     * 保存用户
     * @param userVo
     * @return
     */
    UserBo saveUser(UserVo userVo);

    /**
     * 查询用户列表
     * @param userVo
     * @return
     */
    List<UserBo> findUserList(UserVo userVo);

    /**
     * 分页用户查询
     * @param userVo
     * @return
     */
    IPage<UserBo> pageUser(UserVo userVo);



}
