package com.pug.zixun.service.adminlogs;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pug.zixun.pojo.AdminLogs;
import com.pug.zixun.vo.AdminLogsVo;
import com.pug.zixun.bo.AdminLogsBo;
import com.pug.zixun.service.BaseService;
import java.util.List;

/**
 * IAdminLogsService接口
 * 创建人:yykk<br/>
 * 时间：2022-07-26 20:20:25 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
public interface IAdminLogsService extends IService<AdminLogs>,BaseService{


    /**
     * 查询后台操作日志列表信息
     * @method: findAdminLogsList
     * @result : List<AdminLogsBo>
     * 创建人:yykk
     * 创建时间：2022-07-26 20:20:25
     * @version 1.0.0
     * @return
     */
    List<AdminLogsBo> findAdminLogsList() ;

	/**
     * 查询后台操作日志列表信息并分页
     * 方法名：findAdminLogss<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-26 20:20:25<br/>
     * @param adminlogsVo
     * @return IPage<AdminLogs><br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
	IPage<AdminLogsBo> findAdminLogsPage(AdminLogsVo adminlogsVo);

    /**
     * 保存&修改后台操作日志
     * 方法名：saveupdateAdminLogs<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-26 20:20:25<br/>
     * @param adminlogs 
     * @return AdminLogs<br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    AdminLogsBo saveupdateAdminLogs(AdminLogs adminlogs);

    /**
     * 根据Id删除后台操作日志
     * 方法名：deleteAdminLogsById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-26 20:20:25<br/>
     * @param id
     * @return int <br />
     * @throws <br/>
     * @since 1.0.0<br />
     */

    /**
     * 根据Id查询后台操作日志明细信息
     * 方法名：getAdminLogsById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-26 20:20:25<br/>
     * @param id
     * @return AdminLogs <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    AdminLogsBo getAdminLogsById(Long id);

    /**
     * 根据后台操作日志ids批量删除后台操作日志
     * 方法名：delBatchAdminLogs<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-26 20:20:25<br/>
     * @param ids
     * @return boolean <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    boolean delBatchAdminLogs(String ids);

}