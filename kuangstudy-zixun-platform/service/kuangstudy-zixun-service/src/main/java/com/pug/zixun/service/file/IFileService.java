package com.pug.zixun.service.file;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pug.zixun.pojo.File;
import com.pug.zixun.vo.FileVo;
import com.pug.zixun.bo.FileBo;
import com.pug.zixun.service.BaseService;
import java.util.List;

/**
 * IFileService接口
 * 创建人:yykk<br/>
 * 时间：2022-10-28 13:23:37 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
public interface IFileService extends IService<File>,BaseService{


    /**
     * 查询文件管理列表信息
     * @method: findFileList
     * @result : List<FileBo>
     * 创建人:yykk
     * 创建时间：2022-10-28 13:23:37
     * @version 1.0.0
     * @return
     */
    List<FileBo> findFileList() ;

	/**
     * 查询文件管理列表信息并分页
     * 方法名：findFiles<br/>
     * 创建人：yykk <br/>
     * 时间：2022-10-28 13:23:37<br/>
     * @param fileVo
     * @return IPage<File><br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
	IPage<FileBo> findFilePage(FileVo fileVo);
    IPage<FileBo> findFilePage2(FileVo fileVo);
    /**
     * 保存&修改文件管理
     * 方法名：saveupdateFile<br/>
     * 创建人：yykk <br/>
     * 时间：2022-10-28 13:23:37<br/>
     * @param file 
     * @return File<br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    FileBo saveupdateFile(File file);

    /**
     * 根据Id删除文件管理
     * 方法名：deleteFileById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-10-28 13:23:37<br/>
     * @param id
     * @return int <br />
     * @throws <br/>
     * @since 1.0.0<br />
     */
    int deleteFileById(Long id) ;
    int deleteFileById2(Long id) ;
    /**
     * 根据Id查询文件管理明细信息
     * 方法名：getFileById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-10-28 13:23:37<br/>
     * @param id
     * @return File <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    FileBo getFileById(Long id);

    /**
     * 根据文件管理ids批量删除文件管理
     * 方法名：delBatchFile<br/>
     * 创建人：yykk <br/>
     * 时间：2022-10-28 13:23:37<br/>
     * @param ids
     * @return boolean <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    boolean delBatchFile(String ids);
    boolean delBatchFile2(String ids);
}