package com.pug.zixun.service.sepsispatients;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pug.zixun.pojo.SepsisPatients;
import com.pug.zixun.vo.SepsisPatientsVo;
import com.pug.zixun.bo.SepsisPatientsBo;
import com.pug.zixun.service.BaseService;
import java.util.List;

/**
 * ISepsisPatientsService接口
 * 创建人:yykk<br/>
 * 时间：2022-11-14 19:09:44 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
public interface ISepsisPatientsService extends IService<SepsisPatients>,BaseService{


    /**
     * 查询脓毒症信息管理列表信息
     * @method: findSepsisPatientsList
     * @result : List<SepsisPatientsBo>
     * 创建人:yykk
     * 创建时间：2022-11-14 19:09:44
     * @version 1.0.0
     * @return
     */
    List<SepsisPatientsBo> findSepsisPatientsList() ;

	/**
     * 查询脓毒症信息管理列表信息并分页
     * 方法名：findSepsisPatientss<br/>
     * 创建人：yykk <br/>
     * 时间：2022-11-14 19:09:44<br/>
     * @param sepsispatientsVo
     * @return IPage<SepsisPatients><br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
	IPage<SepsisPatientsBo> findSepsisPatientsPage(SepsisPatientsVo sepsispatientsVo);
    IPage<SepsisPatientsBo> findSepsisPatientsPage2(SepsisPatientsVo sepsispatientsVo);

    /**
     * 保存&修改脓毒症信息管理
     * 方法名：saveupdateSepsisPatients<br/>
     * 创建人：yykk <br/>
     * 时间：2022-11-14 19:09:44<br/>
     * @param sepsispatients 
     * @return SepsisPatients<br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    SepsisPatientsBo saveupdateSepsisPatients(SepsisPatients sepsispatients);

    /**
     * 根据Id删除脓毒症信息管理
     * 方法名：deleteSepsisPatientsById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-11-14 19:09:44<br/>
     * @param id
     * @return int <br />
     * @throws <br/>
     * @since 1.0.0<br />
     */
    int deleteSepsisPatientsById(Integer id) ;

    /**
     * 根据Id查询脓毒症信息管理明细信息
     * 方法名：getSepsisPatientsById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-11-14 19:09:44<br/>
     * @param id
     * @return SepsisPatients <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    SepsisPatientsBo getSepsisPatientsById(Integer id);

    /**
     * 根据脓毒症信息管理ids批量删除脓毒症信息管理
     * 方法名：delBatchSepsisPatients<br/>
     * 创建人：yykk <br/>
     * 时间：2022-11-14 19:09:44<br/>
     * @param ids
     * @return boolean <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    boolean delBatchSepsisPatients(String ids);
    boolean delBatchSepsisPatients2(String ids);

}