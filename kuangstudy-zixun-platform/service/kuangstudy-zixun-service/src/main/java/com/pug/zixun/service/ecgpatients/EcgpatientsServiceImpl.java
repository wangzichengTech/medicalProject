package com.pug.zixun.service.ecgpatients;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pug.zixun.mapper.EcgpatientsMapper;
import com.pug.zixun.pojo.Ecgpatients;
import com.pug.zixun.pojo.File;
import com.pug.zixun.vo.EcgpatientsVo;
import com.pug.zixun.bo.EcgpatientsBo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;



/**
 * EcgpatientsServiceImpl实现类
 * 创建人:yykk<br/>
 * 时间：2022-11-02 19:18:02 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>1i
*/
@Service
@Slf4j
public class EcgpatientsServiceImpl extends ServiceImpl<EcgpatientsMapper,Ecgpatients> implements IEcgpatientsService  {
    @Resource
    private  IEcgpatientsService ecgpatientsService;

    /**
     * 查询分页&搜索脓毒症信息管理
     * @param ecgpatientsVo
     * @return IPage<Ecgpatients>
     * 创建人:yykk
     * 创建时间：2022-11-02 19:18:02
     * @version 1.0.0
     */
    @Override
	public IPage<EcgpatientsBo> findEcgpatientsPage(EcgpatientsVo ecgpatientsVo){
	    // 设置分页信息
		Page<Ecgpatients> page = new Page<>(ecgpatientsVo.getPageNo(),ecgpatientsVo.getPageSize());
		// 设置查询条件
        LambdaQueryWrapper<Ecgpatients> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // lambdaQueryWrapper.select(Ecgpatients.class, column -> !column.getColumn().equals("description"));
        // 根据关键词搜索信息
        lambdaQueryWrapper.like(Vsserts.isNotEmpty(ecgpatientsVo.getKeyword()), Ecgpatients::getName,ecgpatientsVo.getKeyword());
        lambdaQueryWrapper.eq(ecgpatientsVo.getIsDelete() != null , Ecgpatients::getIsdelete,0);
        // 多列搜索
        // lambdaQueryWrapper.and(Vsserts.isNotEmpty(ecgpatientsVo.getKeyword()),wrapper -> wrapper
        //         .like(Ecgpatients::getName, ecgpatientsVo.getKeyword())
        //         .or()
        //         .like(Ecgpatients::getCategoryName, ecgpatientsVo.getKeyword())
        // );
        // 查询分页返回
		IPage<Ecgpatients> results = this.page(page,lambdaQueryWrapper);
		return tranferPageBo(results,EcgpatientsBo.class);
	}
    @Override
    public IPage<EcgpatientsBo> findEcgpatientsPage2(EcgpatientsVo ecgpatientsVo){
        // 设置分页信息
        Page<Ecgpatients> page = new Page<>(ecgpatientsVo.getPageNo(),ecgpatientsVo.getPageSize());
        // 设置查询条件
        LambdaQueryWrapper<Ecgpatients> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // lambdaQueryWrapper.select(Ecgpatients.class, column -> !column.getColumn().equals("description"));
        // 根据关键词搜索信息
        lambdaQueryWrapper.like(Vsserts.isNotEmpty(ecgpatientsVo.getKeyword()), Ecgpatients::getName,ecgpatientsVo.getKeyword());
        lambdaQueryWrapper.eq(ecgpatientsVo.getIsDelete() != null , Ecgpatients::getIsdelete,1);
        // 多列搜索
        // lambdaQueryWrapper.and(Vsserts.isNotEmpty(ecgpatientsVo.getKeyword()),wrapper -> wrapper
        //         .like(Ecgpatients::getName, ecgpatientsVo.getKeyword())
        //         .or()
        //         .like(Ecgpatients::getCategoryName, ecgpatientsVo.getKeyword())
        // );
        // 查询分页返回
        IPage<Ecgpatients> results = this.page(page,lambdaQueryWrapper);
        return tranferPageBo(results,EcgpatientsBo.class);
    }
    /**
     * 查询脓毒症信息管理列表信息
     * @method: findEcgpatientsList
     * @result : List<Ecgpatients>
     * 创建人:yykk
     * 创建时间：2022-11-02 19:18:02
     * @version 1.0.0
     * @return
    */
    @Override
    public List<EcgpatientsBo> findEcgpatientsList() {
     	// 1：设置查询条件
        LambdaQueryWrapper<Ecgpatients> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // 3: 查询返回
        return tranferListBo(this.list(lambdaQueryWrapper),EcgpatientsBo.class);
    }

	/**
     * 根据id查询脓毒症信息管理明细信息
     * @param id
     * @return Ecgpatients
     * 创建人:yykk
     * 创建时间：2022-11-02 19:18:02
     * @version 1.0.0
     */
    @Override
    public EcgpatientsBo getEcgpatientsById(Integer id) {
        return tranferBo(this.getById(id),EcgpatientsBo.class);
    }


    /**
     * 保存&修改脓毒症信息管理
     * @param ecgpatients
     * @return Ecgpatients
     * 创建人:yykk
     * 创建时间：2022-11-02 19:18:02
     * @version 1.0.0
     */
    @Override
	public EcgpatientsBo saveupdateEcgpatients(Ecgpatients ecgpatients){
		boolean flag = this.saveOrUpdate(ecgpatients);
		return flag ? tranferBo(ecgpatients,EcgpatientsBo.class)  : null;
	}


    /**
     * 根据id删除脓毒症信息管理
     * @param id
     * @return int
     * 创建人:yykk
     * 创建时间：2022-11-02 19:18:02
     * @version 1.0.0
     */
    /**
     * 根据id删除
     * @param ids
     * @return boolean
     * 创建人:yykk
     * 创建时间：2022-11-02 19:18:02
     * @version 1.0.0
     */
    @Override
    public boolean delBatchEcgpatients(String ids) {
        try {
            // 1 : 把数据分割
            String[] strings = ids.split(",");
            // 2: 组装成一个List<Ecgpatients>
            List<Ecgpatients> fileList = Arrays.stream(strings).map(idstr -> {
                Ecgpatients ecgpatients = new Ecgpatients();
                ecgpatients.setId(new Integer(idstr));
                ecgpatients.setIsdelete(1);
                return ecgpatients;
            }).collect(Collectors.toList());

            // 3: 批量删除
            return this.updateBatchById(fileList);
        } catch (Exception ex) {
            throw new PugValidatorException(ResultStatusEnum.SERVER_DB_ERROR);
        }
    }
    @Override
    public boolean delBatchEcgpatients2(String ids) {
        try {
            // 1 : 把数据分割
            String[] strings = ids.split(",");
            // 2: 组装成一个List<Ecgpatients>
            List<Integer> integers = Arrays.stream(strings).map(idstr -> {
                return new Integer(idstr);
            }).collect(Collectors.toList());

            // 3: 批量删除
            return ecgpatientsService.removeByIds(integers);
        } catch (Exception ex) {
            throw new PugValidatorException(ResultStatusEnum.SERVER_DB_ERROR);
        }
    }


}