package com.pug.zixun.service.coursecomment;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pug.zixun.bo.CourseCommentBo;
import com.pug.zixun.pojo.CourseComment;
import com.pug.zixun.service.BaseService;
import com.pug.zixun.vo.CourseCommentVo;

import java.util.List;

/**
 * ICourseCommentService接口
 * 创建人:yykk<br/>
 * 时间：2022-07-25 20:22:03 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
public interface ICourseCommentService extends IService<CourseComment>,BaseService{


    /**
     * 查询课程评论列表信息
     * @method: findCourseCommentList
     * @result : List<CourseCommentBo>
     * 创建人:yykk
     * 创建时间：2022-07-25 20:22:03
     * @version 1.0.0
     * @return
     */
    List<CourseCommentBo> findCourseCommentList() ;

	/**
     * 查询课程评论列表信息并分页
     * 方法名：findCourseComments<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-25 20:22:03<br/>
     * @param coursecommentVo
     * @return IPage<CourseComment><br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
	IPage<CourseCommentBo> findCourseCommentPage(CourseCommentVo coursecommentVo);

    /**
     * 保存&修改课程评论
     * 方法名：saveupdateCourseComment<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-25 20:22:03<br/>
     * @param coursecomment 
     * @return CourseComment<br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    CourseCommentBo saveupdateCourseComment(CourseComment coursecomment);

    /**
     * 根据Id删除课程评论
     * 方法名：deleteCourseCommentById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-25 20:22:03<br/>
     * @param id
     * @return int <br />
     * @throws <br/>
     * @since 1.0.0<br />
     */
    int deleteCourseCommentById(Long id) ;

    /**
     * 根据Id查询课程评论明细信息
     * 方法名：getCourseCommentById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-25 20:22:03<br/>
     * @param id
     * @return CourseComment <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    CourseCommentBo getCourseCommentById(Long id);

    /**
     * 根据课程评论ids批量删除课程评论
     * 方法名：delBatchCourseComment<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-25 20:22:03<br/>
     * @param ids
     * @return boolean <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    boolean delBatchCourseComment(String ids);

}