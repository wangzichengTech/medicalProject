package com.pug.zixun.service.adminuser;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pug.zixun.bo.AdminUserBo;
import com.pug.zixun.pojo.AdminUser;
import com.pug.zixun.service.BaseService;
import com.pug.zixun.vo.*;

import java.util.List;

/**
 * IAdminUserService接口
 * 创建人:yykk<br/>
 * 时间：2022-07-15 20:48:26 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
public interface IAdminUserService extends IService<AdminUser>,BaseService{

    /**
     * 保存用户角色信息
     *
     * @param userId
     * @param roleId
     * @return
     */
    int saveUserRole(Long userId,  Long roleId);

    /**
     * 删除用户角色信息
     *
     * @param userId
     * @param roleId
     * @return
     */
    int delUserRole(Long userId,  Long roleId);
    /**
     * 登录
     * @param loginVo
     * @return
     */
    AdminUser login(LoginVo loginVo);

    /**
     * 根据用户id查询角色信息
     * @param userId
     * @return
     */
    List<String> getRoleNames(Long userId);

    /**
     * 根据用户id查询角色信息
     * @param userId
     * @return
     */
    List<Long> findByUserRoleIds(Long userId);

    /**
     * 查询用户对应的权限信息
     * @param userId
     * @return
     */
    List<String> findByUserPermission(Long userId);

    /**
     * 查询后台用户管理列表信息
     * @method: findAdminUserList
     * @result : List<AdminUserBo>
     * 创建人:yykk
     * 创建时间：2022-07-15 20:48:26
     * @version 1.0.0
     * @return
     */
    List<AdminUserBo> findAdminUserList() ;

	/**
     * 查询后台用户管理列表信息并分页
     * 方法名：findAdminUsers<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-15 20:48:26<br/>
     * @param adminuserVo
     * @return IPage<AdminUser><br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
	IPage<AdminUserBo> findAdminUserPage(AdminUserVo adminuserVo);

    /**
     * 保存&修改后台用户管理
     * 方法名：saveupdateAdminUser<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-15 20:48:26<br/>
     * @param adminUserRegVo
     * @return AdminUser<br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    AdminUserBo saveupdateAdminUser( AdminUserRegVo adminUserRegVo);

    /**
     * 根据Id删除后台用户管理
     * 方法名：deleteAdminUserById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-15 20:48:26<br/>
     * @param id
     * @return int <br />
     * @throws <br/>
     * @since 1.0.0<br />
     */
    int deleteAdminUserById(Long id) ;

    /**
     * 根据Id查询后台用户管理明细信息
     * 方法名：getAdminUserById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-15 20:48:26<br/>
     * @param id
     * @return AdminUser <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    AdminUserBo getAdminUserById(Long id);

    /**
     * 根据后台用户管理ids批量删除后台用户管理
     * 方法名：delBatchAdminUser<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-15 20:48:26<br/>
     * @param ids
     * @return boolean <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    boolean delBatchAdminUser(String ids);

    /**
     * 状态更新
     * @param adminUserRegVo
     * @return
     */
    boolean updateAdminUser(StatusUpdateVo adminUserRegVo);

    void register(UserRequest user);

    void sendEmail(String email, String type);

    String passwordReset(UserRequest userRequest);
}