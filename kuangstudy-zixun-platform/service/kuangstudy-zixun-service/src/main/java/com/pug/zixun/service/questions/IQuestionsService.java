package com.pug.zixun.service.questions;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pug.zixun.pojo.Questions;
import com.pug.zixun.vo.QuestionsVo;
import com.pug.zixun.bo.QuestionsBo;
import com.pug.zixun.service.BaseService;
import java.util.List;

/**
 * IQuestionsService接口
 * 创建人:yykk<br/>
 * 时间：2022-07-30 23:58:45 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
public interface IQuestionsService extends IService<Questions>,BaseService{


    /**
     * 查询文章管理列表信息
     * @method: findQuestionsList
     * @result : List<QuestionsBo>
     * 创建人:yykk
     * 创建时间：2022-07-30 23:58:45
     * @version 1.0.0
     * @return
     */
    List<QuestionsBo> findQuestionsList() ;

	/**
     * 查询文章管理列表信息并分页
     * 方法名：findQuestionss<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-30 23:58:45<br/>
     * @param questionsVo
     * @return IPage<Questions><br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
	IPage<QuestionsBo> findQuestionsPage(QuestionsVo questionsVo);

    /**
     * 保存&修改文章管理
     * 方法名：saveupdateQuestions<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-30 23:58:45<br/>
     * @param questions 
     * @return Questions<br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    QuestionsBo saveupdateQuestions(Questions questions);

    /**
     * 根据Id删除文章管理
     * 方法名：deleteQuestionsById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-30 23:58:45<br/>
     * @param id
     * @return int <br />
     * @throws <br/>
     * @since 1.0.0<br />
     */
    int deleteQuestionsById(Long id) ;

    /**
     * 根据Id查询文章管理明细信息
     * 方法名：getQuestionsById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-30 23:58:45<br/>
     * @param id
     * @return Questions <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    QuestionsBo getQuestionsById(Long id);

    /**
     * 根据文章管理ids批量删除文章管理
     * 方法名：delBatchQuestions<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-30 23:58:45<br/>
     * @param ids
     * @return boolean <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    boolean delBatchQuestions(String ids);

}