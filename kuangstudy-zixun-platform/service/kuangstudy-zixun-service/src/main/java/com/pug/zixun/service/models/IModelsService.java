package com.pug.zixun.service.models;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pug.zixun.pojo.Models;
import com.pug.zixun.vo.ModelsVo;
import com.pug.zixun.bo.ModelsBo;
import com.pug.zixun.service.BaseService;
import java.util.List;

/**
 * IModelsService接口
 * 创建人:yykk<br/>
 * 时间：2022-10-08 21:13:25 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
public interface IModelsService extends IService<Models>,BaseService{


    /**
     * 查询模型管理列表信息
     * @method: findModelsList
     * @result : List<ModelsBo>
     * 创建人:yykk
     * 创建时间：2022-10-08 21:13:25
     * @version 1.0.0
     * @return
     */
    List<ModelsBo> findModelsList() ;

	/**
     * 查询模型管理列表信息并分页
     * 方法名：findModelss<br/>
     * 创建人：yykk <br/>
     * 时间：2022-10-08 21:13:25<br/>
     * @param modelsVo
     * @return IPage<Models><br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
	IPage<ModelsBo> findModelsPage(ModelsVo modelsVo);

    /**
     * 保存&修改模型管理
     * 方法名：saveupdateModels<br/>
     * 创建人：yykk <br/>
     * 时间：2022-10-08 21:13:25<br/>
     * @param models 
     * @return Models<br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    ModelsBo saveupdateModels(Models models);

    /**
     * 根据Id删除模型管理
     * 方法名：deleteModelsById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-10-08 21:13:25<br/>
     * @param id
     * @return int <br />
     * @throws <br/>
     * @since 1.0.0<br />
     */
    int deleteModelsById(Long id) ;

    /**
     * 根据Id查询模型管理明细信息
     * 方法名：getModelsById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-10-08 21:13:25<br/>
     * @param id
     * @return Models <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    ModelsBo getModelsById(Long id);

    /**
     * 根据模型管理ids批量删除模型管理
     * 方法名：delBatchModels<br/>
     * 创建人：yykk <br/>
     * 时间：2022-10-08 21:13:25<br/>
     * @param ids
     * @return boolean <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    boolean delBatchModels(String ids);

}