package com.pug.zixun.service.coursecomment;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pug.zixun.bo.CourseCommentBo;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;
import com.pug.zixun.mapper.CourseCommentMapper;
import com.pug.zixun.pojo.CourseComment;
import com.pug.zixun.vo.CourseCommentVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;



/**
 * CourseCommentServiceImpl实现类
 * 创建人:yykk<br/>
 * 时间：2022-07-25 20:22:03 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Service
@Slf4j
public class CourseCommentServiceImpl extends ServiceImpl<CourseCommentMapper,CourseComment> implements ICourseCommentService  {

    /**
     * 查询分页&搜索课程评论
     * @param coursecommentVo
     * @return IPage<CourseComment>
     * 创建人:yykk
     * 创建时间：2022-07-25 20:22:03
     * @version 1.0.0
     */
    @Override
	public IPage<CourseCommentBo> findCourseCommentPage(CourseCommentVo coursecommentVo){
	    // 设置分页信息
		Page<CourseComment> page = new Page<>(coursecommentVo.getPageNo(),coursecommentVo.getPageSize());
		// 设置查询条件
        LambdaQueryWrapper<CourseComment> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // lambdaQueryWrapper.select(CourseComment.class, column -> !column.getColumn().equals("description"));
        // 根据关键词搜索信息
        lambdaQueryWrapper.like(Vsserts.isNotEmpty(coursecommentVo.getKeyword()), CourseComment::getNickname,coursecommentVo.getKeyword());
        // 根据关键词搜索信息
        lambdaQueryWrapper.like(Vsserts.isNotEmpty(coursecommentVo.getKeyword()), CourseComment::getCoursetitle,coursecommentVo.getKeyword());
        // 根据分类查询
        lambdaQueryWrapper.eq(Vsserts.isNotNull(coursecommentVo.getCategoryId()), CourseComment::getCoursecategoryid,coursecommentVo.getCategoryId());
        // 根据关键词搜索信息
        lambdaQueryWrapper.like(Vsserts.isNotEmpty(coursecommentVo.getKeyword()), CourseComment::getCoursecatename,coursecommentVo.getKeyword());
        // 根据关键词搜索信息
        lambdaQueryWrapper.like(Vsserts.isNotEmpty(coursecommentVo.getKeyword()), CourseComment::getReplyusernickname,coursecommentVo.getKeyword());
        // 多列搜索
        // lambdaQueryWrapper.and(Vsserts.isNotEmpty(coursecommentVo.getKeyword()),wrapper -> wrapper
        //         .like(CourseComment::getName, coursecommentVo.getKeyword())
        //         .or()
        //         .like(CourseComment::getCategoryName, coursecommentVo.getKeyword())
        // );
        // 根据时间排降序
        lambdaQueryWrapper.orderByDesc(CourseComment::getCreateTime);
        // 查询分页返回
		IPage<CourseComment> results = this.page(page,lambdaQueryWrapper);
		return tranferPageBo(results,CourseCommentBo.class);
	}

    /**
     * 查询课程评论列表信息
     * @method: findCourseCommentList
     * @result : List<CourseComment>
     * 创建人:yykk
     * 创建时间：2022-07-25 20:22:03
     * @version 1.0.0
     * @return
    */
    @Override
    public List<CourseCommentBo> findCourseCommentList() {
     	// 1：设置查询条件
        LambdaQueryWrapper<CourseComment> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // 2：查询发布的 0 未发布  1 发布
        // 3: 查询返回
        return tranferListBo(this.list(lambdaQueryWrapper),CourseCommentBo.class);
    }

	/**
     * 根据id查询课程评论明细信息
     * @param id
     * @return CourseComment
     * 创建人:yykk
     * 创建时间：2022-07-25 20:22:03
     * @version 1.0.0
     */
    @Override
    public CourseCommentBo getCourseCommentById(Long id) {
        return tranferBo(this.getById(id),CourseCommentBo.class);
    }


    /**
     * 保存&修改课程评论
     * @param coursecomment
     * @return CourseComment
     * 创建人:yykk
     * 创建时间：2022-07-25 20:22:03
     * @version 1.0.0
     */
    @Override
	public CourseCommentBo saveupdateCourseComment(CourseComment coursecomment){
		boolean flag = this.saveOrUpdate(coursecomment);
		return flag ? tranferBo(coursecomment,CourseCommentBo.class)  : null;
	}


    /**
     * 根据id删除课程评论
     * @param id
     * @return int
     * 创建人:yykk
     * 创建时间：2022-07-25 20:22:03
     * @version 1.0.0
     */
    @Override
    public int deleteCourseCommentById(Long id) {
        CourseComment adminRole = this.getById(id);
        if (adminRole != null) {
            CourseComment coursecomment2 = new CourseComment();
            coursecomment2.setId(id);
            boolean b = this.removeById(coursecomment2);
            return 1;
        }
        return 0;
    }

    /**
     * 根据id删除
     * @param ids
     * @return boolean
     * 创建人:yykk
     * 创建时间：2022-07-25 20:22:03
     * @version 1.0.0
     */
    @Override
    public boolean delBatchCourseComment(String ids) {
        try {
            // 1 : 把数据分割
            String[] strings = ids.split(",");
            // 2: 组装成一个List<CourseComment>
            List<CourseComment> coursecommentList = Arrays.stream(strings).map(idstr -> {
                CourseComment coursecomment = new CourseComment();
                coursecomment.setId(new Long(idstr));
                return coursecomment;
            }).collect(Collectors.toList());
            // 3: 批量删除
            return this.removeByIds(coursecommentList);
        } catch (Exception ex) {
            throw new PugValidatorException(ResultStatusEnum.SERVER_DB_ERROR);
        }
    }


}