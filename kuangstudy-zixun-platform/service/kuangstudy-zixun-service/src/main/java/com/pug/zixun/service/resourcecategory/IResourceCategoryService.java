package com.pug.zixun.service.resourcecategory;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pug.zixun.bo.ResourceCategoryBo;
import com.pug.zixun.pojo.ResourceCategory;
import com.pug.zixun.service.BaseService;
import com.pug.zixun.vo.ResourceCategoryVo;

import java.util.List;

/**
 * IResourceCategoryService接口
 * 创建人:yykk<br/>
 * 时间：2022-07-19 23:49:04 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
public interface IResourceCategoryService extends IService<ResourceCategory>,BaseService{


    /**
     * 查询资源分类列表信息
     * @method: findResourceCategoryList
     * @result : List<ResourceCategoryBo>
     * 创建人:yykk
     * 创建时间：2022-07-19 23:49:04
     * @version 1.0.0
     * @return
     */
    List<ResourceCategoryBo> findResourceCategoryList() ;

	/**
     * 查询资源分类列表信息并分页
     * 方法名：findResourceCategorys<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-19 23:49:04<br/>
     * @param resourcecategoryVo
     * @return IPage<ResourceCategory><br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
	IPage<ResourceCategoryBo> findResourceCategoryPage(ResourceCategoryVo resourcecategoryVo);

    /**
     * 保存&修改资源分类
     * 方法名：saveupdateResourceCategory<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-19 23:49:04<br/>
     * @param resourcecategory 
     * @return ResourceCategory<br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    ResourceCategoryBo saveupdateResourceCategory(ResourceCategory resourcecategory);

    /**
     * 根据Id删除资源分类
     * 方法名：deleteResourceCategoryById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-19 23:49:04<br/>
     * @param id
     * @return int <br />
     * @throws <br/>
     * @since 1.0.0<br />
     */
    int deleteResourceCategoryById(Long id) ;

    /**
     * 根据Id查询资源分类明细信息
     * 方法名：getResourceCategoryById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-19 23:49:04<br/>
     * @param id
     * @return ResourceCategory <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    ResourceCategoryBo getResourceCategoryById(Long id);

    /**
     * 根据资源分类ids批量删除资源分类
     * 方法名：delBatchResourceCategory<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-19 23:49:04<br/>
     * @param ids
     * @return boolean <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    boolean delBatchResourceCategory(String ids);

}