package com.pug.zixun.service.file;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pug.zixun.mapper.FileMapper;
import com.pug.zixun.pojo.File;
import com.pug.zixun.vo.FileVo;
import com.pug.zixun.bo.FileBo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;



/**
 * FileServiceImpl实现类
 * 创建人:yykk<br/>
 * 时间：2022-10-28 13:23:37 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Service
@Slf4j
public class FileServiceImpl extends ServiceImpl<FileMapper,File> implements IFileService  {
    @Resource
    private  IFileService fileService;

    /**
     * 查询分页&搜索文件管理
     * @param fileVo
     * @return IPage<File>
     * 创建人:yykk
     * 创建时间：2022-10-28 13:23:37
     * @version 1.0.0
     */
    @Override
    public IPage<FileBo> findFilePage(FileVo fileVo){
        // 设置分页信息
        Page<File> page = new Page<>(fileVo.getPageNo(),fileVo.getPageSize());
        // 设置查询条件
        LambdaQueryWrapper<File> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // lambdaQueryWrapper.select(File.class, column -> !column.getColumn().equals("description"));
        // 根据关键词搜索信息
        lambdaQueryWrapper.like(Vsserts.isNotEmpty(fileVo.getKeyword()), File::getName,fileVo.getKeyword());
        //查询发布的 0 未发布  1 发布
//        lambdaQueryWrapper.eq(fileVo.getStatus() != null ,File::getStatus,fileVo.getStatus());
        lambdaQueryWrapper.eq(fileVo.getIsDelete() != null ,File::getIsdelete,fileVo.getIsDelete());
        // 多列搜索
        // lambdaQueryWrapper.and(Vsserts.isNotEmpty(fileVo.getKeyword()),wrapper -> wrapper
        //         .like(File::getName, fileVo.getKeyword())
        //         .or()
        //         .like(File::getCategoryName, fileVo.getKeyword())
        // );
        // 根据时间排降序
        lambdaQueryWrapper.orderByDesc(File::getCreateTime);
        // 查询分页返回
        IPage<File> results = this.page(page,lambdaQueryWrapper);
        return tranferPageBo(results,FileBo.class);
    }
    @Override
    public IPage<FileBo> findFilePage2(FileVo fileVo){
        // 设置分页信息
        Page<File> page = new Page<>(fileVo.getPageNo(),fileVo.getPageSize());
        // 设置查询条件
        LambdaQueryWrapper<File> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // lambdaQueryWrapper.select(File.class, column -> !column.getColumn().equals("description"));
        // 根据关键词搜索信息
        lambdaQueryWrapper.like(Vsserts.isNotEmpty(fileVo.getKeyword()), File::getName,fileVo.getKeyword());
        //查询发布的 0 未发布  1 发布
//        lambdaQueryWrapper.eq(fileVo.getStatus() != null ,File::getStatus,fileVo.getStatus());
        lambdaQueryWrapper.eq(fileVo.getIsDelete() != null ,File::getIsdelete,1);
        // 多列搜索
        // lambdaQueryWrapper.and(Vsserts.isNotEmpty(fileVo.getKeyword()),wrapper -> wrapper
        //         .like(File::getName, fileVo.getKeyword())
        //         .or()
        //         .like(File::getCategoryName, fileVo.getKeyword())
        // );
        // 根据时间排降序
        lambdaQueryWrapper.orderByDesc(File::getCreateTime);
        // 查询分页返回
        IPage<File> results = this.page(page,lambdaQueryWrapper);
        return tranferPageBo(results,FileBo.class);
    }
    /**
     * 查询文件管理列表信息
     * @method: findFileList
     * @result : List<File>
     * 创建人:yykk
     * 创建时间：2022-10-28 13:23:37
     * @version 1.0.0
     * @return
    */
    @Override
    public List<FileBo> findFileList() {
     	// 1：设置查询条件
        LambdaQueryWrapper<File> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // 2：查询发布的 0 未发布  1 发布
        lambdaQueryWrapper.eq(File::getStatus,1);
        lambdaQueryWrapper.eq(File::getIsdelete,0);
        // 3: 查询返回
        return tranferListBo(this.list(lambdaQueryWrapper),FileBo.class);
    }

	/**
     * 根据id查询文件管理明细信息
     * @param id
     * @return File
     * 创建人:yykk
     * 创建时间：2022-10-28 13:23:37
     * @version 1.0.0
     */
    @Override
    public FileBo getFileById(Long id) {
        return tranferBo(this.getById(id),FileBo.class);
    }


    /**
     * 保存&修改文件管理
     * @param file
     * @return File
     * 创建人:yykk
     * 创建时间：2022-10-28 13:23:37
     * @version 1.0.0
     */
    @Override
	public FileBo saveupdateFile(File file){
		boolean flag = this.saveOrUpdate(file);
		return flag ? tranferBo(file,FileBo.class)  : null;
	}


    /**
     * 根据id删除文件管理
     * @param id
     * @return int
     * 创建人:yykk
     * 创建时间：2022-10-28 13:23:37
     * @version 1.0.0
     */
    @Override
    public int deleteFileById(Long id) {
        File adminRole = this.getById(id);
        if (adminRole != null) {
            File file2 = new File();
            file2.setId(id);
            file2.setIsdelete(1);
            boolean b = this.updateById(file2);
            return 1;
        }
        return 0;
    }
    @Override
    public int deleteFileById2(Long id) {

        File adminRole = this.getById(id);
        if (adminRole != null) {
            boolean b = fileService.removeById(id);

            return 1;
        }
        return 0;
    }
    /**
     * 根据id删除
     * @param ids
     * @return boolean
     * 创建人:yykk
     * 创建时间：2022-10-28 13:23:37
     * @version 1.0.0
     */
    @Override
    public boolean delBatchFile(String ids) {
        try {
            // 1 : 把数据分割
            String[] strings = ids.split(",");
            // 2: 组装成一个List<File>
            List<File> fileList = Arrays.stream(strings).map(idstr -> {
                File file = new File();
                file.setId(new Long(idstr));
                file.setIsdelete(1);
                return file;
            }).collect(Collectors.toList());
            // 3: 批量删除
            return this.updateBatchById(fileList);
        } catch (Exception ex) {
            throw new PugValidatorException(ResultStatusEnum.SERVER_DB_ERROR);
        }
    }
    @Override
    public boolean delBatchFile2(String ids) {
        try {
            // 1 : 把数据分割
            String[] strings = ids.split(",");
            // 2: 组装成一个List<Ecgpatients>
            List<Long> integers = Arrays.stream(strings).map(idstr -> {
                return new Long(idstr);
            }).collect(Collectors.toList());

            // 3: 批量删除
            return fileService.removeByIds(integers);
        } catch (Exception ex) {
            throw new PugValidatorException(ResultStatusEnum.SERVER_DB_ERROR);
        }
    }

}