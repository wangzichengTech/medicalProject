package com.pug.zixun.service.resources;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pug.zixun.bo.ResourcesBo;
import com.pug.zixun.pojo.Resources;
import com.pug.zixun.service.BaseService;
import com.pug.zixun.vo.ResourcesVo;

import java.util.List;

/**
 * IResourcesService接口
 * 创建人:yykk<br/>
 * 时间：2022-07-19 23:58:26 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
public interface IResourcesService extends IService<Resources>,BaseService{


    /**
     * 查询资源库列表信息
     * @method: findResourcesList
     * @result : List<ResourcesBo>
     * 创建人:yykk
     * 创建时间：2022-07-19 23:58:26
     * @version 1.0.0
     * @return
     */
    List<ResourcesBo> findResourcesList() ;

	/**
     * 查询资源库列表信息并分页
     * 方法名：findResourcess<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-19 23:58:26<br/>
     * @param resourcesVo
     * @return IPage<Resources><br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
	IPage<ResourcesBo> findResourcesPage(ResourcesVo resourcesVo);

    /**
     * 保存&修改资源库
     * 方法名：saveupdateResources<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-19 23:58:26<br/>
     * @param resources 
     * @return Resources<br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    ResourcesBo saveupdateResources(Resources resources);

    /**
     * 根据Id删除资源库
     * 方法名：deleteResourcesById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-19 23:58:26<br/>
     * @param id
     * @return int <br />
     * @throws <br/>
     * @since 1.0.0<br />
     */
    int deleteResourcesById(Long id) ;

    /**
     * 根据Id查询资源库明细信息
     * 方法名：getResourcesById<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-19 23:58:26<br/>
     * @param id
     * @return Resources <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    ResourcesBo getResourcesById(Long id);

    /**
     * 根据资源库ids批量删除资源库
     * 方法名：delBatchResources<br/>
     * 创建人：yykk <br/>
     * 时间：2022-07-19 23:58:26<br/>
     * @param ids
     * @return boolean <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    boolean delBatchResources(String ids);

}