package com.pug.zixun.service.adminlogs;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pug.zixun.mapper.AdminLogsMapper;
import com.pug.zixun.pojo.AdminLogs;
import com.pug.zixun.vo.AdminLogsVo;
import com.pug.zixun.bo.AdminLogsBo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import com.pug.zixun.commons.enums.ResultStatusEnum;
import com.pug.zixun.commons.ex.PugValidatorException;
import com.pug.zixun.commons.utils.fn.asserts.Vsserts;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;



/**
 * AdminLogsServiceImpl实现类
 * 创建人:yykk<br/>
 * 时间：2022-07-26 20:20:25 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Service
@Slf4j
public class AdminLogsServiceImpl extends ServiceImpl<AdminLogsMapper,AdminLogs> implements IAdminLogsService  {

    @Resource
    private IAdminLogsService adminLogsService;
    @Override
	public IPage<AdminLogsBo> findAdminLogsPage(AdminLogsVo adminlogsVo){
	    // 设置分页信息
		Page<AdminLogs> page = new Page<>(adminlogsVo.getPageNo(),adminlogsVo.getPageSize());
		// 设置查询条件
        LambdaQueryWrapper<AdminLogs> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // lambdaQueryWrapper.select(AdminLogs.class, column -> !column.getColumn().equals("description"));
        // 根据关键词搜索信息
        lambdaQueryWrapper.like(Vsserts.isNotEmpty(adminlogsVo.getKeyword()), AdminLogs::getMethodname,adminlogsVo.getKeyword());
        // 根据关键词搜索信息
        lambdaQueryWrapper.like(Vsserts.isNotEmpty(adminlogsVo.getKeyword()), AdminLogs::getClassname,adminlogsVo.getKeyword());
        // 根据关键词搜索信息
        lambdaQueryWrapper.like(Vsserts.isNotEmpty(adminlogsVo.getKeyword()), AdminLogs::getUsername,adminlogsVo.getKeyword());
        // 根据时间排降序
        lambdaQueryWrapper.orderByDesc(AdminLogs::getCreateTime);
        // 查询分页返回
		IPage<AdminLogs> results = this.page(page,lambdaQueryWrapper);
		return tranferPageBo(results,AdminLogsBo.class);
	}

    /**
     * 查询后台操作日志列表信息
     * @method: findAdminLogsList
     * @result : List<AdminLogs>
     * 创建人:yykk
     * 创建时间：2022-07-26 20:20:25
     * @version 1.0.0
     * @return
    */
    @Override
    public List<AdminLogsBo> findAdminLogsList() {
     	// 1：设置查询条件
        LambdaQueryWrapper<AdminLogs> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // 2：查询发布的 0 未发布  1 发布
        // 3: 查询返回
        return tranferListBo(this.list(lambdaQueryWrapper),AdminLogsBo.class);
    }

	/**
     * 根据id查询后台操作日志明细信息
     * @param id
     * @return AdminLogs
     * 创建人:yykk
     * 创建时间：2022-07-26 20:20:25
     * @version 1.0.0
     */
    @Override
    public AdminLogsBo getAdminLogsById(Long id) {
        return tranferBo(this.getById(id),AdminLogsBo.class);
    }


    /**
     * 保存&修改后台操作日志
     * @param adminlogs
     * @return AdminLogs
     * 创建人:yykk
     * 创建时间：2022-07-26 20:20:25
     * @version 1.0.0
     */
    @Override
	public AdminLogsBo saveupdateAdminLogs(AdminLogs adminlogs){
		boolean flag = this.saveOrUpdate(adminlogs);
		return flag ? tranferBo(adminlogs,AdminLogsBo.class)  : null;
	}




    /**
     * 根据id删除
     * @param ids
     * @return boolean
     * 创建人:yykk
     * 创建时间：2022-07-26 20:20:25
     * @version 1.0.0
     */
    @Override
    public boolean delBatchAdminLogs(String ids) {
        try {
            // 1 : 把数据分割
            String[] strings = ids.split(",");
            List<Long> Longs = Arrays.stream(strings).map(idstr -> {
                return new Long(idstr);
            }).collect(Collectors.toList());

            // 3: 批量删除
            return adminLogsService.removeByIds(Longs);
        } catch (Exception ex) {
            throw new PugValidatorException(ResultStatusEnum.SERVER_DB_ERROR);
        }
    }


}