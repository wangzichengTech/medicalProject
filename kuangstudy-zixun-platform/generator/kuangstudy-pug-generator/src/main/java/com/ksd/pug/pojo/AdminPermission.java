package com.ksd.pug.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * AdminPermission实体
 * 创建人:yykk<br/>
 * 时间：2022-07-17 18:38:05 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AdminPermission implements java.io.Serializable {

    private String name;
    private Integer sorted;
    private String path;
    private String icon;
    private Integer status;
    private Date createTime;
    private Date updateTime;
    private Long pid;
    private String pathname;
    private String component;
    //1菜单 2 权限
    private Integer type;
    //权限代号
    private String code;
}