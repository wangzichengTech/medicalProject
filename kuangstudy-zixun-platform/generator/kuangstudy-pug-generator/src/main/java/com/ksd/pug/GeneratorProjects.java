package com.ksd.pug;


import com.ksd.pug.generator.config.PugDataSourceConfig;
import com.ksd.pug.generator.config.PugDataTableSourceConfig;
import com.ksd.pug.generator.config.PugGlobalConfig;
import com.ksd.pug.generator.config.PugPackageConfig;
import com.ksd.pug.pojo.AdminPermission;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 飞哥B站地址：https://space.bilibili.com/490711252
 * 记得关注和三连哦！
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2022/2/13$ 19:11$
 */
public class GeneratorProjects {

    public static void main(String[] args) {

        String tablename = "kss_models_category";
        String model = "ModelsCategory";
        String title = "模型分类管理";

        // 初始化数据源
        PugDataSourceConfig dataSourceConfig = PugDataSourceConfig.builder()
                .url("jdbc:mysql://127.0.0.1:3306/kss-zixun-db?serverTimezone=GMT%2b8&useUnicode=true&characterEncoding=utf-8&useSSL=false")
                .username("root")
                .password("123456").build();
        // 获取数据表信息
        PugDataTableSourceConfig pugDataTableSourceConfig = PugDataTableSourceConfig.builder().tablename(tablename).buildTableInfo(dataSourceConfig);
        // 查询所有的表信息
        //PugDataTableSourceConfig pugDataTableSourceConfig1 = PugDataTableSourceConfig.builder().buildTables(dataSourceConfig);
        // 全局设置
        PugGlobalConfig pugGlobalConfig = PugGlobalConfig.builder()
                .isoverride()
                .author("yykk")
                .gitlink("前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git")
                .url("后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git")
                .title(title)
                .commentDate("yyyy-MM-dd HH:mm:ss")
                .version("1.0.0")
                .port("8877")
                .redisport("6379")
                .bootversion("2.5.8")
                .apititle("在线接口文档")
                .apidesc("在线接口文档我是一个描述")
                .apimemebers("yykk,kuangshen")
                .apiversion("1.0.0")
                .apiurl("http://localhost:8877/admin")
                .build();

        // 开始设置实体名称和包
        PugPackageConfig pugPackageConfig = PugPackageConfig.builder()
                .dataSourceConfig(dataSourceConfig)
                .dataTableSourceConfig(pugDataTableSourceConfig)
                .globalConfig(pugGlobalConfig)
                .outputDir()
                .model(model)
                .istree(false)
                .entity("pojo").template("pojo.tml").projectName("/domain/kuangstudy-zixun-domain").rootPackage("com.pug.zixun").compareEnity().create()
                .bo("bo").template("bo.tml").projectName("/domain/kuangstudy-zixun-domain").rootPackage("com.pug.zixun").compareBo().create()
                .mapper("mapper").template("mapper.tml").projectName("/mapper/kuangstudy-zixun-mapper").rootPackage("com.pug.zixun").compareMapper().create()
                .mapperxml("mapper").template("mapperxml.tml").projectName("/mapper/kuangstudy-zixun-mapper").compareMapperXml().create()
                .vo("vo").template("vo.tml").projectName("/domain/kuangstudy-zixun-domain").rootPackage("com.pug.zixun").compareVo().create()
                .service("service").template("service2.tml").projectName("/service/kuangstudy-zixun-service").rootPackage("com.pug.zixun").compareService().create()
                .serviceImpl("service").template("serviceImpl2.tml").projectName("/service/kuangstudy-zixun-service").rootPackage("com.pug.zixun").compareServiceImpl().create()
                .controller("controller").template("controller2.tml").projectName("/platform/kuangstudy-zixun-admin-api").rootPackage("com.pug.zixun").compareController().create()
                .build();


        // 构建前端
        PugPackageConfig frontPackageConfig = PugPackageConfig.builder()
                .dataSourceConfig(dataSourceConfig)
                .dataTableSourceConfig(pugDataTableSourceConfig)
                .globalConfig(pugGlobalConfig)
                .outputDir("D:\\系统默认\\桌面\\桌面文件夹\\学相伴项目\\第十七课：项目实战开放 - 自动构建初探和项目菜单的规划和调整\\源码\\")
                .model(model)
                .istree(false)
                .listPage().template("/front/ListPage.tml").projectName("/ksd-web-admin/src/views").copareListPage("List.vue").create()
                .listPage().template("/front/ListJs.tml").projectName("/ksd-web-admin/src/services").copareListJs().create()
                .build();


        // 含子类
        insertPermissionAll(dataSourceConfig.getConn(), "模型分类信息","ModelsCategory", "MC" ,"Book", 18,"ModelsCategory/List.vue");
        // 单级
        //insertPermissionSingle(dataSourceConfig.getConn(), "文章","questions", "W" ,"Book", 13,"questions/List.vue");
        List<AdminPermission> adminPermission = findAdminPermission(dataSourceConfig.getConn());
        // 构建前端
        PugPackageConfig frontPackageConfig2 = PugPackageConfig.builder()
                .dataSourceConfig(dataSourceConfig)
                .dataTableSourceConfig(pugDataTableSourceConfig)
                .globalConfig(pugGlobalConfig)
                .outputDir("D:\\系统默认\\桌面\\桌面文件夹\\学相伴项目\\第十七课：项目实战开放 - 自动构建初探和项目菜单的规划和调整\\源码\\")
                .model(model)
                .istree(false)
                .permissions(adminPermission)
                .listPage().template("/front/ListPage.tml").projectName("/ksd-web-admin/src/views").copareListPage("List.vue").create()
                .listPage().template("/front/ListJs.tml").projectName("/ksd-web-admin/src/services").copareListJs().create()
                .routerJs().template("/front/dynamic.tml").projectName("/ksd-web-admin/src/router").copareRouterJs("dynamic.js").create().build();



    }

    // 查询所有的权限菜单信息，自动生成路由
    public static List<AdminPermission> findAdminPermission(Connection connection){
        try {
            String sql = "SELECT * FROM kss_permission WHERE type =1 and path is not null";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            List<AdminPermission> adminPermissionList = new ArrayList<>();
            while(rs.next()){
                AdminPermission adminPermission = new AdminPermission();
                adminPermission.setName(rs.getString("name"));
                adminPermission.setComponent(rs.getString("component"));
                adminPermission.setPath(rs.getString("path"));
                adminPermission.setIcon(rs.getString("icon"));
                adminPermission.setCode(rs.getString("code"));
                adminPermissionList.add(adminPermission);
            }
            rs.close();
            statement.close();
            connection.close();
            return adminPermissionList;
        }catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }

    // 批量保存菜单
    public static void insertPermissionAll(Connection connection,
                                           String ctitle, String cmodel, String prefix,String icon, Integer sorted,String component) {

        try {
            Long parentId = System.currentTimeMillis();
            Long id1 = parentId+1;
            Long id2 = parentId+2;
            Long id3 = parentId+3;
            Long id4 = parentId+4;
            Long id5 = parentId+5;
            Long id6 = parentId+6;
            Long id7 = parentId+7;
            Long id8 = parentId+8;



            String batchInsertSql = "INSERT INTO kss_permission\n" +
                    "(id,name,sorted,path,icon,status,create_time,update_time,pid,pathname,isdelete,type,code,component)\n" +
                    "VALUES\n" +
                    "(" + parentId + ",'" + ctitle + "管理'," + sorted + ",null,'" + icon + "',1,now(),now(),0,null,0,1,'" + prefix + "10001','')," +
                    "(" + (id1) + ",'" + ctitle + "管理',1,'/"+cmodel+"/list','" + icon + "',1,now(),now()," + parentId + ",'/"+cmodel+"/list',0,1,'" + prefix + "10002','"+component+"')," +
                    "(" + (id2) + ",'" + ctitle + "搜索',2,'','" + icon + "',1,now(),now()," + parentId + ",'',0,2,'" + prefix + "10003','')," +
                    "(" + (id3) + ",'" + ctitle + "添加',3,'','" + icon + "',1,now(),now()," + parentId + ",'',0,2,'" + prefix + "10004','')," +
                    "(" + (id4) + ",'" + ctitle + "更新',4,'','" + icon + "',1,now(),now()," + parentId + ",'',0,2,'" + prefix + "10005','')," +
                    "(" + (id5) + ",'" + ctitle + "删除',5,'','" + icon + "',1,now(),now()," + parentId + ",'',0,2,'" + prefix + "10006','')," +
                    "(" + (id6) + ",'" + ctitle + "批量删除',6,'','" + icon + "',1,now(),now()," + parentId + ",'',0,2,'" + prefix + "10007','')," +
                    "(" + (id7) + ",'" + ctitle + "分页搜索',7,'','" + icon + "',1,now(),now()," + parentId + ",'',0,2,'" + prefix + "10008','')," +
                    "(" + (id8) + ",'" + ctitle + "明细查询',8,'','" + icon + "',1,now(),now()," + parentId + ",'',0,2,'" + prefix + "10009','')";

            String batchInsertSql2 = "INSERT INTO kss_roles_permission (role_id,permission_id) values " +
                    "(1," + parentId + ")," +
                    "(1," + id1 + ")," +
                    "(1," + id2 + ")," +
                    "(1," + id3 + ")," +
                    "(1," + id4 + ")," +
                    "(1," + id5 + ")," +
                    "(1," + id6 + ")," +
                    "(1," + id7 + ")," +
                    "(1," + id8 + ")";


            PreparedStatement statement = connection.prepareStatement(batchInsertSql);
            PreparedStatement statement1 = connection.prepareStatement(batchInsertSql2);
            statement.executeUpdate();
            statement1.executeUpdate();

            statement1.close();
            statement.close();
            connection.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    // 保存单个菜单
    public static void insertPermissionSingle(Connection connection,
                                              String ctitle, String cmodel, String prefix,String icon, Integer sorted,
             String component) {

        try {
            Long parentId = System.currentTimeMillis();


            String batchInsertSql = "INSERT INTO kss_permission\n" +
                    "(id,name,sorted,path,icon,status,create_time,update_time,pid,pathname,isdelete,type,code)\n" +
                    "VALUES\n" +
                    "(" + (parentId) + ",'" + ctitle + "管理',1,'/"+cmodel+"/list','" + icon + "',1,now(),now()," + parentId + ",'/"+cmodel+"/list',0,1,'" + prefix + "10002','"+component+"')" ;

            String batchInsertSql2 = "INSERT INTO kss_roles_permission (role_id,permission_id) values " +
                    "(1," + parentId + ")";

            PreparedStatement statement = connection.prepareStatement(batchInsertSql);
            PreparedStatement statement1 = connection.prepareStatement(batchInsertSql2);
            statement.executeUpdate();
            statement1.executeUpdate();

            statement1.close();
            statement.close();
            connection.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}