package com.ksd.pug;


import com.ksd.pug.generator.config.PugDataSourceConfig;
import com.ksd.pug.generator.config.PugDataTableSourceConfig;
import com.ksd.pug.generator.config.PugGlobalConfig;
import com.ksd.pug.generator.config.PugPackageConfig;

import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 飞哥B站地址：https://space.bilibili.com/490711252
 * 记得关注和三连哦！
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2022/2/13$ 19:11$
 */
public class GeneratorProjects2 {

    public static void main(String[] args) {

        String tablename = "kss_ct";
        String model = "CT";
        String title = "CT信息管理";

        // 初始化数据源
        PugDataSourceConfig dataSourceConfig = PugDataSourceConfig.builder()
                .url("jdbc:mysql://127.0.0.1:3306/kss-zixun-db?serverTimezone=GMT%2b8&useUnicode=true&characterEncoding=utf-8&useSSL=false")
                .username("root")
                .password("123456").build();
        // 获取数据表信息
        PugDataTableSourceConfig pugDataTableSourceConfig = PugDataTableSourceConfig.builder().tablename(tablename).buildTableInfo(dataSourceConfig);
        // 查询所有的表信息
        //PugDataTableSourceConfig pugDataTableSourceConfig1 = PugDataTableSourceConfig.builder().buildTables(dataSourceConfig);
        // 全局设置
        PugGlobalConfig pugGlobalConfig = PugGlobalConfig.builder()
                .isoverride()
                .author("yykk")
                .gitlink("前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git")
                .url("后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git")
                .title(title)
                .commentDate("yyyy-MM-dd HH:mm:ss")
                .version("1.0.0")
                .port("8877")
                .redisport("6379")
                .redisip("120.77.34.190")
                .redispwd("mkxiaoer1986.")
                .bootversion("2.5.8")
                .apititle("在线接口文档")
                .apidesc("在线接口文档我是一个描述")
                .apimemebers("yykk,wzc")
                .apiversion("1.0.0")
                .apiurl("http://localhost:8877/admin")
                .build();

        // 开始设置实体名称和包
        PugPackageConfig pugPackageConfig = PugPackageConfig.builder()
                .dataSourceConfig(dataSourceConfig)
                .dataTableSourceConfig(pugDataTableSourceConfig)
                .globalConfig(pugGlobalConfig)
                .outputDir()
                .model(model)
                .istree(false)
                .entity("pojo").template("pojo.tml").projectName("xq_ten_minute_app").rootPackage("com.xq.app").compareEnity().create()
                .bo("bo").template("bo.tml").projectName("xq_ten_minute_app").rootPackage("com.xq.app").compareBo().create()
                .mapper("mapper").template("mapper.tml").projectName("xq_ten_minute_app").rootPackage("com.xq.app").compareMapper().create()
                .mapperxml("mapper").template("mapperxml.tml").projectName("xq_ten_minute_app").compareMapperXml().create()
                .vo("vo").template("vo.tml").projectName("xq_ten_minute_app").rootPackage("com.xq.app").compareVo().create()
                .service("service").template("service2.tml").projectName("xq_ten_minute_app").rootPackage("com.xq.app").compareService().create()
                .serviceImpl("service").template("serviceImpl2.tml").projectName("xq_ten_minute_app").rootPackage("com.xq.app").compareServiceImpl().create()
                .controller("controller").template("controller2.tml").projectName("xq_ten_minute_app").rootPackage("com.xq.app").compareController().create().build();


    }
}
