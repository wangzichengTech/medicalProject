package com.ksd.pug;


import com.ksd.pug.generator.config.PugDataSourceConfig;
import com.ksd.pug.generator.config.PugDataTableSourceConfig;
import com.ksd.pug.generator.config.PugGlobalConfig;
import com.ksd.pug.generator.config.PugPackageConfig;
import com.ksd.pug.pojo.AdminPermission;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.*;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 飞哥B站地址：https://space.bilibili.com/490711252
 * 记得关注和三连哦！
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2022/2/13$ 19:11$
 */
public class GeneratorProjects3 {

    public static void main(String[] args) {

        String tablename = "kss_music";
        String model = "Music";
        String title = "音乐管理";

        // 初始化数据源
        PugDataSourceConfig dataSourceConfig = PugDataSourceConfig.builder()
                .url("jdbc:mysql://127.0.0.1:3306/kss-zixun-db?serverTimezone=GMT%2b8&useUnicode=true&characterEncoding=utf-8&useSSL=false")
                .username("root")
                .password("123456").build();


        // 获取数据表信息
        PugDataTableSourceConfig pugDataTableSourceConfig = PugDataTableSourceConfig.builder().tablename(tablename).buildTableInfo(dataSourceConfig);
        // 查询所有的表信息
        //PugDataTableSourceConfig pugDataTableSourceConfig1 = PugDataTableSourceConfig.builder().buildTables(dataSourceConfig);
        // 全局设置
        PugGlobalConfig pugGlobalConfig = PugGlobalConfig.builder()
                .isoverride()
                .author("yykk")
                .gitlink("前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git")
                .url("后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git")
                .title(title)
                .commentDate("yyyy-MM-dd HH:mm:ss")
                .version("1.0.0")
                .port("8877")
                .redisport("6379")
                .redisip("120.77.34.190")
                .redispwd("mkxiaoer1986.")
                .bootversion("2.5.8")
                .apititle("学相伴在线接口文档")
                .apidesc("学相伴在线接口文档我是一个描述")
                .apimemebers("yykk,kuangshen")
                .apiversion("1.0.0")
                .apiurl("http://localhost:8877/admin")
                .build();

        // 构建前端
        PugPackageConfig frontPackageConfig = PugPackageConfig.builder()
                .dataSourceConfig(dataSourceConfig)
                .dataTableSourceConfig(pugDataTableSourceConfig)
                .globalConfig(pugGlobalConfig)
                .outputDir("D:\\2022年独立特训营第一期\\10、实战开发 - Vue3+Vite + 实战开发后台管理系统\\第十四课：实战开发-关于产品的相关操作和AOP日志拦截统一处理\\源码\\")
                .model(model)
                .istree(false)
                .listPage().template("/front/ListPage.tml").projectName("/ksd-web-admin/src/views").copareListPage("List.vue").create()
                .listPage().template("/front/ListJs.tml").projectName("/ksd-web-admin/src/services").copareListJs().create()
                .build();



        // 批量保存权限和角色的绑定
        insertPermission(dataSourceConfig.getConn(),"笔记","Z","Burger",12);

    }


    public static void insertPermission(Connection connection,
                                        String ctitle, String prefix, String icon, Integer sorted) {

        try {
            Long parentId = System.currentTimeMillis();
            String batchInsertSql = "INSERT INTO kss_permission\n" +
                    "(id,name,sorted,path,icon,status,create_time,update_time,pid,pathname,isdelete,type,code)\n" +
                    "VALUES\n" +
                    "("+parentId+",'"+ctitle+"管理',"+sorted+",'/note/list','"+icon+"',1,now(),now(),0,'/note/list',0,1,'"+prefix+"10001')," +
                    "("+(parentId+1)+",'"+ctitle+"管理',1,'/note/list','"+icon+"',1,now(),now(),"+parentId+",'/note/list',0,1,'"+prefix+"10002'),"+
                    "("+(parentId+2)+",'"+ctitle+"搜索',2,'','"+icon+"',1,now(),now(),"+parentId+",'',0,2,'"+prefix+"10003'),"+
                    "("+(parentId+3)+",'"+ctitle+"添加',3,'','"+icon+"',1,now(),now(),"+parentId+",'',0,2,'"+prefix+"10004'),"+
                    "("+(parentId+4)+",'"+ctitle+"更新',4,'','"+icon+"',1,now(),now(),"+parentId+",'',0,2,'"+prefix+"10005'),"+
                    "("+(parentId+5)+",'"+ctitle+"删除',5,'','"+icon+"',1,now(),now(),"+parentId+",'',0,2,'"+prefix+"10006'),"+
                    "("+(parentId+6)+",'"+ctitle+"批量删除',6,'','"+icon+"',1,now(),now(),"+parentId+",'',0,2,'"+prefix+"10007'),"+
                    "("+(parentId+7)+",'"+ctitle+"分页搜索',7,'','"+icon+"',1,now(),now(),"+parentId+",'',0,2,'"+prefix+"10008'),"+
                    "("+(parentId+8)+",'"+ctitle+"明细查询',8,'','"+icon+"',1,now(),now(),"+parentId+",'',0,2,'"+prefix+"10009')";

            String batchInsertSql2 = "INSERT INTO kss_roles_permission (role_id,permission_id) values " +
                    "(1,"+parentId+")," +
                    "(1,"+parentId+1+")," +
                    "(1,"+parentId+2+")," +
                    "(1,"+parentId+3+")," +
                    "(1,"+parentId+4+")," +
                    "(1,"+parentId+5+")," +
                    "(1,"+parentId+6+")," +
                    "(1,"+parentId+7+")," +
                    "(1,"+parentId+8+")";


            PreparedStatement statement = connection.prepareStatement(batchInsertSql);
            PreparedStatement statement1 = connection.prepareStatement(batchInsertSql2);
            statement.executeUpdate();
            statement1.executeUpdate();

            statement1.close();
            statement.close();
            connection.close();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

}
