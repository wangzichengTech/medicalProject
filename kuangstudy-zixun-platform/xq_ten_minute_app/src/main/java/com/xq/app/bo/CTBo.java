package com.xq.app.bo;

import java.util.Date;
import lombok.*;
import org.pug.generator.anno.PugDoc;

/**
 * CT实体
 * 创建人:yykk<br/>
 * 时间：2023-02-17 21:53:53 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CTBo  implements java.io.Serializable {

    @PugDoc(name="CTID")
    private Long id;
    @PugDoc(name="序列号")
    private String se;
    @PugDoc(name="第几个图片")
    private String im;
    @PugDoc(name="切片厚度")
    private String thick;
    @PugDoc(name="检查日期")
    private String examinedate;
    @PugDoc(name="检查时间")
    private String examinetime;
    @PugDoc(name="医院")
    private String hospital;
    @PugDoc(name="姓名")
    private String name;
    @PugDoc(name="性别")
    private String sex;
    @PugDoc(name="年龄")
    private String age;
    @PugDoc(name="CT图片绝对路径")
    private String imagepath;
    @PugDoc(name="逻辑删除 0存在  id删除")
    private Integer isdelete;
    @PugDoc(name="发布时间")
    private Date createTime;
    @PugDoc(name="更新时间")
    private Date updateTime;
}