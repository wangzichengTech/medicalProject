package com.xq.app.service.ct;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xq.app.mapper.CTMapper;
import com.xq.app.pojo.CT;
import com.xq.app.vo.CTVo;
import com.xq.app.bo.CTBo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import com.xq.app.commons.enums.ResultStatusEnum;
import com.xq.app.commons.ex.PugValidatorException;
import com.xq.app.commons.utils.fn.asserts.Vsserts;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;



/**
 * CTServiceImpl实现类
 * 创建人:yykk<br/>
 * 时间：2023-02-17 21:53:53 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Service
@Slf4j
public class CTServiceImpl extends ServiceImpl<CTMapper,CT> implements ICTService  {

    /**
     * 查询分页&搜索CT信息管理
     * @param ctVo
     * @return IPage<CT>
     * 创建人:yykk
     * 创建时间：2023-02-17 21:53:53
     * @version 1.0.0
     */
    @Override
	public IPage<CTBo> findCTPage(CTVo ctVo){
	    // 设置分页信息
		Page<CT> page = new Page<>(ctVo.getPageNo(),ctVo.getPageSize());
		// 设置查询条件
        LambdaQueryWrapper<CT> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // lambdaQueryWrapper.select(CT.class, column -> !column.getColumn().equals("description"));
        // 根据关键词搜索信息
        lambdaQueryWrapper.like(Vsserts.isNotEmpty(ctVo.getKeyword()), CT::getName,ctVo.getKeyword());
        // 多列搜索
        // lambdaQueryWrapper.and(Vsserts.isNotEmpty(ctVo.getKeyword()),wrapper -> wrapper
        //         .like(CT::getName, ctVo.getKeyword())
        //         .or()
        //         .like(CT::getCategoryName, ctVo.getKeyword())
        // );
        // 根据时间排降序
        lambdaQueryWrapper.orderByDesc(CT::getCreateTime);
        // 查询分页返回
		IPage<CT> results = this.page(page,lambdaQueryWrapper);
		return tranferPageBo(results,CTBo.class);
	}

    /**
     * 查询CT信息管理列表信息
     * @method: findCTList
     * @result : List<CT>
     * 创建人:yykk
     * 创建时间：2023-02-17 21:53:53
     * @version 1.0.0
     * @return
    */
    @Override
    public List<CTBo> findCTList() {
     	// 1：设置查询条件
        LambdaQueryWrapper<CT> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // 2：查询发布的 0 未发布  1 发布
        lambdaQueryWrapper.eq(CT::getStatus,1);
        lambdaQueryWrapper.eq(CT::getIsdelete,0);
        // 3: 查询返回
        return tranferListBo(this.list(lambdaQueryWrapper),CTBo.class);
    }

	/**
     * 根据id查询CT信息管理明细信息
     * @param id
     * @return CT
     * 创建人:yykk
     * 创建时间：2023-02-17 21:53:53
     * @version 1.0.0
     */
    @Override
    public CTBo getCTById(Long id) {
        return tranferBo(this.getById(id),CTBo.class);
    }


    /**
     * 保存&修改CT信息管理
     * @param ct
     * @return CT
     * 创建人:yykk
     * 创建时间：2023-02-17 21:53:53
     * @version 1.0.0
     */
    @Override
	public CTBo saveupdateCT(CT ct){
		boolean flag = this.saveOrUpdate(ct);
		return flag ? tranferBo(ct,CTBo.class)  : null;
	}


    /**
     * 根据id删除CT信息管理
     * @param id
     * @return int
     * 创建人:yykk
     * 创建时间：2023-02-17 21:53:53
     * @version 1.0.0
     */
    @Override
    public int deleteCTById(Long id) {
        CT adminRole = this.getById(id);
        if (adminRole != null) {
            CT ct2 = new CT();
            ct2.setId(id);
            ct2.setIsdelete(1);
            boolean b = this.updateById(ct2);
            return 1;
        }
        return 0;
    }

    /**
     * 根据id删除
     * @param ids
     * @return boolean
     * 创建人:yykk
     * 创建时间：2023-02-17 21:53:53
     * @version 1.0.0
     */
    @Override
    public boolean delBatchCT(String ids) {
        try {
            // 1 : 把数据分割
            String[] strings = ids.split(",");
            // 2: 组装成一个List<CT>
            List<CT> ctList = Arrays.stream(strings).map(idstr -> {
                CT ct = new CT();
                ct.setId(new Long(idstr));
                ct.setIsdelete(1);
                return ct;
            }).collect(Collectors.toList());
            // 3: 批量删除
            return this.updateBatchById(ctList);
        } catch (Exception ex) {
            throw new PugValidatorException(ResultStatusEnum.SERVER_DB_ERROR);
        }
    }


}