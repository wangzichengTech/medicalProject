package com.xq.app.service.ct;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xq.app.pojo.CT;
import com.xq.app.vo.CTVo;
import com.xq.app.bo.CTBo;
import com.xq.app.service.BaseService;
import java.util.List;

/**
 * ICTService接口
 * 创建人:yykk<br/>
 * 时间：2023-02-17 21:53:53 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
public interface ICTService extends IService<CT>,BaseService{


    /**
     * 查询CT信息管理列表信息
     * @method: findCTList
     * @result : List<CTBo>
     * 创建人:yykk
     * 创建时间：2023-02-17 21:53:53
     * @version 1.0.0
     * @return
     */
    List<CTBo> findCTList() ;

	/**
     * 查询CT信息管理列表信息并分页
     * 方法名：findCTs<br/>
     * 创建人：yykk <br/>
     * 时间：2023-02-17 21:53:53<br/>
     * @param ctVo
     * @return IPage<CT><br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
	IPage<CTBo> findCTPage(CTVo ctVo);

    /**
     * 保存&修改CT信息管理
     * 方法名：saveupdateCT<br/>
     * 创建人：yykk <br/>
     * 时间：2023-02-17 21:53:53<br/>
     * @param ct 
     * @return CT<br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    CTBo saveupdateCT(CT ct);

    /**
     * 根据Id删除CT信息管理
     * 方法名：deleteCTById<br/>
     * 创建人：yykk <br/>
     * 时间：2023-02-17 21:53:53<br/>
     * @param id
     * @return int <br />
     * @throws <br/>
     * @since 1.0.0<br />
     */
    int deleteCTById(Long id) ;

    /**
     * 根据Id查询CT信息管理明细信息
     * 方法名：getCTById<br/>
     * 创建人：yykk <br/>
     * 时间：2023-02-17 21:53:53<br/>
     * @param id
     * @return CT <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    CTBo getCTById(Long id);

    /**
     * 根据CT信息管理ids批量删除CT信息管理
     * 方法名：delBatchCT<br/>
     * 创建人：yykk <br/>
     * 时间：2023-02-17 21:53:53<br/>
     * @param ids
     * @return boolean <br />
     * @throws <br/>
     * @since 1.0.0<br />
    */
    boolean delBatchCT(String ids);

}