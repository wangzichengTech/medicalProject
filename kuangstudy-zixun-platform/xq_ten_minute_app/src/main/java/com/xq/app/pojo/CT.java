package com.xq.app.pojo;

import java.util.Date;
import lombok.*;
import com.baomidou.mybatisplus.annotation.*;

import org.pug.generator.anno.PugDoc;
/**
 * CT实体
 * 创建人:yykk<br/>
 * 时间：2023-02-17 21:53:53 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("kss_ct")
public class CT  implements java.io.Serializable {

    @PugDoc(name="CTID")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;
    @PugDoc(name="序列号")
    private String se;
    @PugDoc(name="第几个图片")
    private String im;
    @PugDoc(name="切片厚度")
    private String thick;
    @PugDoc(name="检查日期")
    private String examinedate;
    @PugDoc(name="检查时间")
    private String examinetime;
    @PugDoc(name="医院")
    private String hospital;
    @PugDoc(name="姓名")
    private String name;
    @PugDoc(name="性别")
    private String sex;
    @PugDoc(name="年龄")
    private String age;
    @PugDoc(name="CT图片绝对路径")
    private String imagepath;
    @PugDoc(name="逻辑删除 0存在  id删除")
    private Integer isdelete;
    @PugDoc(name="发布时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @PugDoc(name="更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

}