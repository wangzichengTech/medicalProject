package com.xq.app.mapper;

import com.xq.app.pojo.CT;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * CTMapper
 * 创建人:yykk<br/>
 * 时间：2023-02-17 21:53:53 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/
public interface CTMapper extends BaseMapper<CT>{
}