package com.xq.app.controller.ct;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xq.app.service.ct.ICTService;
import com.xq.app.pojo.CT;
import com.xq.app.vo.CTVo;
import com.xq.app.bo.CTBo;
import com.xq.app.commons.enums.ResultStatusEnum;
import com.xq.app.commons.ex.PugValidatorException;
import com.xq.app.commons.utils.fn.asserts.Vsserts;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import com.xq.app.controller.BaseController;
import java.util.List;
import org.pug.generator.anno.PugDoc;
/**
 * CTController
 * 创建人:yykk<br/>
 * 时间：2023-02-17 21:53:53 <br/>
 * 源码下载：前台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-ui.git
 * 飞哥B站地址：后台代码 git clone https://gitee.com/kekesam/kuangstudy-pug-parent.git
 * @version 1.0.0<br/>
 *
*/

@RestController
@RequiredArgsConstructor
@Slf4j
@PugDoc(name="CT信息管理",tabname="kss_ct")
public class CTController extends BaseController{

    private final ICTService ctService;


    /**
     * 查询CT信息管理列表信息
     * @path : /admin/ct/list
     * @method: findCTs
     * @result : List<CTBo>
     * 创建人:yykk
     * 创建时间：2023-02-17 21:53:53
     * @version 1.0.0
     * @return
     */
    @PostMapping("/ct/list")
    @PugDoc(name="查询CT信息管理列表信息")
    public List<CTBo> findCTList() {
        return ctService.findCTList();
    }

	/**
	 * 查询CT信息管理列表信息并分页
	 * @path : /admin/ct/load
     * @method: findCTs
     * @param : ctVo
     * @result : IPage<CTBo>
	 * 创建人:yykk
	 * 创建时间：2023-02-17 21:53:53
	 * @version 1.0.0
	*/
    @PostMapping("/ct/load")
    @PugDoc(name="查询CT信息管理列表信息并分页")
    public IPage<CTBo> findCTs(@RequestBody CTVo ctVo) {
        return ctService.findCTPage(ctVo);
    }


    /**
     * 根据CT信息管理id查询明细信息
     * @method: get/{id}
     * @path : /admin/ct/get/{id}
     * @param : id
     * @result : CTBo
     * 创建人:yykk
     * 创建时间：2023-02-17 21:53:53
     * @version 1.0.0
    */
    @PostMapping("/ct/get/{id}")
    @PugDoc(name="根据CT信息管理id查询明细信息")
    public CTBo getCTById(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
           throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return ctService.getCTById(new Long(id));
    }


	/**
	 * 保存和修改CT信息管理
     * @method: saveupdate
	 * @path : /admin/ct/save
     * @param : ct
     * @result : CTBo
	 * 创建人:yykk
	 * 创建时间：2023-02-17 21:53:53
	 * @version 1.0.0
	*/
    @PostMapping("/ct/saveupdate")
    @PugDoc(name="保存和修改CT信息管理")
    public CTBo saveupdateCT(@RequestBody CT ct) {
        return ctService.saveupdateCT(ct);
    }


    /**
	 * 根据CT信息管理id删除CT信息管理
     * @method: delete/{id}
     * @path : /admin/ct/delete/{id}
     * @param : id
     * @result : int
	 * 创建人:yykk
	 * 创建时间：2023-02-17 21:53:53
	 * @version 1.0.0
	*/
    @PostMapping("/ct/delete/{id}")
    @PugDoc(name="根据CT信息管理id删除CT信息管理")
    public int deleteCTById(@PathVariable("id") String id) {
        if(Vsserts.isEmpty(id)){
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return ctService.deleteCTById(new Long(id));
    }


   /**
   	 * 根据CT信息管理ids批量删除CT信息管理
     * @method: ct/delBatch
     * @path : /admin/ct/delBatch
     * @param : ctVo
     * @result : boolean
   	 * 创建人:yykk
   	 * 创建时间：2023-02-17 21:53:53
   	 * @version 1.0.0
   	*/
    @PostMapping("/ct/delBatch")
    @PugDoc(name="根据CT信息管理ids批量删除CT信息管理")
    public boolean delCT(@RequestBody CTVo ctVo) {
        log.info("你要批量删除的IDS是:{}", ctVo.getBatchIds());
        if (Vsserts.isEmpty(ctVo.getBatchIds())) {
            throw new PugValidatorException(ResultStatusEnum.ID_NOT_EMPTY);
        }
        return ctService.delBatchCT(ctVo.getBatchIds());
    }
}