package com.pug.zixun.commons.enums;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 飞哥B站地址：https://space.bilibili.com/490711252
 * 记得关注和三连哦！
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2022/5/15$ 23:57$
 */
public enum AdminErrorResultEnum implements AdminResultInterface{

    SERVER_ERROR(500, "server error"),
    FILE_UPLOAD_LIMIT_ERROR(901, "文件不能超过2MB"),
    FILE_UPLOAD_TYPE_ERROR(902, "类型不匹配"),
    FILE_UPLOAD_FAIL_ERROR(903, "文件上传失败");

    private Integer code;
    private String msg;

    AdminErrorResultEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getMsg() {
        return msg;
    }
}
