package com.pug.zixun.commons.utils.fn.inter;

@FunctionalInterface
public interface HandlerFn {

    void handler();
}
