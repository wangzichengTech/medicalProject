package com.pug.zixun.commons.enums;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 飞哥B站地址：https://space.bilibili.com/490711252
 * 记得关注和三连哦！
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2022/5/15$ 23:57$
 */
public enum AdminUserResultEnum implements AdminResultInterface{



    USER_LOGIN_SAME(100610, "你已经在别地方登录了"),

    TOKEN_ERROR(100604, "token 过期了"),
    TOKEN_NOT_FOUND(100605, "token 找不到"),
    TOKEN_ERROR_STATUS(100609, "token无效"),

    USER_NULL_ERROR(110601, "用户不存在"),
    USER_REGISTER_OVER(120003, "用户已注册"),
    USER_SERVER_ERROR(110602, "服务出现故障"),
    USER_INPUT_USERNAME_ERROR(110603, "用户名或密码输入有误"),
    USER_FORBIDDEN_ERROR(110606, "用户异常,请联系管理员"),
    USER_NAME_NOT_EMPTY(110607, "账号不能是空"),
    USER_PWD_NOT_EMPTY(110608, "密码不能为空"),
    USER_CODE_NOT_EMPTY(110609, "验证码不能为空"),
    USER_LOGIN_UUID_EMPTY(110610, "会话过期了..."),
    USER_CODE_INPUT_ERROR(110611, "验证码输入有误"),
    ACCOUNT_REG_ERROR(110612, "账号已经被注册了"),
    USER_ROLE_AUTH_ERROR(110613, "授权失败"),
    USER_ROLE_AUTH_EMPTY(110614, "没有权限"),
    EMAIL_NOT_VALIDATE(120001,"不支持的邮箱验证类型"),
    EMAIL_MORE_VALIDATE(120002,"发送邮箱验证过于频繁"),
    EMAIL_REGISTER_OVER(120005,"邮箱已注册"),
    NULL_ERROR(120009,"内容&id不允许为空");





    private Integer code;
    private String msg;

    AdminUserResultEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getMsg() {
        return msg;
    }
}
