package com.pug.zixun.commons.result;

import com.pug.zixun.commons.enums.AdminResultInterface;
import com.pug.zixun.commons.enums.AdminSuccessResultEnum;
import lombok.Data;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 飞哥B站地址：https://space.bilibili.com/490711252
 * 记得关注和三连哦！
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2022/5/15$ 23:24$
 */
@Data
public class R {
    /**
     * 业务错误码
     */
    private Integer code;
    /**
     * 描述
     */
    private Object msg;
    /**
     * 结果集
     */
    private Object data;


    // 私有化的目的：不是单列，是为了约束外界不允许通过实例化和set方法的方式进行复制调用返回
    // 全部统一使用静态方法 success和fail进行调用。从而达到统一的目的
    private R(Integer code, Object msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }


    /**
     * 成功方法的封装
     *
     * @param data
     * @return
     */
    public static R success(Object data) {
        return new R(AdminSuccessResultEnum.SUCCESS_RESULT.getCode(),
                AdminSuccessResultEnum.SUCCESS_RESULT.getMsg(), data);
    }

    public static R fail(AdminResultInterface adminResultEnum) {
        return new R(adminResultEnum.getCode(), adminResultEnum.getMsg(), null);
    }

    /**
     * 成功方法的封装
     * 成功只有一种 200 成功
     * 失败就N中，也就code不同，原因不同，
     * @param code
     * @param msg
     * @return
     */
    public static R fail(Integer code,Object msg,Object data) {
        return new R(code, msg, data);
    }

    /**
     *
     * @return
     */
    public R msg(String msg) {
        this.setMsg(msg);
        return this;
    }

    /**
     *
     * @return
     */
    public R code(Integer code) {
        this.setCode(code);
        return this;
    }

}

