package com.pug.zixun.commons.enums;

/**
 * Description:
 * Author: yykk Administrator
 * Version: 1.0
 * Create Date Time: 2021/12/16 21:05.
 * Update Date Time:
 *
 * @see
 */
public enum ResultStatusEnum implements AdminResultInterface {

    ID_NOT_EMPTY(701100, "id不允许为空"),
    SERVER_DB_ERROR(702100, "数据库服务器出现故障"),
    USER_PWD_STATUS(601100, "用户密码有误"),
    USER_USERNAME_STATUS(602100, "请输入用户名"),
    USER_PWD_STATUS_INPUT(603100, "请输入密码"),
    USER_CODE_STATUS_INPUT(604100, "请输入验证码"),
    USER_CODE_INPUT_ERROR(605100, "验证码输入有误"),
    USER_CODE_ERROR_CACHE(606100, "缓存验证码为空"),
    TOKEN_EMPTY(607100, "Token不能为空!"),
    TOKEN_EMPTY_EXPIRED(608100, "Token已过期!"),
    TOKEN_USER_EMPTY(609100, "用户不存在!"),
    TOKEN_UN_VALID(610100, "Token无效!"),
    EMPTY_IDS_VALID(700100, "没有要删除的数据!"),
    ORDER_ERROR_STATUS(611100, "订单有误"),
    NO_PERMISSION(708100, "没有访问权限"),
    NULL_DATA(709100, "找不到数据!");

    public static final Integer A1 = 100; // 用户登录
    public static final Integer A2 = 101; // 支付
    public static final Integer A3 = 102; // 产品课程


    ResultStatusEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }


    private int code;
    private String msg;

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getMsg() {
        return msg;
    }
}
