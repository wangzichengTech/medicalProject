package com.pug.zixun.commons.ex;


/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2021/6/2 10:32
 */
public class ErrorHandler {
    // ErrorHandler === R 答案：不想破坏R类。
    // 异常的状态码，从枚举中获得
    private Integer status;
    // 异常的消息，写用户看得懂的异常，从枚举中得到
    private Object message;
    // 异常的名字
    private String exception;


    public static ErrorHandler fail(Integer code,Object message, Throwable throwable) {
        ErrorHandler errorHandler = new ErrorHandler();
        errorHandler.setMessage(message);
        errorHandler.setStatus(code);
        errorHandler.setException(throwable.getClass().getName());
        return errorHandler;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }
}