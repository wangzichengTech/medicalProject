package com.pug.zixun.commons.utils.fn.inter;

@FunctionalInterface
public interface HandlerCallBackFn<T> {

    T handler();
}
