package com.pug.zixun.commons.enums;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 飞哥B站地址：https://space.bilibili.com/490711252
 * 记得关注和三连哦！
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2022/5/15$ 23:57$
 */
public enum AdminSuccessResultEnum implements AdminResultInterface{

    SUCCESS_RESULT(200, "success");

    private Integer code;
    private String msg;

    AdminSuccessResultEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getMsg() {
        return msg;
    }
}
