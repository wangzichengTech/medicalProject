package com.pug.zixun.commons.utils.fn.inter;

@FunctionalInterface
public interface VSupplier<T> {

    T get();
}
