package com.pug.zixun.commons.ex;

import com.pug.zixun.commons.enums.AdminResultInterface;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 飞哥B站地址：https://space.bilibili.com/490711252
 * 记得关注和三连哦！
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2022/5/16$ 21:16$
 */
public class PugBusinessException extends RuntimeException {

    private Integer code;
    private String msg;

    public PugBusinessException(AdminResultInterface adminResultInterface) {
        super(adminResultInterface.getMsg());
        this.code = adminResultInterface.getCode();
        this.msg = adminResultInterface.getMsg();
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
