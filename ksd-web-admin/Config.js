const fs = require('fs')

/** 读取环境变量文件 */
function getEnvConfig() {
    const path = fs.existsSync('.env.local') ? '.env.local' : 'env'
    const content = fs.readFileSync(path, 'utf-8')
    return parse(content)
}

/** 解析环境变量内容 */
function parse(string) {
    const obj = {}
    const regExp = '(\\S+)\\s*=\\s*[\'|\"]?(\\S+)[\'|\"]?'
    const list = string.match(new RegExp(regExp, 'g'))
    list &&
        list.forEach((item) => {
            const data = item.match(new RegExp(regExp))
            const key = data ? data[1].trim() : undefined
            const value = data ? data[2].trim() : undefined
            key && (obj[key] = value)
        })
    return obj
}

export default getEnvConfig()