import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import WindiCSS from 'vite-plugin-windicss'
import path from 'path'

//const config = loadEnv('development', './')
export default ({ mode }) => {

    return defineConfig({
        publicPath: '/',
        outputDir: 'dist',
        assetsDir: 'static',
        resolve: {
            alias: {
                '@': path.resolve(__dirname, 'src'),
                '@i': path.resolve(__dirname, './src/assets')
            }
        },
        server: {
            // 请求代理
            proxy: {
                [`${loadEnv(mode, process.cwd()).VITE_APP_BASE_API}`]: {
                    // 这里的地址是后端数据接口的地址
                    target: loadEnv(mode, process.cwd()).VITE_BASE_URL,
                    //rewrite: (path) => path.replace(/^\/admin/, ''),
                    // 允许跨域
                    changeOrigin: true
                },
                "/zj":{
                    target: "http://192.168.1.6:5001/",
                    changeOrigin: true
                },
                "/jcr":{
                    target: "http://192.168.1.6:5000/",
                    changeOrigin: true
                },
                "/gzx":{
                    target: "http://192.168.1.6:5003/",
                    changeOrigin: true
                },
                "/lr":{
                    target: "http://192.168.1.6:5002/",
                    changeOrigin: true
                }

            }
        },
        plugins: [vue(), WindiCSS()]
    })
}