const childrenRoutes = [{
    path: '/',
    name: "/",
    meta: { title: "主控台", code: "10101" },
    component: () =>
        import ('../views/dashboard/Index.vue'),
},{
    path: '/course/list',
    name: "/course/list",
    meta: { title: "课程管理", code: "30101" },
    component: () =>
        import ('../views/course/List.vue'),
},{
    path: '/category/list',
    name: "/category/list",
    meta: { title: "课程分类", code: "30102" },
    component: () =>
        import ('../views/category/List.vue'),
},{
    path: '/user/list',
    name: "/user/list",
    meta: { title: "用户列表", code: "70101" },
    component: () =>
        import ('../views/user/List.vue'),
},{
    path: '/level/list',
    name: "/level/list",
    meta: { title: "会员等级", code: "70102" },
    component: () =>
        import ('../views/level/List.vue'),
},{
    path: '/order/list',
    name: "/order/list",
    meta: { title: "订单列表", code: "100101" },
    component: () =>
        import ('../views/order/List.vue'),
},{
    path: '/comment/list',
    name: "/comment/list",
    meta: { title: "课程评论", code: "100102" },
    component: () =>
        import ('../views/comment/List.vue'),
},{
    path: '/manager/list',
    name: "/manager/list",
    meta: { title: "管理员列表", code: "130101" },
    component: () =>
        import ('../views/manager/List.vue'),
},{
    path: '/permission/list',
    name: "/permission/list",
    meta: { title: "权限管理", code: "130103" },
    component: () =>
        import ('../views/permission/List.vue'),
},{
    path: '/role/list',
    name: "/role/list",
    meta: { title: "角色管理", code: "130102" },
    component: () =>
        import ('../views/role/List.vue'),
},{
    path: '/setting/base',
    name: "/setting/base",
    meta: { title: "基础设置", code: "140101" },
    component: () =>
        import ('../views/base/List.vue'),
},{
    path: '/distribution/index',
    name: "/distribution/index",
    meta: { title: "分销员管理", code: "150101" },
    component: () =>
        import ('../views/distributer/List.vue'),
},{
    path: '/distribution/setting',
    name: "/distribution/setting",
    meta: { title: "分销设置", code: "150102" },
    component: () =>
        import ('../views/dissetting/List.vue'),
},{
    path: '/image/list',
    name: "/image/list",
    meta: { title: "图库列表", code: "240101" },
    component: () =>
        import ('../views/image/List.vue'),
},{
    path: '/log/list',
    name: "/log/list",
    meta: { title: "日志管理", code: "240102" },
    component: () =>
        import ('../views/log/List.vue'),
},{
    path: '/note/list',
    name: "/note/list",
    meta: { title: "笔记管理", code: "Z10002" },
    component: () =>
        import ('../views/note/List.vue'),
},{
    path: '/questions/list',
    name: "/questions/list",
    meta: { title: "文章管理", code: "W10002" },
    component: () =>
        import ('../views/questions/List.vue'),
},{
    path: '/models/list',
    name: "/models/list",
    meta: { title: "模型管理", code: "M10002" },
    component: () =>
        import ('../views/models/List.vue'),
},{
    path: '/file/list',
    name: "/file/list",
    meta: { title: "预处理", code: "F10002" },
    component: () =>
        import ('../views/file/List.vue'),
},{
    path: '/ecgpatients/list',
    name: "/ecgpatients/list",
    meta: { title: "房颤信息管理", code: "E10002" },
    component: () =>
        import ('../views/ecgpatients/List.vue'),
},{
    path: '/sepsispatients/list',
    name: "/sepsispatients/list",
    meta: { title: "脓毒症信息管理", code: "S10002" },
    component: () =>
        import ('../views/sepsispatients/List.vue'),
},{
    path: '/sepsispatients/listTwo',
    name: "/sepsispatients/listTwo",
    meta: { title: "脓毒症信息回收站", code: "De1002" },
    component: () =>
        import ('../views/deletedatabase/sepsisList.vue'),
},{
    path: '/ecgpatients/listTwo',
    name: "/ecgpatients/listTwo",
    meta: { title: "房颤信息回收站", code: "De1001" },
    component: () =>
        import ('../views/deletedatabase/ecgList.vue'),
},{
    path: '/pre/list',
    name: "/pre/list",
    meta: { title: "待预处理信息回收站", code: "De1003" },
    component: () =>
        import ('../views/deletedatabase/preList.vue'),
},{
    path: '/modelDash/list',
    name: "/modelDash/list",
    meta: { title: "模型库图表化展示", code: "Da1001" },
    component: () =>
        import ('../views/models/dashList.vue'),
},{
    path: '/predict/list',
    name: "/predict/list",
    meta: { title: "不完整多模态预测", code: "D1002" },
    component: () =>
        import ('../views/predict/List.vue'),
},{
    path: '/predictTwo/list',
    name: "/predictTwo/list",
    meta: { title: "完整多模态预测", code: "D1003" },
    component: () =>
        import ('../views/predictTwo/List.vue'),
},{
    path: '/predictThree/list',
    name: "/predictThree/list",
    meta: { title: "心电图房颤预测", code: "E10010" },
    component: () =>
        import ('../views/predictThree/List.vue'),
},{
    path: '/predictFour/list',
    name: "/predictFour/list",
    meta: { title: "脓毒症预测", code: "S10010" },
    component: () =>
        import ('../views/predictFour/List.vue'),
},{
    path: '/dashboardOne/list',
    name: "/dashboardOne/list",
    meta: { title: "脓毒症数据报表", code: "S10011" },
    component: () =>
        import ('../views/dashboardOne/List.vue'),
},{
    path: '/predictFour/list',
    name: "/predictFour/list",
    meta: { title: "脓毒症预测", code: "S10010" },
    component: () =>
        import ('../views/predictFour/List.vue'),
}]
export default childrenRoutes;