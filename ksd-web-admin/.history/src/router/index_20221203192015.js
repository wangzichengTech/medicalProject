import { createRouter, createWebHistory } from 'vue-router'
import store from '@/store'
import { showFullLoading, hideFullLoading, toastError } from '@/utils'
import PugAdmin from '@/views/PugAdmin.vue'
// 定义子路由控制
import asyncRoutes from './dynamic';

//4 :定义路由配置规则
const routes = [{
    path: "/",
    meta: { title: store.state.i18n.langs[store.state.i18n.lan].menu["901"], code: "901" },
    name: "PugAdmin",
    component: PugAdmin
}, {
    path: "/login",
    name: "login",
    meta: { title: store.state.i18n.langs[store.state.i18n.lan].menu["902"], code: "902" },
    component: () =>
        import ('../views/Login.vue')
}, {
    path: "/upload",
    name: "upload",
    meta: { title: store.state.i18n.langs[store.state.i18n.lan].menu["903"], code: "903" },
    component: () =>
        import ('../views/upload/Index.vue')
}, {
    path: "/toLogin",
    redirect: "/login"
}, {
    path: '/:pathMatch(.*)*',
    name: '404',
    meta: { title: "404" },
    component: () =>
        import ('../views/error/404.vue')
}]


//2 :创建路由对象
const router = createRouter({
    // 引入访问模式
    history: createWebHistory(),
    routes
})



// 动态注册路由方法- 考虑到
export function registerRoutes(menuList) {
    // 是否有新的路由
    let hasNewRoutes = false
    const findAndAddRoutesByMenus = (arr) => {
        arr.forEach(e => {
            // 一定要查询我router.js中asyncRoutes存在的路由，才去绑定，否则不绑定
            let item = asyncRoutes.find(o => o.path == e.path)
                // item存在，并且没有绑定过router.hasRoute，
            if (item && !router.hasRoute(item.path)) {
                // 如果没有绑定，就开始绑定。这里就是admin,代表不论多个子元素，最终绑定全部挂载admin这集
                // 也就意味着：后续所有的子元素，孙子元素等的路由访问，都跳转到PuaAdmin.vue的router-view的位置
                router.addRoute("PugAdmin", item)
                hasNewRoutes = true
            }

            // 递归，如果当前子菜单还有子元素，
            if (e.children && e.children.length > 0) {
                findAndAddRoutesByMenus(e.children)
            }
        })
    }
    // 开始递归调用动态绑定路由关系
    findAndAddRoutesByMenus(menuList)

    // 第一次绑定：就是 true, 
    // 如果已经全部绑定过就是： false
    // 返回这个作用：就是为了让后续的路由访问只绑定一次，没必要每次访问都绑定
    return hasNewRoutes
}


/* 
   这个开关，是用来控制动态路由注册只绑定一次 
   只要不刷新、F5，第一次就是false, 后面永远都是 true
*/
let loadNewRoute = false;
// 定义后置守卫--拦截器思想
router.beforeEach(async(to, from, next) => {
    // 全屏动画开启
    showFullLoading()

    // 判断是否已经登录
    var isLogin = store.getters["user/isLogin"];

    // 没有登录，强制跳转回登录页
    if (!isLogin && to.path != "/login") {
        toastError("请先登录", "error")
        next("/toLogin")
    }

    // 防止重复登录
    if (isLogin && to.path == "/login") {
        toastError("请勿重复登录", "error")
        next({ path: from.path ? from.path : "/" })
    }


    let hasNewRoute = false; //F5的问题
    if (isLogin && !loadNewRoute) {
        // 锁住
        loadNewRoute = true;

        // 从数据查询菜单信息，开始进行动态注册, 这里会同步一次状态管理
        let menusList = await store.dispatch("menu/asyncGetMenuList")

        // 动态注册路由
        hasNewRoute = registerRoutes(menusList);
    }

    // hasNewRoute=true，初次加载动态路由注册完毕。
    // hasNewRoute=false，后续访问路由的，动态路由注册完毕。永远都是false
    // 因为用户每次刷新，其实动态路由的注册都重新开始，所以 ： hasNewRoute=true 。抓住这个特点
    // next(to.fullPath) 刷新在哪里，跳转到自己这里, 因为如果直接next刷新就进入404
    // next() 非刷新的情况的正常路由跳转。
    hasNewRoute ? next(to.fullPath) : next();
})



/* 后置守卫 */
router.afterEach((to, from) => {
    // 结束全屏动画
    hideFullLoading();
    // 标题切换
    document.title = to.meta.title + "-PugAdmin-后台管理系统" || "PugAdmin-后台管理系统";
})




// 3: 导出即可生效
export default router