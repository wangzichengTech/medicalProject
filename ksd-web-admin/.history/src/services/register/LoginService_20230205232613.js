import request from '@/utils/request'

export default {

    /**
     * 登录逻辑
     * @param {} user 
     * @returns 
     */
    toLogin(user) {
        return request.post("/login/toLogin", user);
    },
    
    /**
     * 退出逻辑
     * @returns 
     */
    toLogout() {
        return request.post("/login/logout");
    }


}