import router from '@/router'
import axios from 'axios'
import errorCode from '@/utils/errorCode'
import store from '@/store'
import { toastError } from '@/utils'
import cache from "@/utils/cache"

const request = axios.create({
    // 如果在执行异步请求的时候，如果baseUrl
    // baseURL: "http://api.txnh.net/admin",
    baseURL: import.meta.env.VITE_APP_BASE_API,
    // timeout` 指定请求超时的毫秒数(0 表示无超时时间)
    timeout: 10000,
    // 默认就是会给请求头增加token 
    istoken: true
})

// 添加请求拦截器
request.interceptors.request.use(config => {
    // 在发送请求之前做些什么
    if (config.istoken) {
        const token = store.getters['user/getToken']
        const tokenUuid = store.getters['user/getTokenUuid']
        const userId = store.getters['user/getUserId']
        const lang = cache.local.get("lang") || "zh_CN";
        if (token) {
            config.headers["lang"] = lang;
            config.headers['token'] = ['pugbear_', token].join('')
            config.headers['token-uuid'] = tokenUuid
            config.headers['token-userid'] = userId
        }
    }

    // 对象{id:1,name:'zhangsan'}转换成参数?id=1&name=zhangsan
    if (config.method === 'get' && config.params) {
        let url = config.url + '?' + tansParams(config.params)
        url = url.slice(0, -1)
        config.params = {}
        config.url = url
    }

    // 防止表单重复提交的一个处理
    if (config.method === 'post' || config.method === 'put') {}

    return config
}, function(error) {
    // 对请求错误做些什么
    console.log(error) // for debug
    return Promise.reject(error)
})

// 添加响应拦截器
request.interceptors.response.use((response) => {
    // 获取服务器的数据
    let res = response.data
    if (typeof res == "string") {
        res = JSON.parse(res);
    }
    // 获取服务器返回状态
    const code = res.code
    if (code == 200) {
        return res
    } else {
        if (code == '110606') {
            toastError('用户已被禁止,请联系管理员.')
            router.push('/toLogin')
            return
        }

        // token失效
        if (code == '100608' || code == '100604' || code == '100605' || code == '100609') {
            toastError('会话已失效...')

            // 执行退出
            store.dispatch('user/toLogout')

            // 执行退出
            router.push('/toLogin')
            return
        }

        //  挤下线
        if (code == '100610') {
            toastError('你已经在别地方登录了')

            // 执行退出
            store.dispatch('user/toLogout')
            router.push('/toLogin')
            return
        }

        // 如果没有权限直接返回404
        if (code == '110614') {
            toastError('你没有访问权限..')

            router.push('/404')
            return;
        }

        // 获取错误消息状态
        const msgObj = errorCode[code] || { msg: res.msg, code: code, field: '' } || errorCode['default']
            // 业务的错误全部抛出在页面上去处理---catch /async+await+try/catch
        return Promise.reject(msgObj)
    }
}, (err) => {


    // 捕捉超时异常
    if (err.code == 'ERR_CONNECTION_REFUSED' ||
        err.code == 'ERR_BAD_RESPONSE') {
        toastError('服务器脱离了地球表面，待会试试看...')
        return
    }

    // 捕捉服务器异常
    if (err.code == 'ECONNABORTED') {
        toastError('服务器脱离了地球表面，刷新试试看...')
        return
    }

    // 服务器的错误
    if (err.response.code == 500 || err.response.code == 401 || err.response.code == 404) {
        toastError('服务器脱离了地球表面..')
        router.push('/404')
        return
    }

    return Promise.reject(error)
})


/*导出异步请求 */
export default request