import { ref, reactive, onMounted } from 'vue'
import store from '@/store';
import router from '@/router';
import RegisterService from '../services/register/RegisterService';
import captchaService from '@/services/code/CaptchaService'
import { encryptByDES, showFullLoading, hideFullLoading, toastError, toast} from '@/utils'
import request from '@/utils/request'
// 定义类的方式封装
function UserRegister() {

    //控制登录的按钮动画
    const loading = ref(false);
    //定义form表单对象，用于校验
    const userFormRef = ref(null);
    const time = ref(0)
    const interval = ref(-1)
    //2: 获取用户输入的form数据，是响应式的
    const user = reactive({
        username: '',
        password: '',
        account: '',
        email: '',
        emailCode:''
    })


    //3:  登录提交表单
    const handleRegisterSubmit = () => {
        // 1: 提交表单校验 校验表单输入
        userFormRef.value.validate(async(valid) => {
            // 如果为valid = false 还存在非法数据.
            if (!valid) {
                return;
            }

            try {
                showFullLoading();
                loading.value = true;
                // 加密传输
                var username = encryptByDES(user.username);
                var password = encryptByDES(user.password);
                var { account, email,emailCode } = user;
                // 执行状态的异步请求
                await RegisterService.register({ username, password, account, email, emailCode })
                // 跳转首页去
                router.push("/login")
            } catch (err) {
                if (err.field == "code") {
                    toastError(err.msg);
                    user.code = "";
                    document.getElementById("code").focus();
                    return;
                }

                if (err.field == "password") {
                    toastError(err.msg);
                    user.password = "";
                    user.code = "";
                    document.getElementById("password").focus();
                    // 重新生成新的验证码
                    createCaptcha();
                    return;
                }


            } finally {
                loading.value = false;
                hideFullLoading();
            }
        });
    };
    const confirmPassword = (rule, value, callback) => {
        if (value === '') {
          callback(new Error('请确认密码'))
        }
        if (user.password !== value) {
          callback(new Error('两次输入密码不一致'))
        }
        callback()
      }
    const reg = /^[A-Za-z0-9\u4e00-\u9fa5]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/
    const checkEmail = (rule, value, callback) => {
        if(!reg.test(value)) {  // test可以校验你的输入值
          return callback(new Error('邮箱格式不合法'));
        }
        callback()
      }
    //4: 定义验证规则
    const rules = reactive({
        account: [
            { required: true, message: '请输入用户名', trigger: 'submit' },
            { min: 4, max: 20, message: '你的用户名必须是 4 to 20', trigger: 'submit' }
        ],
        password: [
            { required: true, message: '请输入密码', trigger: 'submit' },
            { min: 4, max: 20, message: '你的密码必须是 4 to 20', trigger: 'submit' }
        ],
        emailCode: [
            { required: true, message: '请输入验证码', trigger: 'blur'},
          ],
        confirm: [
            { validator: confirmPassword, trigger: 'blur'},
          ],
        email: [
            { validator: checkEmail, trigger: 'blur'},
          ],

    });
    const sendEmail = () => {
        if(!reg.test(user.email)){
            toastError("请输入合法邮箱")
        }
        console.log(typeof user.email)
        const times = () => {
            // 清空定时器
            if (interval.value >= 0) {
              clearInterval(interval.value)
            }
            time.value = 10
            interval.value = setInterval(() => {
              if (time.value > 0) {
                time.value --
              }
            }, 1000)
          }
        request.get("/email", {
            params: {
              email: user.email,
              type: "REGISTER"
            }
          }).then(res =>{
            if(res.data == 1){
            toast('发送成功，有效期5分钟')
            times()  // 倒计时
            }else{
                toastError(res.msg)
            }
            
        })
    }
    // 暴露方法给页面使用
    return {
        userFormRef,
        user,
        rules,
        handleRegisterSubmit,
        loading,
        time,
        sendEmail
    }
}

// 5 : 导出
export default UserRegister;