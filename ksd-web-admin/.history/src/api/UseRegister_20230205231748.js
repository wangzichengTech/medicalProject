import { ref, reactive, onMounted } from 'vue'
import store from '@/store';
import router from '@/router';
import captchaService from '@/services/code/CaptchaService'
import { encryptByDES, showFullLoading, hideFullLoading, toastError } from '@/utils'

// 定义类的方式封装
function UserLogin() {

    //控制登录的按钮动画
    const loading = ref(false);
    //定义form表单对象，用于校验
    const userFormRef = ref(null);
    //定义验证码的图片地址
    const captchaData = ref(null);
    //2: 获取用户输入的form数据，是响应式的
    const user = reactive({
        username: '',
        password: '',
        account: '',
        
    })


    //3:  登录提交表单
    const handleLoginSubmit = () => {
        // 1: 提交表单校验 校验表单输入
        userFormRef.value.validate(async(valid) => {
            // 如果为valid = false 还存在非法数据.
            if (!valid) {
                return;
            }

            try {
                showFullLoading();
                loading.value = true;
                // 加密传输
                var username = encryptByDES(user.username);
                var password = encryptByDES(user.password);
                var { code, codeUuid } = user;
                // 执行状态的异步请求
                await store.dispatch("user/toLogin", { username, password, codeUuid, code });
                // 跳转首页去
                router.push("/")
            } catch (err) {
                if (err.field == "code") {
                    toastError(err.msg);
                    user.code = "";
                    document.getElementById("code").focus();
                    return;
                }

                if (err.field == "password") {
                    toastError(err.msg);
                    user.password = "";
                    user.code = "";
                    document.getElementById("password").focus();
                    // 重新生成新的验证码
                    createCaptcha();
                    return;
                }


            } finally {
                loading.value = false;
                hideFullLoading();
            }
        });
    };

    //4: 定义验证规则
    const userLoginRules = reactive({
        username: [
            { required: true, message: '请输入用户名', trigger: 'submit' },
            { min: 4, max: 20, message: '你的用户名必须是 4 to 20', trigger: 'submit' }
        ],
        password: [
            { required: true, message: '请输入密码', trigger: 'submit' },
            { min: 4, max: 20, message: '你的密码必须是 4 to 20', trigger: 'submit' }
        ],

        code: [{
            required: true,
            message: '请输入验证码',
            trigger: 'submit'
        }]
    });

    // 5 : 生成验证码
    const createCaptcha = async() => {
        try {
            showFullLoading();
            var serverCode = await captchaService.createCaptcha();
            captchaData.value = serverCode.data.img;
            // 每次加载新的都把最新的uuid给用户登录对象
            user.codeUuid = serverCode.data.codeToken;
        } finally {
            hideFullLoading();
        }
    };


    // 6 : 生命周期初始化验证码
    onMounted(() => {
        // 执行创建验证码
        createCaptcha();
        // 定时每隔四分钟执行一次重新生成验证码
        setInterval(createCaptcha, 4 * 60 * 1000);
    });



    // 暴露方法给页面使用
    return {
        userFormRef,
        user,
        userLoginRules,
        handleLoginSubmit,
        loading,

        captchaData,
        createCaptcha
    }
}

// 5 : 导出
export default UserRegister;