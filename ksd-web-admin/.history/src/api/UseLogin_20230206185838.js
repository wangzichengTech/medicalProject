import { nextTick,ref, reactive, onMounted } from 'vue'
import store from '@/store';
import router from '@/router';
import captchaService from '@/services/code/CaptchaService'
import { encryptByDES, showFullLoading, hideFullLoading, toastError,toast } from '@/utils'
import request from '@/utils/request'
// 定义类的方式封装
function UserLogin() {
    const passwordVis = ref(false)
    //控制登录的按钮动画
    const loading = ref(false);
    //定义form表单对象，用于校验
    const userFormRef = ref(null);
    //定义验证码的图片地址
    const captchaData = ref(null);
    const time = ref(0)
    const interval = ref(-1)
    //2: 获取用户输入的form数据，是响应式的
    const user = reactive({
        username: '',
        password: '',
        code: '',
        codeUuid: ""
    })
    const rulePasswordFormRef = ref()
    const passwordForm = reactive({})
    //3:  登录提交表单
    const handleLoginSubmit = () => {
        // 1: 提交表单校验 校验表单输入
        userFormRef.value.validate(async(valid) => {
            // 如果为valid = false 还存在非法数据.
            if (!valid) {
                return;
            }

            try {
                showFullLoading();
                loading.value = true;
                // 加密传输
                var username = encryptByDES(user.username);
                var password = encryptByDES(user.password);
                var { code, codeUuid } = user;
                // 执行状态的异步请求
                await store.dispatch("user/toLogin", { username, password, codeUuid, code });
                // 跳转首页去
                router.push("/")
            } catch (err) {
                if (err.field == "code") {
                    toastError(err.msg);
                    user.code = "";
                    document.getElementById("code").focus();
                    return;
                }

                if (err.field == "password") {
                    toastError(err.msg);
                    user.password = "";
                    user.code = "";
                    document.getElementById("password").focus();
                    // 重新生成新的验证码
                    createCaptcha();
                    return;
                }


            } finally {
                loading.value = false;
                hideFullLoading();
            }
        });
    };
    //4: 定义验证规则
    const userLoginRules = reactive({
        username: [
            { required: true, message: '请输入用户名', trigger: 'submit' },
            { min: 4, max: 20, message: '你的用户名必须是 4 to 20', trigger: 'submit' }
        ],
        password: [
            { required: true, message: '请输入密码', trigger: 'submit' },
            { min: 4, max: 20, message: '你的密码必须是 4 to 20', trigger: 'submit' }
        ],

        code: [{
            required: true,
            message: '请输入验证码',
            trigger: 'submit'
        }]
    });

    // 5 : 生成验证码
    const createCaptcha = async() => {
        try {
            showFullLoading();
            var serverCode = await captchaService.createCaptcha();
            captchaData.value = serverCode.data.img;
            // 每次加载新的都把最新的uuid给用户登录对象
            user.codeUuid = serverCode.data.codeToken;
        } finally {
            hideFullLoading();
        }
    };


    // 6 : 生命周期初始化验证码
    onMounted(() => {
        // 执行创建验证码
        createCaptcha();
        // 定时每隔四分钟执行一次重新生成验证码
        setInterval(createCaptcha, 4 * 60 * 1000);
    });
    const passwordRules = reactive({
        email: [
          { required: true, message: '请输入账号', trigger: 'blur'},
        ],
        emailCode: [
          { required: true, message: '请输入验证码', trigger: 'blur'},
        ],
      })
    // 点击忘记密码触发
    const handleResetPassword = () => {
    passwordVis.value = true
    // 触发表单重置
    nextTick(() => {   // 下个时钟触发
      rulePasswordFormRef.value.resetFields()
    })
  }
  const times = () => {
    // 清空定时器
    if (interval.value >= 0) {
      clearInterval(interval.value)
    }
    time.value = 10
    interval.value = setInterval(() => {
      if (time.value > 0) {
        time.value --
      }
    }, 1000)
  }

  const sendEmail = async() => {
    const reg = /^[A-Za-z0-9\u4e00-\u9fa5]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/
    if(!reg.test(passwordForm.email)){
        toastError("请输入合法邮箱")
    }
    console.log(typeof passwordForm.email)

      try{
        const res = await request.get("/email", {
            params: {
              email: passwordForm.email,
              type: "RESETPASSWORD"
            }
          })
            toast('发送成功，有效期5分钟')
            times()  // 倒计时
      }catch(err){
        toastError(err.msg);
      }
}
  const resetPassword = () => {
    rulePasswordFormRef.value.validate(valid => {
        if (valid) {
          request.post("/password/reset", passwordForm).then(res => {
              toast('重置成功，您的密码为：' + res.data)
              passwordVis.value = false
          
            })
        }
      })
  }
    // 暴露方法给页面使用
    return {
        userFormRef,
        user,
        userLoginRules,
        handleLoginSubmit,
        loading,

        captchaData,
        createCaptcha,
        handleResetPassword,
        passwordForm,
        passwordRules,
        resetPassword,
        passwordVis,
        time,
        interval,
        rulePasswordFormRef,
        sendEmail
    }
}

// 5 : 导出
export default UserLogin;