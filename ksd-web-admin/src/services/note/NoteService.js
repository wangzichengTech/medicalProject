import request from '@/utils/request'
export default {

  /**
    * @desc 查询笔记专栏管理列表信息并分页
    * @author yykk
    * @date 2022-07-30 21:42:35
    * @tablename kss_note
  */

  findNotes(params = { pageNo: 1, pageSize: 10 }) {
    return request.post('/note/load', params)
  },

  /**
    * @desc 分页笔记专栏管理信息
    * @author yykk
  */

  findNoteList() {
    return request.post('/note/list')
  },

  /**
     * @desc 根据笔记专栏管理id查询明细信息
     * @author yykk
   */

  getNoteById(id) {
    if (!id)return
    return request.post('/note/get/' + id)
  },

  /**
   * @desc 保存笔记专栏管理
   * @author yykk
   */
  saveupdateNote(note = {}) {
    return request.post('/note/saveupdate', note)
  },

  /**
   * @desc 根据id查询明细
   * @author yykk
   */

  getNoteById(id) {
    if (!id) return
    return request.post(`/note/get/${id}`)
  },

  /**
   * @desc 根据笔记专栏管理id删除笔记专栏管理
   * @author yykk
   */
  deleteNoteById(id) {
    if (!id) return
    return request.post(`/note/delete/${id}`)
  },

  /**
   * @desc 根据笔记专栏管理ids批量删除
   * @author yykk
   */
  delBatchNote(batchIds) {
    if (!batchIds) return
    return request.post(`/note/delBatch`, { batchIds})
  },

  /**
   * @desc 状态的更新
   * @author yykk
   */
  updateStatus(params = { id: '' }) {
    if (!params.id) return
    return request.post('/note/update', params)
  }

}
