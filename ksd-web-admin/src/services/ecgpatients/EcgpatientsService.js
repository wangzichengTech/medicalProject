import request from '@/utils/request'
export default {

  /**
    * @desc 查询脓毒症信息管理列表信息并分页
    * @author yykk
    * @date 2022-11-02 19:18:02
    * @tablename kss_ecg_patients
  */

  findEcgpatientss(params = { pageNo: 1, pageSize: 10 }) {
    return request.post('/ecgpatients/load', params)
  },
  findEcgpatientss2(params = { pageNo: 1, pageSize: 10 }) {
    return request.post('/ecgpatients/load2', params)
  },
  /**
    * @desc 分页脓毒症信息管理信息
    * @author yykk
  */

  findEcgpatientsList() {
    return request.post('/ecgpatients/list')
  },

  /**
     * @desc 根据脓毒症信息管理id查询明细信息
     * @author yykk
   */

  getEcgpatientsById(id) {
    if (!id)return
    return request.post('/ecgpatients/get/' + id)
  },

  /**
   * @desc 保存脓毒症信息管理
   * @author yykk
   */
  saveupdateEcgpatients(ecgpatients = {}) {
    return request.post('/ecgpatients/saveupdate', ecgpatients)
  },
  /**
   * @desc 根据id查询明细
   * @author yykk
   */

  getEcgpatientsById(id) {
    if (!id) return
    return request.post(`/ecgpatients/get/${id}`)
  },
  predictBaths(){
    return request.post('/ecgpatients/export')
  },

  /**
   * @desc 根据脓毒症信息管理id删除脓毒症信息管理
   * @author yykk
   */
  deleteEcgpatientsById(id) {
    if (!id) return
    return request.post(`/ecgpatients/delete/${id}`)
  },
  deleteEcgpatientsById2(id) {
    if (!id) return
    return request.post(`/ecgpatients/delete2/${id}`)
  },

  /**
   * @desc 根据脓毒症信息管理ids批量删除
   * @author yykk
   */
  delBatchEcgpatients(batchIds) {
    if (!batchIds) return
    return request.post(`/ecgpatients/delBatch`, { batchIds})
  },
  delBatchEcgpatients2(batchIds) {
    if (!batchIds) return
    return request.post(`/ecgpatients/delBatch2`, { batchIds})
  }

}
