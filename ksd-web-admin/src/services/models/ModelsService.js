import request from '@/utils/request'
export default {

  /**
    * @desc 查询模型管理列表信息并分页
    * @author yykk
    * @date 2022-10-08 21:13:25
    * @tablename kss_models 
  */

  findModelss(params = { pageNo: 1, pageSize: 10 }) {
    return request.post('/models/load', params)
  },

  /**
    * @desc 分页模型管理信息
    * @author yykk
  */

  findModelsList() {
    return request.post('/models/list')
  },

  /**
     * @desc 根据模型管理id查询明细信息
     * @author yykk
   */

  getModelsById(id) {
    if (!id)return
    return request.post('/models/get/' + id)
  },

  /**
   * @desc 保存模型管理
   * @author yykk
   */
  saveupdateModels(models = {}) {
    return request.post('/models/saveupdate', models)
  },

  /**
   * @desc 根据id查询明细
   * @author yykk
   */

  getModelsById(id) {
    if (!id) return
    return request.post(`/models/get/${id}`)
  },

  /**
   * @desc 根据模型管理id删除模型管理
   * @author yykk
   */
  deleteModelsById(id) {
    if (!id) return
    return request.post(`/models/delete/${id}`)
  },

  /**
   * @desc 根据模型管理ids批量删除
   * @author yykk
   */
  delBatchModels(batchIds) {
    if (!batchIds) return
    return request.post(`/models/delBatch`, { batchIds})
  },

  /**
   * @desc 状态的更新
   * @author yykk
   */
  updateStatus(params = { id: '' }) {
    if (!params.id) return
    return request.post('/models/update', params)
  }

}
