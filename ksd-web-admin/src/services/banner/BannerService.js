import request from '@/utils/request';


export default {
    findBannerList() {
        return request.post("/banner/list");
    }
}