import request from '@/utils/request'
export default {

  /**
    * @desc 查询音乐管理列表信息并分页
    * @author yykk
    * @date 2022-07-28 19:38:03
    * @tablename kss_music
  */

  findMusics(params = { pageNo: 1, pageSize: 10 }) {
    return request.post('/music/load', params)
  },

  /**
    * @desc 分页音乐管理信息
    * @author yykk
  */

  findMusicList() {
    return request.post('/music/list')
  },

  /**
     * @desc 根据音乐管理id查询明细信息
     * @author yykk
   */

  getMusicById(id) {
    if (!id)return
    return request.post('/music/get/' + id)
  },

  /**
   * @desc 保存音乐管理
   * @author yykk
   */
  saveupdateMusic(music = {}) {
    return request.post('/music/saveupdate', music)
  },

  /**
   * @desc 根据id查询明细
   * @author yykk
   */

  getMusicById(id) {
    if (!id) return
    return request.post(`/music/get/${id}`)
  },

  /**
   * @desc 根据音乐管理id删除音乐管理
   * @author yykk
   */
  deleteMusicById(id) {
    if (!id) return
    return request.post(`/music/delete/${id}`)
  },

  /**
   * @desc 根据音乐管理ids批量删除
   * @author yykk
   */
  delBatchMusic(batchIds) {
    if (!batchIds) return
    return request.post(`/music/delBatch`, { batchIds})
  },

  /**
   * @desc 状态的更新
   * @author yykk
   */
  updateStatus(params = { id: '' }) {
    if (!params.id) return
    return request.post('/music/update', params)
  }

}
