import request from '@/utils/request'
export default {

  /**
    * @desc 查询文章管理列表信息并分页
    * @author yykk
    * @date 2022-07-30 23:58:45
    * @tablename kss_questions
  */

  findQuestionss(params = { pageNo: 1, pageSize: 10 }) {
    return request.post('/questions/load', params)
  },

  /**
    * @desc 分页文章管理信息
    * @author yykk
  */

  findQuestionsList() {
    return request.post('/questions/list')
  },

  /**
     * @desc 根据文章管理id查询明细信息
     * @author yykk
   */

  getQuestionsById(id) {
    if (!id)return
    return request.post('/questions/get/' + id)
  },

  /**
   * @desc 保存文章管理
   * @author yykk
   */
  saveupdateQuestions(questions = {}) {
    return request.post('/questions/saveupdate', questions)
  },

  /**
   * @desc 根据id查询明细
   * @author yykk
   */

  getQuestionsById(id) {
    if (!id) return
    return request.post(`/questions/get/${id}`)
  },

  /**
   * @desc 根据文章管理id删除文章管理
   * @author yykk
   */
  deleteQuestionsById(id) {
    if (!id) return
    return request.post(`/questions/delete/${id}`)
  },

  /**
   * @desc 根据文章管理ids批量删除
   * @author yykk
   */
  delBatchQuestions(batchIds) {
    if (!batchIds) return
    return request.post(`/questions/delBatch`, { batchIds})
  },

  /**
   * @desc 状态的更新
   * @author yykk
   */
  updateStatus(params = { id: '' }) {
    if (!params.id) return
    return request.post('/questions/update', params)
  }

}
