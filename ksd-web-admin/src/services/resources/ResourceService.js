import request from '@/utils/request.js'

export default {

    /**
     * 根据资源分类id删除资源分类
     */
    delResourcecategory(id) {
        return request.post(`/resourcecategory/delete/${id}`);
    },


    /**
     * 根据id更改排序和相关信息
     */
    updateRelation(category) {
        return request.post(`/resourcecategory/update`, category);
    },


    /**
     * 保存和修改资源分类
     */
    saveResourcecategory(category = {}) {
        return request.post("/resourcecategory/saveupdate", category);
    },


    /**
     * 查询资源分类列表信息并分页
     */
    findResourceCategorys(params = { pageNo: 1, pageSize: 10, keyword: "" }) {
        return request.post("/resourcecategory/load", params);
    },


    /**
     * 查询资源库列表信息并分页
     */
    findResources(params = { pageNo: 1, pageSize: 36, keyword: "" }) {
        return request.post("/resources/load", params);
    },

    /**
     * 统计分类下是否存在资源信息
     */

    countResourceByCid(cid) {
        if (!cid) return;
        return request.post(`/resources/count/${cid}`);
    }
}