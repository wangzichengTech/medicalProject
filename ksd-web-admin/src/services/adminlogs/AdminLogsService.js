import request from '@/utils/request'
export default {

  /**
    * @desc 查询后台操作日志列表信息并分页
    * @author yykk
    * @date 2022-07-26 23:56:30
    * @tablename kss_admin_logs
  */

  findAdminLogss(params = { pageNo: 1, pageSize: 10 }) {
    return request.post('/adminlogs/load', params)
  },

  /**
    * @desc 分页后台操作日志信息
    * @author yykk
  */

  findAdminLogsList() {
    return request.post('/adminlogs/list')
  },

  /**
     * @desc 根据后台操作日志id查询明细信息
     * @author yykk
   */

  getAdminLogsById(id) {
    if (!id)return
    return request.post('/adminlogs/get/' + id)
  },

  /**
   * @desc 保存后台操作日志
   * @author yykk
   */
  saveupdateAdminLogs(adminlogs = {}) {
    return request.post('/adminlogs/saveupdate', adminlogs)
  },

  /**
   * @desc 根据id查询明细
   * @author yykk
   */

  getAdminLogsById(id) {
    if (!id) return
    return request.post(`/adminlogs/get/${id}`)
  },

  /**
   * @desc 根据后台操作日志id删除后台操作日志
   * @author yykk
   */
  deleteAdminLogsById(id) {
    if (!id) return
    return request.post(`/adminlogs/delete/${id}`)
  },

  /**
   * @desc 根据后台操作日志ids批量删除
   * @author yykk
   */
  delBatchAdminLogs(batchIds) {
    if (!batchIds) return
    return request.post(`/adminlogs/delBatch`, { batchIds})
  },

}
