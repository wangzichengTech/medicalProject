import request from '@/utils/request'
export default {

  /**
    * @desc 查询脓毒症信息管理列表信息并分页
    * @author yykk
    * @date 2022-11-14 19:09:44
    * @tablename kss_sepsis_patients
  */

  findSepsisPatients(params = { pageNo: 1, pageSize: 10 }) {
    return request.post('/sepsispatients/load', params)
  },
  findSepsisPatients2(params = { pageNo: 1, pageSize: 10 }) {
    return request.post('/sepsispatients/load2', params)
  },

  /**
    * @desc 分页脓毒症信息管理信息
    * @author yykk
  */

  findSepsisPatientsList() {
    return request.post('/sepsispatients/list')
  },

  /**
     * @desc 根据脓毒症信息管理id查询明细信息
     * @author yykk
   */

  getSepsisPatientsById(id) {
    if (!id)return
    return request.post('/sepsispatients/get/' + id)
  },

  /**
   * @desc 保存脓毒症信息管理
   * @author yykk
   */
  saveupdateSepsisPatients(sepsispatients = {}) {
    return request.post('/sepsispatients/saveupdate', sepsispatients)
  },

  /**
   * @desc 根据id查询明细
   * @author yykk
   */

  getSepsisPatientsById(id) {
    if (!id) return
    return request.post(`/sepsispatients/get/${id}`)
  },

  /**
   * @desc 根据脓毒症信息管理id删除脓毒症信息管理
   * @author yykk
   */
  deleteSepsisPatientsById(id) {
    if (!id) return
    return request.post(`/sepsispatients/delete/${id}`)
  },
  deleteSepsisPatientsById2(id) {
    if (!id) return
    return request.post(`/sepsispatients/delete2/${id}`)
  },

  /**
   * @desc 根据脓毒症信息管理ids批量删除
   * @author yykk
   */
  delBatchSepsisPatients(batchIds) {
    if (!batchIds) return
    return request.post(`/sepsispatients/delBatch`, { batchIds})
  },
  delBatchSepsisPatients2(batchIds) {
    if (!batchIds) return
    return request.post(`/sepsispatients/delBatch2`, { batchIds})
  },
  predictBaths(){
    return request.post("/sepsispatients/export");
  }
}
