import request from '@/utils/request'
export default {
    // 查询角色信息
    findAdminRoleList() {
        return request.post('/adminrole/list')
    },

    // 分页查询角色
    findAdminRoles(params = { pageNo: 1, pageSize: 10 }) {
        return request.post('/adminrole/load', params)
    },

    // 查询用户对应的角色信息
    findRoleUsers(userId) {
        return request.post("/adminrole/user/role?userId=" + userId);
    },

    // 保存用户角色信息
    saveupdateAdminRole(adminRole) {
        return request.post('/adminrole/saveupdate', adminRole)
    },

    // 根据后台角色管理id删除后台角色管理
    deleteAdminRoleById(id) {
        if (!id) return;
        return request.post(`/adminrole/delete/${id}`)
    },

    // 根据后台角色管理ids批量删除后台角色管理
    delAdminRole(batchIds) {
        if (!batchIds) return;
        return request.post('/adminrole/delBatch', { batchIds })
    },

    // 状态的修改
    updateAdminRole(params) {
        if (!params.id) return;
        return request.post('/adminrole/update', params)
    },

    // 查询角色对用的权限
    findPermissionByRoleId(roleId) {
        if (!roleId) return;
        return request.post('/adminrole/findPermission', { roleId })
    },

    // 给角色授权
    savePermissionByRoleId(roleId, permissionIds) {
        if (!roleId) return;
        return request.post('/adminrole/savepermission', { roleId, permissionIds })
    }

}