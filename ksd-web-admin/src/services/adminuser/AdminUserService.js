import request from '@/utils/request';
export default {
    // 保存后台
    saveupdateAdminUser(adminuser = {}) {
        return request.post("/adminuser/saveupdate", adminuser);
    },

    // 查询分页
    findAdminUsers(params = {}) {
        return request.post("/adminuser/load", params);
    },


    // 删除
    deleteAdminUserById(id) {
        return request.post("/adminuser/delete/" + id);
    },

    // 更新状态
    updateAdminUser(params = { id: "" }) {
        if (!params.id) return;
        return request.post("/adminuser/update", params);
    },

    //批量删除
    delAdminUser(batchIds) {
        if (!batchIds) return;
        return request.post("/adminuser/delBatch", { batchIds });
    },

    // 绑定角色
    bingRole(userId, roleId) {
        if (!userId) return;
        if (!roleId) return;
        return request.post(`/adminuser/binding/role?userId=${userId}&roleId=${roleId}`);
    },

    // 解绑角色
    unBingRole(userId, roleId) {
        if (!userId) return;
        if (!roleId) return;
        return request.post(`/adminuser/unbind/role?userId=${userId}&roleId=${roleId}`);
    }
}