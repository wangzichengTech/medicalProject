import request from '@/utils/request'
export default {

  /**
    * @desc 查询文件管理列表信息并分页
    * @author yykk
    * @date 2022-10-28 13:23:37
    * @tablename kss_file
  */

  findFiles(params = { pageNo: 1, pageSize: 10 }) {
    return request.post('/file/load', params)
  },
  findFiles2(params = { pageNo: 1, pageSize: 10 }) {
    return request.post('/file/load2', params)
  },
  /**
    * @desc 分页文件管理信息
    * @author yykk
  */

  findFileList() {
    return request.post('/file/list')
  },

  /**
     * @desc 根据文件管理id查询明细信息
     * @author yykk
   */

  getFileById(id) {
    if (!id)return
    return request.post('/file/get/' + id)
  },

  /**
   * @desc 保存文件管理
   * @author yykk
   */
  saveupdateFile(file = {}) {
    return request.post('/file/saveupdate', file)
  },

  /**
   * @desc 根据id查询明细
   * @author yykk
   */

  getFileById(id) {
    if (!id) return
    return request.post(`/file/get/${id}`)
  },

  /**
   * @desc 根据文件管理id删除文件管理
   * @author yykk
   */
  deleteFileById(id) {
    if (!id) return;
    return request.post(`/file/delete/${id}`)
  },
  deleteFileById2(id) {
    if (!id) return;
    return request.post(`/file/delete2/${id}`)
  },
  /**
   * @desc 根据文件管理ids批量删除
   * @author yykk
   */
  delBatchFile(batchIds) {
    if (!batchIds) return;
    return request.post(`/file/delBatch`, { batchIds})
  },
  delBatchFile2(batchIds) {
    if (!batchIds) return;
    return request.post(`/file/delBatch2`, { batchIds})
  },
  /**
   * @desc 状态的更新
   * @author yykk
   */
  updateStatus(params = { id: '' }) {
    if (!params.id) return;
    return request.post('/file/saveupdate', params)
  }

}
