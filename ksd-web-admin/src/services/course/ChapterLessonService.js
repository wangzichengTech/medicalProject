import request from '@/utils/request'

export default {

    /**
     * 查询章节管理列表信息
     * @param courseId 
     * @returns 
     */
    findChapterList(courseId) {
        if (!courseId) return;
        return request.post(`/chapterlesson/list/${courseId}`)
    },

    /**
     * 根据章节管理id查询明细信息(包含了节信息)
     * @param opid 章节id 
     * @returns 
     */
    getChapterLessons(opid) {
        if (!opid) return;
        return request.post(`/chapterlesson/get/${opid}`)
    },

    /**
     * 拷贝章节信息
     * @param opid 章节id 
     * @returns 
     */
    copyChapterLesson(opid) {
        if (!opid) return;
        return request.post(`/chapterlesson/copy/${opid}`)
    },

    /**
     * 保存和修改章节管理
     * @param opid 章节id 
     * @returns 
     */
    saveupdateChapterLesson(params = {}) {
        return request.post(`/chapterlesson/saveupdate`, params)
    },

    /**
     * 根据章节管理id删除章节管理
     * @param opid 章节id 
     * @returns 
     */
    deleteChapterLessonById(opid) {
        if (!opid) return;
        return request.post(`/chapterlesson/delete/${opid}`)
    },

    /**
     * 物理删除章节信息
     * @param opid 章节id 
     * @returns 
     */
    removeChapterLessonById(opid) {
        if (!opid) return;
        return request.post(`/chapterlesson/remove/${opid}`)
    },

    /**
     * 恢复删除
     * @param opid 章节id 
     * @returns 
     */
    rebackChapterLessonById(opid) {
        if (!opid) return;
        return request.post(`/chapterlesson/reback/${opid}`)
    }

}