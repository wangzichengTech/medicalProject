import request from '@/utils/request'

export default {

    /**
     * @desc 分页课程信息
     * @author yykk
     */

    findCourses(params = { pageNo: 1, pageSize: 10 }) {
        return request.post("/course/load", params);
    },

    /**
     * @desc 查询课程列表信息
     * @author yykk
     */

    findCoursesList() {
        return request.post("/course/list");
    },


    /**
     * 保存课程
     */
    saveupdateCourse(course = {}) {
        return request.post("/course/saveupdate", course);
    },

    /**
     * 拷贝章节信息
     * @param courseId 课程ID 
     * @returns 
     */
    copyCourse(courseId) {
        if (!courseId) return;
        return request.post(`/course/copy/${courseId}`)
    },


    /**
     * 根据id查询明细
     */

    getCourseById(id) {
        if (!id) return;
        return request.post(`/course/get/${id}`);
    },

    /**
     * 根据课程id删除课程
     */
    deleteCourseById(id) {
        if (!id) return;
        return request.post(`/course/delete/${id}`);
    },

    /**
     * 根据课程ids批量删除课程
     */
    delBatchCourse(batchIds) {
        if (!batchIds) return;
        return request.post(`/course/delBatch`, { batchIds });
    },

    /**
     * 相关状态的更新
     */
    updateStatus(params = { id: "" }) {
        if (!params.id) return;
        return request.post("/course/update", params);
    }
}