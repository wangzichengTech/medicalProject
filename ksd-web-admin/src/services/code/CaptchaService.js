import request from '@/utils/request'

export default {

    /**
     * 创建验证码
     */
    createCaptcha() {
        return request.post("/captcha");
    }

}