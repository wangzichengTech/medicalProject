import request from '@/utils/request';
export default {

    /**
     * 查询管理分类
     */
    findCategoryTree(keyword = "") {
        return request.post("/category/tree?keyword=" + keyword);
    },
    findCategoryById(id){
        if (!id) return
    return request.post(`/category/get/${id}`);
    }
    ,

    /**
     * 保存和修改文章分类管理
     */
    saveupdateCategory(category = {}) {
        return request.post("/category/saveupdate", category);
    },

    /**
     * 根据id删除分类
     */
    deleteCategoryById(id) {
        if (!id) return;
        return request.post(`/category/delete/${id}`);
    },

    /**
     * 批量删除
     */
    delBatchCategory(batchIds) {
        if (!batchIds) return;
        return request.post(`/category/delBatch`, { batchIds });
    }
}