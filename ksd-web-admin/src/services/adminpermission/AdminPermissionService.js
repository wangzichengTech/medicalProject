import request from '@/utils/request'
export default {
    /**
     * 查询菜单
     */
    loadNavMenu() {
        return request.post("/adminpermission/menu/tree");
    },


    // 查询菜单和权限
    permissionTree() {
        return request.post('/adminpermission/tree')
    },

    /**
     * 保存菜单
     */
    saveupdateAdminPermission(params = {}) {
        return request.post("/adminpermission/saveupdate", params)
    },

    /**
     * 删除菜单
     */
    deleteAdminPermissionById(id) {
        if (!id) return;
        return request.post(`/adminpermission/delete/${id}`)
    },

    /**
     * 状态的修改
     */
    updateAdminPermission(params = { id: "" }) {
        if (!params.id) return;
        return request.post(`/adminpermission/update`, params)
    }

}