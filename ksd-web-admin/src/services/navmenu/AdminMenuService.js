import request from '@/utils/request'

export default {

    /**
     * 查询菜单
     */
    loadNavMenu() {
        return request.post("/adminpermission/menu/tree");
    }

}