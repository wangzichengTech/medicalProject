import tinymce from 'tinymce/tinymce'
import config from "./config";
window.tinymce = tinymce
tinymce.PluginManager.add('imgUpload', function(editor, url) {
    function openFileUpload(e) {
        let input = document.createElement("input");
        input.type = "file";
        input.accept = "image/jpeg,image/png";
        input.click();

        //添加事件
        input.addEventListener("change", function(e) {
            let files = e.target.files;
            let file = {};
            if (files.length > 1) {
                file = files[0];
                console.log("富文本编辑器中上传的图片超出一个，这里只取第一个");
            } else {
                file = files[0];
            }
            config.imgChange(file).then(url => {
                //向富文本插入内容
                editor.insertContent(`<img src="${url.src}">`)

                //移除标签
                input.remove();
            });
        })
    }

    // 注册一个工具栏按钮名称
    editor.ui.registry.addButton('imgUpload', {
        icon: 'image',
        tooltip: '图片上传',
        onAction: function(...e) {
            editor.insertContent(`1111111111111`)

        }
    });



    // 注册一个菜单项名称 menu/menubar
    editor.ui.registry.addMenuItem('imgUpload', {
        icon: 'image',
        tooltip: '图片上传',
        onAction: function(...e) {
            openFileUpload(e);
        }
    });



    return {
        getMetadata: function() {
            return {
                name: "imgUpload", //插件名称
                url: "#", //作者网址
            };
        }
    };
});