/**
 * 富文本编辑器的宽度
 * @type {number}
 */
const width = 700;
/**
 * 富文本大小是否可拖动
 * @type {boolean}
 */
const resize = false;
/**
 * 富文本编辑器的高度
 * @type {number}
 */
const height = 500;
/**
 * 定义图片的chang事件
 * @param file
 * @returns {Promise<unknown>}
 */
const imgChange = function(file) {
    //富文本的图片选择改变事件，你需要在这里自定义图片处理逻辑
    return new Promise((resolve, reject) => {
        //console.log("获取到的文件", file);
        //let defaultUrl = "https://www.tiny.cloud/docs/images/logos/for-base/logo-spaces-docs-for-base.svg";
        // 你需要在这里自定义图片处理逻辑
        // 返回给富文本内容的必须是个完整的图片地址
        //resolve(url || defaultUrl);

        var reader = new FileReader(); //创建文件读取对象
        reader.readAsDataURL(file);
        //监听文件读取结束后事件
        reader.onloadend = function(e) {
            var img = new Image();
            img.src = e.target.result
            resolve(img);
        };
    });
};

export default {
    imgChange,
    width,
    height,
    resize
}