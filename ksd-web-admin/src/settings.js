export default {
    title: 'Pug Admin',
    // 注意必须是http或者https的图片地址或者svg地址
    logo: '',
    /**
     * @type {boolean} true | false
     * @description Whether show the settings menu
     */
    menushow: true,
    /**
     * @type {Number} default 15
     * @description Whether show the settings menu
     */
    menucount: 10,
    /**
     * 缓存前缀模板
     */
    cacheSuffix: "pug_storage_"
}