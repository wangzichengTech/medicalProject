// 导入状态管理
import store from '@/store'

export default function(app) {
    // 注册一个全局自定义指令 `v-permission`
    app.directive('permission', {
        mounted(el, binding) {
            // 获取指令上的权限代号
            const diretiveValue = typeof binding.value == "object" ? binding.value : [binding.value];
            // 开始和用户角色对应的权限列表进行校验
            const checkePermission = hasPermission(diretiveValue);
            // 如果没有权限
            if (!checkePermission) {
                // 从dom元素上删除，你用debug工具是看不到
                el.parentElement.removeChild(el);
            }
        }
    })


    const hasPermission = (diretiveValue = []) => {
        // 获取状态管理的权限信息
        const permissionList = store.state.user.permissionList;
        // 获取权限代号的结果比较
        const filters = diretiveValue.filter(code => {
            return permissionList.includes(code + "");
        });

        return filters.length > 0;
    }

}