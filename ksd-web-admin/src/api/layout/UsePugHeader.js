 // 导入状态管理
 import { ref, reactive } from 'vue';
 // 导入状态管理
 import { useStore } from 'vuex';
 // 导入路由
 import { useRouter } from 'vue-router';
 // 导入全屏组件
 import { useFullscreen } from '@vueuse/core'
 import { toast, confirm } from '@/utils'


 export default function() {
     // isFullscreen 是否是全屏 , toogle 全屏切换
     const { isFullscreen, toggle } = useFullscreen()
     const store = useStore();
     const router = useRouter();
     const PugEvent = {};
     // 获取修改密码的抽屉弹窗对象
     const updatePwdRef = ref(null);

     // 跳转首页
     const handleIndex = () => {
         router.push("/");
     }

     // 折叠菜单
     const handleExpand = () => {
         store.commit("menu/handleSlideWidth");
     }

     // 刷新
     const handleRefresh = () => {
         window.location.reload();
     }

     // 全屏幕
     const handleFullScreen = () => {
         toggle();
     }


     /*用户菜单 */
     const handleCommand = async(command) => {
         PugEvent['handle' + command]();
     };

     PugEvent.handleLogout = async() => {
         var cresult = await confirm("你确定退出登录吗？");
         if (cresult) {
             // 然后在去执行异步请求
             await store.dispatch("user/toLogout");
             // 这里肯定是退出成功才会进入
             router.push("/toLogin");
         }
     }

     PugEvent.handleUpdatePwd = () => {
         // 这里对吗？
         updatePwdRef.value.open();
     }


     // 修改密码的数据模型
     const userPwdForm = reactive({
         oldpassword: "",
         newpassword: "",
         confirmpassword: ""
     })

     // 获取form对象
     const userPwdFormRef = ref(null);

     // 提交修改密码
     const handleOpen = () => {
         userPwdFormRef.value.validate((valid) => {
             if (!valid) {
                 alert("验证失败了....")
                 return;
             }

             alert("2---->提交密码保存")
         })
     }


     // 校验规则
     const rules = reactive({
         pwd1: [
             { required: true, message: '请输入旧密码', trigger: 'blur' },
             { min: 3, max: 20, message: '密码长度应该是 3 to 20', trigger: 'blur' },
         ],
         pwd2: [
             { required: true, message: '请输入新密码', trigger: 'blur' },
             { min: 3, max: 20, message: '新密码长度应该是 3 to 20', trigger: 'blur' },
         ],
         pwd3: [
             { required: true, message: '请输入确认密码', trigger: 'blur' },
             { min: 3, max: 20, message: '确认密码长度应该是 3 to 20', trigger: 'blur' },
         ]
     });


     return {
         isFullscreen,
         handleRefresh,
         handleIndex,
         handleOpen,
         updatePwdRef,
         handleRefresh,
         handleCommand,
         handleFullScreen,
         handleExpand,

         userPwdForm,
         userPwdFormRef,
         rules

     }
 }