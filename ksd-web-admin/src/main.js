import { createApp } from 'vue'
// 导入首页
import App from './App.vue'
// 全局导入windicss样式
import 'virtual:windi.css'
// 注册路由
import router from '@/router'
// 导入状态管理
import store from '@/store'
// 引入自定义组件
import PugUI from '@/components'
// 引入自定义组件
import directives from '@/directives'
// 引入element-plus模块
import ElementPlus from 'element-plus'
/*element-plus的样式 */
import 'element-plus/dist/index.css'
/*交互进度条的样式 */
import 'nprogress/nprogress.css'
/*导入所有的图标组件*/
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
const app = createApp(App);

// 初始化指令
directives(app);

// 注册插件ElementPlus
app.use(ElementPlus)

// 注册状态管理
// 注册插件router app.config.globalProperties.$store
app.use(store)

app.use(PugUI)

// 注册插件router app.config.globalProperties.$route
// 注册插件router app.config.globalProperties.$router
app.use(router)
// 注册element-plus所有的图标组件
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}


app.mount('#app')