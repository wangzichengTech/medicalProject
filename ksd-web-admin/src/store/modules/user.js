import loginService from '@/services/login/LoginService';
import { toast, showFullLoading, hideFullLoading } from '@/utils'

export default {
    namespaced: true,
    // 定义全局状态管理的响应式数据
    state() {
        return {
            // 管理的用户对象信息
            userId: "",
            username: "",
            avatar: "",
            token: "",
            roleList: [],
            permissionList: []
        }
    },

    // 定义修改状态管理的方法
    // 结论：修改state的全局状态数据只能通过mutations的方法进行修改。
    // 调用通过：store.commit方法执行和调用
    mutations: {
        // 同步state中用户的信息
        toggleUser(state, serverUserData) {
            state.token = serverUserData.token;
            state.tokenUuid = serverUserData.tokenUuid;
            state.userId = serverUserData.user.id;
            state.username = serverUserData.user.username;
            state.avatar = serverUserData.user.avatar;
            state.roleList = serverUserData.roleNames;
            state.permissionList = serverUserData.permissions;
        },
        // 清除状态
        clearUser(state) {
            state.userId = "";
            state.account = "";
            state.avatar = "";
            state.token = "";
            state.roleList = [];
            state.permissionList = [];
            // 这里记得把持久化的状态管理清除。这里不执行也可以
            //localStorage.removeItem("pug-admin-web-vuex");
        },
        toggleUserAvatar(state,avatars){
            state.avatar = avatars
        }
    },

    // 异步修改状态的的方法
    // 注意：actions定义的方式，不能直接修改state的状态数据
    // 只能通过context.commit去条件mutations的方法去修改state的数据。它是一个间接的方式
    // 调用通过：store.dispatch方法执行和调用
    actions: {
        async toLogout(context) {
            //1: 异步请求去执行服务器退出操作
            //2: 执行页面状态清空
            try {
                showFullLoading();
                // 获取服务端的返回操作
                var serverResponse = await loginService.toLogout();
                // 清楚本地状态和缓存相关信息
                context.commit("clearUser");
                // 清除菜单
                context.commit('menu/clearMenu', null, { root: true });
                // 返回,这里是promise
                return Promise.resolve(serverResponse);
            } catch (err) {
                toast("退出失败" + err)
                    // 为什么不在这里处理呢，因为在这里没有办法和页面进行交互。
                return Promise.reject(err);
            } finally {
                hideFullLoading();
            }
        },

        // 从登录以后的axios请求中获取serverUserData
        async toLogin(context, loginUserData) {
            try {
                // 发起登录请求
                const serverResponse = await loginService.toLogin(loginUserData);
                // 通过context.commit去修改mutations中toggleUser去同步state数据
                context.commit("toggleUser", serverResponse.data);
                //var menuResponse = await adminMenuService.loadNavMenu();
                //context.commit('menu/addMenuList', menuResponse.data, { root: true });
                // 返回,这里是promise
                return Promise.resolve(serverResponse);
            } catch (err) {
                // 为什么不在这里处理呢，因为在这里没有办法和页面进行交互。
                return Promise.reject(err);
            }
        }
    },

    // 对state数据的改造和加工处理。给未来的页面和组件进行调用。
    // 从而达到一个封装的目录 computed
    getters: {
        // 组装一个角色信息返回
        getRoleName(state) {
            return state.roleList.join(",");
        },
        // 获取权限
        getPermissions(state) {
            return state.permissionList;
        },
        // 获取用户ID
        getUserId(state) {
            return state.userId;
        },
        // 获取token
        getToken(state) {
            return state.token;
        },
        // 获取tokenUuid 用于同一账号挤下线
        // 因为你每次登录都会产生新的uuid。所以旧的就被你新挤掉
        getTokenUuid(state) {
            return state.tokenUuid;
        },
        // 判断是否登录
        isLogin(state) {
            return state.userId != "";
        }
    }
}