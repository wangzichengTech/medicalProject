import adminMenuService from '@/services/navmenu/AdminMenuService';


export default {
    namespaced: true,
    state() {
        return {
            // 左侧菜单数据
            menuList: [],
            /*选中问题 */
            cpath: "",
            /*折叠 */
            slideWidth: "248px",
            /*折叠状态控制i */
            isCollapse: false,
        }
    },
    mutations: {

        /*折叠方法*/
        handleSlideWidth(state) {
            state.slideWidth = state.slideWidth == "248px" ? "64px" : "248px"
            state.isCollapse = state.slideWidth == "64px"
        },

        // 添加菜单
        addMenuList(state, menuList) {
            state.menuList = menuList
        }
    },

    actions: {

        // 查询路由
        async asyncGetMenuList({ commit }) {

            // 1: 异步查询路由
            var menuResponse = await adminMenuService.loadNavMenu()

            // 2: 把查询出来的菜单数据，同步到状态state.menuList中
            commit('addMenuList', menuResponse.data)

            // 3: 承诺返回
            return menuResponse.data
        }

    },
    getters: {

        menuLen(state) {
            return state.menus.length;
        },

        cpath(state) {
            return state.cpath
        }
    }
}