// 在setup中使用mapGetters的hooks
import { mapGetters, createNamespacedHelpers } from 'vuex'
import { useMapper } from './useMapper'
import store from '@/store'


export function useMutations() {
    // 拿到store对象
    const args = arguments;
    if (args.length == 2) {
        return store.commit(args[0], args[1]);
    } else {
        return store.commit(args[0] + "/" + args[1], args[2]);
    }
}

export function useActions() {
    // 拿到store对象
    const args = arguments;
    if (args.length == 2) {
        return store.dispatch(args[0], args[1]);
    } else {
        return store.dispatch(args[0] + "/" + args[1], args[2]);
    }
}

export function useGetters(moduleName, mapper) {
    let mapperFn = mapGetters
    if (typeof moduleName === 'string' && moduleName.length > 0) {
        mapperFn = createNamespacedHelpers(moduleName).mapGetters
    } else {
        mapper = moduleName
    }
    return useMapper(mapper, mapperFn)
}

// 传入一个模块名
export function useState(moduleName, mapper) {
    let mapperFn = mapState
        // 判断传入的模块名是否是String类型，且长度不为0
    if (typeof moduleName === 'string' && moduleName.length > 0) {
        mapperFn = createNamespacedHelpers(moduleName).mapState
    } else {
        mapper = moduleName
    }
    return useMapper(mapper, mapperFn)
}