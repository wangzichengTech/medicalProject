// useMapper.js 核心
import { computed } from 'vue';
import { useStore } from 'vuex'

// mapper传入的, mapFn是使用的map方法
export function useMapper(mapper, mapFn) {
    // 拿到store对象
    const store = useStore()
    console.log("storestorestorestorestore", store)

    // 获取到对应的对象的function {counter: function, name: function, age: function}
    const storeDataFns = mapFn(mapper)
        // 这些对应的是一个个对象， 函数

    // 对数据进行转换 {counter: ref, name: ref, age: ref}
    const storeData = {}

    // 使用Object.keys获取storeGettersFns的key值
    // 在使用forEach遍历每一个key
    Object.keys(storeDataFns).forEach(fnKey => {
        // 取出storeDataFns中的函数，调用函数的bind绑定一个this，fn就有了this，
        // 这个this必须是个对象，且需要有$store属性，且需要个值
        const fn = storeDataFns[fnKey].bind({ $store: store });
        // 取出的函数用computed包裹，根据key重新赋值给storeGetters
        storeData[fnKey] = computed(fn);
    })

    return storeData
}