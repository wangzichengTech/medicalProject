export default {
    "110601": { code: "110601", msg: "用户不存在", field: "account" },
    "110602": { code: "110602", msg: "服务出现故障", field: "code" },
    "110603": { code: "110603", msg: "用户名或密码输入有误", field: "password" },
    "110606": { code: "110606", msg: "用户异常,请联系管理员", field: "code" },
    "110607": { code: "110607", msg: "账号不能是空", field: "account" },
    "110608": { code: "110608", msg: "密码不能为空", field: "password" },
    "110609": { code: "110609", msg: "验证码不能为空", field: "code" },
    "110610": { code: "110610", msg: "会话过期了", field: "code" },
    "110611": { code: "110611", msg: "验证码输入有误", field: "code" },
    "default": { code: "10000", msg: "默认错误", field: "" },
}