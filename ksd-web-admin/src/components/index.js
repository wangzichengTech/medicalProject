import PugDrawer from '@/components/PugDrawer.vue'
import PugNum from '@/components/PugNum.vue'
import PugChooseImage from '@/components/PugChooseImage.vue'
import PugUploadFile from '@/components/PugUploadFile.vue'
import PugDialog from '@/components/PugDialog.vue'
import PugEditor from '@/components/PugEditor.vue'
import PugImage from '@/components/PugImage.vue'
import PugImageMain from '@/components/PugImageMain.vue'
import PugIconSelect from '@/components/PugIconSelect.vue'
import PugChapterLesson from '@/components/PugChapterLesson.vue'

export default {
    // 利用插件机制i
    install(app) {
        // 全局注册
        app.component('PugDrawer', PugDrawer)
        app.component('PugEditor', PugEditor)
        app.component('PugImage', PugImage)
        app.component('PugImageMain', PugImageMain)
        app.component('PugNum', PugNum)
        app.component('PugDialog', PugDialog)
        app.component('PugUploadFile', PugUploadFile)
        app.component('PugChooseImage', PugChooseImage)
        app.component('PugIconSelect', PugIconSelect);
        app.component('PugChapterLesson', PugChapterLesson);
    }
}