tinymce.PluginManager.add('addtemplate', function(editor, url) {
    // 注册一个工具栏按钮名称
    editor.ui.registry.addButton('addtemplate', {
        text: '添加模板',
        onAction: function() {
            editor.insertContent('插入的文字是: 11111');
        }
    });

    // 注册一个菜单项名称 menu/menubar
    editor.ui.registry.addMenuItem('addtemplate', {
        text: '添加模板',
        onAction: function() {
            editor.insertContent('插入的文字是: 11111');
        }
    });

    return {
        getMetadata: function() {
            return {
                //插件名和链接会显示在“帮助”→“插件”→“已安装的插件”中
                name: "Example plugin", //插件名称
                url: "http://exampleplugindocsurl.com", //作者网址
            };
        }
    };
});